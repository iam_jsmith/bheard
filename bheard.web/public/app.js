var app = angular.module('app', [
        'angular-loading-bar',
        'angulike',
        'infinite-scroll',
        'ngAnimate',
        'ngCookies',
        'ngSanitize',
        'duScroll',
        'slick',
        'wu.masonry',
		'com.2fdevs.videogular',
		'com.2fdevs.videogular.plugins.controls',
		'com.2fdevs.videogular.plugins.overlayplay',
		'com.2fdevs.videogular.plugins.poster',
        'ngOpbeat',
        'ngToast',
        'ngtweet',
        'ng.deviceDetector',
        'ui.bootstrap',
        'ui.gravatar',
        'ui.router',
        'focus-if',
        'account',
        'admin',
        'aboutus',
        'bCookieConsent',
        'byob',
        'charities',
        'company',
        'contactus',
        'changecoins',
        'democracy',
        'education',
        'energy',
        'fatherhood',
        'government',
        'healthcare',
        'home',
        'insurance',
        'investors',
        'legal',
        'login',
        'money',
        'news',
        'phoneandinternet',
        'register',
        'resetpassword',
        'review',
        'switch',
        'seeresults',
        'opinion',
        'ui-demo'
    ])
    .constant('dateFormat', 'YYYY-MM-DDTHH:mm:ss.SSSZ')
    .config(['$httpProvider', '$locationProvider', '$urlMatcherFactoryProvider', '$stateProvider', 'cfpLoadingBarProvider', '$opbeatProvider', 'ngToastProvider',
        function ($httpProvider, $locationProvider, $urlMatcherFactoryProvider, $stateProvider, cfpLoadingBarProvider, $opbeatProvider, ngToastProvider) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
            $locationProvider.html5Mode(true);
            $urlMatcherFactoryProvider.strictMode(false);
            cfpLoadingBarProvider.includeSpinner = false;
            $opbeatProvider.config({
                        orgId: 'e9002834b3634d96b3c17523e9fdc5e9',
                        appId: '95614a0a09'
                    });
            ngToastProvider.configure({ animation: 'fade' });

            var templateUrl = '/views/';

            $stateProvider
                .state('layout', {
                    abstract: true,
                    controller: 'DefaultController',
                    resolve: {
                        // todo I think the bug with the changecoins might be happening here
                        changeCoinEvents: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getEvents();
                        }],
                        currentUser: ['currentUserService', function (currentUserService) {
                            return currentUserService.get();
                        }]
                    },
                    templateUrl: templateUrl + 'layout.html',
                    url: ''
                })
                .state('default', {
                    abstract: true,
                    parent: 'layout',
                    url: '',
                    views: {
                        'nav': {
                            controller: 'NavController',
                            templateUrl: templateUrl + 'nav.html'
                        },
                        'footer': {
                            templateUrl: templateUrl + 'footer.html'
                        }
                    }
                });
    }])
    .run(['$window', '$location', '$rootScope', '$state', '$stateParams', function ($window, $location, $rootScope, $state) {
        $rootScope.$state = $state;
        $rootScope.previousStateName = null;
        $rootScope.previousStateParams = null;

        $rootScope.$on('$stateChangeSuccess', function(e, toState, toParams, fromState, fromParams) {
            $rootScope.bodyClass = toState.name.replace(/\./g, '-');
            $rootScope.extraClass = toState.data.class;
            $rootScope.previousStateName = fromState.name;
            $rootScope.previousStateParams = fromParams;

            if (toState.data && toState.data.sector) {
                $rootScope.sector = toState.data.sector;
            } else {
                $rootScope.sector = null;
            }

            if (toState.data && toState.data.title) {
                $rootScope.title = toState.data.title;
            } else {
                $rootScope.title = null;
            }

            $window.ga('send', 'pageview', { page: $location.url() });
        });

        $rootScope.$on('401', function () {
            $state.go('login');
        });

        $rootScope.$on('404', function () {
            $state.go('home');
        });
    }])
    .factory('apiKeys', ['$window', function ($window) {
      return {
        google: $window.document.getElementById('googleApiKey').content,
      }
    }])
    .value('duScrollOffset', 35);


// todo this is config for gravatar, should config for other modules be in this file?
angular.module('ui.gravatar').config([
    'gravatarServiceProvider', function(gravatarServiceProvider) {
        gravatarServiceProvider.defaults = {
            size     : 100,
            "default": 'blank'  // No Image as default for missing avatars
        };
    }
]);

// For cookies consent, sitewide
angular.module('bCookieConsent', ['ngCookies'])
.directive('bCookieConsent', function ($cookies) {
    return {
        scope: {},
        template:
        '<div class="cookie-consent">' +
        '<div class="cookie-consent__container" ng-hide="cookieConsent()">' +
        '<p class="cookie-consent__text">bheard.com uses cookies to ensure you have the best experience on our platform. ' +
        'Cookies are tiny text files that are stored by your browser when you visit a website.  ' +
        'By continuing to use our website, you agree to the use of cookies.</p>' +
        '<div class="cookie-consent__buttons"><a class="cookie-consent__agree" href="" ng-click="cookieConsent(true)"><input type="checkbox"/> I agree</a>' +
        '<a class="cookie-consent__learn-more" ui-sref="cookies">Learn more</a></div>' +
        '</div>' +
        '</div>',
        controller: function ($scope) {
            var _cookieConsent = $cookies.get('cookieConsent');
            $scope.cookieConsent = function (cookieConsent) {
                if (cookieConsent === undefined) {
                    return _cookieConsent;
                } else if (cookieConsent) {
                    $cookies.put('cookieConsent', true);
                    _cookieConsent = true;
                }
            };
        }
    };
});
