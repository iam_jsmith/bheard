app.directive('bStarRating', function () {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            rating: '=ngModel'
        },
        templateUrl: '/views/starrating.html',
        link: function (scope, element, attrs, ngModel) {
            var modelName = _.last(attrs.ngModel.split('.'));

            scope.highlight = null;
            scope.id = _.kebabCase(modelName);
            scope.isRequired = !!attrs.required;
            scope.name = modelName;
            scope.mode = attrs.bMode;

            scope.mouseover = function (i) {
                scope.highlight = i;
            };

            scope.mouseout = function () {
                scope.highlight = null;
            };
        }
    };
});
