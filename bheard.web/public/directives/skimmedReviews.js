app.directive('bSkimmedReviews', ['$rootScope', 'suppliersService', '$interval', function ($rootScope, suppliersService, $interval) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            availableReviews: '=ngModel'
        },
        templateUrl: '/views/skimmedreviews.html',
        link: function (scope, element) {
            var animationTime = 5000; // can use this to disable animation (if need to use console to edit)
            var timeoutId;
            var uniqueRandoms = [];
            var numVisibleReviews = Math.min(8, scope.availableReviews.length);
            
            function makeUniqueRandom() {
                if (!uniqueRandoms.length) {
                    for (var i = 0; i < numVisibleReviews; i++) {
                        uniqueRandoms.push(i);
                    }
                }
                var index = Math.floor(Math.random() * uniqueRandoms.length);
                var val = uniqueRandoms[index];
                
                uniqueRandoms.splice(index,1);
                return val;
            }
            
            var nextReviewToSwapOut = makeUniqueRandom(), nextReviewToSwapIn = numVisibleReviews;
            
            scope.videoPlayers = [];
            
            scope.visibleReviews = _.slice(scope.availableReviews, 0, nextReviewToSwapIn);
        
            scope.swapReview = function() {
                // console.log("vrCounter: " + vrCounter + ", arCounter: " + arCounter);
                nextReviewToSwapOut = makeUniqueRandom();
                if (nextReviewToSwapOut === scope.playing) {
                    // we have a review that is currently playing a video so we should do nothing
                    return;
                }
                scope.visibleReviews.splice(nextReviewToSwapOut, 1);
                scope.visibleReviews.splice(nextReviewToSwapOut, 0, scope.availableReviews[nextReviewToSwapIn]);
                nextReviewToSwapIn = nextReviewToSwapIn >= scope.availableReviews.length - 1 ? 0 : nextReviewToSwapIn + 1;

            };
            
            scope.setVideoPlaying = function(index) {
                scope.playing = index;
            }
            
            // console.log("Number of reviews: " + scope.availableReviews.length);
            if (scope.availableReviews.length > 8) {
                timeoutId = $interval( function() {
                    scope.swapReview();
                }, animationTime);
            }
            
            // We need to cancel the interval call to avoid memory leaks
            element.on('$destroy', function() {
                $interval.cancel(timeoutId);
            });
        }
    };
}]);
