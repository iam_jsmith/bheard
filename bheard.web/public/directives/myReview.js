app.directive('bMyReview', ['$rootScope', 'suppliersService', function ($rootScope, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            supplier: '=bSupplier',
        },
        templateUrl: '/views/unapprovedreview.html',
        link: function (scope) {
            scope.review = function (supplier) {
                return suppliersService.getReviewByIp({supplierId: supplier._id});
            };
        }
    };
}]);
