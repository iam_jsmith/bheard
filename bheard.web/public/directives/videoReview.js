app.directive('bVideoReview', ['$sce', function ($sce) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            review: '=',
            players: '=',
            index: '='
        },
        link: function(scope, element, attrs) {
            scope.playerReady = function(api) {
                scope.players[scope.index] = api;
            };
            
            scope.canPlay = function() {
                // console.log('player ' + scope.index + ': can play');
            };
            
            scope.stateChange = function(state) {
                if (state === 'play') {
                    // pause other players
                    for (var i =0, l=scope.players.length; i < l; i++) {
                        if (i != scope.index) {
                            scope.players[i].pause();
                        }
                    }
                } else if (state === 'stop') {
                    // console.log('player ' + scope.index + ': stopped');
                }
            }
            
            scope.videoConfig = {
                preload: "none",
                sources: [
                    {src: $sce.trustAsResourceUrl(scope.review.videoUrl), type: "video/mp4"},
                ],
                theme: "bower_components/videogular-themes-default/videogular.css",
            };        
        },
        templateUrl: '/views/videoreview.html'
    };
}]);