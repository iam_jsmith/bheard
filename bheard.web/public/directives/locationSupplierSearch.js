app.directive('bLocationSupplierSearch', ['$rootScope', '$state', function ($rootScope, $state) {
    return {
        replace: true,
        restrict: 'E',
        scope: {},
        templateUrl: '/views/locationsuppliersearch.html',
        link: function (scope) {
            scope.sector = null;
            scope.postcode = $rootScope.currentUser ? $rootScope.currentUser.postcode : null;

            scope.submit = function (sector, postcode) {
                $state.go(sector + (sector !== 'money' ? '.suppliers' : '.products.currentaccounts'), { postcode: postcode });
            };
        }
    };
}]);
