app.directive('head', ['$rootScope', '$compile', function ($rootScope, $compile) {
    return {
        restrict: 'E',
        link: function (scope, element) {
            var html = '<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700&lang=en" rel="stylesheet" ng-if="model.isAdmin" />' +
                       '<link href="modules/admin/module.css" rel="stylesheet" ng-if="model.isAdmin" />';

            scope.model = {
                isAdmin: false
            };

            element.append($compile(html)(scope));

            $rootScope.$on('$stateChangeStart', function (e, toState) {
                scope.model.isAdmin = _.first(toState.name.split('.')) === 'admin';
            });
        }
    };
}]);
