app.directive('bSectorSupplierRankings', ['$state', function ($state) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            suppliers: '=ngModel'
        },
        templateUrl: '/views/sectorsupplierrankings.html'
    };
}]);
