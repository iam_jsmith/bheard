app.directive('bError', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('error', function () {
                scope.$eval(attrs.bError);
            });
        }
    };
});
