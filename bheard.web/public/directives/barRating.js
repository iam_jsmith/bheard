app.directive('bBarRating', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            rating: '=bRating',
            showRatingText: '=?bShowRatingText'
        },
        templateUrl: '/views/barrating.html',
        link: function (scope, element, attrs) {
            if (scope.showRatingText == null) {
                scope.showRatingText = true;
            }

            scope.size = attrs.bSize;
            scope.text = attrs.bText;
        }
    };
});
