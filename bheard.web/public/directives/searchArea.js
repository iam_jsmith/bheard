app.directive('bSearchArea', ['$state', '$rootScope', function ($state, $rootScope) {
    return {
        replace: true,
        restrict: 'AE',
        scope: {
            destination: '@destination'
        },
        templateUrl: '/views/searcharea.html',
        link: function (scope) {
            // todo better not to use rootScope, but currentUser not injecting properly
            scope.options = {};
            scope.options.postcode = $rootScope.currentUser ? $rootScope.currentUser.postcode : null;
            scope.submit = function (options) {
                $state.go(scope.destination, options);
            };
        }
    };
}]);