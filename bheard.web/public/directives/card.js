/*
A container that displays its contents inside a card with a drop shadow

Usage:
    <b-card [b-title=""] [b-subtitle=""] [b-speech]>
        (contents)
    </b-card>

Attributes:
    b-title     -- Text for title
    b-subtitle  -- Text for subtitle
    b-speech    -- Presence denotes appearing as a speech bubble
    b-compact   -- More compact layout
*/

app.directive('bCard', function() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            title: '@bTitle',
            subtitle: '@bSubtitle',
            speech: '@bSpeech',
            compact: '@bCompact'
        },

        templateUrl: '/views/card.html',
        link: function(scope, el, attrs, controller, transclude) {
            transclude(function(clone) {
                if (clone.length > 0) {
                    scope.hasContent = true;
                }
            });
        }
    };
});
