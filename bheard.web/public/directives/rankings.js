app.directive('bRankings', ['$timeout', function ($timeout) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            supplier: '=bSupplier',
            suppliers: '=bSuppliers',
            urlFragment: '=?bUrlFragment'
        },
        templateUrl: '/views/rankings.html',
        link: function (scope) {
            if (scope.urlFragment == null) {
                scope.urlFragment = '';
            }

            scope.jumpToTop = function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
            }
        }
    };
}]);
