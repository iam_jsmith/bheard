app.directive('bClickOff', ['$document', function ($document) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $document.on('mousedown touchstart', function (e) {
                if (element[0] !== e.target && !$(element).has(e.target).length) {
                    scope.$apply(function () {
                        scope.$eval(attrs.bClickOff);
                    });
                }
            });
        }
    };
}]);
