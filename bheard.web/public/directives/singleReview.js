app.directive('bSingleReview', ['$rootScope', '$state', 'suppliersService', function ($rootScope, $state, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            review: '=ngModel'
        },
        templateUrl: '/views/singlereview.html',
        link: function (scope) {            
            scope.helpful = function (review) {
                suppliersService.rateReview(review._id, { type: 'helpful' }).then(function () {
                    review.helpful++;
                    review.isRated = true;
                });
            };

            scope.unhelpful = function (review) {
                suppliersService.rateReview(review._id, { type: 'unhelpful' }).then(function () {
                    review.unhelpful++;
                    review.isRated = true;
                });
            };
        }
    };
}]);
