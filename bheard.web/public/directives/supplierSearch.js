app.directive('bSupplierSearch', ['$state', 'suppliersService', function ($state, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {},
        templateUrl: '/views/suppliersearch.html',
        link: function (scope) {
            scope.name = null;

            scope.search = function (name) {
                return suppliersService.search({ name: name, limit: 5 });
            };

            scope.select = function (supplier) {
                $state.go(supplier.sector + '.supplier', { id: supplier._id });
            };
        }
    };
}]);
