app.directive('bQmark', function () {
    return {
        link: function (scope, element, attrs) {
            element.children('.qmark-circle-open').click(function() {
                $('.pop-sub, .pop-main').hide();
                $(this).next('.pop-sub, .pop-main').fadeIn(200);
            });

            element.find('.qmark-circle-close, .qmark-close').on("click", function() {
                $(this).closest('.pop-sub, .pop-main').fadeOut(200);
            });
        }
    };
});