app.directive('bSectorSupplierSearch', ['$rootScope', '$state', 'suppliersService', function ($rootScope, $state, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {},
        templateUrl: '/views/sectorsuppliersearch.html',
        link: function (scope) {
            scope.name = null;

            scope.search = function (name) {
                return suppliersService.search({ name: name, sector: $rootScope.sector });
            };

            scope.select = function (supplier) {
                $state.go(supplier.sector + '.supplier', { id: supplier._id });
            };
        }
    };
}]);
