app.directive('bPages', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            animated: '@bAnimated',
            selectedPage: '@bSelected',
        },

        templateUrl: '/views/pages.html',
        link: function($scope, $el) {
            var domEl = $el.get(0);

            domEl.getSelectedEl = function () {
                var currentPageName = $el.attr('b-selected');
                var currentPage = $el.find('.b-pages__page[b-name="' + currentPageName + '"]');

                return currentPage;
            }

            domEl.isFirstSelected = function () {
                var currentPage = this.getSelectedEl();
                var prevPage = currentPage.prev();

                return prevPage.length === 0;
            };

            domEl.isLastSelected = function () {
                var currentPage = this.getSelectedEl();
                var nextPage = currentPage.next();

                return nextPage.length === 0;
            };

            domEl.goToPrev = function () {
                var currentPage = this.getSelectedEl();
                var prevPage = currentPage.prev();

                if (prevPage.length === 1) {
                    $el.attr('b-selected', prevPage.attr('b-name'));
                }
            };

            domEl.goToNext = function () {
                var currentPage = this.getSelectedEl();
                var nextPage = currentPage.next();

                if (nextPage.length === 1) {
                    $el.attr('b-selected', nextPage.attr('b-name'));
                }
            };

            domEl.setSelected = function (name) {
                $el.attr('b-selected', name);
            };

            // TODO: should this be in a controller?

            function setSelected () {
                var targetPage = $el.find('.b-pages__page[b-name="' + $scope.selectedPage + '"]');

                if (targetPage.length === 1) {
                    targetPage.siblings('.b-pages__page--selected').removeClass('b-pages__page--selected');
                    targetPage.siblings().removeClass('b-pages__page--before b-pages__page--after');

                    targetPage.addClass('b-pages__page--selected');

                    targetPage.prevAll().addClass('b-pages__page--before');
                    targetPage.nextAll().addClass('b-pages__page--after');
                }
            };

            setSelected();

            if (!MutationObserver) {
                return;
            }

            // manual data binding 😢
            // TODO: stop manually binding data
            // TODO: figure out how to automatically bind data

            var observer = new MutationObserver(function (changes) {
                changes.forEach(function (change) {
                    var attribute = change.attributeName;
                    var newValue = $el.attr(attribute);

                    console.debug(attribute + ' set to ' + newValue);

                    // apply new attribute values

                    switch (attribute) {
                        case 'b-animated':
                            console.debug('updating scope.animated');
                            $scope.animated = newValue;
                            break;

                        case 'b-selected':
                            console.debug('updating scope.selectedPage');
                            $scope.selectedPage = newValue;

                            setSelected();

                            break;
                    }

                    // commit changes
                    $scope.$apply();
                });
            });

            // start observing attribute changes
            observer.observe(domEl, {
                attributes: true
            });
        }
    };
});
