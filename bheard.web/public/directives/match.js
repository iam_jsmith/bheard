app.directive('bMatch', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, ngModelCtrl) {
            scope.$watch(attrs.ngModel, function (value) {
                ngModelCtrl.$setValidity('match', value === scope.$eval(attrs.bMatch));
            });

            scope.$watch(attrs.bMatch, function (value) {
                ngModelCtrl.$setValidity('match', value === scope.$eval(attrs.ngModel));
            });
        }
    };
});
