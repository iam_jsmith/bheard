app.directive('bReviewForm', ['$state', '$stateParams', 'suppliersService', 'twitterService', 'facebookService', 'googleService', 'loginService', 'registerService', 'notificationUtility', function ($state, $stateParams, suppliersService, twitterService, facebookService, googleService, loginService, registerService, notificationUtility) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            supplier: '=ngModel'
        },
        templateUrl: '/views/reviewform.html',
        link: function (scope) {
            scope.model = {
                login: {
                    email: null,
                    password: null
                },
                mode: 'default',
                register: {
                    unconfirmedEmail: null,
                    password: null,
                    confirmPassword: null,
                    firstName: null,
                    lastName: null,
                    postcode: null
                },
                review: {
                    overallOpinion: null,
                    customerService: null,
                    valueForMoney: null,
                    title: null,
                    text: null,
                    isAnonymous: false,
                    isCertified: false
                },
                submittedReview: $stateParams.submittedReview
            };

            function success(model) {
                switch (model.mode) {
                    case 'twitter':
                    case 'facebook':
                    case 'google':
                    case 'login':
                    case 'register':
                        $state.go($state.current.name, { submittedReview: true }, { reload: true });
                        break;
                    default:
                        model.submittedReview = true;
                        break;
                }
            }

            scope.submitAnonymousReview = function (model) {
                model.review.isAnonymous = true;
                model.mode = 'default';
            };

            scope.submitAuthenticatedReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'default';
            };

            scope.loginAndSubmitReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'login';
            };

            scope.loginWithTwitterAndSubmitReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'twitter';
            };

            scope.loginWithFacebookAndSubmitReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'facebook';
            };

            scope.loginWithGoogleAndSubmitReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'google';
            };

            scope.registerAndSubmitReview = function (model) {
                model.review.isAnonymous = false;
                model.mode = 'register';
            };

            scope.submitReview = function(model) {
                switch (model.mode) {
                    case 'twitter':
                        twitterService.login().then(
                            function () {
                                suppliersService.postReview(scope.supplier._id, model.review).then(function () {
                                    success(model);
                                });
                            },
                            function () {
                                notificationUtility.alert('There was a problem logging in with Twitter, please try again.')
                            }
                        );
                        break;
                    case 'facebook':
                        facebookService.login().then(
                            function () {
                                suppliersService.postReview(scope.supplier._id, model.review).then(function () {
                                    success(model);
                                });
                            },
                            function () {
                                notificationUtility.alert('There was a problem logging in with Facebook, please try again.')
                            }
                        );
                        break;
                    case 'google':
                        googleService.login().then(
                            function () {
                                suppliersService.postReview(scope.supplier._id, model.review).then(function () {
                                    success(model);
                                });
                            },
                            function () {
                                notificationUtility.alert('There was a problem logging in with Google, please try again.')
                            }
                        );
                        break;
                    case 'login':
                        loginService.login(model.login).then(
                            function () {
                                suppliersService.postReview(scope.supplier._id, model.review).then(function () {
                                    success(model);
                                });
                            },
                            function (errors) {
                                scope.form.errors = errors;
                            }
                        );
                        break;
                    case 'register':
                        registerService.register(model.register).then(
                            function (data) {
                                suppliersService.postReview(scope.supplier._id, _.extend({ token: data.token }, model.review)).then(function () {
                                    success(model);
                                });
                            },
                            function (errors) {
                                scope.form.errors = errors;
                            }
                        );
                        break;
                    default:
                        suppliersService.postReview(scope.supplier._id, model.review).then(function () {
                            success(model);
                        });
                        break;
                }
            };
        }
    };
}]);
