app.directive('bSwiper', ['$timeout', function ($timeout) {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            items: '=ngModel'
        },
        templateUrl: '/views/swiper.html',
        link: function (scope, element) {
            var container = element.find('.swiper-container'),
                pagination = element.find('.swiper-pagination'),
                nextButton = element.find('.swiper-button-next'),
                prevButton = element.find('.swiper-button-prev');

            $timeout(function () {
                new Swiper(container[0], {
                    pagination: pagination[0],
                    slidesPerView: 5,
                    paginationClickable: true,
                    prevButton: prevButton[0],
                    nextButton: nextButton[0],
                    spaceBetween: 15,
                    loop: true
                });
            });
        }
    };
}]);
