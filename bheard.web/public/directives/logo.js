app.directive('bLogo', function () {
    return {
        require: 'ngModel',
        restrict: 'E',
        scope: {
            supplier: '=ngModel',
            urlFragment: '=?bUrlFragment',
            noLink: '=?bNoLink'
        },
        templateUrl: '/views/logo.html',
        link: function (scope) {
            if (scope.urlFragment == null) {
                scope.urlFragment = '';
            }

            if (scope.noLink == null) {
                scope.noLink = false;
            }

            var path = '/images/logos/' +
                       scope.supplier.sector.toLowerCase() +
                       '/' +
                       _.kebabCase(scope.supplier.name);

            scope.show = scope.supplier.sector === 'energy' ||
                         scope.supplier.sector === 'money' ||
                         scope.supplier.sector === 'phoneandinternet';

            scope.src = path + '.png';
            scope.src2x = path + '@2x.png';
        }
    };
});
