app.directive('bSwitchAlert', ['$state', function ($state) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: '/views/switchalert.html',
        link: function (scope) {
        }
    };
}]);
