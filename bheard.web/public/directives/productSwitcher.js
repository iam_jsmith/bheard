app.directive('bProductSwitcher', ['$window', '$location', '$timeout', function ($window, $location, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            if (scope.urlFragment !== '') {
                element.ready(function(){
                    // wait so page position doesn't jump down automatically to hash
                    $timeout(function() {
                        scope.switchProduct(scope.urlFragment, false);
                    }, 1000)
                });
            }

            scope.switchProduct = function (product, animate) {
                element.find('.prod-cat').removeClass('active');
                element.find('#'+product+'-button').addClass('active');
                element.find('#' + product).addClass('active'); // this may not be needed

                // jquery animations (replace with angular animations?)
                $('.products-container').hide();
                $('#'+product).fadeIn();

                if (animate === true) {
                    $('html,body').animate({scrollTop: $("#more").offset().top}, 'slow');
                }
            };
        }
    };
}]);
