var supportsPassive = false;
var opts = Object.defineProperty({}, 'passive', {
    get: function() {
        supportsPassive = true;
    }
});

window.addEventListener('test', null, opts);
window.removeEventListener('test', null, opts);

console.debug("supports passive listeners:", supportsPassive);

app.directive("bScroll", function ($window) {
    return function(scope, element, attrs) {
        $window.addEventListener("scroll", function() {
            if (this.pageYOffset >= 50) {
                scope.scrolledDown = true;
            } else {
                scope.scrolledDown = false;
            }

            scope.$apply();
        }, supportsPassive ? {passive: true} : false);

        $(window).resize(function() {
            if($(window).width() < 780) {
                $('.nav__item--dropdown').click(function(e) {
                    e.preventDefault();
                })
            } else {
                $('.nav__item--dropdown').unbind('click');
            }
        }).resize();
    };
});

app.directive("bMobileNav", function ($window) {
    return function(scope, element, attrs) {
        scope.toggleNav = false;
    };
});
