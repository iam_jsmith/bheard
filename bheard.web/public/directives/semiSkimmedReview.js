app.directive('bSemiSkimmedReview', ['suppliersService', function (suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            review: '=ngModel'
        },
        templateUrl: '/views/semiskimmedreview.html',
        link: function (scope) {
            scope.helpful = function (review) {
                suppliersService.rateReview(review._id, { type: 'helpful' }).then(function () {
                    review.helpful++;
                    review.isRated = true;
                });
            };

            scope.unhelpful = function (review) {
                suppliersService.rateReview(review._id, { type: 'unhelpful' }).then(function () {
                    review.unhelpful++;
                    review.isRated = true;
                });
            };
        }
    };
}]);
