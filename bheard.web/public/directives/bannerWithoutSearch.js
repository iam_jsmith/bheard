app.directive('bBannerWithoutSearch', ['$rootScope', '$state', function ($rootScope, $state, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            showSuggestion: '=bShowSuggestion'
        },
        templateUrl: '/views/bannerwithoutsearch.html',
        link: function (scope, element, attrs) {
            scope.title = attrs.bTitle;
            scope.text = attrs.bText;
            scope.name = null;
        }
    };
}]);
