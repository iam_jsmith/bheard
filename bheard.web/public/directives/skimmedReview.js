app.directive('bSkimmedReview', ['$sce', function ($sce) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            review: '=',
            videoPlayers: '=',
            index: '=',
            setPlaying: '&'
        },
        link: function(scope) {
            scope.trustSrc = function(filePath) {
                return $sce.trustAsResourceUrl(filePath);
            };
            scope.getTrustedUrl = function () {
                return scope.trustSrc(scope.review.videoUrl);
            };

            scope.videoConfig = {
                sources: [
                    {src: $sce.trustAsResourceUrl(scope.review.videoUrl), type: "video/mp4"},
                ],
                theme: "bower_components/videogular-themes-default/videogular.css",
            };
            
            scope.playerReady = function(api) {
                scope.videoPlayers[scope.index] = api;
            };
            
            scope.stateChange = function(state) {
                if (state === 'play') {
                    scope.setPlaying();
                    // pause other players
                    for (var player in scope.videoPlayers) {
                        if (player != scope.index) {
                            scope.videoPlayers[player].pause();
                        }
                    }
                }
            };

            scope.getUserPictureUrl = function() {
                var url = '../images/user-anon.png';
                if (scope.review.user) {
                    if (scope.review.user.facebookPictureUrl) url = scope.review.user.facebookPictureUrl;
                    else if (scope.review.user.twitterPictureUrl) url = scope.review.user.twitterPictureUrl;
                    else if (scope.review.user.googlePictureUrl) url = scope.review.user.googlePictureUrl;
                }
                return url;
            }
        },
        templateUrl: '/views/skimmedreview.html'
    };
}]);
