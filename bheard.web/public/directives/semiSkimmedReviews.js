app.directive('bSemiSkimmedReviews', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            reviews: '=ngModel'
        },
        templateUrl: '/views/semiskimmedreviews.html'
    };
});
