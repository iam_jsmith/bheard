app.directive('bMakeItRight', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: '/views/makeitright.html'
    };
});
