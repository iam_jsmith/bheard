app.directive('bBarChart', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            // I think this should this be = and not &, because the function is in the controller
            data: '=bData',
            // showRatingText: '=?bShowRatingText'
        },
        templateUrl: '/views/barchart.html',
        link: function (scope, element, attrs) {
            if (scope.showRatingText == null) {
                scope.showRatingText = true;
            }

            scope.size = attrs.bSize;
            scope.text = attrs.bText;
        }
    };
});
