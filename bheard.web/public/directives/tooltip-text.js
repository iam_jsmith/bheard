app.directive('bTooltipText', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            revealer: "@bRevealer",
            circle: "@bCircle",
            suspended: "@bSuspended",
        },

        templateUrl: '/views/tooltip-text.html',
    };
});
