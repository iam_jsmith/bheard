app.directive('bCircleRating', function () {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            rating: '=bRating',
            showRatingText: '=?bShowRatingText'
        },
        templateUrl: '/views/circlerating.html',
        link: function (scope, element, attrs) {
            if (scope.showRatingText == null) {
                scope.showRatingText = true;
            } else {
                scope.showRatingText = false;
            }

            scope.ratingType = attrs.bRatingType;
            scope.text = attrs.bText;
        }
    };
});
