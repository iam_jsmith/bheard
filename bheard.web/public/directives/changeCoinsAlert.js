app.directive('bChangeCoinsAlert', ['$state', function ($state) {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            donation: '=ngModel'
        },
        templateUrl: '/views/changecoinsalert.html'
    };
}]);
