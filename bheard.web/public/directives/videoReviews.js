app.directive('bVideoReviews', ['$uibModal', function ($uibModal) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            reviews: '=',
            topic: '='
        },
        templateUrl: '/views/videoreviews.html',
        link: function(scope) {
            
            // Used to keep a reference of each player that has been setup. Useful if we only want to play one video at a time
            scope.players = [];

            scope.openRecordModal = function() {
                var modalInstance = $uibModal.open({
                    windowClass: 'top-class',
                    template: '<div style="text-align: center;"><style type="text/css">#vpm-iframe{width:260px;height:325px;margin:10px 0}#vpm-iframe.fix-iframe{width:640px;height:480px;margin:10px 0}@media (min-width:501px){#vpm-iframe{width:500px;height:375px;margin:10px 0}}@media (min-width:641px){#vpm-iframe{width:640px;height:480px;margin:10px 0}}</style><iframe allowfullscreen="" frameborder="0" height="480" id="vpm-iframe" scrolling="no" src="https://www.voxpopme.com/record/' + scope.topic.main.content.vpmId + '?layout=simple" width="100%"></iframe></div>'
                });    
            };
        }
    };
}]);
