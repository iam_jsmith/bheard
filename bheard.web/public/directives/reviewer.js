app.directive('bReviewer', ['$rootScope', '$state', 'suppliersService', '$window', '$location', '$uibModal', '$document',
function ($rootScope, $state, suppliersService, $window, $location, $uibModal, $document) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            showSuggestion: '=bShowSuggestion'
        },
        templateUrl: '/views/reviewer2.html',
        link: function ($scope, element, attrs) {
            var pages = $('#reviewer2-pages').get(0);

            $scope.title = attrs.bTitle;
            $scope.caption = attrs.bText;
            $scope.columns = attrs.bColumns || 8;
            $scope.offset = attrs.bOffset || 2;
            $scope.name = null;

            // hide more arrow if 'b no more' is true
            $scope.nomore = attrs.bNoMore || false;

            var iters = 0;

            $scope.isFirst = function () {
                console.log(pages.isFirstSelected);
                window.pages = pages;
                return pages.isFirstSelected();
            };

            $scope.isLast = function () {
                return pages.isLastSelected();
            };

            $scope.canGoNext = function () {
                console.log('hi' + pages);
                switch (pages.attr('b-selected')) {
                    case 'organisation':
                        return $scope.name != null;

                    default:
                        return !$scope.isLast;
                }
            };

            $scope.canGoPrev = function () {
                return !$scope.isFirst;
            };

            $scope.model = {
                review: {
                    overallOpinion: null,
                    customerService: 1,
                    valueForMoney: 1,
                    title: null,
                    text: null,
                    isAnonymous: true,
                    isCertified: false,
                    suggestedCompanyName: null
                },

                name: null,
                page: 1,
                prevPage: 0,
                numPages: 5,
                reviewSubmitted: false
            };

            $scope.sector = $rootScope.sector;

            $scope.goToNextPage = function () {
                pages.goToNext();
                $scope.$apply();
            };

            $scope.goToPage = function (page) {
                pages.setSelected(page);
                $scope.$apply();
            };

            $scope.goToPrevPage = function () {
                pages.goToPrev();
                $scope.$apply();
            };

            $scope.search = function (name) {
                return suppliersService.search({name: name, sector: $rootScope.sector});
            };

            $scope.select = function (supplier) {
                $scope.goToNextPage();
            };

            $scope.openRecordPopup = function () {
                $scope.model.review.text = " ";
                var modalInstance = $uibModal.open({
                    windowClass: 'top-class',
                    template: '<div style="text-align: center;"><style type="text/css">#vpm-iframe{width:260px;height:325px;margin:10px 0}#vpm-iframe.fix-iframe{width:640px;height:480px;margin:10px 0}@media (min-width:501px){#vpm-iframe{width:500px;height:375px;margin:10px 0}}@media (min-width:641px){#vpm-iframe{width:640px;height:480px;margin:10px 0}}</style><iframe allowfullscreen="" frameborder="0" height="480" id="vpm-iframe" scrolling="no" src="https://www.voxpopme.com/record/a1de28a2a234e602c3f2462636f92045?layout=simple" width="100%"></iframe></div>'
                });
            };

            $scope.getShareUrl = function() {
                if ($scope.name != null && $scope.name._id != null) {
                    return $state.href($scope.name.sector + '.supplier', {id: $scope.name._id}, {absolute: true});
                } else {
                    return encodeURIComponent($location.absUrl());
                }
            };

            $scope.getShareText = function() {
                if ($scope.name != null && $scope.name.name != null) {
                    return encodeURIComponent("I've just reviewed " + $scope.name.name + " on B.heard");
                } else {
                    return encodeURIComponent("I've just left a review on B.heard");
                }
            };

            $scope.submitReview = function() {
                $scope.model.reviewSubmitted = true;
                var urlId = null;

                // check suggested Company Name first, otherwise can go backwards (after picking supplier) and submit wrong Id
                if (typeof $scope.name._id === 'undefined') {
                    urlId =  'anonymousSupplier';
                    $scope.model.review.suggestedCompanyName = $scope.name;
                } else if ($scope.name !== null && $scope.name._id !== null) {
                    urlId = $scope.name._id;
                }

                suppliersService.postReview(urlId, $scope.model.review).then(function (review) {
                    $scope.model.reviewSubmitted = true;
                    $scope.goToNextPage();
                    $scope.sector = review.sector;
                    $scope.subsector = review.subsector;
                }).catch(function(response) {
                    // console.log(response);
                    $scope.model.reviewSubmitted = false;
                });
            };

            $scope.getThanksTitle = function() {
                return {
                    "1": "Ouch",
                    "2": "That's not great!",
                    "3": "Pretty average...",
                }[$scope.model.review.overallOpinion] || "Thank you";
            };


            $scope.getSwitchData = function() {
                var switchData = {
                    subtitle: null,
                    buttons: []
                };

                if ($scope.model.review.overallOpinion < 4 && $scope.name && $scope.name.sector) {
                    switch ($scope.name.sector) {
                        case 'energy':
                        switchData.subtitle = "Want to change?";
                        switchData.buttons[1] = {
                            label: 'Look for a better energy deal',
                            url: 'http://energy.bheard.com'
                        };
                        break;
                        case 'phoneandinternet':
                        if ($scope.name.mobile_provider && !$scope.name.broadband_provider) {
                            switchData.subtitle = "Want to change?";
                            switchData.buttons[1] = {
                                label: 'Look for a better mobile deal',
                                url: 'http://mobile.bheard.com'
                            };
                        } else if (!$scope.name.mobile_provider && $scope.name.broadband_provider) {
                            switchData.subtitle = "Want to change?";
                            switchData.buttons[1] = {
                                label: 'Look for a better internet deal',
                                url: 'http://media.bheard.com'
                            };
                        } else {
                            switchData.subtitle = "Or make a change to your...";
                            switchData.buttons[1] = {
                                label: 'Mobile',
                                url: 'http://mobile.bheard.com'
                            };
                            switchData.buttons[2] = {
                                label: 'Internet',
                                url: 'http://media.bheard.com'
                            };
                        }
                        break;
                    }
                }
                return switchData;
            };

            $scope.share = function (shareLink) {
                $window.open(shareLink, '_blank');
            };

            $scope.shareOnFB = function () {
                $scope.share('http://facebook.com/sharer.php?u=' + $scope.getShareUrl());
            };

            $scope.shareOnTwitter = function () {
                $scope.share('http://twitter.com/intent/tweet?text=' +  $scope.getShareText() + '%20' + $scope.getShareUrl());
            };

            $scope.shareOnGooglePlus = function () {
                $scope.share('https://plus.google.com/share?url=' + $scope.getShareUrl());
            };

            // Overlay
            // element.find('input, textarea').focus(function() {
            //     $('.overlay').addClass('open');
            // });
            //
            // $('.overlay').click(function(e) {
            //     e.preventDefault();
            //     $(this).removeClass('open');
            // });
        }
    };
}]);
