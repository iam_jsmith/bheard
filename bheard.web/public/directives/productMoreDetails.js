app.directive('bProductMoreDetails', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.click(function(e) {
                e.preventDefault();

                var text = '<span class="fa fa-chevron-down"></span>More';

                $(this).toggleClass('open');
                
                if($(this).html() == text) {
                    $(this).html('<span class="fa fa-chevron-up"></span>Less');
                } else {
                    $(this).html('<span class="fa fa-chevron-down"></span>More');
                }


                $(this).parent().next('.product-details').slideToggle();
            });
        }
    };
});