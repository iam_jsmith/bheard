app.directive('body', ['$rootScope', '$state', '$timeout', '$window', function ($rootScope, $state, $timeout, $window) {
    return {
        restrict: 'E',
        link: function () {
            $rootScope.$on('$stateChangeSuccess', function (e, toState, toParams, fromState) {
                if (!$state.includes(fromState.name) && !(fromState.data && fromState.data.isModal) && !(toState.data && toState.data.isModal)) {
                    $window.scrollTo(0, 0);
                }
            });
        }
    };
}]);
