app.directive('bServerValidate', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            scope.$watch(attrs.ngModel, function () {
                ngModelCtrl.$setValidity('server', true);
                ngModelCtrl.$error.server = null;
            });

            scope.$watch('form.errors', function () {
                if (scope.form.errors) {
                    var key = _.last((attrs.mServerValidate || attrs.ngModel)
                                .split('.'))
                                .replace('$index', scope.$index);

                    if (scope.form.errors[key]) {
                        ngModelCtrl.$setValidity('server', false);
                        ngModelCtrl.$error.server = scope.form.errors[key];
                    } else {
                        ngModelCtrl.$setValidity('server', true);
                        ngModelCtrl.$error.server = null;
                    }
                }
            });
        }
    };
});
