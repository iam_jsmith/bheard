var legal = angular.module('legal', []).config(['$stateProvider', function ($stateProvider) {
    var templateUrl = '/modules/legal/views/';

    $stateProvider
        .state('legal', {
            abstract: true,
            data: {
                title: 'Legal',
                class: 'no-feature',
            },
            parent: 'default',
            views: {
                '@layout': {
                    templateUrl: templateUrl + 'legal.html'
                }
            }
        })
        .state('cookies', {
            data: {
                title: 'Cookies',
                class: 'no-feature',
            },
            parent: 'legal',
            url: '/cookies',
            views: {
                '@legal': {
                    controller: 'CookiesController',
                    templateUrl: templateUrl + 'cookies.html'
                }
            }
        })
        .state('privacy', {
            data: {
                title: 'Privacy policy',
                class: 'no-feature',
            },
            parent: 'legal',
            url: '/privacy',
            views: {
                '@legal': {
                    controller: 'PrivacyController',
                    templateUrl: templateUrl + 'privacy.html'
                }
            }
        })
        .state('terms', {
            data: {
                title: 'Terms & conditions',
                class: 'no-feature',
            },
            parent: 'legal',
            url: '/terms',
            views: {
                '@legal': {
                    controller: 'TermsController',
                    templateUrl: templateUrl + 'terms.html'
                }
            }
        })
        .state('guidelines', {
            data: {
                title: 'Review Guidelines',
                class: 'no-feature',
            },
            parent: 'legal',
            url: '/guidelines',
            views: {
                '@legal': {
                    controller: 'GuidelinesController',
                    templateUrl: templateUrl + 'guidelines.html'
                }
            }
        })
        .state('complaints', {
            data: {
                title: 'Complaints',
                class: 'no-feature'
            },
            parent: 'legal',
            url: '/complaints',
            views: {
                '@legal': {
                    controller: 'ComplaintsController',
                    templateUrl: templateUrl + 'complaints.html'
                }
            }
        })
}]);
