insurance.factory('insuranceSuppliersService', ['api', function (api) {
    var urlPrefix = '/insurancesuppliers';

    return {
        getSuppliers: function (type) {
            return api.get(urlPrefix, { params: { type: type } });
        }
    };
}]);
