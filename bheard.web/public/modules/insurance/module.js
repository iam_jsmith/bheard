var insurance = angular.module('insurance', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/insurance/views/';

    $stateProvider
        .state('insurance', {
            abstract: true,
            data: {
                sector: 'insurance'
            },
            parent: 'default',
            url: '/insurance'
        })
        .state('insurance.home', {
            data: {
                title: 'Review & Compare Insurance | B.heard'
            },
            url: '',
            views: {
                '@layout': {
                    controller: 'InsuranceHomeController',
                    resolve: {
                        latestDonation: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getLatestDonation();
                        }],
                        rankedSuppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('insurance');
                        }],
                        reviews: ['suppliersService', function (suppliersService) {
                            return suppliersService.getLatestReviewsBySector('insurance', 16);
                        }]
                    },
                    templateUrl: templateUrl + 'insurance.home.html'
                }
            }
        })
        .state('insurance.suppliers', {
            params: {
                insuranceType: 'car'
            },
            url: '/suppliers/{insuranceType:car|van|home|bike|travel|breakdown|pet|life|health|youngdriver}',
            views: {
                'banner@layout': {
                    controller: 'InsuranceSuppliersController',
                    templateUrl: templateUrl + 'insurance.banner.html',
                    resolve: {
                        suppliers: [ function () { return {}; }] // variable needed by the controller
                    }
                },
                '@layout': {
                    controller: 'InsuranceSuppliersController',
                    resolve: {
                        suppliers: ['$stateParams', 'insuranceSuppliersService',
                            function ($stateParams, insuranceSuppliersService) {
                                return insuranceSuppliersService.getSuppliers($stateParams.insuranceType);
                        }]
                    },
                    templateUrl: templateUrl + 'insurance.suppliers.html'
                }
            }
        })
        .state('insurance.switch', {
            url: '/switch/{insuranceType:car|van|home|bike|travel|breakdown|pet|life|health|youngdriver}',
            params: {
                insuranceType: 'car'
            },
            views: {
                'banner@layout': {
                    controller: 'InsuranceSwitchController',
                    templateUrl: templateUrl + 'insurance.banner.html'
                },
                '@layout': {
                    controller: 'InsuranceSwitchController',
                    templateUrl: templateUrl + 'insurance.switch.html'
                }
            }
        })
        .state('insurance.supplier', {
            data: {
                title: 'Company'
            },
            params: {
                submittedReview: false
            },
            url: '/companies/:id',
            views: {
                '@layout': {
                    controller: 'InsuranceSupplierController',
                    resolve: {
                        company: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                            return suppliersService.getSupplierById($stateParams.id);
                        }],
                        reviews : ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getLatestReviewsBySupplierId($stateParams.id);
                        }],
                        suppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('insurance', 4);
                        }]
                    },
                    templateUrl: templateUrl + 'insurance.supplier.html'
                }
            }
        });
}]);
