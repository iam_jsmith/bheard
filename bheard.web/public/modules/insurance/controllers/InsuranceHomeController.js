insurance.controller('InsuranceHomeController', ['latestDonation', 'rankedSuppliers', 'reviews', '$scope',
function (latestDonation, rankedSuppliers, reviews, $scope) {
    $scope.model = {
        latestDonation: latestDonation,
        rankedSuppliers: rankedSuppliers,
        reviews: reviews
    };
}]);