insurance.controller('InsuranceSuppliersController', ['$scope', '$rootScope', '$stateParams', 'suppliers',
function($scope, $rootScope, $stateParams, suppliers) {
    var categories = {
        'car': {
                title: 'Car',
                key: 'car',
                link: 'insurance.suppliers({insuranceType: "car"})'
            },
        'van':  {
                title: 'Van',
                key: 'van',
                link: 'insurance.suppliers({insuranceType: "van"})'
            },
        'home': {
                title: 'Home',
                key: 'home',
                link: 'insurance.suppliers({insuranceType: "home"})'
            },
        'bike': {
                title: 'Motorcycle',
                key: 'bike',
                link: 'insurance.suppliers({insuranceType: "bike"})'
            },
        'travel': {
                title: 'Travel',
                key: 'travel',
                link: 'insurance.suppliers({insuranceType: "travel"})'
            },
        'breakdown': {
                title: 'Breakdown',
                key: 'breakdown',
                link: 'insurance.suppliers({insuranceType: "breakdown"})'
            },
        'pet': {
                title: 'Pet',
                key: 'pet',
                link: 'insurance.suppliers({insuranceType: "pet"})'
            }
            // The following categories are excluded until we have supplier data on them
        // 'life': {
        //         title: 'Life',
        //         key: 'life',
        //         link: 'insurance.suppliers({insuranceType: "life"})'
        //     },
        // 'health': {
        //         title: 'Health',
        //         key: 'health',
        //         link: 'insurance.suppliers({insuranceType: "health"})'
        //     },
        // 'youngdriver': {
        //         title: 'Young Driver',
        //         key: 'youngdriver',
        //         link: 'insurance.suppliers({insuranceType: "youngdriver"})'
        //     }
        };
    $scope.model = {
        suppliers: suppliers,
        insuranceCategories: categories,
        currentState: categories[$stateParams.insuranceType],
        limitTo: 10
    };

    $rootScope.title = $scope.model.currentState.title;

    $scope.more = function () {
        $scope.model.limitTo += 10;
    };
}]);
