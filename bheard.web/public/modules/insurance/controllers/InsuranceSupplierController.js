insurance.controller('InsuranceSupplierController', ['$rootScope', '$scope', '$window', '$location', 'company',
    'reviews', 'suppliers',
function ($rootScope, $scope, $window, $location, company, reviews, suppliers) {
    $rootScope.title = company.name;

    $scope.model = {
        company: company,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
