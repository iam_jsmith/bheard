insurance.controller('InsuranceSwitchController', ['$scope', '$rootScope', '$stateParams',
function($scope, $rootScope, $stateParams) {
    var categories = {
        'car': {
                title: 'Car',
                key: 'car',
                link: 'insurance.switch({insuranceType: "car"})'
            },
        'van':  {
                title: 'Van',
                key: 'van',
                link: 'insurance.switch({insuranceType: "van"})'
            },
        'home': {
                title: 'Home',
                key: 'home',
                link: 'insurance.switch({insuranceType: "home"})'
            },
        'bike': {
                title: 'Motorcycle',
                key: 'bike',
                link: 'insurance.switch({insuranceType: "bike"})'
            },
        'travel': {
                title: 'Travel',
                key: 'travel',
                link: 'insurance.switch({insuranceType: "travel"})'
            },
        'breakdown': {
                title: 'Breakdown',
                key: 'breakdown',
                link: 'insurance.switch({insuranceType: "breakdown"})'
            },
        'pet':  {
                title: 'Pet',
                key: 'pet',
                link: 'insurance.switch({insuranceType: "pet"})'
            },
        'life': {
                title: 'Life',
                key: 'life',
                link: 'insurance.switch({insuranceType: "life"})'
            },
        'health': {
                title: 'Health',
                key: 'health',
                link: 'insurance.switch({insuranceType: "health"})'
            },
        'youngdriver': {
                title: 'Young Driver',
                key: 'youngdriver',
                link: 'insurance.switch({insuranceType: "youngdriver"})'
            }
        };
    $scope.model = {
        insuranceCategories: categories,
        currentState: categories[$stateParams.insuranceType],
        limitTo: 10
    };

    $rootScope.title = $scope.model.currentState.title;

    $scope.more = function () {
        $scope.model.limitTo += 10;
    };
}]);
