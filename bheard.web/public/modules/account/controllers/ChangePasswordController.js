account.controller('ChangePasswordController', ['$scope', 'currentUser', 'currentUserService', 'notificationUtility', function($scope, currentUser, currentUserService, notificationUtility) {
    $scope.model = {
        oldPassword: null,
        newPassword: null,
        confirmNewPassword: null
    };

    $scope.save = function(model) {
        currentUserService.changePassword(model).then(
            function(user) {
                angular.extend(currentUser, user);
                notificationUtility.success('Your password was changed.');
            },
            function(errors) {
                $scope.form.errors = errors;
            });
    };
}]);