account.controller('ChangeEmailAddressController', ['$scope', 'currentUser', 'currentUserService', 'notificationUtility', function ($scope, currentUser, currentUserService, notificationUtility) {
    $scope.model = {
        form: {
            unconfirmedEmail: currentUser.email
        },
        formSubmitted: false
    };

    $scope.save = function (model) {
        currentUserService.changeEmailAddress(model.form).then(
            function () {
                model.formSubmitted = true;
            },
            function (errors) {
                $scope.form.errors = errors;
            });
    };
}]);