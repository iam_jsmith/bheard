account.controller('PersonalInformationController', ['$scope', 'currentUser', 'currentUserService', 'notificationUtility', function ($scope, currentUser, currentUserService, notificationUtility) {
    $scope.model = _.pick(currentUser, 'firstName', 'lastName', 'postcode');

    $scope.save = function (model) {
        currentUserService.update(model).then(
            function (user) {
                _.extend(currentUser, user);
                notificationUtility.success('Your changes were saved.');
            },
            function (errors) {
                $scope.form.errors = errors;
            });
    };
}]);