var account = angular.module('account', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/account/views/';

    $stateProvider
        .state('account', {
            abstract: true,
            onEnter: ['$state', 'currentUser', function($state, currentUser) {
                if (!currentUser) {
                    $state.go('login');
                }
            }],
            parent: 'default',
            url: '/account',
            views: {
                '@layout': {
                    templateUrl: templateUrl + 'account.html'
                }
            }
        })
        .state('account.personalinformation', {
            data: {
                title: 'Personal information',
                class: 'no-feature'
            },
            url: '/personalinformation',
            views: {
                '@account': {
                    controller: 'PersonalInformationController',
                    templateUrl: templateUrl + 'account.personalinformation.html'
                }
            }
        })
        .state('account.myratings', {
            data: {
                title: 'My ratings & review',
                class: 'no-feature'
            },
            url: '/myratings',
            views: {
                '@account': {
                    controller: 'MyRatingsController',
                    resolve: {
                        reviews: ['currentUser', 'suppliersService', function (currentUser, suppliersService) {
                            return suppliersService.getLatestReviewsByUserId(currentUser._id);
                        }]
                    },
                    templateUrl: templateUrl + 'account.myratings.html'
                }
            }
        })
        .state('account.changecoinsaccount', {
            data: {
                title: 'Changecoin&reg; account',
                class: 'no-feature'
            },
            url: '/changecoinsaccount',
            views: {
                '@account': {
                    controller: 'ChangeCoinsAccountController',
                    resolve: {
                        changeCoinsLedger: ['currentUserService', function (currentUserService) {
                            return currentUserService.getChangeCoinsLedger();
                        }]
                    },
                    templateUrl: templateUrl + 'account.changecoinsaccount.html'
                }
            }
        })
        .state('account.changeemailaddress', {
            data: {
                title: 'Change email address',
                class: 'no-feature'
            },
            url: '/changeemailaddress',
            views: {
                '@account': {
                    controller: 'ChangeEmailAddressController',
                    templateUrl: templateUrl + 'account.changeemailaddress.html'
                }
            }
        })
        .state('account.changeemailaddress.step2', {
            data: {
                title: 'Change email address',
                class: 'no-feature'
            },
            url: '/:uuid',
            views: {
                '@layout': {
                    resolve: {
                        ok: ['$stateParams', 'currentUserService', function ($stateParams, currentUserService) {
                            return currentUserService.changeEmailAddressStep2($stateParams.uuid);
                        }]
                    },
                    templateUrl: templateUrl + 'account.changeemailaddress.step2.html'
                }
            }
        })
        .state('account.changepassword', {
            data: {
                title: 'Change password',
                class: 'no-feature'
            },
            url: '/changepassword',
            views: {
                '@account': {
                    controller: 'ChangePasswordController',
                    templateUrl: templateUrl + 'account.changepassword.html'
                }
            }
        });
}]);