healthcare.controller('HealthcareSuppliersController', ['$scope', '$location', 'options', 'isTouched', 'location', 'surgeries', 'geoService', 'surgeriesService', function ($scope, $location, options, isTouched, location, surgeries, geoService, surgeriesService) {
    function search(location, options) {
        surgeriesService.search(location, options).then(function (surgeries) {
            $location.search(options);
            $scope.model.location = location;
            $scope.model.surgeries = surgeries;
            $scope.model.isTouched = true;
        });
    }

    $scope.model = {
        location: location,
        surgeries: surgeries,
        options: options,
        isTouched: isTouched
    };

    $scope.search = function (options) {
        if (options.postcode) {
            geoService.geocode(options.postcode).then(function (location) {
                search(location, options);
            });
        } else {
            search(location, options);
        }
    };

    $scope.change = function (options) {
        search($scope.model.location, options);
    };
}]);