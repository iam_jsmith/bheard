energy.controller('HealthcareSupplierController', ['$rootScope', '$scope', 'surgery', 'reviews', 'suppliers', function ($rootScope, $scope, surgery, reviews, suppliers) {
    $rootScope.title = surgery.name;

    $scope.model = {
        surgery: surgery,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
