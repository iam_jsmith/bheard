healthcare.controller('HealthcareHomeController', ['currentUser', 'latestDonation', 'reviews', '$scope', function (currentUser, latestDonation, reviews, $scope) {
    $scope.model = {
        latestDonation: latestDonation,
        reviews: reviews,
        options: {
            rating: null
        }
    };
}]);
