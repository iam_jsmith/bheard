var healthcare = angular.module('healthcare', [])
    .constant('surgeriesFusionTableId', '1ONcDdYIZ5ga9XjbOjAvc0giZhb6oMpJHvn-kNJNG')
    .config(['$stateProvider', function ($stateProvider) {
        var templateUrl = '/modules/healthcare/views/';

        $stateProvider
            .state('healthcare', {
                abstract: true,
                data: {
                    sector: 'healthcare'
                },
                parent: 'default',
                url: '/healthcare'
            })
            .state('healthcare.home', {
                data: {
                    title: 'Rate & Compare GP Practices | B.heard'
                },
                url: '',
                views: {
                    '@layout': {
                        controller: 'HealthcareHomeController',
                        resolve: {
                            latestDonation: ['changeCoinsService', function (changeCoinsService) {
                                return changeCoinsService.getLatestDonation();
                            }],
                            reviews: ['suppliersService', function (suppliersService) {
                                return suppliersService.getLatestReviewsBySector('healthcare', 16);
                            }]
                        },
                        templateUrl: templateUrl + 'healthcare.home.html'
                    }
                }
            })
            .state('healthcare.suppliers', {
                data: {
                    title: 'Surgeries'
                },
                reloadOnSearch: false,
                url: '/surgeries?postcode&rating',
                views: {
                    '@layout': {
                        controller: 'HealthcareSuppliersController',
                        templateUrl: templateUrl + 'healthcare.suppliers.html',
                        resolve: {
                            options: ['$stateParams', function ($stateParams) {
                                return _.extend({}, _.omit($stateParams, 'return'));
                            }],
                            isTouched: ['options', function (options) {
                                return _.some(_.values(options));
                            }],
                            location: ['options', 'geoService', 'randomPostcodeUtility', function (options, geoService, randomPostcodeUtility) {
                                return geoService.geocode(options.postcode || randomPostcodeUtility.get());
                            }],
                            surgeries: ['options', 'location', 'surgeriesService', function (options, location, surgeriesService) {
                                return surgeriesService.search(location, options);
                            }]
                        }
                    }
                }
            })
            .state('healthcare.supplier', {
                data: {
                    title: 'Surgery'
                },
                params: {
                    submittedReview: false
                },
                url: '/surgeries/:id',
                views: {
                    '@layout': {
                        controller: 'HealthcareSupplierController',
                        resolve: {
                            surgery: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                                return suppliersService.getSupplierById($stateParams.id);
                            }],
                            reviews : ['surgery', 'suppliersService', function (surgery, suppliersService) {
                                return suppliersService.getLatestReviewsBySupplierId(surgery._id);
                            }],
                            suppliers: ['suppliersService', function (suppliersService) {
                                return suppliersService.getRankedSuppliersBySector('healthcare', 4);
                            }]
                        },
                        templateUrl: templateUrl + 'healthcare.supplier.html'
                    }
                }
            });
}]);