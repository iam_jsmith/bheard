healthcare.factory('surgeriesService', ['$http', 'apiKeys', 'surgeriesFusionTableId', 'fusionTableCsvSerializer', 'suppliersService', 'notificationUtility', function ($http, apiKeys, surgeriesFusionTableId, fusionTableCsvSerializer, suppliersService, notificationUtility) {
    var urlPrefix = 'https://www.googleapis.com/fusiontables/v2/query?key=' + apiKeys.google + '&sql=';

    return {
        search: function (location, options) {
            var query =
                "SELECT 'CQC Practice Identifier' " +
                "FROM " + surgeriesFusionTableId + " " +
                (options.rating ? "WHERE 'Score' >= '0." + options.rating + "' " : "") +
                "ORDER BY ST_DISTANCE(Postcode, LATLNG(" + location.lat + ',' + location.lng + ")) " +
                "LIMIT " + (options.limit || 25);

            return $http.get(urlPrefix + encodeURIComponent(query)).then(
                function (response) {
                    var surgeries = fusionTableCsvSerializer.deserialize(response.data);
                    return suppliersService.getSuppliersByNos(_.pluck(surgeries, 'cqcPracticeIdentifier'));
                },
                function () {
                    notificationUtility.alert('There was a problem loading the page, please try again.');
                });
        }
    };
}]);