healthcare.directive('bSurgeriesMap', ['surgeriesFusionTableId', 'geoService', function (surgeriesFusionTableId, geoService) {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            model: '=ngModel'
        },
        templateUrl: '/views/map.html',
        link: function (scope, element) {
            var map = new google.maps.Map(element[0], { mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false, zoom: 14 }),
                layer = new google.maps.FusionTablesLayer({ map: map, options: { styleId: 4, templateId: 6 } }),
                marker = null;
                
            scope.$watch('model.location', function (location) {
                if (!marker) {
                    marker = new google.maps.Marker({ map: map, position: location });
                } else {
                    marker.setPosition(location);
                }
            });

            scope.$watch('model.options.rating', function (rating) {
                layer.setOptions({
                    query: {
                        select: 'Postcode',
                        from: surgeriesFusionTableId,
                        where: rating ? "'Score' >= '0." + rating + "'" : ""
                    }
                });
            });

            scope.$watch('model.surgeries', function (surgeries) {
                geoService.geocode(surgeries[0].postcode).then(function (location) {
                    map.panTo(location);
                    map.setZoom(14);
                });
            });
        }
    };
}]);