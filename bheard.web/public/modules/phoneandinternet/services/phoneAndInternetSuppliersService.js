phoneandinternet.factory('phoneAndInternetSuppliersService', ['api', function (api) {
    var urlPrefix = '/phoneandinternetsuppliers';

    return {
        getSupplierByServiceType: function (type) {
            return api.get(urlPrefix, { params: { type: type } });
        }
    };
}]);
