phoneandinternet.controller('PhoneAndInternetSuppliersController', ['$scope', 'suppliers', function ($scope, suppliers) {
    $scope.model = {
        suppliers: suppliers,
        limitTo: 10
    };

    $scope.more = function () {
        $scope.model.limitTo += 10;
    };
}]);
