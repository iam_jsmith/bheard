phoneandinternet.controller('PhoneAndInternetSupplierController', ['$rootScope', '$scope', 'network', 'reviews', 'suppliers', function ($rootScope, $scope, network, reviews, suppliers) {
    $rootScope.title = network.name;

    $scope.model = {
        network: network,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
