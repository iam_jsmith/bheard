var phoneandinternet = angular.module('phoneandinternet', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/phoneandinternet/views/';

    $stateProvider
        .state('phoneandinternet', {
            abstract: true,
            data: {
                sector: 'phoneandinternet'
            },
            parent: 'default',
            url: '/phoneandinternet'
        })
        .state('phoneandinternet.home', {
            data: {
                title: 'Review & Compare Home Phone & Broadband Deals | B.heard'
            },
            url: '',
            views: {
                '@layout': {
                    controller: 'PhoneAndInternetHomeController',
                    resolve: {
                        latestDonation: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getLatestDonation();
                        }],
                        rankedSuppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('phoneandinternet');
                        }],
                        reviews: ['suppliersService', function (suppliersService) {
                            return suppliersService.getLatestReviewsBySector('phoneandinternet', 16);
                        }]
                    },
                    templateUrl: templateUrl + 'phoneandinternet.home.html'
                }
            }
        })
        .state('phoneandinternet.suppliers', {
            data: {
                title: 'Networks'
            },
            params: {
                type: 'internet'
            },
            url: '/networks?type',
            views: {
                'banner@layout': {
                    templateUrl: templateUrl + 'phoneandinternet.suppliers.banner.html'
                },
                '@layout': {
                    controller: 'PhoneAndInternetSuppliersController',
                    resolve: {
                        suppliers: ['$stateParams', 'phoneAndInternetSuppliersService', function ($stateParams, phoneAndInternetSuppliersService) {
                            return phoneAndInternetSuppliersService.getSupplierByServiceType($stateParams.type);
                        }]
                    },
                    templateUrl: templateUrl + 'phoneandinternet.suppliers.html'
                }
            }
        })
        .state('phoneandinternet.supplier', {
            data: {
                title: 'Network'
            },
            params: {
                submittedReview: false
            },
            url: '/networks/:id',
            views: {
                '@layout': {
                    controller: 'PhoneAndInternetSupplierController',
                    resolve: {
                        network: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                            return suppliersService.getSupplierById($stateParams.id);
                        }],
                        reviews : ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getLatestReviewsBySupplierId($stateParams.id);
                        }],
                        suppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('phoneandinternet', 4);
                        }]
                    },
                    templateUrl: templateUrl + 'phoneandinternet.supplier.html'
                }
            }
        });
}]);