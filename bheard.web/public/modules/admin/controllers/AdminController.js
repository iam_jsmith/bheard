admin.controller('AdminController', ['$rootScope', 'currentUser', function($rootScope, currentUser) {
    $rootScope.currentUser = currentUser;
    $rootScope.copyrightYear = moment().format('YYYY');
}]);