admin.controller('UnmoderatedSupplierReviewsAdminController', ['$scope', 'reviews', 'suppliersAdminService', 'notificationUtility', function ($scope, reviews, suppliersAdminService, notificationUtility) {
    $scope.model = {
        reviews: reviews,
        reviewOptions : [{ key: 'Select', value: null }, { key: 'Yes', value: true }, { key: 'No', value: false }]
    };

    $scope.save = function (reviews) {
        suppliersAdminService.updateUnmoderatedReviews(reviews).then(function () {
            suppliersAdminService.getUnmoderatedReviews().then(function (reviews) {
                $scope.model.reviews = reviews;
                notificationUtility.success('Your changes were saved.');
            });
        });
    };
}]);