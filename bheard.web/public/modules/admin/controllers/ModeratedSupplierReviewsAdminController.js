admin.controller('ModeratedSupplierReviewsAdminController', ['$scope', 'reviews', 'suppliersAdminService', 'notificationUtility', function ($scope, reviews, suppliersAdminService, notificationUtility) {
    var skip = 0,
        take = 100;

    $scope.model = {
        reviews: reviews
    };

    $scope.more = function (model) {
        suppliersAdminService.getModeratedReviews(skip += take, take).then(function (reviews) {
            model.reviews.push.apply(model.reviews, reviews);
        });
    };
    
    $scope.save = function (reviews) {
        suppliersAdminService.updateModeratedReviews(reviews).then(function () {
            suppliersAdminService.getModeratedReviews().then(function (reviews) {
                $scope.model.reviews = reviews;
                notificationUtility.success('Your changes were saved.');
            });
        });
    };
}]);