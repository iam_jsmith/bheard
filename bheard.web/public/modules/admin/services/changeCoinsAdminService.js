admin.factory('changeCoinsAdminService', ['api', function(api) {
    var urlPrefix = '/admin/changecoins';

    return {
        getChangeCoinsLedger: function () {
            return api.get(urlPrefix);
        }
    };
}]);