admin.factory('suppliersAdminService', ['api', function(api) {
    var urlPrefix = '/admin/suppliers';

    return {
        getModeratedReviews: function (skip, take) {
            return api.get(urlPrefix + '/reviews', { params: { isModerated: true, skip: skip, take: take } });
        },
        updateModeratedReviews: function (reviews) {
            return api.put(urlPrefix + '/reviews', reviews);
        },
        getUnmoderatedReviews: function () {
            return api.get(urlPrefix + '/reviews', { params: { isModerated: false } });
        },
        updateUnmoderatedReviews: function (reviews) {
            return api.put(urlPrefix + '/reviews', reviews);
        }
    };
}]);