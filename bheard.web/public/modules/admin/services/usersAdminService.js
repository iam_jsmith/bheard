admin.factory('usersAdminService', ['api', function(api) {
    var urlPrefix = '/admin/users';

    return {
        getAllUsers: function () {
            return api.get(urlPrefix);
        }
    };
}]);