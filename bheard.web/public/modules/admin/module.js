var admin = angular.module('admin', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/admin/views/';

    $stateProvider
        .state('admin', {
            abstract: true,
            controller: 'AdminController',
            resolve: {
                currentUser: ['currentUserService', function(currentUserService) {
                    return currentUserService.get();
                }]
            },
            templateUrl: templateUrl + 'layout.html',
            url: '/admin'
        })
        .state('admin.supplierreviews', {
            abstract: true,
            templateUrl: templateUrl + 'supplierreviews.html',
            url: ''
        })
        .state('admin.supplierreviews.unmoderated', {
            controller: 'UnmoderatedSupplierReviewsAdminController',
            data: {
                title: 'Admin - Unmoderated Reviews'
            },
            resolve: {
                reviews: ['suppliersAdminService', function(suppliersAdminService) {
                    return suppliersAdminService.getUnmoderatedReviews();
                }]
            },
            templateUrl: templateUrl + 'supplierreviews.unmoderated.html',
            url: ''
        })
        .state('admin.supplierreviews.moderated', {
            controller: 'ModeratedSupplierReviewsAdminController',
            data: {
                title: 'Admin - Moderated Reviews'
            },
            resolve: {
                reviews: ['suppliersAdminService', function(suppliersAdminService) {
                    return suppliersAdminService.getModeratedReviews();
                }]
            },
            templateUrl: templateUrl + 'supplierreviews.moderated.html',
            url: '/moderatedreviews'
        })
        .state('admin.changecoins', {
            controller: 'ChangeCoinsAdminController',
            data: {
                title: 'Admin - Changecoin&reg;'
            },
            resolve: {
                changeCoinsLedger: ['changeCoinsAdminService', function(changeCoinsAdminService) {
                    return changeCoinsAdminService.getChangeCoinsLedger();
                }]
            },
            templateUrl: templateUrl + 'changecoins.html',
            url: '/changecoins'
        })
        .state('admin.users', {
            controller: 'UsersAdminController',
            data: {
                title: 'Admin - Users'
            },
            resolve: {
                users: ['usersAdminService', function(usersAdminService) {
                    return usersAdminService.getAllUsers();
                }]
            },
            templateUrl: templateUrl + 'users.html',
            url: '/users'
        });
}]);