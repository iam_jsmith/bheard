var uiDemo = angular.module('ui-demo', []);

uiDemo.config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/ui-demo/views/';

    $stateProvider.state('ui-demo', {
        data: {
            title: 'UI Demo',
            sector: 'ui-demo'
        },
        parent: 'default',
        url: '/ui-demo',
        views: {
            '@layout': {
                templateUrl: templateUrl + 'ui-demo.html'
            }
        }
    });
}]);
