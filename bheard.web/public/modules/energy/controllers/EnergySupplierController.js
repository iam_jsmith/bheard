energy.controller('EnergySupplierController', ['$rootScope', '$scope', 'energySupplier', 'reviews', 'suppliers', function ($rootScope, $scope, energySupplier, reviews, suppliers) {
    $rootScope.title = energySupplier.name;
    $scope.model = {
        energySupplier: energySupplier,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
