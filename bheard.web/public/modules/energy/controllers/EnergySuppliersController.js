energy.controller('EnergySuppliersController', ['$scope', 'suppliers', function ($scope, suppliers) {
    $scope.model = {
        suppliers: suppliers,
        limitTo: 10
    };

    $scope.more = function () {
        $scope.model.limitTo += 10;
    };

    var energyData = [
        {
            'name': 'Coal',
            'value': 0
        },
        {
            'name': 'Gas',
            'value': 0
        },
        {
            'name': 'Nuclear',
            'value': 0
        },
        {
            'name': 'Green',
            'value': 0
        }
    ];

    $scope.getEnergyData = function (supplier) {
        energyData[0].value = supplier.coal;
        energyData[1].value = supplier.gas;
        energyData[2].value = supplier.nuclear;
        energyData[3].value = supplier.renewable;
        return energyData;
    }

}]);
