energy.controller('MoveMeController', ['$scope', '$window', 'currentUser', function ($scope, $window, currentUser) {
    $scope.model = {
        postcode: currentUser === null ? null : currentUser.postcode
    };

    $scope.submit = function (model) {
        $window.location = 'http://www.energyhelpline.com?aid=3557&postcode=' + model.postcode
    };
}]);