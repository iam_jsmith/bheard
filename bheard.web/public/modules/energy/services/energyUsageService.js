energy.factory('energyUsageService', ['api', function(api) {
    var urlPrefix = '/energyusage';

    return {
        get: function(params) {
            return api.get(urlPrefix, { params: params });
        }
    };
}]);
