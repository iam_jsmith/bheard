energy.factory('energySuppliersService', ['api', function (api) {
    var urlPrefix = '/energysuppliers';

    return {
        getSupplierByFuelType: function (type) {
            return api.get(urlPrefix, { params: { type: type } });
        }
    };
}]);
