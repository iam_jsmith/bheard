var energy = angular.module('energy', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/energy/views/';

    $stateProvider
        .state('energy', {
            abstract: true,
            data: {
                sector: 'energy'
            },
            parent: 'default',
            url: '/energy'
        })
        .state('energy.home', {
            data: {
                title: 'Rate & Compare Energy Suppliers | B.heard'
            },
            url: '',
            views: {
                '@layout': {
                    controller: 'EnergyHomeController',
                    resolve: {
                        latestDonation: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getLatestDonation();
                        }],
                        rankedSuppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('energy');
                        }],
                        reviews: ['suppliersService', function (suppliersService) {
                            return suppliersService.getLatestReviewsBySector('energy', 16);
                        }]
                    },
                    templateUrl: templateUrl + 'energy.home.html'
                }
            }
        })
        .state('energy.suppliers', {
            data: {
                title: 'Suppliers'
            },
            params: {
                type: 'dual'
            },
            url: '/suppliers?type',
            views: {
                'banner@layout': {
                    templateUrl: templateUrl + 'energy.suppliers.banner.html'
                },
                '@layout': {
                    controller: 'EnergySuppliersController',
                    resolve: {
                        suppliers: ['$stateParams', 'energySuppliersService', function ($stateParams, energySuppliersService) {
                            return energySuppliersService.getSupplierByFuelType($stateParams.type);
                        }]
                    },
                    templateUrl: templateUrl + 'energy.suppliers.html'
                }
            }
        })
        .state('energy.suppliers.moveme', {
            data: {
                isModal: true
            },
            url: '/:id/moveme',
            onEnter: ['$uibModal', '$state', 'currentUser', function ($uibModal, $state, currentUser) {
                $uibModal.open({
                    controller: 'MoveMeController',
                    resolve: {
                        currentUser: function () {
                            return currentUser;
                        }
                    },
                    templateUrl: templateUrl + 'energy.suppliers.moveme.html'
                }).result.then(
                    function () {
                    },
                    function() {
                        $state.go('energy.suppliers');
                    }
                );
            }]
        })
        .state('energy.supplier', {
            data: {
                title: 'Supplier'
            },
            params: {
                submittedReview: false
            },
            url: '/suppliers/:id',
            views: {
                '@layout': {
                    controller: 'EnergySupplierController',
                    resolve: {
                        energySupplier: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                            return suppliersService.getSupplierById($stateParams.id);
                        }],
                        reviews : ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getLatestReviewsBySupplierId($stateParams.id);
                        }],
                        suppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('energy', 4);
                        }]
                    },
                    templateUrl: templateUrl + 'energy.supplier.html'
                }
            }
        })
        .state('energy.campaign', {
            data: {
                title: 'Energy campaign'
            },
            url: '/campaign',
            views: {
                '@layout': {
                    controller: 'EnergyCampaignController',
                    templateUrl: templateUrl + 'energy.campaign.html'
                }
            }
        });
}]);