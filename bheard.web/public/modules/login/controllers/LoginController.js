login.controller('LoginController', ['$scope', '$state', 'loginService', 'twitterService', 'facebookService', 'googleService', 'notificationUtility', function ($scope, $state, loginService, twitterService, facebookService, googleService, notificationUtility) {
    function success () {
        $state.go('home', null, { reload: true });
    }

    $scope.model = {
        email: null,
        password: null
    };

    $scope.login = function (model) {
        loginService.login(model).then(
            function () {
                success();
            },
            function (errors) {
                $scope.form.errors = errors;
            }
        );
    };

    $scope.loginWithTwitter = function () {
        twitterService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Twitter, please try again.')
            }
        );
    };

    $scope.loginWithFacebook = function () {
        facebookService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Facebook, please try again.')
            }
        );
    };

    $scope.loginWithGoogle = function () {
        googleService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Google, please try again.')
            }
        );
    };
}]);