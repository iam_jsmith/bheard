login.controller('LogoutController', ['$scope', '$state', function ($scope, $state) {
    $state.go($scope.previousStateName, $scope.previousStateParams, { reload: true });
}]);