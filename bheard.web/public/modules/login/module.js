var login = angular.module('login', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/login/views/';

    $stateProvider
        .state('login', {
            data: {
                title: 'Login',
                class: 'no-feature',
            },
            onEnter: ['$state', 'currentUser', function($state, currentUser) {
                if (currentUser) {
                    $state.go('home');
                }
            }],
            parent: 'default',
            url: '/login',
            views: {
                '@layout': {
                    controller: 'LoginController',
                    templateUrl: templateUrl + 'login.html'
                }
            }
        })
        .state('logout', {
            controller: 'LogoutController',
            data: {
                title: 'Logout',
                class: 'no-feature',
            },
            url: '/logout',
            resolve: {
                logout: ['loginService', function (loginService) {
                    return loginService.logout();
                }]
            }
        });
}]);