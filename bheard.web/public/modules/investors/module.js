var investors = angular.module('investors', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/investors/views/';

    $stateProvider
        .state('investors', {
            data: {
                title: 'Investors | B.heard'
            },
            parent: 'default',
            url: '/investors',
            views: {
                '@layout': {
                    controller: 'InvestorsController',
                    resolve: {
                    },
                    templateUrl: templateUrl + 'index.html'
                }
            }
        });
}]);