contactus.controller('SuggestionController', ['$scope', 'suggestionService', function ($scope, suggestionService) {
    $scope.model = {
        form: {
            whichSector: null,
            whichBrand: null,
            moreInfo: null,
            email: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        suggestionService.post(model.form).then(function () {
            model.formSubmitted = true;
        });
    };
}]);
