contactus.controller('ForCompaniesController', ['$scope', 'forCompaniesService', function ($scope, forCompaniesService) {
    $scope.model = {
        form: {
            companyName: null,
            yourName: null,
            email: null
        },

        formSubmitted: false
    };

    $scope.submit = function (model) {
        forCompaniesService.post(model.form).then(function () {
            model.formSubmitted = true;
        });
    };
}]);
