contactus.controller('HaveYourSayController', ['$scope', 'haveYourSayService', function ($scope, makeItRightService) {
    $scope.model = {
        form: {
            whatHappened: null,
            whichOrganisationWasIt: null,
            email: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        makeItRightService.post(model.form).then(function () {
            model.formSubmitted = true;
        });
    };
}]);
