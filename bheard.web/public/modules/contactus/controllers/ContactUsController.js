contactus.controller('ContactUsController', ['$scope', 'contactUsService', function ($scope, contactUsService) {
    $scope.model = {
        form: {
            name: null,
            email: null,
            natureOfEnquiry: null,
            message: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        contactUsService.post(model.form).then(function () {
            model.formSubmitted = true;
        });
    };
}]);
