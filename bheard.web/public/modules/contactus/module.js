var contactus = angular.module('contactus', []).config(['$stateProvider', function ($stateProvider) {
    var templateUrl = '/modules/contactus/views/';

    $stateProvider
        .state('contactus', {
            data: {
                title: 'Contact us'
            },
            parent: 'default',
            url: '/contactus',
            views: {
                '@layout': {
                    controller: 'ContactUsController',
                    templateUrl: templateUrl + 'contactus.html'
                }
            }
        })
        .state('haveyoursay', {
            data: {
                title: 'Have your say',
                sector: 'haveyoursay'
            },
            parent: 'default',
            url: '/haveyoursay',
            views: {
                '@layout': {
                    controller: 'HaveYourSayController',
                    templateUrl: templateUrl + 'haveyoursay.html'
                }
            }
        })
        .state('forcompanies', {
            data: {
                title: 'For Companies',
                sector: 'forcompanies'
            },
            parent: 'default',
            url: '/forcompanies',
            views: {
                '@layout': {
                    controller: 'ForCompaniesController',
                    templateUrl: templateUrl + 'forcompanies.html'
                }
            }
        })
        .state('suggestion', {
            data: {
                title: 'Suggestion',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/suggestion',
            views: {
                '@layout': {
                    controller: 'SuggestionController',
                    templateUrl: templateUrl + 'suggestion.html'
                }
            }
        });
}]);