contactus.factory('haveYourSayService', ['api', function (api) {
    var urlPrefix = '/haveyoursay';

    return {
        post: function (data) {
            return api.post(urlPrefix, data);
        }
    };
}]);
