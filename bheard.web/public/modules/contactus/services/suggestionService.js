contactus.factory('suggestionService', ['api', function (api) {
    var urlPrefix = '/suggestion';

    return {
        post: function (data) {
            return api.post(urlPrefix, data);
        }
    };
}]);
