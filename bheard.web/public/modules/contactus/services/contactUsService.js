contactus.factory('contactUsService', ['api', function (api) {
    var urlPrefix = '/contactus';

    return {
        post: function (data) {
            return api.post(urlPrefix, data);
        }
    };
}]);
