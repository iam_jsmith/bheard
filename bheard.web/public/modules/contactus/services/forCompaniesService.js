contactus.factory('forCompaniesService', ['api', function (api) {
    var urlPrefix = '/forcompanies';

    return {
        post: function (data) {
            return api.post(urlPrefix, data);
        }
    };
}]);
