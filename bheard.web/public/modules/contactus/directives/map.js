contactus.directive('bMap', function () {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: '/modules/contactus/views/map.html',
        link: function (scope, element) {
            var location = new google.maps.LatLng(51.516037, -0.138266),
                map = new google.maps.Map(element[0], { mapTypeId: google.maps.MapTypeId.ROADMAP, center: location, zoom: 12 }),
                marker = new google.maps.Marker({ position: location, map: map, title: 'We\'re here!' });
        }
    };
});
 