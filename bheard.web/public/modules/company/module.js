var company = angular.module('company', []).config(['$stateProvider', function ($stateProvider) {
    var templateUrl = '/modules/company/views/';

    $stateProvider
        .state('corporate', {
            abstract: true,
            data: {
                title: 'Corporate',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/corporate',
            views: {
                '@layout': {
                    templateUrl: templateUrl + 'corporate.html'
                }
            }
        })
        .state('corporate.aboutus', {
            data: {
                title: 'About Us',
                class: 'no-feature',
            },
            url: '',
            views: {
                '@corporate': {
                    controller: 'CorporateController',
                    templateUrl: templateUrl + 'corporate.aboutus.html'
                }
            }
        })
        .state('corporate.goals', {
            data: {
                title: 'Goals & strategy',
                class: 'no-feature',
            },
            url: '/goals',
            views: {
                '@corporate': {
                    controller: 'GoalsController',
                    templateUrl: templateUrl + 'corporate.goals.html'
                }
            }
        })
        .state('corporate.changecoins', {
            data: {
                title: 'Why Changecoin&reg;?',
                class: 'no-feature',
            },
            url: '/changecoins',
            views: {
                '@corporate': {
                    controller: 'ChangecoinsController',
                    templateUrl: templateUrl + 'corporate.changecoins.html'
                }
            }
        })
        .state('corporate.investors', {
            data: {
                title: 'Investors',
                class: 'no-feature',
            },
            url: '/investors',
            views: {
                '@corporate': {
                    controller: 'InvestorsController',
                    templateUrl: templateUrl + 'corporate.investors.html'
                }
            }
        })
        .state('corporate.company', {
            data: {
                title: 'Company information',
                class: 'no-feature',
            },
            url: '/company',
            views: {
                '@corporate': {
                    controller: 'CompanyController',
                    templateUrl: templateUrl + 'corporate.company.html'
                }
            }
        })
        .state('corporate.jobs', {
            data: {
                title: 'Jobs',
                class: 'no-feature',
            },
            url: '/jobs',
            views: {
                '@corporate': {
                    controller: 'JobsController',
                    templateUrl: templateUrl + 'corporate.jobs.html'
                }
            }
        })
        // todo this should really be somewhere else I think
        .state('sitemap', {
            data: {
                title: 'Site Map',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/sitemap',
            views: {
                '@layout': {
                    controller: 'SitemapController',
                    templateUrl: templateUrl + 'sitemap.html'
                }
            }
        });
}]);
