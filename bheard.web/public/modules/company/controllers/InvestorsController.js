company.controller('InvestorsController', ['$scope', function ($scope) {
    $scope.model = {
        selected: null
    };

    $scope.click = function(name) {
        $scope.model.selected = $scope.model.selected !== name ? name : null;
    };
}]);
