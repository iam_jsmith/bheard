var changecoins = angular.module('changecoins', []).config(['$stateProvider', function ($stateProvider) {
    var templateUrl = '/modules/changecoins/views/';

    $stateProvider
        .state('changecoins', {
            data: {
                title: 'Changecoin&reg; for Charity | B.heard',
                sector: 'changecoins'
            },
            parent: 'default',
            url: '/changecoins',
            views: {
                '@layout': {
                    controller: 'ChangeCoinsController',
                    resolve: {
                        latestDonation: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getLatestDonation();
                        }]
                    },
                    templateUrl: templateUrl + 'changecoins.html'
                }
            }
        })
        .state('changecoins.donate', {
            data: {
                isModal: true
            },
            url: '/donate/:id',
            onEnter: ['$uibModal', '$state', 'currentUser', function ($uibModal, $state, currentUser) {
                $uibModal.open({
                    controller: 'DonateController',
                    resolve: {
                        currentUser: function () {
                            return currentUser;
                        }
                    },
                    templateUrl: templateUrl + 'changecoins.donate.html'
                }).result.then(
                    function () {
                    },
                    function () {
                        $state.go('^');
                    }
                );
            }]
        })
        .state('nominate', {
            data: {
                title: 'Nominate a charity',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/nominate',
            views: {
                '@layout': {
                    controller: 'NominateController',
                    templateUrl: templateUrl + 'nominate.html'
                }
            }
        });
}]);