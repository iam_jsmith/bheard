changecoins.controller('NominateController', ['$scope', 'changeCoinsService', function ($scope, changeCoinsService) {
    $scope.model = {
        form: {
            charityName: null,
            whyShouldWeSupportThem: null,
            email: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        changeCoinsService.nominate(model.form).then(function () {
            model.formSubmitted = true;
        });
    };
}]);