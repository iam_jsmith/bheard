changecoins.controller('DonateController', ['$scope', '$stateParams', '$modalInstance', 'changeCoinsService', 'currentUser', 'currentUserService', function ($scope, $stateParams, $modalInstance, changeCoinsService, currentUser, currentUserService) {
    $scope.model = {
        form: {
            charityId: $stateParams.id,
            amount: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        changeCoinsService.donate(model.form).then(
            function () {
                currentUserService.get().then(function (user) {
                    _.extend(currentUser, user);
                    model.formSubmitted = true;
                });
            },
            function (errors) {
                $scope.form.errors = errors;
            }
        );
    };

    $scope.$on('$stateChangeStart', function () {
        $modalInstance.dismiss();
    });
}]);