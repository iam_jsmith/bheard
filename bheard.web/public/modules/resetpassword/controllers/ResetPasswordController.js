resetpassword.controller('ResetPasswordController', ['$scope', '$state', 'resetPasswordService', function ($scope, $state, resetPasswordService) {
    $scope.model = {
        form: {
            email: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        resetPasswordService.resetPassword(model.form).then(
            function () {
                model.formSubmitted = true;
            },
            function (errors) {
                $scope.form.errors = errors;
            }
        );
    };
}]);