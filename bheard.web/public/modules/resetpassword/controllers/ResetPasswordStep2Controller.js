resetpassword.controller('ResetPasswordStep2Controller', ['$scope', '$stateParams', 'resetPasswordService', function ($scope, $stateParams, resetPasswordService) {
    $scope.model = {
        form: {
            newPassword: null,
            confirmNewPassword: null
        },
        formSubmitted: false
    };

    $scope.submit = function (model) {
        resetPasswordService.resetPasswordStep2($stateParams.uuid, model.form).then(
            function () {
                model.formSubmitted = true;
            },
            function (errors) {
                $scope.form.errors = errors;
            }
        );
    };
}]);