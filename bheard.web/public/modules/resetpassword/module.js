var resetpassword = angular.module('resetpassword', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/resetpassword/views/';

    $stateProvider
        .state('resetpassword', {
            data: {
                title: 'Reset password',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/resetpassword',
            views: {
                '@layout': {
                    controller: 'ResetPasswordController',
                    templateUrl: templateUrl + 'resetpassword.html'
                }
            }
        })
        .state('resetpassword.step2', {
            data: {
                title: 'Reset password',
                class: 'no-feature',
            },
            url: '/:uuid',
            views: {
                '@layout': {
                    controller: 'ResetPasswordStep2Controller',
                    templateUrl: templateUrl + 'resetpassword.step2.html'
                }
            }
        });
}]);