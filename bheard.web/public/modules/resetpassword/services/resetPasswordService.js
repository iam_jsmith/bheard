resetpassword.factory('resetPasswordService', ['api', function (api) {
    var urlPrefix = '/resetpassword';

    return {
        resetPassword: function (data) {
            return api.post(urlPrefix, data);
        },
        resetPasswordStep2: function (uuid, data) {
            return api.post(urlPrefix + '/' + uuid, data);
        }
    };
}]);
