var aboutus = angular.module('aboutus', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/aboutus/views/';

    $stateProvider
        .state('aboutus', {
            data: {
                title: 'About Us | B.heard'
            },
            parent: 'default',
            url: '/aboutus',
            views: {
                '@layout': {
                    controller: 'AboutUsController',
                    templateUrl: templateUrl + 'aboutus.html'
                }
            }
        });
}]);