government.factory('councilsService', ['$http', 'apiKeys', 'councilsFusionTableId', 'fusionTableCsvSerializer', 'suppliersService', 'notificationUtility', function ($http, apiKeys, councilsFusionTableId, fusionTableCsvSerializer, suppliersService, notificationUtility) {
    var urlPrefix = 'https://www.googleapis.com/fusiontables/v2/query?key=' + apiKeys.google + '&sql=';

    return {
        search: function (location, options) {
            var query =
                "SELECT Name " +
                "FROM " + councilsFusionTableId + " " +
                "ORDER BY ST_DISTANCE(Postcode, LATLNG(" + location.lat + "," + location.lng + ")) " +
                "LIMIT " + (options.limit || 5);

            return $http.get(urlPrefix + encodeURIComponent(query)).then(
                function (response) {
                    var councils = fusionTableCsvSerializer.deserialize(response.data);
                    return suppliersService.getSuppliersByNos(_.pluck(councils, 'name'));
                },
                function () {
                    notificationUtility.alert('There was a problem syncing, please try again.');
                });
        }
    };
}]);