var government = angular.module('government', [])
    .constant('councilsFusionTableId', '1F3pYHVI72zVCEBFTvQ2gEvIwkViiDfH4JramlOx8')
    .config(['$stateProvider', function($stateProvider) {
        var templateUrl = '/modules/government/views/';

        $stateProvider
            .state('government', {
                abstract: true,
                data: {
                    sector: 'government'
                },
                parent: 'default',
                url: '/government'
            })
            .state('government.home', {
                data: {
                    title: 'Government - Review & Compare Councils | B.heard'
                },
                url: '',
                views: {
                    '@layout': {
                        controller: 'GovernmentHomeController',
                        resolve: {
                            latestDonation: ['changeCoinsService', function (changeCoinsService) {
                                return changeCoinsService.getLatestDonation();
                            }],
                            reviews: ['suppliersService', function (suppliersService) {
                                return suppliersService.getLatestReviewsBySector('government', 16);
                            }]
                        },
                        templateUrl: templateUrl + 'government.home.html'
                    }
                }
            })
            .state('government.suppliers', {
                data: {
                    title: 'Council'
                },
                reloadOnSearch: false,
                url: '/councils?postcode',
                views: {
                    '@layout': {
                        controller: 'GovernmentCouncilsController',
                        templateUrl: templateUrl + 'government.suppliers.html',
                        resolve: {
                            options: ['$stateParams', function ($stateParams) {
                                return _.extend({}, _.omit($stateParams, 'return'));
                            }],
                            isTouched: ['options', function (options) {
                                return _.some(_.values(options));
                            }],
                            location: ['options', 'geoService', 'randomPostcodeUtility', function (options, geoService, randomPostcodeUtility) {
                                return geoService.geocode(options.postcode || randomPostcodeUtility.get());
                            }],
                            councils: ['options', 'location', 'councilsService', function (options, location, councilsService) {
                                return councilsService.search(location, options);
                            }]
                        }
                    }
                }
            })
            .state('government.supplier', {
                data: {
                    title: 'Council'
                },
                params: {
                    submittedReview: false
                },
                url: '/councils/:id',
                views: {
                    '@layout': {
                        controller: 'GovernmentSupplierController',
                        resolve: {
                            council: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                                return suppliersService.getSupplierById($stateParams.id);
                            }],
                            reviews : ['council', 'suppliersService', function (council, suppliersService) {
                                return suppliersService.getLatestReviewsBySupplierId(council._id);
                            }],
                            suppliers: ['suppliersService', function (suppliersService) {
                                return suppliersService.getRankedSuppliersBySector('government', 4);
                            }]


                            // todo adding search by location so gets the 4 nearest (lower priority at the moment)
                            // council: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                            //     return suppliersService.getSupplierById($stateParams.id);
                            // }],
                            // reviews : ['council', 'suppliersService', function (council, suppliersService) {
                            //     return suppliersService.getLatestReviewsBySupplierId(council._id);
                            // }],
                            //
                            // // options: ['$stateParams', function ($stateParams) {
                            // //     return _.extend({}, _.omit($stateParams, 'return'));
                            // // }],
                            // location: ['council', 'geoService', 'randomPostcodeUtility', function (council, geoService, randomPostcodeUtility) {
                            //     return geoService.geocode(council.postcode || randomPostcodeUtility.get());
                            // }],
                            //
                            // suppliers: ['location', 'councilsService', function (location, councilsService) {
                            //     councilsService.search(location, {'limit': 4}).then(function(suppliers){
                            //         suppliers.forEach(function(supplier, i){
                            //             supplier.rank = i+1;
                            //         })
                            //     });
                            // }]
                            //
                            //
                            // // suppliers: ['suppliersService', function (suppliersService) {
                            // //     return suppliersService.getRankedSuppliersBySector('government', 4);
                            // // }]


                        },
                        templateUrl: templateUrl + 'government.supplier.html'
                    }
                }
            })
            .state('government.local', {
                data: {
                    title: 'Local government'
                },
                reloadOnSearch: false,
                url: '/local',
                views: {
                    '@layout': {
                        controller: 'GovernmentLocalController',
                        templateUrl: templateUrl + 'government.local.html'
                    }
                }
            });
}]);