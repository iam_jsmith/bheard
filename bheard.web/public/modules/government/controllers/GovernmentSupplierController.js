government.controller('GovernmentSupplierController', ['$rootScope', '$scope', 'council', 'reviews', 'suppliers', function ($rootScope, $scope, council, reviews, suppliers) {
    $rootScope.title = council.name;

    $scope.model = {
        council: council,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
