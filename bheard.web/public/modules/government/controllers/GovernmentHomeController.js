government.controller('GovernmentHomeController', ['currentUser', 'latestDonation', 'reviews', '$scope', '$state', function (currentUser, latestDonation, reviews, $scope, $state) {
    $scope.model = {
        latestDonation: latestDonation,
        reviews: reviews,
        options: {
            postcode: currentUser ? currentUser.postcode : null
        }
    };

    $scope.submit = function (options) {
        $state.go('government.suppliers', options);
    };
}]);