government.controller('GovernmentCouncilsController', ['$scope', '$location', 'options', 'isTouched', 'location', 'councils', 'geoService', 'councilsService', function ($scope, $location, options, isTouched, location, councils, geoService, councilsService) {
    function search(location, options) {
        councilsService.search(location, options).then(function (councils) {
            $location.search(options);
            $scope.model.location = location;
            $scope.model.councils = councils;
            $scope.model.isTouched = true;
        });
    }

    $scope.model = {
        councils: councils,
        location: location,
        options: options,
        isTouched: isTouched
    };

    $scope.search = function (options) {
        if (options.postcode) {
            geoService.geocode(options.postcode).then(function (location) {
                search(location, options);
            });
        } else {
            search(location, options);
        }
    };
}]);