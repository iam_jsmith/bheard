government.directive('bCouncilsMap', ['councilsFusionTableId', 'geoService', function (councilsFusionTableId, geoService) {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            model: '=ngModel'
        },
        templateUrl: '/views/map.html',
        link: function (scope, element) {
            var map = new google.maps.Map(element[0], { mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false, zoom: 14 }),
                layer = new google.maps.FusionTablesLayer({ map: map, options: { styleId: 3, templateId: 5 } }),
                marker = null;

            layer.setOptions({
                query: {
                    select: 'Postcode',
                    from: councilsFusionTableId
                }
            });

            scope.$watch('model.location', function (location) {
                if (!marker) {
                    marker = new google.maps.Marker({ map: map, position: location });
                } else {
                    marker.setPosition(location);
                }
            });

            scope.$watch('model.councils', function (councils) {
                geoService.geocode(councils[0].postcode).then(function (location) {
                    map.panTo(location);
                    map.setZoom(14);
                });
            });
        }
    };
}]);