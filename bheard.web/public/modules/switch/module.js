// can't use the reserved keyword switch
var switchmodule = angular.module('switch', [])
.config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/switch/views/';

    $stateProvider
        .state('switch', {
            data: {
                title: 'Switch | B.heard'
            },
            parent: 'default',
            url: '/switch',
            views: {
                '@layout': {
                    controller: 'SwitchController',
                    templateUrl: templateUrl + 'switch.html'
                }
            }
        });
}]);