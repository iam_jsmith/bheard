switchmodule.controller('SwitchController', ['$scope', function($scope) {
    $scope.model = {
        services: [
            {
                title: 'Energy',
                buttonText: 'View all',
                class: 'energy',
                link: 'energy.suppliers'
            },

            {
                title: 'Insurance',
                buttonText: 'View all',
                class: 'insurance',
                link: 'insurance.suppliers'
            },

            {
                title: 'Banks',
                buttonText: 'View all',
                class: 'money',
                link: 'money.products.creditcards'
            },

            {
                title: 'Phone & Internet',
                buttonText: 'View all',
                class: 'phoneandinternet',
                link: 'phoneandinternet.suppliers'
            }

        ]
    };
}]);
