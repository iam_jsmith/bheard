opinion.controller('OpinionController', ['$scope', 'questions', 'userAnswers', '$rootScope', 'opinionService',
function ($scope, questions, userAnswers, $rootScope, opinionService) {
    $scope.model = {
        questions: questions,
        userAnswers: userAnswers
    };

    // console.log(JSON.stringify(userAnswers));

    $scope.answerQuestion = function (rating, question) {
        question.showMode = 'result';
        $rootScope.$broadcast('masonry.reload');

        $scope.model.userAnswers[question.id] = rating;
        opinionService.set(question, rating);
    };

    $scope.getUserAnswer = function (question) {
        // put in some logic for getting the answer
        return $scope.model.userAnswers[question.id];
    };

    $scope.resetCard = function (question) {
        question.showMode = 'question';
    };


    $scope.model.questions.forEach(function (question) {

        if (question.id in userAnswers) {
            question.showMode = 'result';
        }

        switch (question.questionType) {
            case 'multiplechoice':
                question.stats.multiplechoiceGraphData = getMultiplechoiceGraph(question);
                break;
            // case 'sentiment':
            //     question.stats.sentimentGraphData = getSentimentGraph(question);
            //     break;

        }
    });

    var likertData = [
        {
            'name': 'Strongly Agree',
            'value': 0
        },
        {
            'name': 'Agree',
            'value': 0
        },
        {
            'name': 'Disagree',
            'value': 0
        },
        {
            'name': 'Strongly Disagree',
            'value': 0
        }
    ];
    var multiplechoiceData = [];

    function getLikertGraph(question) {
        likertData[0] = question.stats.responsePercentages.stronglyagree;
        likertData[1] = question.stats.responsePercentages.agree;
        likertData[2] = question.stats.responsePercentages.disagree;
        likertData[3] = question.stats.responsePercentages.stronglydisagree;
        return likertData;
    }

    var sentimentData = [];
    function getSentimentGraph(question) {
        sentimentData = [
            {
                'name': 'happy',
                'value': question.stats.responsePercentages['happy']
            },
            {
                'name': 'worried',
                'value': question.stats.responsePercentages['worried']
            },
            {
                'name': 'angry',
                'value': question.stats.responsePercentages["angry"]
            },
            {
                'name': 'neutral',
                'value': question.stats.responsePercentages["neutral"]
            }
        ];
        return sentimentData;
    }

    // todo shouldn't responses be an array? can't loop otherwise
    function getMultiplechoiceGraph(question) {
        multiplechoiceData = [];
        for (var key in question.responses) {
            multiplechoiceData.push({
                'name': question.responses[key],
                'value': question.stats.responsePercentages[key]
            });
        };
        return multiplechoiceData;
    }
}]);