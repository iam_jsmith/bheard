// youropinion.controller('YourOpinionRepMeController', ['$scope', '$state', 'representMeService', '$timeout', 'firstQuestion',
//     function($scope, $state, representMeService, $timeout, firstQuestion) {
//         var questions = [
//             571, 1885, 126, 63, 395, 598, 1232, 77, 61
//         ];
//
//         $scope.model = {
//             items: [
//                 {
//                     id: firstQuestion.id,
//                     question: firstQuestion.question,
//                     stats: firstQuestion.stats,
//                     showMode: 'answer'
//                 }
//             ],
//             questionIndex: 1, // First question has already been loaded in the resolve
//                             // method in the controller so we should only fetch it once
//             question: {},
//             questionStats: {},
//             showStats: false,
//             get currentQuestion () {
//                 return questions[this.questionIndex];
//             }
//         };
//
//         $scope.answerQuestion = function(rating, item) {
//             item.showMode = 'result';
//             $scope.nextQuestion();
//         };
//
//         $scope.nextQuestion = function () {
//             if ($scope.model.currentQuestion) {
//                 representMeService.getQuestionById($scope.model.currentQuestion)
//                     .then(function (question) {
//                         $timeout(function () {
//                             var item = {
//                                 id: question.id,
//                                 question: question.question,
//                                 stats: question.stats,
//                                 showMode: 'answer' // options: 'answer', 'result'
//                             };
//                             $scope.model.items.unshift(item);
//                             $scope.model.questionIndex++;
//                         }, 1000);
//                     }).catch(function(response) {
//                     console.log(response);
//                 });
//             }
//         };
// }]);