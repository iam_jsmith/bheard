opinion.controller('OpinionDetailController', ['$scope', '$uibModal', '$sce', 'topic', 'opinions', function($scope, $uibModal, $sce, topic, opinions) {
    $scope.model = {
        topic: topic,
        opinions: opinions
    };
    
    $scope.getSurveyUrl = function() {
        return $sce.trustAsResourceUrl(topic.main.content.qualtricsUrl);
    }
    
    $scope.config = {
        headerVideoSources: [
            {
                src: $sce.trustAsResourceUrl(topic.main.header.videoUrl),
                type: "video/mp4"
            }
        ],
        theme: "bower_components/videogular-themes-default/videogular.css"
    };
    
    $scope.openRecordModal = function() {
        var modalInstance = $uibModal.open({
            windowClass: 'top-class',
            template: '<div style="text-align: center;"><style type="text/css">#vpm-iframe{width:260px;height:325px;margin:10px 0}#vpm-iframe.fix-iframe{width:640px;height:480px;margin:10px 0}@media (min-width:501px){#vpm-iframe{width:500px;height:375px;margin:10px 0}}@media (min-width:641px){#vpm-iframe{width:640px;height:480px;margin:10px 0}}</style><iframe allowfullscreen="" frameborder="0" height="480" id="vpm-iframe" scrolling="no" src="https://www.voxpopme.com/record/' + topic.main.content.vpmId + '?layout=simple" width="100%"></iframe></div>'
        });    
    };
    
    $scope.openVideoModal = function() {
        var modalInstance = $uibModal.open({
            controller: 'ModalInstanceController',
            resolve: {
                topic: function() {
                    return $scope.model.topic;
                }
            },
            templateUrl: '/views/videoModal.html'
        });
    }
}]);

opinion.controller('ModalInstanceController', ['$scope', '$modalInstance', '$sce', 'topic', function ($scope, $modalInstance, $sce, topic) {
    $scope.config = {
        sources: [
            {
                src: $sce.trustAsResourceUrl(topic.main.content.videoUrl),
                type: "video/mp4"
            }
        ],
        theme: "bower_components/videogular-themes-default/videogular.css"
    };
    
    $scope.onPlayerReady = function(API) {
        $scope.API = API;
        $scope.API.setVolume(1);
    }
}]);