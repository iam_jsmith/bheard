app.factory('opinionService', ['$cookies', 'api', 'currentUserService',
    function ($cookies, api, currentUserService) {
    var urlPrefix = '/opinion';
    var cookieName = 'myOpinions';

    return {
        getAllTopics: function() {
            return api.get(urlPrefix + '/topics');
        },
        getTopicById: function(id) {
            return api.get(urlPrefix + '/topics/' + id);
        },
        getAllOpinions: function() {
            return api.get(urlPrefix + '/opinions');
        },
        getOpinionsByTopicId: function(topicId, limit) {
            return api.get(urlPrefix + '/opinions/' + topicId, { params: { limit: limit } });
        },
        getQuestions: function() {
            return api.get(urlPrefix + '/questions');
        },
        getUserAnswers: function() {
            return $cookies.getObject(cookieName) ? $cookies.getObject(cookieName) : {};
        },
        set: function (question, rating) {
            // $cookies.putObject(cookieName, null); // for debugging, reset cookie
            var opinionData = $cookies.getObject(cookieName) ? $cookies.getObject(cookieName) : {};
            opinionData[question.id] = rating;
            return $cookies.putObject(cookieName, opinionData);
        }
    };
}]);
