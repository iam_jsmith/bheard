var opinion = angular.module('opinion', [])
    .config(['$stateProvider', function($stateProvider) {
        var templateUrl = '/modules/opinion/views/';

        $stateProvider
            // .state('youropinion', {
            //     data: {
            //         title: 'Your Opinion',
            //         sector: 'youropinion'
            //     },
            //     parent: 'default',
            //     url: '/youropinion',
            //     views: {
            //         '@layout': {
            //             controller: 'YourOpinionController',
            //             resolve: {
            //                 topics: ['yourOpinionService', function(yourOpinionService) {
            //                     return yourOpinionService.getAllTopics();
            //                 }]
            //             },
            //             templateUrl: templateUrl + 'youropinion.html'
            //         }
            //     }
            // })
            .state('opinion', {
                data: {
                    title: 'Share Opinions',
                    sector: 'opinion'
                },
                parent: 'default',
                url: '/opinion',
                views: {
                    '@layout': {
                        controller: 'OpinionController',
                        resolve: {
                            questions: ['opinionService', function (opinionService) {
                                return opinionService.getQuestions();
                            }],
                            userAnswers: ['opinionService', function (opinionService) {
                                return opinionService.getUserAnswers();
                            }]
                        },
                        templateUrl: templateUrl + 'opinion.html'
                    }
                }
            });
            // .state('youropinion.detail', {
          //       data: {
          //           title: 'Your Opinion'
          //       },
          //       url: '/:topicId',
          //       views: {
          //           '@layout': {
          //               controller: 'YourOpinionDetailController',
          //               resolve: {
          //                   topic: ['$stateParams', 'yourOpinionService', function($stateParams, yourOpinionService) {
          //                       return yourOpinionService.getTopicById($stateParams.topicId);
          //                   }],
          //                   opinions: ['$stateParams', 'yourOpinionService', function($stateParams, yourOpinionService) {
          //                       return yourOpinionService.getOpinionsByTopicId($stateParams.topicId, 8);
          //                   }]
          //               },
          //               templateUrl: templateUrl + 'youropinion.detail.html'
          //           }
          //       }
          //   });
    }]);