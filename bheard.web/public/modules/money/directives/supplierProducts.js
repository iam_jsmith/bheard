app.directive('bSupplierProducts', ['$state', 'suppliersService', function ($state, suppliersService) {
    return {
        replace: true,
        restrict: 'E',
        scope: {
            products: '=ngModel',
            urlFragment: '=bUrlFragment'
        },
        templateUrl: '/modules/money/views/supplierproducts.html',
        link: function(scope) {
            // used by scope.getCategorisedBenefits to preserve object identity. Not to be accessed directly
            scope.categorisedBenefits = {};

            // Credit card functions
            scope.getIntroTrfRate = function (product) {
                if (product.rates.balanceTransfers.introTrf) {
                    for (var i = 0; i < product.rates.balanceTransfers.introTrf.iTrfRates.length; i++) {
                        return product.rates.balanceTransfers.introTrf.iTrfRates[i];
                    }
                }
                return null;
            };
            scope.getIntroRate = function (product) {
                if (product.rates.introductory) {
                    for (var i = 0; i < product.rates.introductory.introRates.length; i++) {
                        var introRate = product.rates.introductory.introRates[i];
                        if (introRate.rateType == 'Intro Purchases') {
                            return introRate;
                        }
                    }
                }
                return null;
            };
            scope.getStandardRate = function (product) {
                for (var i = 0; i < product.rates.standard.stdRates.length; i++) {
                    var stdRate = product.rates.standard.stdRates[i];
                    if (stdRate.rateType == 'Standard Purchases') {
                        return stdRate;
                    }
                }
                return null;
            };

            // Current accounts functions

            scope.getCurrentAccountInterestRate = function (product) {
                // Some products don't have 'in credit' rates so we say their interest rate is 0
                if (!product.inCredits) return 0.00;//'<b>0% AER</b> (variable)';
                // First find the rate that applies for the start of the account.
                var firstPeriod = product.inCredits[0].periods.reduce(
                    function (first, period) {
                        return period.mthsLower == "1" ? period : first;
                    }, null);
                // Now we have the correct period we need to find the rate that corresponds to the £1000 tier
                var rate = firstPeriod.rates.reduce(
                    function (result, rate) {
                        return rate.tierUpper >= 1000 && rate.tierLower <= 1000 ? rate : result;
                    }, null);

                    return rate != null ? rate.aer : 0.00;
                    // return '<b>' + rate.aer.format(2) + '% AER</b> (variable)';
            };

            scope.getCurrentAccountRatesInTiers = function (product) {
                return product && product.inCredits && product.inCredits[0] ? product.inCredits[0].periods.map(function (period) {
                    return period.rates;
                    // return period.rates.map(function(rate) {
                    //     var rate = rate;
                    //     rate.period = {
                    //         mthsLower: period.mthsLower,
                    //         mthsUpper: period.mthsUpper
                    //     };
                    //     return rate;
                    //     // return _.extend(rate, {
                    //         // mthsLower: period.mthsLower,
                    //         // mthsUpper: period.mthsUpper
                    //     // });
                    // });
                })
                : null;
            };

            scope.getCategorisedBenefits = function getCategorisedBenefits(product) {
                // easier alias
                var sections = scope.categorisedBenefits;

                product.benefits.forEach(function(benefit) {
                    if (sections[benefit.section] === undefined) {
                        // new section, initialise with array containing first benefit
                        sections[benefit.section] = [benefit];
                    } else {
                        // section already exists

                        // check benefit does not exist before inserting

                        var benefitExists = false;
                        sections[benefit.section].forEach(function(section) {
                            if (section.type === benefit.type) {
                                benefitExists = true;
                            }
                        });

                        if (!benefitExists) {
                            sections[benefit.section].push(benefit);
                        }
                    }
                });

                return sections;
            };

            // Loan functions
            scope.getLoanRepExample = function (product) {
                var example = null;
                for (var i = 0; i < product.rates.examples.length; i++) {
                    if (product.rates.examples[i].exampleAdvance == '10000') {
                        example = product.rates.examples[i];
                    } else if (example == null && product.rates.examples[i].exampleRepresentativeAPR
                            == product.rates.repAprAdvertised) {
                        example = product.rates.examples[i];
                    }
                }
                if (example == null) {
                    example = product.rates.examples[product.rates.examples.length - 1];
                }
                return example;
            };

            // Savings functions
            scope.getSavingsAccountInterestRate = function (product) {
                if (product.rates.length > 1) {
                    var highestRate = product.rates.reduce(
                        function (maxRate, rate) {
                            return Math.max(maxRate, rate.grossAER);
                        }, 0);
                    return 'Up to ' + highestRate + '% Gross AER';
                } else {
                    // not sure if grossAER is a string or number so convert to be safe
                    return '' + product.rates[0].grossAER + '% Gross AER'
                }
            };
        }
    };
}]);
