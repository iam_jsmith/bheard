money.controller('BankController', ['$rootScope', '$scope', '$window', '$location', 'bank', 'reviews', 'products',
    'creditCardsService', 'currentAccountsService', 'unsecuredLoansService', 'savingAccountsService',
function ($rootScope, $scope, $window, $location, bank, reviews, products,
    creditCardsService, currentAccountsService, unsecuredLoansService, savingAccountsService
) {
    $rootScope.title = bank.name;

    $scope.urlFragment = $window.location.hash.substr(1);
    $scope.model = {
        bank: bank,
        reviews: reviews,
        products: products
    };

    // watch the hash and change scope variable on change
    $scope.$on('$locationChangeSuccess', function(event, newUrl, oldUrl){
        $scope.urlFragment = $window.location.hash.substr(1);
        getSuppliers();
    });

    getSuppliers();

    // todo can someone tell me (Tim) if this is the correct Angular way of doing function in controllers which dont need to be available on scope
    function getSuppliers () {
        switch ($scope.urlFragment) {
            case 'credit-cards':
                // this gets ranked
                creditCardsService.getCreditCards(0, 4).then(function (suppliers) {
                    suppliers = suppliers.map(function(supplier, i){
                        // show product rank instead of supplier rank
                        supplier.bank.rank = i+1;
                        return supplier.bank;
                    });
                    $scope.model.suppliers = suppliers;
                });
                break;
            case 'current-accounts':
                currentAccountsService.getCurrentAccounts(0, 4).then(function (suppliers) {
                    suppliers = suppliers.map(function(supplier, i){
                        // show product rank instead of supplier rank
                        supplier.bank.rank = i+1;
                        return supplier.bank;
                    });
                    $scope.model.suppliers = suppliers;
                });
                break;
            case 'loans':
                unsecuredLoansService.getUnsecuredLoans(0, 4).then(function (suppliers) {
                    suppliers = suppliers.map(function(supplier, i){
                        // show product rank instead of supplier rank
                        supplier.bank.rank = i+1;
                        return supplier.bank;
                    });
                    $scope.model.suppliers = suppliers;
                });
                break;
            case 'savings':
                savingAccountsService.getSavingAccounts(0, 4).then(function (suppliers) {
                    suppliers = suppliers.map(function(supplier, i){
                        // show product rank instead of supplier rank
                        supplier.bank.rank = i+1;
                        return supplier.bank;
                    });
                    $scope.model.suppliers = suppliers;
                });
                break;

        }
    }
}]);
