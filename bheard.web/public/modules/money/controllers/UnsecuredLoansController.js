money.controller('UnsecuredLoansController', ['$scope', 'unsecuredLoans', 'unsecuredLoansService', function ($scope, unsecuredLoans, unsecuredLoansService) {
    var skip = 0,
        take = 10;

    $scope.urlFragment = 'loans';
    $scope.model = {
        unsecuredLoans: unsecuredLoans
    };

    $scope.more = function (model) {
        unsecuredLoansService.getUnsecuredLoans(skip += take, take).then(function (unsecuredLoans) {
            model.unsecuredLoans.push.apply(model.unsecuredLoans, unsecuredLoans);
        });
    };
}]);