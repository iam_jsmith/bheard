money.controller('CreditCardsController', ['$scope', 'creditCards', 'creditCardsService', function ($scope, creditCards, creditCardsService) {
    var skip = 0,
        take = 10;

    $scope.urlFragment = 'credit-cards';
    $scope.model = {
        creditCards: creditCards
    };

    $scope.more = function (model) {
        creditCardsService.getCreditCards(skip += take, take).then(function (creditCards) {
            model.creditCards.push.apply(model.creditCards, creditCards);
        });
    };
}]);