money.controller('SavingAccountsController', ['$scope', 'savingAccounts', 'savingAccountsService', function ($scope, savingAccounts, savingAccountsService) {
    var skip = 0,
        take = 10;

    $scope.urlFragment = 'savings';
    $scope.model = {
        savingAccounts: savingAccounts
    };

    $scope.more = function (model) {
        savingAccountsService.getSavingAccounts(skip += take, take).then(function (savingAccounts) {
            model.savingAccounts.push.apply(model.savingAccounts, savingAccounts);
        });
    };
}]);