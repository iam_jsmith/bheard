money.controller('CurrentAccountsController', ['$scope', 'currentAccounts', 'currentAccountsService', function ($scope, currentAccounts, currentAccountsService) {
    var skip = 0,
        take = 10;

    $scope.urlFragment = 'current-accounts';
    $scope.model = {
        currentAccounts: currentAccounts
    };

    $scope.more = function (model) {
        currentAccountsService.getCurrentAccounts(skip += take, take).then(function (currentAccounts) {
            model.currentAccounts.push.apply(model.currentAccounts, currentAccounts);
        });
    };
}]);