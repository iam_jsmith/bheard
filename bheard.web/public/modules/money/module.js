var money = angular.module('money', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/money/views/';

    $stateProvider
        .state('money', {
            abstract: true,
            data: {
                sector: 'money'
            },
            parent: 'default',
            url: '/money'
        })
        .state('money.home', {
            data: {
                title: 'Rate & Compare Banks and Building Societies | B.heard'
            },
            url: '',
            views: {
                '@layout': {
                    controller: 'MoneyHomeController',
                    resolve: {
                        latestDonation: ['changeCoinsService', function (changeCoinsService) {
                            return changeCoinsService.getLatestDonation();
                        }],
                        rankedSuppliers: ['suppliersService', function (suppliersService) {
                            return suppliersService.getRankedSuppliersBySector('money');
                        }],
                        reviews: ['suppliersService', function (suppliersService) {
                            return suppliersService.getLatestReviewsBySector('money', 16);
                        }]
                    },
                    templateUrl: templateUrl + 'money.home.html'
                }
            }
        })
        .state('money.products', {
            abstract: true,
            url: '/products',
            views: {
                'banner@layout': {
                    templateUrl: templateUrl + 'money.products.banner.html'
                }
            }
        })
        .state('money.products.currentaccounts', {
            data: {
                title: 'Current accounts'
            },
            url: '/currentaccounts',
            views: {
                '@layout': {
                    controller: 'CurrentAccountsController',
                    resolve: {
                        currentAccounts: ['currentAccountsService', function (currentAccountsService) {
                            return currentAccountsService.getCurrentAccounts(0, 10);
                        }]
                    },
                    templateUrl: templateUrl + 'money.products.currentaccounts.html'
                }
            }
        })
        .state('money.products.savingaccounts', {
            data: {
                title: 'Saving accounts'
            },
            url: '/savingaccounts',
            views: {
                '@layout': {
                    controller: 'SavingAccountsController',
                    resolve: {
                        savingAccounts: ['savingAccountsService', function (savingAccounts) {
                            return savingAccounts.getSavingAccounts(0, 10);
                        }]
                    },
                    templateUrl: templateUrl + 'money.products.savingaccounts.html'
                }
            }
        })
        .state('money.products.unsecuredloans', {
            data: {
                title: 'Unsecured loans'
            },
            url: '/unsecuredloans',
            views: {
                '@layout': {
                    controller: 'UnsecuredLoansController',
                    resolve: {
                        unsecuredLoans: ['unsecuredLoansService', function (unsecuredLoansService) {
                            return unsecuredLoansService.getUnsecuredLoans(0, 10);
                        }]
                    },
                    templateUrl: templateUrl + 'money.products.unsecuredloans.html'
                }
            }
        })
        .state('money.products.creditcards', {
            data: {
                title: 'Credit cards'
            },
            url: '/creditcards',
            views: {
                '@layout': {
                    controller: 'CreditCardsController',
                    resolve: {
                        creditCards: ['creditCardsService', function (creditCardsService) {
                            return creditCardsService.getCreditCards(0, 10);
                        }]
                    },
                    templateUrl: templateUrl + 'money.products.creditcards.html'
                }
            }
        })
        .state('money.supplier', {
            data: {
                title: 'Bank'
            },
            params: {
                submittedReview: false
            },
            url: '/bank/:id',
            views: {
                '@layout': {
                    controller: 'BankController',
                    resolve: {
                        bank : ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getSupplierById($stateParams.id);
                        }],
                        reviews : ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getLatestReviewsBySupplierId($stateParams.id);
                        }],
                        products: ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getSupplierProducts($stateParams.id);
                        }]
                    },
                    templateUrl: templateUrl + 'money.supplier.html'
                }
            }
        })
}]);
