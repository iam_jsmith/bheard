money.factory('unsecuredLoansService', ['api', function (api) {
    var urlPrefix = '/unsecuredloans';

    return {
        getUnsecuredLoans: function (skip, take, onePerSupplier, fca, whitelisted) {
            onePerSupplier = onePerSupplier || true;
            fca = fca || true;
            whitelisted = whitelisted || true;
            return api.get(urlPrefix, { params:
                { skip: skip, take: take, onePerSupplier: onePerSupplier, fca: fca, whitelisted: whitelisted }
            });
        },
        get: function (id) {
            return api.get(urlPrefix + (id ? '/' + id : ''));
        }
    };
}]);
