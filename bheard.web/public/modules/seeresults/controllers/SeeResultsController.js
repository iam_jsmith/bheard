seeresults.controller('SeeResultsController', ['$scope', function($scope) {
    $scope.model = {
        public: [
            {
                title: 'Local Councils',
                buttonText: 'Find your council',
                class: 'government',
                link: 'government.suppliers'
            },

            {
                title: 'Local Schools',
                buttonText: 'Search near you',
                class: 'education',
                link: 'education.suppliers'
            },

            {
                title: 'Local GPs',
                buttonText: 'Find your local GP',
                class: 'healthcare',
                link: 'healthcare.suppliers'
            }
        ],

        private: [
            {
                title: 'Energy',
                buttonText: 'View all',
                class: 'energy',
                link: 'energy.suppliers'
            },

            {
                title: 'Banks',
                buttonText: 'View all',
                class: 'money',
                link: 'money.products.creditcards'
            },

            {
                title: 'Phone & Internet',
                buttonText: 'View all',
                class: 'phoneandinternet',
                link: 'phoneandinternet.suppliers'
            },

            // {
            //     title: 'Insurance',
            //     buttonText: 'View all',
            //     class: 'insurance',
            //     link: 'insurance.home'
            // }
        ]
    };
}]);
