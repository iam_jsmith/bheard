var seeresults = angular.module('seeresults', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/seeresults/views/';

    $stateProvider
        .state('seeresults', {
            data: {
                title: 'See Results | B.heard'
            },
            parent: 'default',
            url: '/seeresults',
            views: {
                '@layout': {
                    controller: 'SeeResultsController',
                    templateUrl: templateUrl + 'seeresults.html'
                }
            }
        });
}]);