news.factory('newsService', ['api', function (api) {
    var urlPrefix = '/news';

    return {
        getPosts: function (pageToken) {
            return api.get(urlPrefix + '/posts', { params: { pageToken: pageToken } });
        },
        
        getPostsWithLabels: function(pageToken, label) {
            return api.get(urlPrefix + '/posts/labels', { params: { pageToken: pageToken, labels: label } });
        },

        getPost: function (id) {
            return api.get(urlPrefix + '/posts/' + id);
        }
    };
}]);