news.controller('NewsController', ['posts', '$scope', 'newsService', function(posts, $scope, newsService) {
    var nextPageToken = posts.nextPageToken;
    var previousToken;

    $scope.model = {
        posts: posts.items,
        maxNumberOfChars: 300,
        selectedCategory: null
    };
    
    $scope.categories = [
        { name: 'Our news', selected: false },
        { name: 'Banking', selected: false },
        { name: 'Brexit', selected: false },
        { name: 'Business', selected: false },
        { name: 'Customer Service', selected: false },
        { name: 'Dependability Index', selected: false },
        { name: 'Energy', selected: false },
        { name: 'Fatherly', selected: false },
        { name: 'Healthcare', selected: false },
        { name: 'Latest Campaign News', selected: false },
        { name: 'Leave', selected: false },
        { name: 'Local Council', selected: false },
        { name: 'Parenting', selected: false },
        { name: 'Phone & Internet', selected: false },
        { name: 'Politics', selected: false },
        { name: 'Press', selected: false },
        { name: 'Price Comparison', selected: false },
        { name: 'Remain', selected: false },
        { name: 'The Great Unheard', selected: false },
        { name: 'Transport', selected: false }
    ];
    
    $scope.toggleFilter = function (position) {
        var cat = $scope.categories[position];
        if (cat === $scope.model.selectedCategory) {
            // Remove filter
            newsService.getPosts()
                .then(function (posts) {
                    nextPageToken = posts.nextPageToken;
                    $scope.model.posts = posts.items;
                })
                .catch(function (err) {
                });
            // Update view
            cat.selected = false;
            $scope.model.selectedCategory = null;
        } else {
            $scope.model.selectedCategory = cat;
            angular.forEach($scope.categories, function(category, index) {
                if (position != index) {
                    category.selected = false;
                }
            });
            newsService.getPostsWithLabels(null, $scope.model.selectedCategory.name)
                .then(function(posts) {
                    nextPageToken = posts.nextPageToken;
                    $scope.model.posts = posts.items;
                })
                .catch(function (err) {
                });
        }
    }
    
    $scope.isChecked = function(category) {
        return category === $scope.model.selectedCategory;
    }
    
    $scope.getNewsCategory = function(post) {
        
        for (var i = 0; i < $scope.categories.length; i++) {
            if (post.labels.indexOf($scope.categories[i].name) > -1) {
                return $scope.categories[i].name;
            }
        }
        return '';
    }

    $scope.more = function (model) {
        if (nextPageToken && nextPageToken !== previousToken) {
            previousToken = nextPageToken;
            if (model.selectedCategory !== null) {
                newsService.getPostsWithLabels(nextPageToken, model.selectedCategory.name).then(function(posts) {
                    nextPageToken = posts.nextPageToken;
                    model.posts.push.apply(model.posts, posts.items);
                });
            } else {
                newsService.getPosts(nextPageToken).then(function (posts) {
                    nextPageToken = posts.nextPageToken;
                    model.posts.push.apply(model.posts, posts.items);
                });
            }
        }
    };
}]);
