news.controller('NewsPostController', ['post', '$rootScope', '$scope', function(post, $rootScope, $scope) {
    $rootScope.title = post.title;

    $scope.model = {
        post: post
    };
}]);
