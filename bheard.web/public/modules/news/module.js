var news = angular.module('news', [])
    .config(['$stateProvider', function($stateProvider) {
        var templateUrl = '/modules/news/views/';

        $stateProvider
            .state('news', {
                data: {
                    title: 'News',
                    sector: 'news'
                },
                parent: 'default',
                url: '/news',
                views: {
                    '@layout': {
                        controller: 'NewsController',
                        resolve: {
                            posts: ['newsService', function (newsService) {
                                return newsService.getPosts();
                            }]
                        },
                        templateUrl: templateUrl + 'news.html'
                    }
                }
            })
            .state('newspost', {
                data: {
                    title: 'Post',
                    'class': 'no-feature'
                },
                parent: 'default',
                url: '/news/:id',
                views: {
                    '@layout': {
                        controller: 'NewsPostController',
                        resolve: {
                            post: ['$stateParams', 'newsService', function ($stateParams, newsService) {
                                return newsService.getPost($stateParams.id);
                            }]
                        },
                        templateUrl: templateUrl + 'newspost.html'
                    }
                }
            });
}]);