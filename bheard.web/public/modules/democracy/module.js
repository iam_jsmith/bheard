var democracy = angular.module('democracy', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/democracy/views/';

    $stateProvider
        .state('democracy', {
            data: {
                title: 'Democracy | B.heard'
            },
            parent: 'default',
            url: '/democracy',
            views: {
                '@layout': {
                    controller: 'DemocracyController',
                    templateUrl: templateUrl + 'democracy.html'
                }
            }
        })
        .state('democracy.capitalism', {
            data: {
                title: 'Capitalism | B.heard'
            },
            parent: 'default',
            url: '/democracy/capitalism',
            views: {
                '@layout': {
                    controller: 'DemocracyController',
                    templateUrl: templateUrl + 'capitalism.html'
                }
            }
        }
    );
}]);