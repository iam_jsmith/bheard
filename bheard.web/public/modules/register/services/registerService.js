register.factory('registerService', ['api', function(api) {
    var urlPrefix = '/register';

    return {
        register: function(data) {
            return api.post(urlPrefix, data);
        },
        registerStep2: function(uuid) {
            return api.post(urlPrefix + '/' + uuid);
        }
    };
}]);
