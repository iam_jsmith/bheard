register.controller('RegisterController', ['$scope', '$state', 'registerService', 'twitterService', 'facebookService', 'googleService', 'notificationUtility', function ($scope, $state, registerService, twitterService, facebookService, googleService, notificationUtility) {
    $scope.model = {
        form: {
            unconfirmedEmail: null,
            password: null,
            confirmPassword: null,
            firstName: null,
            lastName: null,
            postcode: null,
            hasAgreedToTermsAndConditions: false
        },
        formSubmitted: false
    };

    $scope.register = function (model) {
        registerService.register(model.form).then(
            function () {
                model.formSubmitted = true;
            },
            function (errors) {
                $scope.form.errors = errors;
            }
        );
    };

    $scope.loginWithTwitter = function () {
        twitterService.login().then(
            function () {
                $state.go('home', null, { reload: true });
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Twitter, please try again.')
            }
        );
    };

    $scope.loginWithFacebook = function () {
        facebookService.login().then(
            function () {
                $state.go('home', null, { reload: true });
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Facebook, please try again.')
            }
        );
    };

    $scope.loginWithGoogle = function () {
        googleService.login().then(
            function () {
                $state.go('home', null, { reload: true });
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Google, please try again.')
            }
        );
    };
}]);