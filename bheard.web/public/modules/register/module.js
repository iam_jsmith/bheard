var register = angular.module('register', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/register/views/';

    $stateProvider
        .state('register', {
            data: {
                title: 'Register',
                class: 'no-feature',
            },
            parent: 'default',
            url: '/register',
            views: {
                '@layout': {
                    controller: 'RegisterController',
                    templateUrl: templateUrl + 'register.html'
                }
            }
        })
        .state('register.step2', {
            data: {
                title: 'Register',
                class: 'no-feature',
            },
            url: '/:uuid',
            views: {
                '@layout': {
                    resolve: {
                        ok: ['$stateParams', 'registerService', function ($stateParams, registerService) {
                            return registerService.registerStep2($stateParams.uuid);
                        }]
                    },
                    templateUrl: templateUrl + 'register.step2.html'
                }
            }
        });
}]);