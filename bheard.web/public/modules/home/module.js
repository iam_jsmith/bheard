var home = angular.module('home', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/home/views/';

    $stateProvider
        .state('home', {
            data: {
                title: 'B.heard'
            },
            parent: 'default',
            url: '/',
            views: {
                '@layout': {
                    controller: 'HomeController',
                    resolve: {
                        reviews: ['suppliersService', function (suppliersService) {
                            return suppliersService.getLatestReviews(16);
                        }],
                        // used for linking reviews to an 'anonymous' supplier
                        anonymousSupplier: ['suppliersService', function (suppliersService) {
                            return suppliersService.search({ name: 'Anonymous', limit: 1})
                        }]
                    },
                    templateUrl: templateUrl + 'home.html'
                }// ,
//                 'full-screen@layout' : {
//                     controller: 'HomeVideoController',
//                     templateUrl: templateUrl + 'home.video.html'
//                 }
            }
        });
}]);