home.controller('HomeController',
['reviews', '$scope', 'suppliersService', '$state', 'anonymousSupplier', '$window', '$location', '$uibModal',
function (reviews, $scope, suppliersService, $state, anonymousSupplier, $window, $location, $uibModal, $attrs) {
    $scope.siteAreas = [
        {
            title: 'Opinions',
            subtitle: 'Share opinions on the topics that matter or are just funny!',
            key: 'opinion',
            link: 'opinion'
        },
        {
            title: 'Reviews',
            subtitle: 'Share reviews on the services you like or dislike the most',
            key: 'review',
            link: 'review'
        },
        {
            title: 'Switching',
            subtitle: 'Make your money talk by switching to the best ranked',
            key: 'switch',
            link: 'switch'
        }
    ];

    $scope.model = {
        links: [
            {
                title: 'Opinions',
                buttonText: 'Share opinions on the topics that matter or are just funny!',
                class: 'opinion',
                link: 'opinion'
            },

            {
                title: 'Reviews',
                buttonText: 'Share reviews on the services you like or dislike the most',
                class: 'review',
                link: 'review'
            },

            {
                title: 'Switching',
                buttonText: 'Make your money talk by Switching to the best ranked',
                class: 'switch',
                link: 'switch'
            }
        ],
        reviews: reviews,
        anonymousSupplier: anonymousSupplier,
        review: {
            overallOpinion: null,
            customerService: 1,
            valueForMoney: 1,
            title: null,
            text: null,
            isAnonymous: true,
            isCertified: false,
            suggestedCompanyName: null
        },
        page: 1,
        prevPage: 0,
        numPages: 5
    };

    $scope.name = null;
    $scope.sector = null;
    $scope.subsector = null;

    $scope.goToNextPage = function() {
        if ($scope.model.page < $scope.model.numPages) {
            $scope.model.prevPage = $scope.model.page;
            $scope.model.page++;
        }
    };

    $scope.goToPageFrom = function(page, prevPage) {
        if (page <= $scope.model.numPages) {
            $scope.model.prevPage = prevPage;
            $scope.model.page = page;
        }
    }

    $scope.goToPrevPage = function() {
        if ($scope.model.page > 1) {
            $scope.model.prevPage = $scope.model.page;
            $scope.model.page--;
        }
    };

    $scope.search = function (name) {
        return suppliersService.search({ name: name, limit: 5 });
    };

    $scope.select = function(supplier) {
        $scope.goToNextPage();
    }

    $scope.openRecordPopup = function() {
        $scope.model.review.text = " ";
        var modalInstance = $uibModal.open({
            windowClass: 'top-class',
            template: '<div style="text-align: center;"><style type="text/css">#vpm-iframe{width:260px;height:325px;margin:10px 0}#vpm-iframe.fix-iframe{width:640px;height:480px;margin:10px 0}@media (min-width:501px){#vpm-iframe{width:500px;height:375px;margin:10px 0}}@media (min-width:641px){#vpm-iframe{width:640px;height:480px;margin:10px 0}}</style><iframe allowfullscreen="" frameborder="0" height="480" id="vpm-iframe" scrolling="no" src="https://www.voxpopme.com/record/a1de28a2a234e602c3f2462636f92045?layout=simple" width="100%"></iframe></div>'
        });
    }

    $scope.getShareUrl = function() {
        if ($scope.name != null && $scope.name._id != null) {
            return $state.href($scope.name.sector + '.supplier', {id: $scope.name._id}, {absolute: true});
        } else {
            return encodeURIComponent($location.absUrl());
        }
    };

    $scope.getShareText = function() {
        if ($scope.name != null && $scope.name.name != null) {
            return encodeURIComponent("I've just reviewed " + $scope.name.name + " on B.heard");
        } else {
            return encodeURIComponent("I've just left a review on B.heard");
        }
    }

    $scope.submitReview = function() {
        var urlId = null;
        // check suggested Company Name first, otherwise can go backwards (after picking supplier) and submit wrong Id
        if (typeof $scope.name._id === 'undefined') {
            urlId =  $scope.model.anonymousSupplier[0]._id;
        } else if ($scope.name !== null && $scope.name._id !== null) {
            urlId = $scope.name._id;
        }

        suppliersService.postReview(urlId, $scope.model.review).then(function (review) {
            $scope.goToNextPage();
            $scope.sector = review.sector;
            $scope.subsector = review.subsector;
        }).catch(function(response) {
            console.log(response);
        });
    };

    $scope.getThanksTitle = function() {
        switch($scope.model.review.overallOpinion) {
            case "1":
                return "Ouch!!";
            case "2":
                return "That's not great!";
            case "3":
                return "Pretty average...";
            default:
                return "Thank you";
        }
    };


    $scope.getSwitchData = function() {
        var switchData = {
            subtitle: null,
            buttons: []
        };
        if ($scope.model.review.overallOpinion < 4 && $scope.name && $scope.name.sector) {
            switch ($scope.name.sector) {
                case 'energy':
                    switchData.subtitle = "Want to change?";
                    switchData.buttons[1] = {
                        label: 'Look for a better energy deal',
                        url: 'http://energy.bheard.com'
                    };
                    break;
                case 'phoneandinternet':
                    if ($scope.name.mobile_provider && !$scope.name.broadband_provider) {
                        switchData.subtitle = "Want to change?";
                        switchData.buttons[1] = {
                            label: 'Look for a better mobile deal',
                            url: 'http://mobile.bheard.com'
                        };
                    } else if (!$scope.name.mobile_provider && $scope.name.broadband_provider) {
                        switchData.subtitle = "Want to change?";
                        switchData.buttons[1] = {
                            label: 'Look for a better internet deal',
                            url: 'http://media.bheard.com'
                        };
                    } else {
                        switchData.subtitle = "Or make a change to your...";
                        switchData.buttons[1] = {
                            label: 'Mobile',
                            url: 'http://mobile.bheard.com'
                        };
                        switchData.buttons[2] = {
                            label: 'Internet',
                            url: 'http://media.bheard.com'
                        };
                    }
                    break;
            }
        }
        return switchData;
    };

    $scope.share = function(shareLink) {
        $window.open(shareLink, '_blank');
    };

    $scope.shareOnFB = function() {
        $scope.share('http://facebook.com/sharer.php?u=' + $scope.getShareUrl());
    };

    $scope.shareOnTwitter = function() {
        $scope.share('http://twitter.com/intent/tweet?text=' +  $scope.getShareText() + '%20' + $scope.getShareUrl());
    };

    $scope.shareOnGooglePlus = function() {
        $scope.share('https://plus.google.com/share?url=' + $scope.getShareUrl());
    };

}]);