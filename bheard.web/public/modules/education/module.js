var education = angular.module('education', [])
    .constant('schoolsFusionTableId', '1iWwbb-TCFd2sCnNZt3eRbpxqr3OUaOtVpyCujx9V')
    .config(['$stateProvider', function ($stateProvider) {
        var templateUrl = '/modules/education/views/';

        $stateProvider
            .state('education', {
                abstract: true,
                data: {
                    sector: 'education'
                },
                parent: 'default',
                url: '/education'
            })
            .state('education.home', {
                data: {
                    title: 'Rate & Compare Schools, Colleges and Universities | B.heard'
                },
                url: '',
                views: {
                    '@layout': {
                        controller: 'EducationHomeController',
                        resolve: {
                            latestDonation: ['changeCoinsService', function (changeCoinsService) {
                                return changeCoinsService.getLatestDonation();
                            }],
                            reviews: ['suppliersService', function (suppliersService) {
                                return suppliersService.getLatestReviewsBySector('education', 16);
                            }]
                        },
                        templateUrl: templateUrl + 'education.home.html'
                    }
                }
            })
            .state('education.suppliers', {
                data: {
                    title: 'Schools'
                },
                reloadOnSearch: false,
                url: '/schools?postcode&type&rating',
                views: {
                    '@layout': {
                        controller: 'EducationSuppliersController',
                        resolve: {
                            options: ['$stateParams', function ($stateParams) {
                                return _.extend({}, _.omit($stateParams, 'return'));
                            }],
                            isTouched: ['options', function (options) {
                                return _.some(_.values(options));
                            }],
                            location: ['options', 'geoService', 'randomPostcodeUtility', function (options, geoService, randomPostcodeUtility) {
                                return geoService.geocode(options.postcode || randomPostcodeUtility.get());
                            }],
                            schools: ['options', 'location', 'schoolsService', function (options, location, schoolsService) {
                                return schoolsService.search(location, options);
                            }]
                        },
                        templateUrl: templateUrl + 'education.suppliers.html'
                    }
                }
            })
            .state('education.supplier', {
                data: {
                    title: 'School'
                },
                params: {
                    submittedReview: false
                },
                url: '/schools/:id',
                views: {
                    '@layout': {
                        controller: 'EducationSupplierController',
                        resolve: {
                            school: ['$stateParams', 'suppliersService', function($stateParams, suppliersService) {
                                return suppliersService.getSupplierById($stateParams.id);
                            }],
                            reviews : ['school', 'suppliersService', function (school, suppliersService) {
                                return suppliersService.getLatestReviewsBySupplierId(school._id);
                            }],
                            suppliers: ['suppliersService', function (suppliersService) {
                                return suppliersService.getRankedSuppliersBySector('education', 4);
                            }]
                        },
                        templateUrl: templateUrl + 'education.supplier.html'
                    }
                }
            });
}]);