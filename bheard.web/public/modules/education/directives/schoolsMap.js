education.directive('bSchoolsMap', ['schoolsFusionTableId', 'geoService', function (schoolsFusionTableId, geoService) {
    return {
        replace: true,
        require: 'ngModel',
        restrict: 'E',
        scope: {
            model: '=ngModel'
        },
        templateUrl: '/views/map.html',
        link: function (scope, element) {
            var map = new google.maps.Map(element[0], { mapTypeId: google.maps.MapTypeId.ROADMAP, scrollwheel: false, zoom: 14 }),
                layer = new google.maps.FusionTablesLayer({ map: map, options: { styleId: 2, templateId: 2 } }),
                marker = null;
                
            scope.$watch('model.location', function (location) {
                if (!marker) {
                    marker = new google.maps.Marker({ map: map, position: location });
                } else {
                    marker.setPosition(location);
                }
            });

            scope.$watchGroup(['model.options.type', 'model.options.rating'], function (group) {
                layer.setOptions({
                    query: {
                        select: 'Postcode',
                        from: schoolsFusionTableId,
                        where:
                        (group[0] ? "'Ofsted Phase' = '" + group[0] + "'" : "") +
                        (group[0] && group[1] ? " and " : "") +
                        (group[1] ? "'Overall effectiveness: how good is the school' = '" + group[1] + "'" : "")
                    }
                });
            });

            scope.$watch('model.schools', function (schools) {
                geoService.geocode(schools[0].postcode).then(function (location) {
                    map.panTo(location);
                    map.setZoom(14);
                });
            });
        }
    };
}]);