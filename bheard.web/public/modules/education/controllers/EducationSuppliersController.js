education.controller('EducationSuppliersController', ['$scope', '$location', '$document', 'options', 'isTouched', 'location', 'schools', 'geoService', 'schoolsService', function ($scope, $location, $document, options, isTouched, location, schools, geoService, schoolsService) {

    var httpLocratingUrl = "http://www.locrating.com/scripts/locratingIntegrationScripts.js";
    var httpsLocratingUrl = "https://www.locrating2.co.uk/scripts/locratingIntegrationScripts.js";

    function search(location, options) {
        schoolsService.search(location, options).then(function (schools) {
            $location.search(options);
            $scope.model.location = location;
            $scope.model.schools = schools;
            $scope.model.isTouched = true;
        });
    }

    function updateMapView(postcode) {
        try{setLocratingIFrameProperties({'id':'mapframe','search':postcode});}catch (err) {}
    }

    $scope.model = {
        location: location,
        schools: schools,
        options: options,
        isTouched: isTouched
    };

    $scope.search = function (options) {
        if (options.postcode) {
            geoService.geocode(options.postcode).then(function (location) {
                search(location, options);
            });
            updateMapView(options.postcode);
        } else {
            search(location, options);
        }
    };

    $scope.change = function (options) {
        search($scope.model.location, options);
    };

    function loadLocratingPlugin() {
        // education plugin load correct script based on http or https
        var head = document.getElementsByTagName('head')[0];
        var js = document.createElement("script");
        js.type = "text/javascript";
        js.defer = true;

        if (window.location.protocol != "https:") {
            js.src = httpLocratingUrl;
        }
        else {
            js.src = httpsLocratingUrl;
        }
        head.appendChild(js);

        js.onload = function() {
            updateMapView(options.postcode);
        };
    }

    loadLocratingPlugin();
}]);