energy.controller('EducationSupplierController', ['$rootScope', '$scope', 'school', 'reviews', 'suppliers', function ($rootScope, $scope, school, reviews, suppliers) {
    $rootScope.title = school.name;

    $scope.model = {
        school: school,
        reviews: reviews,
        suppliers: suppliers
    };
}]);
