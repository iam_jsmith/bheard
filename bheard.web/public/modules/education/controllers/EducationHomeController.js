education.controller('EducationHomeController', ['currentUser', 'latestDonation', 'reviews', '$scope', function (currentUser, latestDonation, reviews, $scope) {
    $scope.model = {
        latestDonation: latestDonation,
        reviews: reviews,
        options: {
            type: null,
            rating: null
        }
    };
}]);