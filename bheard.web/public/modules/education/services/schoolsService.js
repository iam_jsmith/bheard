education.factory('schoolsService', ['$http', 'apiKeys', 'schoolsFusionTableId', 'fusionTableCsvSerializer', 'suppliersService', 'notificationUtility', function ($http, apiKeys, schoolsFusionTableId, fusionTableCsvSerializer, suppliersService, notificationUtility) {
    var urlPrefix = 'https://www.googleapis.com/fusiontables/v2/query?key=' + apiKeys.google + '&sql=';

    return {
        search: function (location, options) {
            var query =
                "SELECT URN " +
                "FROM " + schoolsFusionTableId + " " +
                (options.type || options.rating
                    ? "WHERE " +
                        (options.type ? "'Ofsted Phase' = '" + options.type + "' " : "") +
                        (options.type && options.rating ? " AND " : "") +
                        (options.rating ? "'Overall effectiveness: how good is the school' = ' " + options.rating + "' " : "")
                    : "") +
                "ORDER BY ST_DISTANCE(Postcode, LATLNG(" + location.lat + "," + location.lng + ")) " +
                "LIMIT " + (options.limit || 25);

            return $http.get(urlPrefix + encodeURIComponent(query)).then(
                function (response) {
                    var schools = fusionTableCsvSerializer.deserialize(response.data);
                    return suppliersService.getSuppliersByNos(_.pluck(schools, 'urn'));
                },
                function () {
                    notificationUtility.alert('There was a problem loading the page, please try again.');
                });
        }
    };
}]);