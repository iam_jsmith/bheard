var byob = angular.module('byob', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/byob/views/';

    $stateProvider
        .state('byob', {
            data: {
                title: 'Build Your Own Bank | B.heard'
            },
            parent: 'default',
            url: '/byob',
            views: {
                '@layout': {
                    controller: 'ByobController',
                    templateUrl: templateUrl + 'byob.html'
                }
            }
        });
}]);