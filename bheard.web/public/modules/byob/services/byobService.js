byob.factory('byobService', ['api', function (api) {
    var urlPrefix = '/byob';

    return {
        post: function (data) {
            return api.post(urlPrefix, data);
        }
    };
}]);
