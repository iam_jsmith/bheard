byob.controller('ByobController', ['$scope', 'byobService', 'suppliersService', function($scope, byobService, suppliersService) {
    $scope.model = {
        responseData: null,
        form: {
            bankName: null,
            items: [
                // ORDER HERE IS IMPORTANT: IT MUST MATCH THE DATABASE ORDER
                { name:"Living Wage Employer", picked:false },
                { name:"Highest Paid Director vs Avg Staff Pay", picked:false },
                { name:"Tier 1 Capital", picked:false },
                { name:"PPI Provisions Vs Customer Deposits", picked:false },
                { name:"Number of Female Board Members", picked:false },
                { name:"Environmental Impact (CO2 emissions)", picked:false },
                { name:"Charitable Giving", picked:false },
                { name:"Regulatory Complaints", picked:false },
                { name:"Speed of Addressing Complaints (<8 weeks)", picked:false }
            ]
        },
        limit: 3,
        checked: 0,
        formSubmitted: false
    };

    $scope.limitControl = function(item){
        if(item.picked) $scope.model.checked++;
        else $scope.model.checked--;
    };

    $scope.submit = function (model) {
        byobService.post(model.form)
          .then(function (responseData) {
            $scope.model.formSubmitted = true;
            $scope.model.responseData = responseData;
        }).catch(function(response) {
            console.log(response);
        });
    };

    $scope.search = function (name) {
        return suppliersService.search({ name: name, sector: 'money' });
    };
}]);