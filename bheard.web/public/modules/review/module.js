var review = angular.module('review', []).config(['$stateProvider', function ($stateProvider) {
    var templateUrl = '/modules/review/views/';

    $stateProvider
        .state('review', {
            data: {
                title: 'Share Reviews'
            },
            parent: 'default',
            url: '/review',
            views: {
                '@layout': {
                    controller: 'ReviewController',
                    templateUrl: templateUrl + 'review.html'
                }
            }
        })
        .state('singlereview', {
            data: {
                title: 'Review',
                class: 'no-feature'
            },
            parent: 'default',
            url: '/reviews/:id',
            views: {
                '@layout': {
                    controller: 'SingleReviewController',
                    resolve: {
                        review: ['$stateParams', 'suppliersService', function ($stateParams, suppliersService) {
                            return suppliersService.getReviewById($stateParams.id);
                        }],
                        reviews: ['review', 'suppliersService', function (review, suppliersService) {
                            return suppliersService.getLatestReviewsBySupplierId(review.supplierId);
                        }]
                    },
                    templateUrl: templateUrl + 'singlereview.html'
                }
            }
        });
}]);