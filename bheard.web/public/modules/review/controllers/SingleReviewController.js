review.controller('SingleReviewController', ['review', 'reviews', '$rootScope', '$scope', 'suppliersService', '$document', function (review, reviews, $rootScope, $scope, suppliersService, $document) {
    $rootScope.title = 'Review of ' + review.supplier.name;

    $scope.model = {
        review: review,
        reviews: _.remove(reviews, function(rev) {
            return rev._id !== review._id;
        })
    };

    $scope.helpful = function (review) {
        suppliersService.rateReview(review._id, { type: 'helpful' }).then(function () {
            review.helpful++;
            review.isRated = true;
        });
    };

    $scope.unhelpful = function (review) {
        suppliersService.rateReview(review._id, { type: 'unhelpful' }).then(function () {
            review.unhelpful++;
            review.isRated = true;
        });
    };

    $document.scrollTopAnimated(0);
}]);
