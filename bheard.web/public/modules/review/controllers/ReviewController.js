review.controller('ReviewController', ['$scope', function ($scope) {
    $scope.model = {
        sectors: [
            {
                title: 'Energy',
                key: 'energy',
                link: 'energy.home'
            },
            {
                title: 'Insurance',
                key: 'insurance',
                link: 'insurance.home'
            },
            {
                title: 'Banks',
                key: 'money',
                link: 'money.home'
            },
            {
                title: 'Phone & Internet',
                key: 'phoneandinternet',
                link: 'phoneandinternet.home'
            },
            {
                title: 'Local Councils',
                key: 'government',
                link: 'government.home'
            },
            {
                title: 'Local Schools',
                key: 'education',
                link: 'education.home'
            },
            {
                title: 'Local GP\'s',
                key: 'healthcare',
                link: 'healthcare.home'
            },
        ]
    }
}]);