var charities = angular.module('charities', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/charities/views/';

    $stateProvider
        .state('charities', {
            abstract: true,
            parent: 'default',
            url: '/charities',
            views: {
                '@layout': {
                    templateUrl: templateUrl + 'charities.html'
                }
            }
        })
        .state('charities.againstbreastcancer', {
            data: {
                title: 'Against Breast Cancer',
                class: 'no-feature',
            },
            url: '/againstbreastcancer',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.againstbreastcancer.html'
                }
            }
        })
        .state('charities.crosslight', {
            data: {
                title: 'Crosslight',
                class: 'no-feature',
            },
            url: '/crosslight',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.crosslight.html'
                }
            }
        })
        .state('charities.emmaus', {
            data: {
                title: 'Emmaus',
                class: 'no-feature',
            },
            url: '/emmaus',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.emmaus.html'
                }
            }
        })
        .state('charities.insurancecharities', {
            data: {
                title: 'The Insurance Charities',
                class: 'no-feature',
            },
            url: '/insurancecharities',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.insurancecharities.html'
                }
            }
        })
        .state('charities.madeforchange', {
            data: {
                title: 'Made for Change',
                class: 'no-feature',
            },
            url: '/madeforchange',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.madeforchange.html'
                }
            }
        })
        .state('charities.mind', {
            data: {
                title: 'Mind',
                class: 'no-feature',
            },
            url: '/mind',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.mind.html'
                }
            }
        })
        .state('charities.recyclinglives', {
            data: {
                title: 'Recycling Lives',
                class: 'no-feature',
            },
            url: '/recyclinglives',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.recyclinglives.html'
                }
            }
        })
        .state('charities.spear', {
            data: {
                title: 'Spear',
                class: 'no-feature',
            },
            url: '/spear',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.spear.html'
                }
            }
        })
        .state('charities.williamwilberforce', {
            data: {
                title: 'William Wilberforce Trust',
                class: 'no-feature',
            },
            url: '/williamwilberforce',
            views: {
                '@charities': {
                    templateUrl: templateUrl + 'charities.williamwilberforce.html'
                }
            }
        });
}]);