var fatherhood = angular.module('fatherhood', []).config(['$stateProvider', function($stateProvider) {
    var templateUrl = '/modules/fatherhood/views/';

    $stateProvider
        .state('fatherhood', {
            data: {
                title: 'Fatherhood | B.heard'
            },
            parent: 'default',
            url: '/fatherhood',
            views: {
                '@layout': {
                    controller: 'FatherhoodController',
                    resolve: {
                    },
                    templateUrl: templateUrl + 'index.html'
                }
            }
        });
}]);