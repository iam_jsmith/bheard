﻿app.filter('fuelText', function () {
    return function (fuel) {
        if (fuel != null) {
            switch (fuel) {
                case 'gas':
                    return 'Gas';
                case 'electricity':
                    return 'Electricity';
                case 'dual':
                    return 'Dual Fuel (Gas & Electricity)';
                default:
                    throw ':(';
            }
        }

        return fuel;
    };
});
