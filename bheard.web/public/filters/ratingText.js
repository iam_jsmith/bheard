﻿app.filter('ratingText', function () {
    return function (rating, max) {
        if (rating != null) {
            if (max === 5 ? rating === 1 : rating < 20) {
                return 'Terrible';
            } else if (max === 5 ? rating === 2 : rating < 40) {
                return 'Poor';
            } else if (max === 5 ? rating === 3 : rating < 60) {
                return 'Average';
            } else if (max === 5 ? rating === 4 : rating < 80) {
                return 'Good';
            } else if (max === 5 ? rating === 5 : rating <= 100) {
                return 'Outstanding';
            } else {
                throw ':(';
            }
        }

        return rating;
    };
});
