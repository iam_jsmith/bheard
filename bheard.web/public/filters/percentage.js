﻿app.filter('percentage', function () {
    return function (percent, fractionSize) {
        if (percent != null) {
            if (fractionSize != null) {
                percent = parseFloat(percent).toFixed(fractionSize);
            }

            return percent + '%';
        }

        return percent;
    };
});
