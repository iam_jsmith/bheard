app.filter('trim', function () {
    return function (input) {
        if (input != null) {
            return input.trim();
        }

        return input;
    };
});
