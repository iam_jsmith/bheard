﻿app.filter('paymentText', function () {
    return function (payment) {
        if (payment != null) {
            switch (payment) {
                case 'monthly':
                    return 'Monthly Direct Debit';
                case 'quarterly':
                    return 'Quarterly Direct Debit';
                case 'bill':
                    return 'On Receipt of Bill';
                case 'meter':
                    return 'Pre-payment Meter';
                default:
                    throw ':(';
            }
        }

        return payment;
    };
});
