﻿app.filter('changeCoinEventText', function () {
    return function (event) {
        if (event != null) {
            switch (event) {
                case 'register':
                    return 'B.heard sign up';
                case 'login':
                    return 'B.heard login';
                case 'reviewsubmission':
                    return 'Review submission';
                default:
                    throw ':(';
            }
        }

        return event;
    };
});
