﻿app.filter('recommendText', function () {
    return function (rating) {
        if (rating != null) {
            switch (rating) {
                case 1:
                    return 'Not at all likely';
                case 2:
                    return 'Not likely';
                case 3:
                    return 'Neither likely or unlikely';
                case 4:
                    return 'Likely';
                case 5:
                    return 'Extremely Likely';
                default:
                    throw ':(';
            }
        }

        return rating;
    };
});
