﻿app.filter('sectorText', function () {
    return function (sector) {
        if (sector) {
            switch (sector) {
                case 'energy':
                    return 'Energy';
                case 'healthcare':
                    return 'Healthcare';
                case 'money':
                    return 'Money';
                case 'education':
                    return 'Education';
                case 'insurance':
                    return 'Insurance';
                case 'government':
                    return 'Government';
                case 'phoneandinternet':
                    return 'Phone & Internet';
                case 'priceComparison':
                    return 'Price Comparison';
                default:
                    throw ':(';
            }
        }

        return sector;
    };
});
