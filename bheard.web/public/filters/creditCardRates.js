app.filter('creditCardRates', function () {
    return function (rates) {
        var filteredRates = [];

        _.each(rates, function (rate) {
            if (rate.rateType === 'Standard Purchases') {
                if (!_.any(filteredRates, function (rate) { return rate.rateType === 'Standard Purchases'; })) {
                    filteredRates.push(rate);
                }
            } else if (rate.rateType === 'Standard Cash') {
                filteredRates.push(rate);
            }
        });

        return filteredRates;
    };
});