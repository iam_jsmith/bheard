app.filter('isNull', function () {
    return function (input, replacement) {
        if (input != null) {
            return input;
        }

        return replacement;
    };
});
