app.filter('ofstedRatingText', function () {
    return function (rating) {
        if (rating != null) {
            if (rating === 4) {
                return 'Inadequate';
            } else if (rating === 3) {
                return 'Needs Improvement';
            } else if (rating === 2) {
                return 'Good';
            } else {
                return 'Outstanding';
            }
        }

        return rating;
    };
});
