app.controller('DefaultController', ['$rootScope', 'changeCoinEvents', 'currentUser', function ($rootScope, changeCoinEvents, currentUser) {
    $rootScope.changeCoinEvents = changeCoinEvents;
    $rootScope.currentUser = currentUser;
    $rootScope.copyrightYear = moment().format('YYYY');
}]);
