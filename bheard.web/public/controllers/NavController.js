app.controller('NavController', ['$scope', '$state', 'loginService', 'twitterService', 'facebookService', 'googleService', 'notificationUtility', function ($scope, $state, loginService, twitterService, facebookService, googleService, notificationUtility) {
    $scope.model = {
        form: {
            email: null,
            password: null
        },
        showOptions: false
    };

    function success() {
        notificationUtility.success('You\'re now logged in.');

        if ($state.current.name !== 'register' && $state.current.name !== 'login') {
            $state.reload();
        } else {
            $state.go('home', null, { reload: true });
        }
    }

    $scope.login = function (form, model) {
        loginService.login(model.form).then(
            function () {
                success();
            },
            function (errors) {
                form.errors = errors;
            }
        );
    };

    $scope.loginWithTwitter = function () {
        twitterService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Twitter, please try again.')
            }
        );
    };

    $scope.loginWithFacebook = function () {
        facebookService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Facebook, please try again.')
            }
        );
    };

    $scope.loginWithGoogle = function () {
        googleService.login().then(
            function () {
                success();
            },
            function () {
                notificationUtility.alert('There was a problem logging in with Google, please try again.')
            }
        );
    };

    $scope.collapseMenu = function() {
        $scope.toggleNav = false;
    }
    
    // used by the menu bar to determine whether the button goes to login or profile
    $scope.getProfileLink = function() {
        var url;
        if (this.currentUser) {
            url = $state.href('account.personalinformation', {}, {absolute: true});
        } else {
            url = $state.href('login', {}, {absolute: true});
        }

        return url;
    }
}]);