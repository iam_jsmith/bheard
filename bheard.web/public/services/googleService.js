app.factory('googleService', ['$q', '$window', '$interval', '$timeout', function ($q, $window, $interval, $timeout) {
    var popup = null;

    return {
        login: function () {
            var deferred = $q.defer();

            if (popup) {
                popup.close();
            }

            popup = $window.open('/google', 'google', 'height=467,width=580,toolbar=0,menubar=0,location=0');

            if (popup) {
                var check = $interval(function () {
                    if (popup.closed) {
                        $interval.cancel(check);
                        deferred.reject('Google popup closed.');
                    }
                }, 100);

                $window.loginWithGoogle = function (err) {
                    $timeout(function () {
                        popup.close();
                        $interval.cancel(check);

                        if (!err) {
                            deferred.resolve('OK!');
                        } else {
                            deferred.reject(err);
                        }
                    }, 300);
                };
            } else {
                deferred.reject('Google popup failed to open.');
            }

            return deferred.promise;
        }
    };
}]);
