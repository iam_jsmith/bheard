app.factory('geoService', ['$q', '$window', function ($q, $window) {
    var geocoder = new $window.google.maps.Geocoder();

    return {
        geocode: function (address) {
            var deferred = $q.defer();

            geocoder.geocode({ address: address, componentRestrictions: { country: 'UK' }}, function (results, status) {
                if (status === $window.google.maps.GeocoderStatus.OK) {
                    deferred.resolve({
                        lat: results[0].geometry.location.lat(),
                        lng: results[0].geometry.location.lng()
                    });
                } else {
                    deferred.reject(status);
                }
            });

            return deferred.promise;
        },

        geolocate: function () {
            var deferred = $q.defer();

            if ($window.navigator.geolocation) {
                $window.navigator.geolocation.getCurrentPosition(
                    function (position) {
                        deferred.resolve({
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        });
                    },
                    function (err) {
                        deferred.reject(err);
                    }
                );
            } else {
                deferred.reject('Not supported.');
            }

            return deferred.promise;
        }
    };
}]);
