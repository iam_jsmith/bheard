app.factory('api',
           ['$http', '$rootScope', 'notificationUtility', '$q',
           function ($http, $rootScope, notificationUtility, $q) {

    var urlPrefix = '/api';

    function error(response) {
        switch (response.status) {
            case 0:
                break;
            case 401:
                $rootScope.$broadcast('401');
                break;
            case 404:
                $rootScope.$broadcast('404');
                break;
            default:
                if (response.data && response.data.errors) {
                    return $q.reject(response.data.errors);
                } else {
                    notificationUtility.alert('There was a problem loading the page, please try again.');
                }

                break;
        }

        return $q.reject(response);
    }

    return {
        delete: function (url, data, config) {
            return $http.delete(urlPrefix + url, data, config).then(function (response) {
                return response.data;
            }, error);
        },
        get: function (url, config) {
            return $http.get(urlPrefix + url + '?_=' + new Date().getTime(), config).then(function (response) {
                return response.data === 'null' ? null : response.data;
            }, error);
        },
        post: function (url, data, config) {
            return $http.post(urlPrefix + url, data, config).then(function (response) {
                return response.data;
            }, error);
        },
        put: function (url, data, config) {
            return $http.put(urlPrefix + url, data, config).then(function (response) {
                return response.data;
            }, error);
        }
    };
}]);
