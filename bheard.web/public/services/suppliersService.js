app.factory('suppliersService', ['api', function (api) {
    var urlPrefix = '/suppliers';

    return {
        getLatestReviewsBySector: function (sector, limit) {
            return api.get(urlPrefix + '/reviews', { params: {
                sector: sector,
                limit: limit
            }});
        },

        getLatestReviewsBySupplierId: function (supplierId) {
            return api.get(urlPrefix + '/' + supplierId + '/reviews');
        },

        getLatestReviewsByUserId: function (userId) {
            return api.get(urlPrefix + '/reviews', { params: { userId: userId } });
        },

        getRankedSuppliersBySector: function (sector, limit) {
            return api.get(urlPrefix, { params: {
                sector: sector,
                limit: limit
            }});
        },

        getReviewById: function (id) {
            return api.get(urlPrefix + '/reviews/' + id);
        },

        getSupplierProducts: function (supplierId) {
            return api.get(urlPrefix + '/' + supplierId + '/products');
        },

        getSupplierById: function (supplierId) {
            return api.get(urlPrefix + '/' + supplierId);
        },

        getSuppliersByNos: function (nos) {
            return api.get(urlPrefix, { params: { nos: nos } });
        },

        getSupplierBySectorAndNo: function (sector, supplierNo) {
            return api.get(urlPrefix, { params: {
                sector: sector,
                no: supplierNo
            }});
        },

        getLatestReviews: function (limit) {
            return api.get(urlPrefix + '/reviews', { params: { limit: limit } });
        },

        postReview: function (supplierId, review) {
            return api.post(urlPrefix + '/' + supplierId + '/reviews', review);
        },

        rateReview: function (supplierReviewId, rating) {
            return api.post(urlPrefix + '/reviews/' + supplierReviewId + '/ratings', rating);
        },

        search: function (params) {
            return api.get(urlPrefix, { params: params });
        }
    };
}]);
