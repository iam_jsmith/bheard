app.factory('loginService', ['api', function (api) {
    return {
        login: function (data) {
            return api.post('/login', data);
        },
        logout: function () {
            return api.post('/logout');
        }
    };
}]);
