app.factory('currentUserService', ['$cookies', 'api', function ($cookies, api) {
    var urlPrefix = '/me';

    return {
        changeEmailAddress: function (data) {
            return api.post(urlPrefix + '/changeemailaddress', data);
        },
        changeEmailAddressStep2: function (uuid) {
            return api.post(urlPrefix + '/changeemailaddress/' + uuid);
        },
        changePassword: function (data) {
            return api.post(urlPrefix + '/changepassword', data);
        },
        get: function () {
            return $cookies.get('bheard') ? api.get(urlPrefix) : null;
        },
        getChangeCoinsLedger: function () {
            return api.get(urlPrefix + '/changecoinsledger');
        },
        update: function (user) {
            return api.put(urlPrefix, user);
        }
    };
}]);
