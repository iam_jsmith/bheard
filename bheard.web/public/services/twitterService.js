app.factory('twitterService', ['$q', '$window', '$interval', function ($q, $window, $interval) {
    var popup = null;

    return {
        login: function () {
            var deferred = $q.defer();

            if (popup) {
                popup.close();
            }

            popup = $window.open('/twitter', 'twitter', 'height=467,width=580,toolbar=0,menubar=0,location=0');

            if (popup) {
                var check = $interval(function () {
                    if (popup.closed) {
                        $interval.cancel(check);
                        deferred.reject('Twitter popup closed.');
                    }
                }, 100);

                $window.loginWithTwitter = function (err) {
                    popup.close();
                    $interval.cancel(check);

                    if (!err) {
                        deferred.resolve('OK!');
                    } else {
                        deferred.reject(err);
                    }
                };
            } else {
                deferred.reject('Twitter popup failed to open.');
            }

            return deferred.promise;
        }
    };
}]);
