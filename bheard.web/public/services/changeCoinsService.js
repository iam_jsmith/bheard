app.factory('changeCoinsService', ['api', function(api) {
    var urlPrefix = '/changecoins';

    return {
        getEvents: function () {
            return api.get(urlPrefix + '/events');
        },
        getLatestDonation: function () {
            return api.get(urlPrefix + '/donations');
        },
        donate: function (data) {
            return api.post(urlPrefix + '/donations', data);
        },
        nominate: function (data) {
            return api.post(urlPrefix + '/nominations', data);
        }
    };
}]);
