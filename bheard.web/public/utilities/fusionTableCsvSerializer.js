app.factory('fusionTableCsvSerializer', function () {
    return {
        deserialize: function (data) {
            var results = [];

            _.forEach(data.rows, function (row, i) {
                results.push({});

                _.forEach(data.columns, function (column, j) {
                    results[i][_.camelCase(column)] = row[j];
                });
            });

            return results;
        }
    };
});
