app.factory('notificationUtility', ['ngToast', function (ngToast) {
    return {
        success: function (message) {
            ngToast.create({
                className: 'success',
                content: '<strong>OK!</strong> ' + message
            });
        },

        alert: function (message) {
            ngToast.create({
                className: 'danger',
                content: '<strong>Oops!</strong> ' + message
            });
        }
    };
}]);
