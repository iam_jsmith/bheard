app.factory('randomPostcodeUtility', function () {
    var postcodes = [
        'CO10 7NU', // Eastern
        'DE1 0AL', // East Midlands
        'SW1V 1AD', // London
        'LL57 1AD', // Manweb
        'B1 1BD', // Midlands
        'NE1 1AD', // Northern
        'FY1 1AD', // Norweb
        //'IV1 1AD', // Scottish Hydro
        //'EH1 1BJ', // Scottish Power
        'BN1 1AD', // Seeboard
        'PO1 1AE', // Southern
        'SA1 1AF', // SWALEC
        'EX1 3TQ', // SWEB
        'LS1 1BA' // Yorkshire
    ];

    return {
        get: function () {
            return postcodes[Math.floor((Math.random() * postcodes.length) + 1)];
        }
    };
});
