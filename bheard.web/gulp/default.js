'use strict';

var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', ['build']);
gulp.task('full-build', function (callback) {
    runSequence('retire', 'lint', 'build', callback);
});
