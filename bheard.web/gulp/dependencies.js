var gulp = require('gulp');
var path = require('path');
var rimraf = require('rimraf');
var install = require('gulp-install');

gulp.task('updatebheard', function () {
    rimraf(path.join('./node_modules', 'bheard'), function (err) {
        if (err) {
            console.log('error', err);
        }

        gulp.src(['./package.json']).pipe(install());
    });
});
