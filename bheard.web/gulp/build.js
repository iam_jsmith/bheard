var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var flatten = require('gulp-flatten');
var uglify = require('gulp-uglify');
var minifyCss = require('gulp-minify-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var cache = require('gulp-cache');
var browserSync = require('browser-sync').create();

gulp.task('build', ['css', 'js', 'sass', 'assets']);
gulp.task('js', ['js-lib', 'js-app']);
gulp.task('assets', ['copy-images', 'copy-fonts']);

gulp.task('css', function () {
    var files = [
        './public/bower_components/angular-loading-bar/build/loading-bar.css',
        './public/bower_components/fontawesome/css/font-awesome.css',
        './public/bower_components/ngtoast/dist/ngToast.css',
        './public/bower_components/ngtoast/dist/ngToast-animations.css',
        './public/bower_components/slick-carousel/slick/slick.css',
        './public/bower_components/swiper/dist/css/swiper.css'
    ];

    return gulp.src(files, { 'base': 'public/bower_components' })
               .pipe(concat('lib.css'))
               .pipe(minifyCss())
               .pipe(gulp.dest('./public/build/css/'));
});

gulp.task('js-lib', function () {
    var files = [
        'public/bower_components/jquery/dist/jquery.js',
        'public/bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
        'public/bower_components/angular/angular.js',
        'public/bower_components/angular-animate/angular-animate.js',
        'public/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
        'public/bower_components/angular-cookies/angular-cookies.js',
        'public/bower_components/angular-gravatar/build/angular-gravatar.js',
        'public/bower_components/angular-loading-bar/build/loading-bar.js',
        'public/bower_components/angular-sanitize/angular-sanitize.js',
        'public/bower_components/angular-scroll/angular-scroll.js',
        'public/bower_components/angular-ui-router/release/angular-ui-router.js',
        'public/bower_components/angulike/angulike.js',
        'public/bower_components/lodash/lodash.js',
        'public/bower_components/moment/moment.js',
        'public/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
        'public/bower_components/ngtoast/dist/ngToast.js',
        'public/bower_components/ngtweet/dist/ngtweet.js',
        'public/bower_components/swiper/dist/js/swiper.jquery.js',
        'public/bower_components/re-tree/re-tree.js',
        'public/bower_components/ng-device-detector/ng-device-detector.js',
        'public/bower_components/ng-focus-if/focusIf.js',

        // Used for angular-slick
        'public/bower_components/slick-carousel/slick/slick.js',
        'public/bower_components/angular-slick/dist/slick.js',

        // Used for angular-masonry
        'public/bower_components/jquery-bridget/jquery-bridget.js',
        'public/bower_components/ev-emitter/ev-emitter.js',
        'public/bower_components/desandro-matches-selector/matches-selector.js',
        'public/bower_components/fizzy-ui-utils/utils.js',
        'public/bower_components/get-size/get-size.js',
        'public/bower_components/outlayer/item.js',
        'public/bower_components/outlayer/outlayer.js',
        'public/bower_components/masonry/masonry.js',
        'public/bower_components/imagesloaded/imagesloaded.js',
        'public/bower_components/angular-masonry/angular-masonry.js',

        // Used for videogular
        'public/bower_components/videogular/videogular.js',
        'public/bower_components/videogular-controls/vg-controls.js',
        'public/bower_components/videogular-overlay-play/vg-overlay-play.js',
        'public/bower_components/videogular-poster/vg-poster.js',
        'public/bower_components/videogular-buffering/vg-buffering.js',

        // Used for opbeat (if there are errors, try clearing cookies for the site)
        // todo check this: changed to unminified, because all the above are unminified because they get minified by gulp
        'public/bower_components/opbeat-angular/opbeat-angular.js'
    ];

    if (process.env.NODE_ENV === 'production') {
        return gulp.src(files, { 'base': 'public/bower_components/' })
                   .pipe(uglify({
                       mangle: false,
                       compress: {
                           drop_console: true
                       }
                   }))
                   .pipe(concat('lib.js'))
                   .pipe(gulp.dest('./public/build/js/'));
    } else {
        return gulp.src(files, { 'base': 'public/bower_components/' })
                   .pipe(sourcemaps.init())
                   .pipe(uglify({mangle: false}))
                   .pipe(concat('lib.js'))
                   .pipe(sourcemaps.write('./'))
                   .pipe(gulp.dest('./public/build/js/'));
    }
});

gulp.task('js-app', function () {
    var files = [
        'public/app.js',
        'public/controllers/*.js',
        'public/directives/*.js',
        'public/filters/*.js',
        'public/models/*.js',
        'public/modules/*/*.js',
        'public/modules/**/*.js',
        'public/services/*.js',
        'public/utilities/*.js'
    ];

    if (process.env.NODE_ENV === 'production') {
        return gulp.src(files, { 'base': 'public/' })
                   .pipe(uglify({
                       mangle: false,
                       compress: {
                           drop_console: true
                       }
                   }))
                   .pipe(concat('app.js'))
                   .pipe(gulp.dest('./public/build/js/'));
    } else {
        return gulp.src(files, { 'base': 'public/' })
                   .pipe(browserSync.stream()) // only do this with local files not libraries (ignored if `run-dev`?)
                   .pipe(sourcemaps.init())
                   .pipe(uglify({mangle: false}))
                   .pipe(concat('app.js'))
                   .pipe(sourcemaps.write('./'))
                   .pipe(gulp.dest('./public/build/js/'));
    }
});

gulp.task('sass', function () {
    var files = [
        './public/sass/app.scss'
    ];

    if (process.env.NODE_ENV === 'production') {
        return gulp.src(files)
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(gulp.dest('./public/build/css'));
    } else {
        return gulp.src(files)
            .pipe(browserSync.stream()) // only do this with local files not libraries (ignored if `run-dev`?)
            .pipe(sass({outputStyle: 'compressed'}))
            .pipe(gulp.dest('./public/build/css'));
    }
});

/**
 * Copy images to build folder and minify.
 * This now uses caching, if there are issues, try clearing the cache (see gulp task below)
 *
 */
gulp.task('copy-images', function () {
    return gulp.src('./public/images/**')
               .pipe(cache(imagemin({
                    progressive: true,
                    svgoPlugins: [{removeViewBox: false}],
                    use: [pngquant()]
                })))
               .pipe(gulp.dest('./public/build/images/'));
});

gulp.task('copy-fonts', function () {
    var fonts = [
        './public/bower_components/fontawesome/fonts/**',
        './public/bower_components/bootstrap-sass/assets/fonts/bootstrap/*'
    ];

    return gulp.src(fonts)
               .pipe(flatten())
               .pipe(gulp.dest('./public/build/fonts'));
});

/**
 * Clear gulp cache (used with the copy-images function so images aren't minified each time)
 */
gulp.task('clear-gulp-cache', function (done) {
    return cache.clearAll(done);
});
