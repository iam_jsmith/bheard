var gulp = require('gulp'),
    eslint = require('gulp-eslint');

gulp.task('lint', function () {
  return gulp.src(['**/*.js',
                   '!**/node_modules/**',
                   '!**/logs/**',
                   '!**/public/modules/**',
                   '!**/public/build/**',
                   '!**/test/**'])
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});
