var gulp = require('gulp');
var Nodemon = require('gulp-nodemon');
var browserSync = require('browser-sync').create();

/**
 * `run-dev` runs the site on localhost:5555
 * `runr` or auto browser reload (runr short for run-reload) runs the site on localhost:3000
 *
 * and benefits from auto browser reload. (browsersync can't be run on the same port as the website)
 */

var browserReload = false;

gulp.task('run-dev', ['build', 'auto-restart', 'watch-js', 'watch-sass', 'watch-images']);

gulp.task('runr', ['build', 'auto-restart', 'watch-templates', 'watch-js', 'watch-sass', 'watch-images'], function () {
    browserReload = true;
    browserSync.init({
        open: false, // don't open localhost:3000 initially, because doesn't seem to load
        port: 3000,
        proxy: {
            target: 'localhost:5555' // todo abstract this
            // ws: true // enables websockets (this may not be needed)
        }
    });
});

// IF ANY OTHER JAVASCRIPT DIRECTORIES ARE ADDED TO THE BASE, WHICH AREN'T HANDLED BY NODE, THEN ADD TO IGNORE LIST
gulp.task('auto-restart', function () {
    var nodeArgs = [];
    if (process.env.DEBUGGER) {
        nodeArgs.push('--debug');
    }

    return Nodemon({
        script: 'app.js',
        ext: 'js',
        ignore: [
            'gulp/**/*',
            'public/**/*',
            'logs/**/*',
            'node_modules/**/*',
            'test/**/*'
        ],
        nodeArgs: nodeArgs
    })
    .on('restart', function (files) {
        console.log('change detected:', files);
    });
});

/**
 * Watch html templates
 */
gulp.task('watch-templates', function () {
    var files = [
        './public/views/**/*.html',
        './public/modules/**/*.html'
    ];

    // doesn't need to rebuild anything, so reload straight away
    return gulp.watch(files, ['reload-browser'])
        .on('change', function (event) {
            console.log('Change (' + event.type + '): ' + event.path);
            console.log('Running templates task');
        });
});

gulp.task('watch-js', function () {
    var files = [
        './public/**/*.js',
        '!./public/build/**',
        '!./public/bower_components/**',
        '!./public/test/**'
    ];

    // todo fix so reload-browser runs when previous task is finished
    return gulp.watch(files, ['js-app', 'reload-browser'])
        .on('change', function (event) {
            console.log('Change (' + event.type + '): ' + event.path);
            console.log('Running js task');
        });
});

gulp.task('watch-sass', function () {
    var filesToWatch = [
        './public/sass/**/*.scss',
        '!./public/build/**',
        '!./public/bower_components/**'
    ];

    // todo fix so reload-browser runs when previous task is finished
    return gulp.watch(filesToWatch, ['sass', 'reload-browser'])
        .on('change', function (event) {
            console.log('Change (' + event.type + '): ' + event.path);
            console.log('Running sass task');
        });
});

gulp.task('watch-images', function () {
    var filesToWatch = [
        './public/images/**'
    ];

    // todo fix so reload-browser runs when previous task is finished
    return gulp.watch(filesToWatch, ['copy-images', 'reload-browser'])
        .on('change', function (event) {
            console.log('Change (' + event.type + '): ' + event.path);
            console.log('Running copy-images task');
        });
});

gulp.task('reload-browser', function () {
    if (browserReload === false) {
        console.log('BrowserSync not running: use `runr` task instead.');
        return;
    }
    // this is a temporary hack because the reload-browser task isn't run synchronously,
    // and sometimes everything isn't built
    setTimeout(function(){
        return browserSync.reload();
    }, 1500);
});
