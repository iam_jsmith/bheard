var gulp = require('gulp');
var spawn = require('child_process').spawn;
var gutil = require('gulp-util');

gulp.task('retire:watch', ['retire'], function (done) {
  // Watch all javascript files an the package.json
  return gulp.watch(['package.json'], ['retire']);
});

gulp.task('retire', function (cb) {
  // Spawn Retire.js as a child process
  // You can optionally add option parameters to the second argument (array)
  var child = spawn('retire', [], {
    cwd: process.cwd()
  });

  child.on('exit', function (error) {
    console.log('error', error);
    cb();
  });

  child.stdout.setEncoding('utf8');
  child.stdout.on('data', function (data) {
    gutil.log(data);
  });

  child.stderr.setEncoding('utf8');
  child.stderr.on('data', function (data) {
    gutil.log(gutil.colors.red(data));
    gutil.beep();
  });
});
