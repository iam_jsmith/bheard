var Lab = require('lab');
var Code = require('code');
var request = require('supertest');

var express = require('express');

var changeCoinsMiddleware = require('../../middleware/changeCoins');

var lab = exports.lab = Lab.script();
var pointsAdded = 0;
var ledger = {
    addCoins: function (user, event, points, cb) {
        pointsAdded += points;
        cb(null, pointsAdded);
    }
};

var app;

lab.experiment('Change Coins middleware', function () {
    lab.beforeEach(function (done) {
        app = express();
        pointsAdded = 0;
        done();
    });

    lab.test('Change Coins responds to register event', function (done) {
        var rewards = [
            {
                event: 'event:changecoins:register',
                coins: 10
            }
        ];

        changeCoinsMiddleware(app, ledger, rewards);
        app.get('/', function (req, res) {
            var eventData = {
                user: {},
                event: 'register'
            };

            req.app.emit('event:changecoins:register', eventData);
            Code.expect(pointsAdded).to.equal(10);

            res.sendStatus(200);
        });

        request(app)
            .get('/')
            .expect(200, done);
    });

    lab.test('Change coins responds to multiple events', function (done) {
        var rewards = [
            {
                event: 'event:changecoins:register',
                coins: 10
            },
            {
                event: 'event:changecoins:login',
                coins: 1
            }
        ];

        changeCoinsMiddleware(app, ledger, rewards);
        app.get('/', function (req, res) {
            req.app.emit('event:changecoins:register', {
                user: {},
                event: 'register'
            });

            req.app.emit('event:changecoins:login', {
                user: {},
                event: 'login'
            });


            res.sendStatus(200);
        });

        request(app)
            .get('/')
            .expect(200, function () {
                Code.expect(pointsAdded).to.equal(11);
                request(app)
                    .get('/')
                    .expect(200, function () {
                        Code.expect(pointsAdded).to.equal(22);
                        done();
                    });
            });
    });
});
