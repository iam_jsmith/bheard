// var Lab = require('lab');
// var Code = require('code');
// var Path = require('path');

// var companyAPIController = require('../../../api/bheard/suppliers');

// var lab = exports.lab = Lab.script();

// lab.experiment('Company API', function () {
//     lab.test('Handles errors thrown by DB', function (done) {
//         var companyModel = {
//             find: function (query, cb) {
//                 cb(new Error('There is an error here'));
//             }
//         };

//         var brokenDb = {};
//         brokenDb.get = function () {
//             return companyModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/company',
//             app: {
//                 'get': function () {
//                     return brokenDb;
//                 }
//             }
//         };

//         companyAPIController.handle(request, {
//             json: function (obj) {
//                 Code.expect(obj.status).to.equal(500);
//                 Code.expect(obj.message).to.equal(
//                     'There was an error loading companies'
//                 );

//                 done();
//             },
//             status: function (status) {
//                 Code.expect(status).to.equal(500);
//                 return this;
//             }
//         }, function (err) {
//             throw err;
//         });
//     });

//     lab.test('makes the right query', function (done) {
//         var companyModel = {
//             find: function (query, cb) {
//                 Code.expect(query).to.deep.equal({});

//                 cb(null, [
//                     {}
//                 ]);
//             }
//         };

//         var db = {};
//         db.get = function () {
//             return companyModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/company',
//             app: {
//                 'get': function () {
//                     return db;
//                 }
//             }
//         };

//         var jsonHandler = function (response) {
//             response = JSON.parse(response);
//             Code.expect(response).to.exist();

//             Code.expect(response).to.have.length(1);

//             done();
//         };

//         companyAPIController.handle(request, {
//             json: function (obj) {
//                 jsonHandler(JSON.stringify(obj));
//             }
//         }, function (err) {
//             throw err;
//         });
//     });
// });
