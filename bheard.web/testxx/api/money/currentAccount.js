// var Lab = require('lab');
// var Code = require('code');
// var Path = require('path');

// var currentAccountAPIController = require('../../../api/money/currentAccount');

// var lab = exports.lab = Lab.script();

// lab.experiment('Current Account API Top 10', function () {
//     lab.test('Handles errors thrown by DB', function (done) {
//         var currentAccountModel = {
//             find: function (query, options, cb) {
//                 cb(new Error('There is an error here'));
//             }
//         };

//         var brokenDb = {};
//         brokenDb.get = function () {
//             return currentAccountModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/currentAccount/top10',
//             app: {
//                 'get': function () {
//                     return brokenDb;
//                 }
//             }
//         };

//         currentAccountAPIController.handle(request, {
//             json: function (obj) {
//                 Code.expect(obj.status).to.equal(500);
//                 Code.expect(obj.message).to.equal(
//                     'There was an error loading top accounts'
//                 );

//                 done();
//             },
//             status: function (status) {
//                 Code.expect(status).to.equal(500);
//                 return this;
//             }
//         }, function (err) {
//             throw err;
//         });
//     });

//     lab.test('makes the right query', function (done) {
//         var currentAccountModel = {
//             find: function (query, options, cb) {
//                 Code.expect(query).to.deep.equal({});
//                 Code.expect(options).to.deep.equal({
//                     sort: {
//                         'score.overallOpinionPercent': -1
//                     },
//                     limit: 10
//                 });

//                 cb(null, [
//                     {}
//                 ]);
//             }
//         };

//         var db = {};
//         db.get = function () {
//             return currentAccountModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/currentAccount/top10',
//             app: {
//                 'get': function () {
//                     return db;
//                 }
//             }
//         };

//         var jsonHandler = function (response) {
//             response = JSON.parse(response);
//             Code.expect(response).to.exist();

//             Code.expect(response).to.have.length(1);

//             done();
//         };

//         currentAccountAPIController.handle(request, {
//             json: function (obj) {
//                 jsonHandler(JSON.stringify(obj));
//             }
//         }, function (err) {
//             throw err;
//         });
//     });
// });

// lab.experiment('Current Account API', function () {
//     lab.test('Handles errors thrown by DB', function (done) {
//         var currentAccountModel = {
//             find: function (query, options, cb) {
//                 cb(new Error('There is an error here'));
//             }
//         };

//         var brokenDb = {};
//         brokenDb.get = function () {
//             return currentAccountModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/currentAccount',
//             app: {
//                 'get': function () {
//                     return brokenDb;
//                 }
//             }
//         };

//         currentAccountAPIController.handle(request, {
//             json: function (obj) {
//                 Code.expect(obj.status).to.equal(500);
//                 Code.expect(obj.message).to.equal(
//                     'There was an error loading accounts'
//                 );

//                 done();
//             },
//             status: function (status) {
//                 Code.expect(status).to.equal(500);
//                 return this;
//             }
//         }, function (err) {
//             throw err;
//         });
//     });

//     lab.test('makes the right query', function (done) {
//         var currentAccountModel = {
//             find: function (query, options, cb) {
//                 Code.expect(query).to.deep.equal({});
//                 Code.expect(options).to.deep.equal({
//                     sort: {
//                         'score.overallOpinionPercent': -1
//                     }
//                 });

//                 cb(null, [
//                     {}
//                 ]);
//             }
//         };

//         var db = {};
//         db.get = function () {
//             return currentAccountModel;
//         };

//         var request = {
//             method: 'GET',
//             url: '/api/money/currentAccount/',
//             app: {
//                 'get': function () {
//                     return db;
//                 }
//             }
//         };

//         var jsonHandler = function (response) {
//             response = JSON.parse(response);
//             Code.expect(response).to.exist();

//             Code.expect(response).to.have.length(1);

//             done();
//         };

//         currentAccountAPIController.handle(request, {
//             json: function (obj) {
//                 jsonHandler(JSON.stringify(obj));
//             }
//         }, function (err) {
//             throw err;
//         });
//     });
// });
