var express = require('express'),
    router = express.Router();

router.get('/register', function (req, res) {
    res.render('register/index', { title: 'Register' });
});

router.get('/register/:verifier', function (req, res) {
    console.log('Verifying email with verifier', req.params.verifier);
    res.render('register/index', { title: 'Verify' });
});

module.exports = router;
