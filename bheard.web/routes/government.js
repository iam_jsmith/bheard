var express = require('express'),
    router = express.Router();

router.get('/government', function (req, res) {
    res.render('government/index', { title: 'Government - Review & Compare Councils | B.heard' });
});

router.get('/government/*', function (req, res) {
    res.render('government/index', { seo: false, title: 'Government' });
});

module.exports = router;
