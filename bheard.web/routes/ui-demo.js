var express = require('express'),
    router = express.Router();

router.get('/ui-demo', function (req, res) {
    res.render('ui-demo/index', { title: 'UI Demo | B.heard' });
});

module.exports = router;
