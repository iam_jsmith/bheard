var express = require('express'),
    router = express.Router(),
    loginWithTwitter = require('../middleware/loginWithTwitter');

router.get('/twitter', loginWithTwitter(), function (req, res) {
    res.render('twitter', { title: 'Twitter', errMessage: req.query.error || null });
});

module.exports = router;
