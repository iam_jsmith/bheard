var express = require('express'),
    router = express.Router();


router.get('/charities/againstbreastcancer', function (req, res) {
    res.render('charities/againstbreastcancer', { title: 'Against Breast Cancer' });
});

router.get('/charities/crosslight', function (req, res) {
    res.render('charities/crosslight', { title: 'Crosslight' });
});

router.get('/charities/emmaus', function (req, res) {
    res.render('charities/emmaus', { title: 'Emmaus' });
});

router.get('/charities/insurancecharities', function (req, res) {
    res.render('charities/insurancecharities', { title: 'The Insurance Charities' });
});

router.get('/charities/madeforchange', function (req, res) {
    res.render('charities/madeforchange', { title: 'Made for Change' });
});

router.get('/charities/mind', function (req, res) {
    res.render('charities/mind', { title: 'Mind' });
});

router.get('/charities/recyclinglives', function (req, res) {
    res.render('charities/recyclinglives', { title: 'Recycling Lives' });
});

router.get('/charities/spear', function (req, res) {
    res.render('charities/spear', { title: 'Spear' });
});

router.get('/charities/williamwilberforce', function (req, res) {
    res.render('charities/williamwilberforce', { title: 'William Wilberforce Trust' });
});

module.exports = router;
