var express = require('express'),
    router = express.Router(),
    loginWithGoogle = require('../middleware/loginWithGoogle');

router.get('/google', loginWithGoogle(), function (req, res) {
    res.render('google', { title: 'Google', errMessage: req.query.error || null });
});

module.exports = router;
