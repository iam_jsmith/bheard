var express = require('express'),
    router = express.Router();

router.get('/healthcare', function (req, res) {
    res.render('healthcare/index', { title: 'Rate & Compare GP Practices | B.heard' });
});

router.get('/healthcare/*', function (req, res) {
    res.render('healthcare/index', { seo: false, title: 'Healthcare' });
});

module.exports = router;
