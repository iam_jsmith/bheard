var express = require('express'),
    router = express.Router();

router.get('/phoneandinternet', function (req, res) {
    res.render('phoneandinternet/index', { title: 'Review & Compare Home Phone & Broadband Deals | B.heard' });
});

router.get('/phoneandinternet/*', function (req, res) {
    res.render('phoneandinternet/index', { seo: false, title: 'Phone & Internet' });
});

module.exports = router;
