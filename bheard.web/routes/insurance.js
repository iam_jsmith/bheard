var express = require('express'),
    router = express.Router();

router.get('/insurance', function (req, res) {
    res.render('insurance/index', { title: 'Review & Compare Insurance | B.heard' });
});

router.get('/insurance/suppliers/:insuranceType(car|van|home|bike|travel|breakdown|pet|life|health|youngdriver)', function (req, res) {
    res.render('insurance/suppliers/shared/index', { seo: false, title: 'Insurance | B.heard' });
});


router.get('/insurance/switch/:insuranceType(car|van|home|bike|travel|breakdown|pet|life|health|youngdriver)', function (req, res) {
    res.render('insurance/switch/shared/index', { seo: false, title: 'Compare Insurance | B.heard' });
});

router.get('/insurance/*', function (req, res) {
    res.render('insurance/index', { seo: false, title: 'Insurance' });
});

module.exports = router;
