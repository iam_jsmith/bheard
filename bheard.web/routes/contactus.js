var express = require('express'),
    router = express.Router();

router.get('/contactus', function (req, res) {
    res.render('contactus/contactus', { title: 'Contact Us' });
});

router.get('/haveyoursay', function (req, res) {
    res.render('contactus/haveyoursay', { title: 'Have your say' });
});

// are these needed?
router.get('/forcompanies', function (req, res) {
    res.render('contactus/forcompanies', { title: 'For Companies' });
});

router.get('/suggestion', function (req, res) {
    res.render('contactus/suggestion', { title: 'Suggestion' });
});

module.exports = router;
