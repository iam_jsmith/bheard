var express = require('express'),
    router = express.Router();

router.get('/education', function (req, res) {
    res.render('education/index', { title: 'Rate & Compare Schools, Colleges and Universities | B.heard' });
});

router.get('/education/*', function (req, res) {
    res.render('education/index', { seo: false, title: 'Education' });
});

module.exports = router;
