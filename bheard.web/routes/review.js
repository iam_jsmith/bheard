var express = require('express'),
    router = express.Router();

router.get('/review', function (req, res) {
    res.render('review/index', { title: 'Share Review' });
});

router.get('/reviews/:id', function (req, res) {
    res.render('review/index', { seo: false, title: 'Review' });
});

module.exports = router;
