var express = require('express'),
    router = express.Router();


router.get('/account', function (req, res) {
    res.render('account/index', { title: 'My Account' });
});

router.get('/account/*', function (req, res) {
    res.render('account/index', { title: 'My Account' });
});

module.exports = router;
