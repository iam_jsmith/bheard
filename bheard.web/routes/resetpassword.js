var express = require('express'),
    router = express.Router();

router.get('/resetpassword', function (req, res) {
    res.render('resetpassword/index', { title: 'Reset Password' });
});

module.exports = router;
