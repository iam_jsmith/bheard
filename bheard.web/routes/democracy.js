var express = require('express'),
    router = express.Router();

router.get('/democracy', function (req, res) {
    res.render('democracy/democracy', { title: 'Democracy' });
});

router.get('/democracy/*', function (req, res) {
    res.render('democracy/democracy', { title: 'Democracy' });
});

module.exports = router;
