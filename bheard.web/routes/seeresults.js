var express = require('express'),
    router = express.Router();

router.get('/seeresults', function (req, res) {
    res.render('seeresults/index', { title: 'See Results | B.heard' });
});

module.exports = router;