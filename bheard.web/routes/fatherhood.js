var express = require('express'),
    router = express.Router();

router.get('/fatherhood', function (req, res) {
    res.render('fatherhood/index', { title: 'Fatherhood' });
});

module.exports = router;
