var express = require('express'),
    router = express.Router();

router.get('/corporate', function (req, res) {
    res.render('company/corporate', { title: 'About Us | B.heard' });
});

router.get('/corporate/goals', function (req, res) {
    res.render('company/goals', { title: 'Goals & Strategy' });
});

router.get('/corporate/changecoins', function (req, res) {
    res.render('company/changecoins', { title: 'Why Changecoin&reg;?' });
});

router.get('/corporate/investors', function (req, res) {
    res.render('company/investors', { title: 'Investors' });
});

router.get('/corporate/company', function (req, res) {
    res.render('company/company', { title: 'Company Information' });
});

router.get('/corporate/jobs', function (req, res) {
    res.render('company/jobs', { title: 'Jobs' });
});

router.get('/sitemap', function (req, res) {
    res.render('company/sitemap', { title: 'Site Map' });
});

module.exports = router;
