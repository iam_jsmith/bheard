var express = require('express'),
    router = express.Router(),
    loginWithFacebook = require('../middleware/loginWithFacebook');

router.get('/facebook', loginWithFacebook(), function (req, res) {
    res.render('facebook', { title: 'Facebook', errMessage: req.query.error || null });
});

module.exports = router;
