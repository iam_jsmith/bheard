var express = require('express'),
    router = express.Router();

router.get('/byob', function (req, res) {
    res.render('byob/index', { title: 'Build Your Own Bank | B.heard' });
});

module.exports = router;
