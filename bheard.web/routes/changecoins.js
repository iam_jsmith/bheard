var express = require('express'),
    router = express.Router();

router.get('/changecoins', function (req, res) {
    res.render('changecoins/index', { title: 'Changecoin&reg; for Charity | B.heard' });
});

module.exports = router;
