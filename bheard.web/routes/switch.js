var express = require('express'),
    router = express.Router();

router.get('/switch', function (req, res) {
    res.render('switch/switch', { title: 'Switch' });
});

// router.get('/switch/*', function (req, res) {
//     res.render('switch/topic', { title: 'Share Opinions' });
// });

module.exports = router;
