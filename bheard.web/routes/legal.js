var express = require('express'),
    router = express.Router();

router.get('/cookies', function (req, res) {
    res.render('legal/cookies', { title: 'Cookies' });
});

router.get('/privacy', function (req, res) {
    res.render('legal/privacy', { title: 'Privacy Policy' });
});

router.get('/terms', function (req, res) {
    res.render('legal/terms', { title: 'Terms & Conditions' });
});

router.get('/guidelines', function (req, res) {
    res.render('legal/guidelines', { title: 'Review Guidelines' });
});

router.get('/complaints', function (req, res) {
    res.render('legal/complaints', { title: 'Complaints' });
});

module.exports = router;
