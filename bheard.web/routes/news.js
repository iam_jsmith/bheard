var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    blogger = require('bheard').blogger();

router.get('/news', function (req, res) {
    blogger.getPosts(req.query.pageToken)
        .then(function (data) {
            res.render('news/index', _.extend({ title: 'News' }, data));
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/news/:id', function (req, res) {
    blogger.getPost(req.params.id)
        .then(function (data) {
            res.render('news/post', _.extend({ title: 'Post' }, data));
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
