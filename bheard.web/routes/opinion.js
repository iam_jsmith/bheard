var express = require('express'),
    router = express.Router();

router.get('/opinion', function (req, res) {
    res.render('opinion/opinion', { title: 'Share Opinions' });
});

router.get('/opinion/*', function (req, res) {
    res.render('opinion/topic', { title: 'Share Opinions' });
});

module.exports = router;
