var express = require('express'),
    router = express.Router();

router.get('/investors', function (req, res) {
    res.render('investors/index', { title: 'Investors' });
});

module.exports = router;
