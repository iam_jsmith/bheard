var express = require('express'),
    router = express.Router();

router.get('/switching/header-energy', function (req, res) {
    res.render('switching/header-energy', { seo: false, title: 'Switch' });
});

router.get('/switching/header-mobile', function (req, res) {
    res.render('switching/header-mobile', { seo: false, title: 'Switch' });
});

router.get('/switching/header', function (req, res) {
    res.render('switching/header', { seo: false, title: 'Switch' });
});


router.get('/switching/footer', function (req, res) {
    res.render('switching/footer', { seo: false, title: 'Switch' });
});

module.exports = router;
