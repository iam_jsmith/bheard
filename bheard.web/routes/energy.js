var express = require('express'),
    router = express.Router();

router.get('/energy', function (req, res) {
    res.render('energy/index', { title: 'Rate & Compare Energy Suppliers | B.heard' });
});

router.get('/energy/*', function (req, res) {
    res.render('energy/index', { seo: false, title: 'Energy' });
});

module.exports = router;
