var express = require('express'),
    router = express.Router();

router.get('/aboutus', function (req, res) {
    res.render('aboutus/index', { title: 'About Us | B.heard' });
});

module.exports = router;