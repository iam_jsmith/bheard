var express = require('express'),
    router = express.Router();

router.get('/money', function (req, res) {
    res.render('money/index', { title: 'Rate & Compare Banks and Building Societies | B.heard' });
});

router.get('/money/*', function (req, res) {
    res.render('money/index', { seo: false, title: 'Money' });
});

module.exports = router;
