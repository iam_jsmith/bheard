var express = require('express'),
    json2csv = require('json2csv'),
    router = express.Router(),
    authorize = require('../middleware/authorize'),
    db = require('bheard').db();

router.get('/admin', function (req, res) {
    res.render('admin/index', { title: 'Admin - Unmoderated Reviews' });
});

router.get('/admin/moderatedreviews', function (req, res) {
    res.render('admin/index', { title: 'Admin - Moderated Reviews' });
});

router.get('/admin/changecoins', function (req, res) {
    res.render('admin/index', { title: 'Admin - Change Coins' });
});

router.get('/admin/users', function (req, res) {
    res.render('admin/index', { title: 'Admin - Users' });
});

router.get('/admin/users.csv', authorize('admin'), function (req, res) {
    db.get('users').find({}, { sort: { created: 1 }}, function (err, users) {
        if (!err) {
            var fields = [
                '_id', 'email', 'unconfirmedEmail', 'twitterId', 'facebookId', 'googleId', 'firstName', 'lastName',
                'postcode', 'changeCoins', 'role', 'created', 'updated'
            ];

            json2csv({ data: users, fields: fields }, function (err, csv) {
                if (!err) {
                    res.setHeader('Content-Type', 'text/csv');
                    res.setHeader('Content-Disposition', 'attachment;filename=users.csv');
                    res.send(csv);
                } else {
                    next(err);
                }
            });
        } else {
            next(err);
        }
    });
});

module.exports = router;
