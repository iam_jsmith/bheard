module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['mocha', 'chai'],


    // list of files / patterns to load in the browser
    // SHOULDN'T REALLY HAVE TO DO THIS MANUALLY (ESP TWICE, SEE GULP BUILD FILE)
    // TODO REFACTOR WHOLE PROJECT TO USE REQUIREJS OR SOMETHING
    files: [
      'public/bower_components/jquery/dist/jquery.js',
      //'public/bower_components/bootstrap-sass/assets/javascripts/bootstrap.js',
      'public/bower_components/angular/angular.js',
      'public/bower_components/angular-mocks/angular-mocks.js',
      'public/bower_components/angular-animate/angular-animate.js',
      'public/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'public/bower_components/angular-cookies/angular-cookies.js',
      'public/bower_components/angular-loading-bar/build/loading-bar.js',
      'public/bower_components/angular-sanitize/angular-sanitize.js',
      'public/bower_components/angular-ui-router/release/angular-ui-router.js',
      'public/bower_components/angulike/angulike.js',
      'public/bower_components/lodash/lodash.js',
      'public/bower_components/moment/moment.js',
      'public/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
      'public/bower_components/ngtoast/dist/ngToast.js',
      'public/bower_components/swiper/dist/js/swiper.jquery.js',
        
      'public/bower_components/videogular/videogular.js',
      'public/bower_components/videogular-controls/vg-controls.js',
      'public/bower_components/videogular-overlay-play/vg-overlay-play.js',
      'public/bower_components/videogular-poster/vg-poster.js',
      'public/bower_components/videogular-buffering/vg-buffering.js',

      'public/app.js',
      'public/controllers/*.js',
      'public/directives/*.js',
      'public/filters/*.js',
      //'public/models/*.js',
      'public/modules/*/*.js',
      'public/modules/**/*.js',
      'public/services/*.js',
      'public/utilities/*.js',

      'test/angularjs/**/*.js'
    ],

    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultanous
    concurrency: Infinity
  })
}
