﻿var opbeat = require('opbeat').start({
      appId: '4be7a5b732',
      organizationId: 'e9002834b3634d96b3c17523e9fdc5e9',
      secretToken: '6f79ecc9fd720e06a574a7d9936e7e714a2c68de'
    }),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    compression = require('compression'),
    express = require('express'),
    expressLayouts = require('express-ejs-layouts'),
    favicon = require('serve-favicon'),
    logger = require('morgan'),
    path = require('path'),
    app = express(),
    api = require('./api'),
    awSnap = require('./middleware/awSnap'),
    config = require('bheard').config(),
    currentUser = require('./middleware/currentUser'),
    db = require('bheard').db(),
    fourOhFour = require('./middleware/fourOhFour'),
    routes = require('./routes'), // this pulls in ./routes/index.js NOT ALL ROUTES (add new route files there)
    seoMan = require('./middleware/seoMan'),
    changeCoinsLedger = require('bheard').changeCoinsLedger,
    changeCoinsMiddleware = require('./middleware/changeCoins');

changeCoinsMiddleware(app, changeCoinsLedger(db), config.get('/changeCoins/events'));

app.set('port', config.get('/server/web/port'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.set('layout', 'shared/layout');
app.locals.googleApiKey = config.get('/google/apiKey');
app.use(compression());
app.use(favicon(path.join(__dirname, 'public/images/favicons', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(cookieParser());
app.use(expressLayouts);
app.use(express.static(path.join(__dirname, 'public')));
app.use(currentUser);
app.use(seoMan);
app.use(api);
app.use(routes);
app.use(fourOhFour);
app.use(awSnap);
app.listen(app.get('port'));
console.log('Running website on port:', app.get('port'));

module.exports = app;
