var _ = require('lodash');

module.exports = function (user) {
    _.extend(this, _.pick(user, [
        '_id',
        'email',
        'hash',
        'firstName',
        'lastName',
        'postcode',
        'changeCoins'
    ]));
};