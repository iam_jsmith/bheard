var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    authorize = require('../../middleware/authorize'),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    loginSchema = require('./schemas/login'),
    UserDto = require('./dtos/user'),
    validate = require('../../middleware/validate');

router.post('/api/login', validate(loginSchema), function (req, res, next) {
    db.get('users').findOne({ email: req.body.email }, function (err, user) {
        if (!err) {
            if (user) {
                console.log('password attempt:', req.body.password);
                console.log('password:', user.password);

                if (encryptor.comparePasswords(req.body.password, user.password)) {
                    res.cookie('bheard', encryptor.encrypt(user._id.toString())).json(new UserDto(user));
                } else {
                    res.status(400).json({
                        message: 'Login failed.',
                        errors: { password: ['The password is incorrect.'] }
                    });
                }
            } else {
                res.status(400).json({
                    message: 'Login failed.',
                    errors: { email: ['The email address cannot be found.'] }
                });
            }
        } else {
            next(err);
        }
    });
});

router.post('/api/logout', authorize(), function (req, res) {
    res.clearCookie('bheard').sendStatus(200);
});

module.exports = router;
