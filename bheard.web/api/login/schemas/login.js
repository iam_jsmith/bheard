var Joi = require('joi');

module.exports = {
    body: {
        email: Joi.string().required().email().trim().max(254).lowercase(),
        password: Joi.string().required().max(140)
    }
};
