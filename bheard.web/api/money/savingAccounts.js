var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/savingaccounts', function (req, res, next) {
    var query = {};
    
    // only show fca data
    var fca = req.query.fca;
    fca = !!(fca === "true" || fca === true); // only set to true if req data is "true" or true, otherwise false
    query.fcaProduct = fca;

    // only show whitelisted data
    var whitelisted = req.query.whitelisted;
    whitelisted = !!(whitelisted === "true" || whitelisted === true); // only set to true if req data is "true" or true, otherwise false
    query.whitelisted = whitelisted;

    var onePerSupplier = req.query.onePerSupplier;
    // only set to true if req data is "true" or true, otherwise false
    onePerSupplier = !!(onePerSupplier === "true" || onePerSupplier === true);
    if (onePerSupplier) {
        // only show the 'best' (whitelisted) product by the supplier (TODO CHECK OK: IF 2 HAVE THE SAME DATA, THEN ONE WILL BE 1 AND ONE 2)
        query.supplierWhitelistedProductRank = 1;
    }
    
    var options = {
        skip: req.query.skip || 0,
        limit: req.query.take && req.query.take <= 100 ? req.query.take : 100,
        sort: { 'score.overallOpinionPercent': -1 }
    };

    db.get('savings').find(query, options)
        .then(function (accounts) {
            return db.get('suppliers').find({ _id: { $in: _.pluck(accounts, 'supplierId') }}).then(function (suppliers) {
                _.each(accounts, function (account) {
                    account.bank = _.find(suppliers, function (supplier) {
                        return supplier._id.equals(account.supplierId);
                    });
                });

                res.json(accounts);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/savingaccounts/:id', function (req, res, next) {
    db.get('savings').findById(req.params.id)
        .then(function (account) {
            return db.get('suppliers').findById(account.supplierId).then(function (supplier) {
                account.bank = supplier;
                res.json(account);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
