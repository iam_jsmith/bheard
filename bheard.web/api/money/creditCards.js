var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/creditcards', function (req, res, next) {
    var query = {};

    // only show fca data
    var fca = req.query.fca;
    fca = !!(fca === "true" || fca === true); // only set to true if req data is "true" or true, otherwise false
    query.fcaProduct = fca;

    // only show whitelisted data
    var whitelisted = req.query.whitelisted;
    whitelisted = !!(whitelisted === "true" || whitelisted === true); // only set to true if req data is "true" or true, otherwise false
    query.whitelisted = whitelisted;

    var onePerSupplier = req.query.onePerSupplier;
    // only set to true if req data is "true" or true, otherwise false
    onePerSupplier = !!(onePerSupplier === "true" || onePerSupplier === true);
    if (onePerSupplier) {
        // only show the 'best' (whitelisted) product by the supplier (TODO CHECK OK: IF 2 HAVE THE SAME DATA, THEN ONE WILL BE 1 AND ONE 2)
        query.supplierWhitelistedProductRank = 1;
    }
    
    var options = {
        skip: req.query.skip || 0,
        limit: req.query.take && req.query.take <= 100 ? req.query.take : 100,
        sort: { 'score.overallOpinionPercent': -1 }
    };

    db.get('creditCards').find(query, options)
        .then(function (creditCards) {
            return db.get('suppliers').find({ _id: { $in: _.pluck(creditCards, 'supplierId') }}).then(function (suppliers) {
                _.each(creditCards, function (creditCard) {
                    creditCard.bank = _.find(suppliers, function (supplier) {
                        return supplier._id.equals(creditCard.supplierId);
                    });
                });

                res.json(creditCards);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/creditcards/:id', function (req, res, next) {
    db.get('creditCards').findById(req.params.id)
        .then(function (creditCard) {
            return db.get('suppliers').findById(creditCard.supplierId).then(function (supplier) {
                creditCard.bank = supplier;
                res.json(creditCard);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
