var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/unsecuredloans', function (req, res, next) {
    var query = {};
    
    // only show fca data
    var fca = req.query.fca;
    fca = !!(fca === "true" || fca === true); // only set to true if req data is "true" or true, otherwise false
    query.fcaProduct = fca;

    // only show whitelisted data
    var whitelisted = req.query.whitelisted;
    whitelisted = !!(whitelisted === "true" || whitelisted === true); // only set to true if req data is "true" or true, otherwise false
    query.whitelisted = whitelisted;

    var onePerSupplier = req.query.onePerSupplier;
    // only set to true if req data is "true" or true, otherwise false
    onePerSupplier = !!(onePerSupplier === "true" || onePerSupplier === true);
    if (onePerSupplier) {
        // only show the 'best' (whitelisted) product by the supplier (TODO CHECK OK: IF 2 HAVE THE SAME DATA, THEN ONE WILL BE 1 AND ONE 2)
        query.supplierWhitelistedProductRank = 1;
    }

    var options = {
        skip: req.query.skip || 0,
        limit: req.query.take && req.query.take <= 100 ? req.query.take : 100,
        sort: { 'score.overallOpinionPercent': -1 }
    };

    db.get('unsecuredLoans').find(query, options)
        .then(function (unsecuredLoans) {
            return db.get('suppliers').find({ _id: { $in: _.pluck(unsecuredLoans, 'supplierId') }}).then(function (suppliers) {
                _.each(unsecuredLoans, function (unsecuredLoan) {
                    unsecuredLoan.bank = _.find(suppliers, function (supplier) {
                        return supplier._id.equals(unsecuredLoan.supplierId);
                    });
                });

                res.json(unsecuredLoans);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/unsecuredloans/:id', function (req, res, next) {
    db.get('unsecuredLoans').findById(req.params.id)
        .then(function (unsecuredLoan) {
            return db.get('suppliers').findById(unsecuredLoan.supplierId).then(function (supplier) {
                unsecuredLoan.bank = supplier;
                res.json(unsecuredLoan);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
