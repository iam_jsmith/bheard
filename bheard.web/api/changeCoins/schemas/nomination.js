var Joi = require('joi');

module.exports = {
    body: {
        charityName: Joi.string().required().trim().max(140),
        whyShouldWeSupportThem: Joi.string().required().trim().max(1000),
        email: Joi.string().email().trim().max(254).allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
