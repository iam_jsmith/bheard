var Joi = require('joi');

module.exports = {
    body: {
        charityId: Joi.string().required(),
        amount: Joi.number().integer().positive().required()
    }
};
