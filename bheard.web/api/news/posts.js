var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    blogger = require('bheard').blogger();

router.get('/api/news/posts', function (req, res, next) {
    blogger.getPosts(req.query.pageToken)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/news/posts/labels', function (req, res, next) {
    blogger.getPostsWithLabels(req.query.pageToken, req.query.labels)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/news/posts/:id', function (req, res, next) {
    blogger.getPost(req.params.id)
        .then(function (data) {
            res.json(data);
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
