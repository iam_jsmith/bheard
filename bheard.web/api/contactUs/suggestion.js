var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    router = express.Router(),
    config = require('bheard').config(),
    db = require('bheard').db(),
    mailer = require('bheard').mailer(),
    suggestionSchema = require('./schemas/suggestion'),
    validate = require('../../middleware/validate');

router.post('/api/suggestion', validate(suggestionSchema), function (req, res, next) {
    mailer.send({
        from: 'B.heard <hello@bheard.com>',
        to: 'info@bheard.com',
        subject: 'Suggestion',
        text: 'Hello\n\n' +
              'You\'ve received a new \'Suggestion\' form submission, here\'s the details:\n\n' +
              'Email: ' + (req.body.email || 'Anonymous') + '\n' +
              'Which sector: ' + req.body.whichSector + '\n' +
              'Which brand: ' + req.body.whichBrand + '\n' +
              'Give us some more information: ' + req.body.moreInfo,
        html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width"><meta charset="utf-8"><title>Suggestion</title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><style type="text/css">@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800);#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic;border:0}body{margin:0;padding:0}img{border:0;height:auto;line-height:100%;outline:0;text-decoration:none}table{border-collapse:collapse!important}#bodyCell,#bodyTable,body{height:100%!important;margin:0;padding:0;width:100%!important}table[class=responsive-table]{width:580px!important}table[class=master-table]{width:600px!important}table[class=copy-table]{width:75%!important}table[class=story]{width:78%!important}.appleLinks a{color:#c2c2c2!important;text-decoration:none}.button{color:#fff!important;text-decoration:none!important}span.preheader{display:none!important}@media only screen and (max-width:599px){table[class=responsive-table]{width:95%!important}table[class=master-table]{width:100%!important}td[class=responsive-header-cell-big]{display:block;text-align:left;width:240px;margin-left:15px;font-size:22px!important}td[class=responsive-header-cell]{display:block;text-align:left;width:240px;margin-left:15px}table[class=copy-table],table[class=story]{width:85%!important}[class].full-card-image{width:95%!important}[class].hero-image{width:100%!important}}@media only screen and (max-width:480px){a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}body{width:100%!important;min-width:100%!important}[class].social-cell{width:100%!important}[class].social-spacer{height:10px!important}}</style></head><body><center><table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#eaeced"><tr height="8" style="font-size:0;line-height:0"><td><tr><td align="center" valign="top"><table class="master-table" width="600"><tr height="10"><td><tr><td align="center" valign="top"><table class="responsive-table" width="580" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="overflow:hidden!important;border-radius:10px"><tr height="30"><td><tr><td align="center" width="70"><a href="https://bheard.com"></a><img style="display:block;margin:0 auto 20px;height:auto" src="https://www.bheard.com/build/images/logo-black-script.png" width="70" alt="B.heard"/><tr><td align="center"><table width="85%"><tr><td align="center"><h2 style="margin:0!important;font-family:"Open Sans",arial,sans-serif!important;font-size:28px!important;line-height:30px!important;font-weight:200!important;color:#252b33!important">Hello!</h2></table><tr height="25"><td><tr><td align="center"><table class="story" border="0" cellpadding="0" cellspacing="0" width="78%"><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important">You\'ve received a new \'Suggestion\' form submission, here\'s the details:</table><tr height="25"><td><tr><td align="center"><table class="story" border="0" cellpadding="0" cellspacing="0" width="78%"><tr><td style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important"><strong>Email:</strong> ' + (req.body.email || 'Anonymous') + '<br/><strong>Which sector:</strong> ' + req.body.whichSector + '<br/><strong>Which brand:</strong> ' + req.body.whichBrand + '<br/><br/><strong>Give us some more information:</strong><br/>' + req.body.moreInfo + '</table><tr height="30"><td></table><tr height="20"><td><tr><td align="center"><table class="responsive-table" width="580" border="0" cellpadding="0" cellspacing="0" valign="top"><tr><td align="center" valign="top"><a href="https://bheard.com"></a><img src="https://www.bheard.com/build/images/logo-black-script.png" width="45" alt="B.heard" style="border:0"><tr height="10"><td><tr><td align="center" valign="top" style="font-family:"Open Sans",sans-serif!important;font-weight:400!important;color:#7e8890!important;font-size:12px!important;text-transform:uppercase!important;letter-spacing:.045em!important">Your voice is important, let it B.heard.<tr height="10" style="padding:0;margin:0;font-size:0;line-height:0"><td><tr><td align="center" style="font-family:Open Sans,sans-serif!important;font-size:12px!important;color:#acacac!important">B.heard Limited is a registered limited company of England and Wales (9082696) located at 21 - 27 Lambs Conduit Street, London, WC1N 3GS<tr height="20" style="padding:0;margin:0;font-size:0;line-height:0"><td></table></table></table></center></body></html>'
    }).then(
        function () {
            res.send(200);
        },
        function (err) {
            next(err)
        }
    );
});

module.exports = router;
