var Joi = require('joi');

module.exports = {
    body: {
        companyName: Joi.string().required().trim().max(140),
        yourName: Joi.string().required().trim().max(140),
        email: Joi.string().email().max(254).allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
