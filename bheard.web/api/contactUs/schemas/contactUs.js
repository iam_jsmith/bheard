var Joi = require('joi');

module.exports = {
    body: {
        name: Joi.string().required().trim().max(280),
        email: Joi.string().email().required().trim().max(254),
        natureOfEnquiry: Joi.string().required().trim().max(140),
        message: Joi.string().required().trim().max(1000)
    }
};
