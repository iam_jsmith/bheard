var Joi = require('joi');

module.exports = {
    body: {
        whichSector: Joi.string().required().trim().max(140),
        whichBrand: Joi.string().required().trim().max(140),
        moreInfo: Joi.string().required().trim().max(1000),
        email: Joi.string().email().max(254).allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
