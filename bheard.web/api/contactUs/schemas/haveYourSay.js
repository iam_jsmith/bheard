var Joi = require('joi');

module.exports = {
    body: {
        whatHappened: Joi.string().required().trim().max(1000),
        whichOrganisationWasIt: Joi.string().required().trim().max(140),
        email: Joi.string().email().max(254).allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
