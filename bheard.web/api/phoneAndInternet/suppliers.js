var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/phoneandinternetsuppliers', function (req, res, next) {
    var query = { sector: 'phoneandinternet' };

    if (req.query.type) {
        switch (req.query.type) {
            case 'internet':
                query.broadband_provider = true;
                break;
            case 'mobile':
                query.mobile_provider = true;
                break;
            case 'telephone':
                query.fixed_line_provider = true;
                break;
            case 'tv':
                query.tv_provider = true;
                break;
            default:
                throw ':('
        }
    }

    db.get('suppliers').find(query, { limit: 500, sort: { 'score.overallOpinionPercent': -1 } })
        .then(function (suppliers) {
            res.json(suppliers);
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
