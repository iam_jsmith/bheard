var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    authorize = require('../../../middleware/authorize'),
    db = require('bheard').db();

router.get('/api/admin/users', authorize('admin'), function (req, res, next) {
    db.get('users').find({}, { sort: { created: 1 }}, function (err, users) {
        if (!err) {
            res.json(users);
        } else {
            next(err);
        }
    });
});

module.exports = router;
