var Joi = require('joi');

module.exports = {
    body: Joi.array().items(Joi.object().keys({
        isApproved: Joi.boolean().required().allow(null)
    }))
};
