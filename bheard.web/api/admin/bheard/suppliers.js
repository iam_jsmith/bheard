var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    q = require('q'),
    router = express.Router(),
    authorize = require('../../../middleware/authorize'),
    db = require('bheard').db(),
    supplierReviewsSchema = require('./schemas/supplierReviews'),
    validate = require('../../../middleware/validate');

router.get('/api/admin/suppliers/reviews', authorize('admin'), function (req, res, next) {
    var query = { isApproved: req.query.isModerated === 'true' ? { $ne: null } : null },
        options = {
            skip: req.query.skip || 0,
            limit: req.query.take || 100,
            sort: { created: -1 }
        };

    db.get('supplierReviews').find(query, options)
        .then(function (reviews) {
            return q.all([
                db.get('suppliers').find({ _id: { $in: _.pluck(reviews, 'supplierId') }}),
                db.get('users').find({ _id: { $in: _.pluck(reviews, 'userId') }})
            ]).then(function (data) {
                res.json(_.filter(reviews, function (review) {
                    review.supplier = _.find(data[0], function (supplier) {
                        return supplier._id.equals(review.supplierId);
                    });

                    review.user = review.isAnonymous ? null : _.find(data[1], function (user) {
                        return user._id.equals(review.userId);
                    });

                    return review.isAnonymous || review.user.email || review.user.isSocial;
                }));
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.put('/api/admin/suppliers/reviews', authorize('admin'), validate(supplierReviewsSchema), function (req, res, next) {
    q.all(_.map(req.body, function (review) {
        return db.get('supplierReviews').update(
            { _id: review._id },
            {
                $set:
                {
                    isApproved: review.isApproved,
                    videoUrl: review.videoUrl,
                    updated: moment.utc().toDate()
                }
            }
        );
    })).then(
        function () {
            res.send(200);
        },
        function (err) {
            next(err);
        }
    );
});

module.exports = router;
