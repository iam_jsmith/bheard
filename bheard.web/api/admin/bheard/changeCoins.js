var express = require('express'),
    _ = require('lodash'),
    q = require('q'),
    router = express.Router(),
    authorize = require('../../../middleware/authorize'),
    db = require('bheard').db(),
    validate = require('../../../middleware/validate');

router.get('/api/admin/changecoins', authorize('admin'), function (req, res, next) {
    db.get('changeCoinsLedger').find({ event: 'donationreceived' }, { sort: { 'enrtyDate': 1 } })
        .then(function (entries) {
            return q.all([
                db.get('charities').find({ _id: { $in: _.pluck(entries, 'charity') }}),
                db.get('users').find({ _id: { $in: _.pluck(entries, 'user') }})
            ]).then(function (data) {
                _.each(entries, function (entry) {
                    entry.charity = _.find(data[0], function (charity) {
                        return charity._id.equals(entry.charity);
                    });

                    entry.user = _.find(data[1], function (user) {
                        return user._id.equals(entry.user);
                    });
                });

                res.json(entries);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
