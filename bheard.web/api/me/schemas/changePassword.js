var Joi = require('joi');

module.exports = {
    body: {
        oldPassword: Joi.string().required().max(140),
        newPassword: Joi.string().required().max(140),
        confirmNewPassword: Joi.any().valid(Joi.ref('newPassword'))
    }
};
