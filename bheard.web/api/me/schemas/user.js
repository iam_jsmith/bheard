var Joi = require('joi');

module.exports = {
    body: {
        firstName: Joi.string().required().trim(),
        lastName: Joi.string().required().trim(),
        postcode: Joi.string().trim().min(5).max(8).uppercase().regex(/^[0-9A-Z]*\s?[0-9A-Z]*$/i, 'Postcode').allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
