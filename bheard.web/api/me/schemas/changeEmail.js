var Joi = require('joi');

module.exports = {
    body: {
        unconfirmedEmail: Joi.string().email().required().trim().max(254).lowercase()
    }
};
