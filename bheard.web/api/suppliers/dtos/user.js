var _ = require('lodash');

module.exports = function (user) {
    _.extend(this, _.pick(user, [
        '_id',
        'hash',
        'firstName',
        'lastName',
        'isSocial',
        'twitterId',
        'twitterPictureUrl',
        'facebookId',
        'facebookPictureUrl',
        'googleId',
        'googlePictureUrl'
    ]));
};