var Joi = require('joi');

module.exports = {
    body: {
        type: Joi.string().required().valid('helpful', 'unhelpful')
    }
};
