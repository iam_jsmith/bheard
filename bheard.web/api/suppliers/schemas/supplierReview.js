var Joi = require('joi');

module.exports = {
    body: {
        overallOpinion: Joi.number().required().integer().min(1).max(5),
        customerService: Joi.number().required().integer().min(1).max(5),
        valueForMoney: Joi.number().required().integer().min(1).max(5),
        title: Joi.string().trim().max(140).allow(null, ''),
        text: Joi.string().trim().max(1200).allow(null, ''),
        isAnonymous: Joi.boolean().required(),
        isCertified: Joi.boolean().required().valid(true),
        token: Joi.string().allow(null, ''),
        //this is used for api validation too, can it be ignored for reviews with suppliers?
        suggestedCompanyName: Joi.string().trim().max(140).allow(null, '')
    },
    options: {
        allowUnknown: false
    }
};
