var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    q = require('q'),
    router = express.Router(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    supplierReviewSchema = require('./schemas/supplierReview'),
    supplierReviewRatingSchema = require('./schemas/supplierReviewRating'),
    UserDto = require('./dtos/user'),
    validate = require('../../middleware/validate');

router.get('/api/suppliers', function (req, res, next) {
    if (req.query.name) { // via have your say page
        var query = {
                $and: _.map(_.compact(req.query.name.split(' ')), function (term) {
                    return { name: new RegExp(term, 'i') };
                })
            },
            take = req.query.limit ? parseInt(req.query.limit) : 9;

        if (req.query.sector) {
            query.sector = req.query.sector;
        }

        db.get('suppliers').col.aggregate([
            { $match : query },
            { $project: {
                name: true,
                sector: true,
                subsector: true,
                mobile_provider: true,
                broadband_provider: true,
                startsWith: {
                    $eq: [
                        { $substr: [{ $toLower: '$name' }, 0, req.query.name.length ] },
                        req.query.name.toLowerCase()
                    ]
                }
            }},
            { $sort: {
                startsWith: -1,
                name: 1
            }},
            { $limit: take }],
            function (err, suppliers) {
                if (!err) {
                    res.json(suppliers);
                } else {
                    next(err);
                }
            }
        );
    } else if (req.query.no) { // via have your say page when linked to from maps
        db.get('suppliers').findOne({ sector: req.query.sector, no: req.query.no }, function (err, supplier) {
            if (!err) {
                res.json(supplier);
            } else {
                next(err);
            }
        });
    } else if (req.query.nos) { // via healthcare, education & government pages
        db.get('suppliers').find(
            { no: { $in: req.query.nos } },
            function (err, suppliers) {
                if (!err) {
                    res.json(_.sortBy(suppliers, function (supplier) {
                        // sort by rank instead
                        switch (supplier.sector) {
                            case 'healthcare':
                                return supplier.score ? supplier.score : 0;
                            case 'government':
                                return supplier.overallRank ? supplier.overallRank : 0;
                            case 'education':
                                return supplier.overallEffectiveness ? supplier.overallEffectiveness : 0;
                        }
                    }).reverse());
                } else {
                    next(err);
                }
            }
        );
    } else if (req.query.sector) { // via ranking pages

        var query = {sector: req.query.sector, rank: { $exists: true }}
        var limit = 100;

        // only show fca data
        if(req.query.sector === "money") {
            var fca = req.query.fca;
            fca = !!(fca === "true" || fca === true); // only set to true if req data is "true" or true, otherwise false

            query.fcaSupplier = fca;
        }

        if(req.query.limit) {
            limit = req.query.limit;
        }

        db.get('suppliers').find(
            query,
            { limit: limit, sort: { rank: 1 } },
            function (err, suppliers) {
                if (!err) {
                    res.json(suppliers);
                } else {
                    next(err);
                }
            }
        );
    }
});

router.get('/api/suppliers/reviews', function (req, res, next) {
    var query = { isApproved: true},
        options = { limit: 100, sort: { created: -1 } };

    if (req.query.limit) {
        options.limit = req.query.limit;
    }

    if (req.query.sector) {
        query.sector = req.query.sector;
    }

    if (req.query.userId) {
        query.userId = db.get('users').id(req.query.userId);
    }

    db.get('supplierReviews').find(query, options)
        .then(function (reviews) {
            return q.all([
                db.get('suppliers').find({ _id: { $in: _.pluck(reviews, 'supplierId') }}),
                db.get('users').find({ _id: { $in: _.pluck(reviews, 'userId') }})
            ]).then(function (data) {
                var ids = req.cookies.datr ? JSON.parse(encryptor.decrypt(req.cookies.datr)) : [];

                _.each(reviews, function (review) {
                    review.supplier = _.find(data[0], function (supplier) {
                        return supplier._id.equals(review.supplierId);
                    });

                    review.user = review.isAnonymous ? null : new UserDto(_.find(data[1], function (user) {
                        return user._id.equals(review.userId);
                    }));

                    review.isRated = _.includes(ids, review._id.toString());
                });

                res.json(reviews);
            });
        })
        .catch(function (err) {
            next(err);
        });
});


router.get('/api/suppliers/reviews/:id', function (req, res, next) {
    db.get('supplierReviews').findOne({ _id: req.params.id, isApproved: true })
        .then(function (review) {
            return q.all([
                db.get('suppliers').findOne({ _id: review.supplierId}),
                db.get('users').findOne({ _id: review.userId})
            ]).then(function (data) {
                var ids = req.cookies.datr ? JSON.parse(encryptor.decrypt(req.cookies.datr)) : [];

                review.supplier = data[0];
                review.user = review.isAnonymous ? null : new UserDto(data[1]);
                review.isRated = _.includes(ids, review._id.toString());

                res.json(review);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/suppliers/:id', function (req, res, next) {
    var query = {};

    if (req.params.id.match(/^[a-fA-F0-9]{24}$/)) {
        query._id = req.params.id;
    } else {
        query.no = req.params.id;
    }

    db.get('suppliers').findOne(query, function (err, supplier) {
        if (!err && supplier) {
            res.json(supplier);
        } else {
            next(err);
        }
    });
});

router.get('/api/suppliers/:id([0-9a-f]{24})/reviews', function (req, res, next) {
    db.get('supplierReviews').find(
        { supplierId: db.get('suppliers').id(req.params.id), isApproved: true },
        { limit: 9, sort: { created: -1 } })
        .then(function (reviews) {
            return q.all([
                db.get('suppliers').find({ _id: { $in: _.pluck(reviews, 'supplierId') }}),
                db.get('users').find({ _id: { $in: _.pluck(reviews, 'userId') }})
            ]).then(function (data) {
                var ids = req.cookies.datr ? JSON.parse(encryptor.decrypt(req.cookies.datr)) : [];

                _.each(reviews, function (review) {
                    review.supplier = _.find(data[0], function (supplier) {
                        return supplier._id.equals(review.supplierId);
                    });

                    review.user = review.isAnonymous ? null : new UserDto(_.find(data[1], function (user) {
                        return user._id.equals(review.userId);
                    }));

                    review.isRated = _.includes(ids, review._id.toString());
                });

                res.json(reviews);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/suppliers/:id([0-9a-f]{24})/my-review', function (req, res, next) {
    db.get('supplierReviews').find(
        { supplierId: db.get('suppliers').id(req.params.id), isApproved: true },
        { limit: 1, sort: { isComplete: -1, created: -1 } })
        .then(function (reviews) {
            return q.all([
                db.get('suppliers').find({ _id: { $in: _.pluck(reviews, 'supplierId') }}),
                db.get('users').find({ _id: { $in: _.pluck(reviews, 'userId') }})
            ]).then(function (data) {
                var ids = req.cookies.datr ? JSON.parse(encryptor.decrypt(req.cookies.datr)) : [];

                _.each(reviews, function (review) {
                    review.supplier = _.find(data[0], function (supplier) {
                        return supplier._id.equals(review.supplierId);
                    });

                    review.user = review.isAnonymous ? null : new UserDto(_.find(data[1], function (user) {
                        return user._id.equals(review.userId);
                    }));

                    review.isRated = _.includes(ids, review._id.toString());
                });

                res.json(reviews);
            });
        })
        .catch(function (err) {
            next(err);
        });
});

router.get('/api/suppliers/:id([0-9a-f]{24})/products', function (req, res, next) {
  // todo not sure this is the best way of getting the supplier id
  var supplierId = db.get('creditCards').id(req.params.id);
  // var whitelisted = req.query.whitelisted ? true : null; // TODO CHECK GETS BOTH TRUE AND FALSE IF NULL

    // TODO MAY NEED FCA SUPPLIER FLAG IN HERE TOO
    // TODO MAY WANT TO HAVE WHITELISTED AS A PARAMETER
  return q.all([
    db.get('creditCards').find({deleted: false, whitelisted: true, supplierId: supplierId}),
    db.get('currentAccounts').find({deleted: false, whitelisted: true, supplierId: supplierId}),
    db.get('savings').find({deleted: false, whitelisted: true, supplierId: supplierId}),
    db.get('unsecuredLoans').find({deleted: false, whitelisted: true, supplierId: supplierId})
  ]).then(function (data) {
    var products = {
      'creditCards': data[0],
      'currentAccounts' : data[1],
      'savingsAccounts' : data[2],
      'unsecuredLoans' : data[3]
    };
    res.json(products);
  })
  .catch(function (err) {
    next(err);
  });
});

router.post('/api/suppliers/:id/reviews', validate(supplierReviewSchema), function (req, res, next) {
  function handleReview (supplier) {
    if (supplier) {
      var userId = null;
      var suggestedCompanyName = req.body.suggestedCompanyName;
      if (!req.body.isAnonymous) {
        if (req.body.token) {
          userId = db.get('users').id(encryptor.decrypt(req.body.token));
        } else {
          userId = req.user._id;
        }
      }
      
      if (suggestedCompanyName) {
        suggestedCompanyName = suggestedCompanyName.toString().toLowerCase(); // so some consistency
      }
      
      var review = _.extend(_.omit(req.body, 'token'), {
        sector: supplier.sector,
        subSector: supplier.subSector,
        supplierId: supplier._id,
        userId: userId,
        created: moment.utc().toDate(),
        isComplete: !!(req.body.title && req.body.text),
        isApproved: null,
        helpful: 0,
        unhelpful: 0,
        ipAddress: req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress,
        suggestedCompanyName: suggestedCompanyName // ideally shouldn't be added for normal reviews, see supplierReview.js
      });
      
      return db.get('supplierReviews').insert(review).then(function (review) {
        if (!review.isAnonymous) {
          req.app.emit('event:changecoins:reviewsubmission', {
            event: 'reviewsubmission',
            user: userId.toString()
          });
        }
        res.json(review);
      });
    } else {
      res.send(404);
    }
  }
  if (req.params.id === 'anonymousSupplier') {
    db.get('suppliers').findOne({name: 'Anonymous'})
        .then(handleReview)
        .catch(function(err) {
          next(err);
        });
  } else {
    db.get('suppliers').findById(req.params.id)
        .then(handleReview)
        .catch(function(err) {
          next(err);
        });
  }
});

router.post('/api/suppliers/reviews/:id/ratings', validate(supplierReviewRatingSchema), function (req, res, next) {
    var ids = req.cookies.datr ? JSON.parse(encryptor.decrypt(req.cookies.datr)) : [];

    if (!_.includes(ids, req.params.id)) {
        var data = {
            $inc: {}
        };

        data.$inc[req.body.type] = 1;

        db.get('supplierReviews').updateById(req.params.id, data, function (err) {
            if (!err) {
                ids.push(req.params.id);
                res.cookie('datr', encryptor.encrypt(JSON.stringify(ids))).send(200);
            } else {
                next(err);
            }
        });
    } else {
        res.status(400).json({
            message: 'Rate review failed.',
            errors: { id: ['The review has already been rated.'] }
        });
    }
});

module.exports = router;
