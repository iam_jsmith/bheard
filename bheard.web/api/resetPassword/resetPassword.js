var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    router = express.Router(),
    uuid = require('node-uuid'),
    config = require('bheard').config(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    mailer = require('bheard').mailer(),
    resetPasswordSchema = require('./schemas/resetPassword'),
    resetPasswordStep2Schema = require('./schemas/resetPasswordStep2'),
    validate = require('../../middleware/validate');

router.post('/api/resetpassword', validate(resetPasswordSchema), function (req, res, next) {
    db.get('users').findOne({ email: req.body.email }, function (err, user) {
        if (!err) {
            if (user) {
                var data = {
                    resetPasswordToken: uuid.v4(),
                    updated: moment.utc().toDate()
                };

                db.get('users').updateById(user._id, { $set: data }, function (err) {
                    if (!err) {
                        mailer.send({
                            from: 'B.heard <hello@bheard.com>',
                            to: user.email,
                            subject: 'B.heard',
                            text: 'Hello ' + user.firstName + '\n\n' +
                                  'You recently requested to reset your password on B.heard. To create a new password please click the following link:\n' +
                                  config.get('/server/web/url') + '/resetpassword/' + data.resetPasswordToken + '\n\n' +
                                  'If you did not request a password reset, just ignore this email.\n\n' +
                                  'B.heard\n' +
                                  'https://bheard.com',
                            html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta content="text/html; charset=UTF-8" http-equiv="Content-Type"><meta content="width=device-width" name="viewport"><meta charset="utf-8"><title>Forgot Password</title><meta content="" name="description"><meta content="width=device-width,initial-scale=1" name="viewport"><style type="text/css">@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800);#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic;border:0}body{margin:0;padding:0}img{border:0;height:auto;line-height:100%;outline:0;text-decoration:none}table{border-collapse:collapse!important}#bodyCell,#bodyTable,body{height:100%!important;margin:0;padding:0;width:100%!important}table[class=responsive-table]{width:580px!important}table[class=master-table]{width:600px!important}table[class=copy-table]{width:75%!important}table[class=story]{width:78%!important}.appleLinks a{color:#c2c2c2!important;text-decoration:none}.button{color:#fff!important;text-decoration:none !important;}span.preheader{display:none!important}@media only screen and (max-width:599px){table[class=responsive-table]{width:95%!important}table[class=master-table]{width:100%!important}td[class=responsive-header-cell-big]{display:block;text-align:left;width:240px;margin-left:15px;font-size:22px!important}td[class=responsive-header-cell]{display:block;text-align:left;width:240px;margin-left:15px}table[class=copy-table],table[class=story]{width:85%!important}[class].full-card-image{width:95%!important}[class].hero-image{width:100%!important}}@media only screen and (max-width:480px){a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}body{width:100%!important;min-width:100%!important}[class].social-cell{width:100%!important}[class].social-spacer{height:10px!important}}</style></head><body><center><table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#eaeced" align="center" height="100%"><tbody><tr height="8" style="font-size:0;line-height:0"><td></td></tr><tr><td align="center" valign="top"><table width="600" class="master-table"><tbody><tr height="10"><td></td></tr><tr><td align="center" valign="top"><table width="580" border="0" cellpadding="0" cellspacing="0" class="responsive-table" valign="top" bgcolor="#ffffff" style="overflow:hidden!important;border-radius:10px"><tbody><tr height="30"><td></td></tr><tr><td align="center" width="70"><a href="https://bheard.com"></a><img alt="B.heard" src="https://www.bheard.com/build/images/logo-black-script.png" style="display:block;margin:0 auto 20px;height:auto" width="70"/></td></tr><tr><td align="center"><table width="85%"><tbody><tr><td align="center"><h2 style="margin:0!important;font-family:"Open Sans",arial,sans-serif!important;font-size:28px!important;line-height:30px!important;font-weight:200!important;color:#252b33!important">Hello ' + user.firstName + '</h2></td></tr></tbody></table></td></tr><tr height="25"><td></td></tr><tr><td align="center"><table width="78%" border="0" cellpadding="0" cellspacing="0" class="story"><tbody><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important">You recently requested to reset your password on B.heard. To create a new password please click the following link:</td></tr></tbody></table></td></tr><tr height="25"><td></td></tr><tr><td align="center" valign="top"><table border="0" cellpadding="0" cellspacing="0"><tbody><tr><td align="center" valign="top"><a href="' + config.get('/server/web/url') + '/resetpassword/' + data.resetPasswordToken + '" style="background-color:#e61b72;padding:14px 28px 14px 28px;-webkit-border-radius:3px;border-radius:3px;line-height:18px!important;letter-spacing:.125em;text-transform:uppercase;font-size:13px;font-family:"Open Sans",Arial,sans-serif;font-weight:400;color:#fff!important;text-decoration:none;display:inline-block;line-height:18px!important;-webkit-text-size-adjust:none;mso-hide:all" class="button">Reset Your Password</a></td></tr></tbody></table></td></tr><tr height="25"><td></td></tr><tr><td align="center"><table width="78%" border="0" cellpadding="0" cellspacing="0" class="story"><tbody><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important">If you did not request a password reset, just ignore this email.</td></tr></tbody></table></td></tr><tr height="25"><td></td></tr><tr><td align="center"><table width="78%" border="0" cellpadding="0" cellspacing="0" class="story"><tbody><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important"><strong>Many thanks</strong><br/>The B.heard team</td></tr></tbody></table></td></tr><tr height="30"><td></td></tr></tbody></table></td></tr><tr height="20"><td></td></tr><tr><td align="center"><table width="580" border="0" cellpadding="0" cellspacing="0" class="responsive-table" valign="top"><tbody><tr><td align="center" valign="top"><a href="https://bheard.com"></a><img alt="B.heard" src="https://www.bheard.com/build/images/logo-black-script.png" style="border:0" width="45"></td></tr><tr height="10"><td></td></tr><tr><td align="center" valign="top" style="font-family:"Open Sans",sans-serif!important;font-weight:400!important;color:#7e8890!important;font-size:12px!important;text-transform:uppercase!important;letter-spacing:.045em!important">Your voice is important, let it B.heard.</td></tr><tr height="10" style="padding:0;margin:0;font-size:0;line-height:0"><td></td></tr><tr><td align="center" style="font-family:Open Sans,sans-serif!important;font-size:12px!important;color:#acacac!important">B.heard Limited is a registered limited company of England and Wales (9082696) located at 21 - 27 Lambs Conduit Street, London, WC1N 3GS</td></tr><tr height="20" style="padding:0;margin:0;font-size:0;line-height:0"><td></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></center></body></html>'
                        }).then(
                            function () {
                                res.send(200);
                            },
                            function (err) {
                                next(err);
                            }
                        );
                    } else {
                        next(err);
                    }
                });
            } else {
                res.status(400).json({
                    message: 'Reset password failed.',
                    errors: { email: ['The email address cannot be found.'] }
                });
            }
        } else {
            next(err);
        }
    });
});

router.post('/api/resetpassword/:uuid', validate(resetPasswordStep2Schema), function (req, res, next) {
    db.get('users').findOne({ resetPasswordToken: req.params.uuid }, function (err, exists) {
        if (!err) {
            if (exists) {
                var data = {
                    password: encryptor.encryptPassword(req.body.newPassword),
                    resetPasswordToken: null,
                    updated: moment.utc().toDate()
                };

                db.get('users').updateById(exists._id, { $set: data }, function (err) {
                    if (!err) {
                        res.send(200);
                    } else {
                        next(err);
                    }
                });
            } else {
                res.send(404);
            }
        } else {
            next(err);
        }
    });
});

module.exports = router;
