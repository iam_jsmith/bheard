var Joi = require('joi');

module.exports = {
    body: {
        email: Joi.string().email().required().trim().max(254).lowercase()
    }
};
