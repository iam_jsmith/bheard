var Joi = require('joi');

module.exports = {
    body: {
        newPassword: Joi.string().required().max(140),
        confirmNewPassword: Joi.any().valid(Joi.ref('newPassword'))
    },
    options: {
        allowUnknown: false
    }
};
