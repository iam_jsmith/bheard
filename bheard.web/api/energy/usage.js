var express = require('express'),
    router = express.Router();

router.get('/api/energyusage', function (req, res) {
    res.json({
        threshold: 'High',
        gas: 111,
        electricity: 222
    });
});

module.exports = router;
