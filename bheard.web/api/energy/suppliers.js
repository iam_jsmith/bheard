var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/energysuppliers', function (req, res, next) {
    var query = { sector: 'energy' };

    if (req.query.type) {
        switch (req.query.type) {
            case 'dual':
                query.$or = [
                    { dualfuel_provider: 'true' },
                    { dualfuel_provider: true }
                ];
                break;
            case 'gas':
                query.$or = [
                    { gas_provider: 'true' },
                    { gas_provider: true }
                ];
                break;
            case 'electricity':
                query.$or = [
                    { electricity_provider: 'true' },
                    { electricity_provider: true }
                ];
                break;
            default:
                throw ':('
        }
    }

    db.get('suppliers').find(query, { limit: 500, sort: { 'score.overallOpinionPercent': -1 } })
        .then(function (suppliers) {
            res.json(suppliers);
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
