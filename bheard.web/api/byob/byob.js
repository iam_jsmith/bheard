var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    router = express.Router(),
    config = require('bheard').config(),
    db = require('bheard').db(),
    byobSchema = require('./schemas/byob'),
    validate = require('../../middleware/validate'),
    async = require('async'),
    processes = require('./processes');

router.post('/api/byob', validate(byobSchema), function (req, res, next) {
    var yourBank = req.body.bankName;
    var items = req.body.items; // todo add to validation
    var topXMatches = 5; // how many top matches
    var userValueIndexes = [];

    // get submitted user values
    var userValues = items.map(function(item) {
        if (typeof item !== 'undefined' && item.picked) { return item.name; }
    });
    userValues.forEach(function(item, i) {
        if (typeof item !== 'undefined') { userValueIndexes.push(i); }
    });


    // Process Data
    async.waterfall([
        function(callback) {
            processes.getData(function(data){
                if (data) {
                    callback(null, data);
                } else {
                    next(err);
                }
            })
        }],
        function(err, data) {
            data = processes.calculateRanks(data);
            data = processes.calculateValueScores(data, userValueIndexes);
            data = processes.calculateValueRanks(data);

            // Response
            var responseData = {
                'matches': processes.getTopMatches(data, topXMatches),
                'yourBank': yourBank,
                'yourBankRank': processes.getYourBankRank(data, yourBank)
            };

            res.json(responseData);
        }
    );
});

module.exports = router;
