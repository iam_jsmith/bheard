var express = require('express'),
    _ = require('lodash'),
    moment = require('moment'),
    router = express.Router(),
    config = require('bheard').config(),
    db = require('bheard').db(),
    byobSchema = require('./schemas/byob'),
    validate = require('../../middleware/validate'),
    async = require('async');

// 1. Living Wage Employer
var livingWageEmployerMin = 1;
var livingWageEmployerMax = 8; // give 8 if false. MAY WANT TO CHANGE THIS NUMBER IF ADD MORE BANKS

module.exports = {

    /**
     * Get data from database
     * @param callback
     */
    getData: function (callback) {
        db.get('byob').find()
            .then(function (data) {
                callback(data);
            })
            .catch(function (err) {
                callback(null)
            });
    },

    /**
     * Calculate Ranks
     *
     * @param data
     * @returns {*}
     */
    calculateRanks: function (data) {
        // create ranks array
        var processedData = data;
        processedData.forEach(function(bank) {
            bank.ranks = [];
        });

        // calculate ranks table
        processedData = livingWageEmployer(processedData);
        processedData = highestPaidDirector(processedData);
        processedData = tierOneCapital(processedData);
        processedData = PPIProvisions(processedData);
        processedData = femaleBoardMembers(processedData);
        processedData = environmentalImpact(processedData);
        processedData = charitableGiving(processedData);
        processedData = regulatoryComplaints(processedData);
        processedData = speedAddressingComplaints(processedData);

        return processedData;
    },

    /**
     * Calculate Scores based on the user's selected values
     *
     * @param data
     * @param valueIndexes
     */
    calculateValueScores: function (data, valueIndexes) {
        data.forEach(function(bank) {
            var total = 0;
            valueIndexes.forEach(function(i) {
                total += bank.ranks[i];
            });

            bank.score = total;
        });

        return data;
    },

    /**
     * Calculate Rank
     *
     * @param data
     */
    calculateValueRanks: function (data) {
        var scoresArray = [];
        var rankArray = [];

        data.forEach(function(bank) {
            scoresArray.push(bank.score);
        });

        rankArray = calculateRank(scoresArray, true);

        data.forEach(function(bank, i) {
            bank.rank = rankArray[i];
        });

        return data;
    },

    /**
     * Get top matches
     *
     * @param data
     * @param topXMatches
     * @returns {Array}
     */
    getTopMatches: function (data, topXMatches) {
        var topMatches = [];
        for (var i = 0; i < topXMatches; i ++) {
            data.forEach(function(bank) {
                if (bank.rank == i+1) {
                    topMatches.push(bank.name);
                }
            });
        }
        return topMatches;
    },

    /**
     * Get user's bank's rank
     * @param data
     * @param yourBank
     */
    getYourBankRank: function (data, yourBank) {
        var yourRank = null;
        data.forEach(function(bank) {
            if (bank.name === yourBank) {
                yourRank = bank.rank;
            }
        });
        return yourRank;
    }
};

/**
 * Value One: Living Wage Employer
 * This one is calculated slightly differently
 *
 * @param data
 * @returns {*}
 */
function livingWageEmployer(data) {
    var valueIndex = 0;
    data.forEach(function(bank) {
        if (bank.values[valueIndex] === true) {
            bank.ranks[valueIndex] = livingWageEmployerMin;
        } else {
            bank.ranks[valueIndex] = livingWageEmployerMax;
        }
    });
    return data;
}

/**
 * Value Two: Highest Paid Director vs Average Staff Pay
 *
 * @param data
 * @returns {*}
 */
function highestPaidDirector(data) {
    var valueIndex = 1;
    var valueColumn = getValueColumn(data, valueIndex, false);
    var ranks = calculateRank(valueColumn, true);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Value Three: Tier 1 Capital
 *
 * @param data
 * @returns {*}
 */
function tierOneCapital(data) {
    var valueIndex = 2;
    var valueColumn = getValueColumn(data, valueIndex, true);
    var ranks = calculateRank(valueColumn, false);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Value Four: PPI Provisions Vs Customer Deposits
 *
 * @param data
 * @returns {*}
 */
function PPIProvisions(data) {
    var valueIndex = 3;
    var valueColumn = getValueColumn(data, valueIndex, false);
    var ranks = calculateRank(valueColumn, true);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}
/**
 * Value Five: Number of Female Board Members
 *
 * @param data
 * @returns {*}
 */
function femaleBoardMembers(data) {
    var valueIndex = 4;
    var valueColumn = getValueColumn(data, valueIndex, true);
    var ranks = calculateRank(valueColumn, false);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data; // todo value is different here, but looks like a problem with the excel
}

/**
 * Value Six: Environmental Impact (CO2 emissions)
 *
 * @param data
 * @returns {*}
 */
function environmentalImpact(data) {
    var valueIndex = 5;
    var valueColumn = getValueColumn(data, valueIndex, false);
    var ranks = calculateRank(valueColumn, true);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Value Seven: Charitable Giving
 *
 * @param data
 * @returns {*}
 */
function charitableGiving(data) {
    var valueIndex = 6;
    var valueColumn = getValueColumn(data, valueIndex, true);
    var ranks = calculateRank(valueColumn, false);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Value Eight: Regulatory Complaints
 *
 * @param data
 * @returns {*}
 */
function regulatoryComplaints(data) {
    var valueIndex = 7;
    var valueColumn = getValueColumn(data, valueIndex, false);
    var ranks = calculateRank(valueColumn, true);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Value Nine: Speed of Addressing Complaints (<8 weeks)
 *
 * @param data
 * @returns {*}
 */
function speedAddressingComplaints(data) {
    var valueIndex = 8;
    var valueColumn = getValueColumn(data, valueIndex, true);
    var ranks = calculateRank(valueColumn, false);

    data.forEach(function(bank, i) {
        bank.ranks[valueIndex] = ranks[i];
    });
    return data;
}

/**
 * Get value column
 *
 * @param data
 * @param valueIndex
 * @param highGood
 */
function getValueColumn(data, valueIndex, highGood) {
    var length = data.length;
    var column = data.map(function(bank) {
        if (bank.values[valueIndex] === null) {
            return highGood ? 0 : length; // todo high == good (return number of banks, or 0 if high is good)
        } else {
            return bank.values[valueIndex];
        }
    });
    return column;
}

/**
 * Calculate Rank
 * Based on this: http://stackoverflow.com/questions/14834571/ranking-array-elements
 * This is quicker than using indexOf
 *
 * @param valueColumn
 * @param ascending
 * @returns {*}
 */
function calculateRank(column, ascending) {
    //check which 'wins' when duplicate
    var rankIndex = column.slice()
        .sort(function (a,b) { return ascending ? a-b : b-a;})
        .reduceRight(function (acc, item, index) {
            acc[item] = index;
            return acc;
        }, Object.create(null));

    return column.map(function(item){ return rankIndex[item]+1; });
}

/**
 * Get rank column
 *
 * @param data
 * @param valueIndex
 */
function getRankColumn(data, valueIndex) {
    var column = data.map(function(bank) {
        return bank.ranks[valueIndex];
    });
    return column;
}

/**
 * Get Rank Table
 *
 * @param data
 */
function getRankTable(data) {
    var rankTable =[];

    return rankTable;
}