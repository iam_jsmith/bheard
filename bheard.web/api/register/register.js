var express = require('express'),
    router = express.Router(),
    _ = require('lodash'),
    moment = require('moment'),
    uuid = require('node-uuid'),
    config = require('bheard').config(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    mailChimp = require('bheard').mailChimp(),
    mailer = require('bheard').mailer(),
    registerSchema = require('./schemas/register'),
    validate = require('../../middleware/validate');

router.post('/api/register', validate(registerSchema), function (req, res, next) {
    // todo add a check here for user's unconfirmedEmail too, or if a user is changing their email, they will be wiped
    db.get('users').findOne({ email: req.body.unconfirmedEmail })
        .then(function (exists) {
            console.log('database responded');

            if (!exists) {
                var user = _.extend(_.omit(req.body, 'password', 'confirmPassword'), {
                    changeCoins: 0,
                    hash: encryptor.hash(req.body.unconfirmedEmail),
                    password: encryptor.encryptPassword(req.body.password),
                    role: null,
                    registerToken: uuid.v4(),
                    resetPasswordToken: null,
                    changeEmailAddressToken: null,
                    created: moment.utc().toDate(),
                    updated: null
                });

                console.log('created user object');

                return db.get('users').findAndModify({ unconfirmedEmail: user.unconfirmedEmail }, user, { new: true, upsert: true })
                    .then(function (doc) {
                        console.log('got user document');

                        req.app.emit('event:changecoins:register', { event: 'register', user: user = doc });
                    })
                    .then(function () {
                        console.log('sending mail');

                        return mailer.send({
                            from: 'B.heard <hello@bheard.com>',
                            to: user.unconfirmedEmail,
                            subject: 'B.heard',
                            text: 'Hello ' + user.firstName + '\n\n' +
                                  'Welcome to B.heard - the UK\'s innovative value comparison website. We provide you with information that you need to ensure that the services you depend upon, are dependable.' + '\n\n' +
                                  'Start your conversation with your service providers today by activating your account on the following link:\n\n' +
                                  config.get('/server/web/url') + '/register/' + user.registerToken + '\n\n' +
                                  'We\'re so excited that you\'ve decided to register with us. You\'ll now be able to use Changecoin&reg;, B.heard\'s way to bring about change in the services that we care for. Each Changecoin&reg; allows you to vote for the charities that are making a difference - as Bansky put it - keep your coins, we want change.' + '\n\n' +
                                  'By registering we\'re giving you 100 Changecoin&reg; to give to one of our suggested charities (https://bheard.com/changecoins) - if your preferred charity is not there, go ahead and nominate (https://bheard.com/nominate) your charity to us so that we can consider it in the near future.' + '\n\n' +
                                  'Your voice is important, let it B.heard.\n' +
                                  'Many thanks.\n\n' +
                                  'The B.heard team\n' +
                                  'https://bheard.com',
                            html: '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width"><meta charset="utf-8"><title>Welcome to B.heard</title><meta name="description" content=""><meta name="viewport" content="width=device-width,initial-scale=1"><style type="text/css">@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800);#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}table,td{mso-table-lspace:0;mso-table-rspace:0}img{-ms-interpolation-mode:bicubic;border:0}body{margin:0;padding:0}img{border:0;height:auto;line-height:100%;outline:0;text-decoration:none}table{border-collapse:collapse!important}#bodyCell,#bodyTable,body{height:100%!important;margin:0;padding:0;width:100%!important}table[class=responsive-table]{width:580px!important}table[class=master-table]{width:600px!important}table[class=copy-table]{width:75%!important}table[class=story]{width:78%!important}.appleLinks a{color:#c2c2c2!important;text-decoration:none}.button{color:#fff!important;text-decoration:none!important}span.preheader{display:none!important}@media only screen and (max-width:599px){table[class=responsive-table]{width:95%!important}table[class=master-table]{width:100%!important}td[class=responsive-header-cell-big]{display:block;text-align:left;width:240px;margin-left:15px;font-size:22px!important}td[class=responsive-header-cell]{display:block;text-align:left;width:240px;margin-left:15px}table[class=copy-table],table[class=story]{width:85%!important}[class].full-card-image{width:95%!important}[class].hero-image{width:100%!important}}@media only screen and (max-width:480px){a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}body{width:100%!important;min-width:100%!important}[class].social-cell{width:100%!important}[class].social-spacer{height:10px!important}}</style></head><body><center><table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#eaeced"><tr height="8" style="font-size:0;line-height:0"><td><tr><td align="center" valign="top"><table class="master-table" width="600"><tr height="10"><td><tr><td align="center" valign="top"><table class="responsive-table" width="580" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" valign="top" style="overflow:hidden!important;border-radius:10px"><tr height="30"><td><tr><td align="center" width="70"><a href="https://bheard.com"></a><img style="display:block;margin:0 auto 20px;height:auto" src="https://www.bheard.com/build/images/logo-black-script.png" width="70" alt="B.heard"/><tr><td align="center"><table width="85%"><tr><td align="center"><h2 style="margin:0!important;font-family:"Open Sans",arial,sans-serif!important;font-size:28px!important;line-height:30px!important;font-weight:200!important;color:#252b33!important">Hello ' + user.firstName + '</h2></table><tr height="25"><td><tr><td align="center"><table class="story" border="0" cellpadding="0" cellspacing="0" width="78%"><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important">Welcome to B.heard - the UK\'s innovative value comparison website. We provide you with information that you need to ensure that the services you depend upon, are dependable.<br/><br/>Start your conversation with your service providers today by activating your account on the following link:</table><tr height="25"><td><tr><td align="center" valign="top"><table border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top"><a href="' + config.get('/server/web/url') + '/register/' + user.registerToken + '" style="background-color:#e61b72;padding:14px 28px 14px 28px;-webkit-border-radius:3px;border-radius:3px;line-height:18px!important;letter-spacing:.125em;text-transform:uppercase;font-size:13px;font-family:"Open Sans",Arial,sans-serif;font-weight:400;color:#fff!important;text-decoration:none!important;display:inline-block;line-height:18px!important;-webkit-text-size-adjust:none;mso-hide:all" class="button">Confirm Email Address</a></table><tr height="25"><td><tr><td align="center"><table class="story" border="0" cellpadding="0" cellspacing="0" width="78%"><tr><td align="center" style="font-family:"Open Sans",arial,sans-serif!important;font-size:16px!important;line-height:22px!important;font-weight:400!important;color:#7e8890!important">We\'re so excited that you\'ve decided to register with us. You\'ll now be able to use Changecoin&reg;, B.heard\'s way to bring about change in the services that we care for. Each Changecoin&reg; allows you to vote for the charities that are making a difference - as Bansky put it - keep your coins, we want change.<br/><br/>By registering we\'re giving you 100 Changecoin&reg; to give to one of our <a style="color:#e61b72;text-decoration:none" href="https://bheard.com/changecoins">suggested charities</a> - if your preferred charity is not there, go ahead and <a style="color:#e61b72;text-decoration:none" href="https://bheard.com/nominate">nominate your charity</a> to us so that we can consider it in the near future.<br/><br/><strong>Many thanks</strong><br/>The B.heard team</table><tr height="30"><td></table><tr height="20"><td><tr><td align="center"><table class="responsive-table" width="580" border="0" cellpadding="0" cellspacing="0" valign="top"><tr><td align="center" valign="top"><a href="https://bheard.com"></a><img src="https://www.bheard.com/build/images/logo-black-script.png" width="45" alt="B.heard" style="border:0"><tr height="10"><td><tr><td align="center" valign="top" style="font-family:"Open Sans",sans-serif!important;font-weight:400!important;color:#7e8890!important;font-size:12px!important;text-transform:uppercase!important;letter-spacing:.045em!important">Your voice is important, let it B.heard.<tr height="10" style="padding:0;margin:0;font-size:0;line-height:0"><td><tr><td align="center" style="font-family:Open Sans,sans-serif!important;font-size:12px!important;color:#acacac!important">B.heard Limited is a registered limited company of England and Wales (9082696) located at 21 - 27 Lambs Conduit Street, London, WC1N 3GS<tr height="20" style="padding:0;margin:0;font-size:0;line-height:0"><td></table></table></table></center></body></html>'
                        });
                    })
                    .then(function () {
                        console.log('creating auth token');

                        res.json({ token: encryptor.encrypt(user._id.toString()) });
                    });
            } else {
                console.log('user already exists');

                res.status(400).json({
                    message: 'Register failed.',
                    errors: { unconfirmedEmail: ['The email address is currently in use.'] }
                });
            }
        })
        .catch(function (err) {
            console.log('error', err);
            next(err);
        });
});

router.post('/api/register/:uuid', function (req, res, next) {
    db.get('users').findOne({ registerToken: req.params.uuid })
        .then(function (exists) {
            if (exists) {
                var user = {
                    email: exists.unconfirmedEmail,
                    unconfirmedEmail: null,
                    registerToken: null,
                    updated: moment.utc().toDate()
                };

                return db.get('users').findAndModify({ _id: exists._id }, { $set: user }, { new: true })
                    .then(function (user) {
                        return mailChimp.subscribe(user);
                    })
                    .then(function () {
                        res.send(200);
                    });
            } else {
                res.send(404);
            }
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
