var Joi = require('joi');

module.exports = {
    body: {
        unconfirmedEmail: Joi.string().email().required().trim().max(254).lowercase(),
        password: Joi.string().required().max(140),
        confirmPassword: Joi.any().valid(Joi.ref('password')),
        firstName: Joi.string().required().trim().max(140),
        lastName: Joi.string().required().trim().max(140),
        postcode: Joi.string().trim().min(5).max(8).uppercase().regex(/^[0-9A-Z]*\s?[0-9A-Z]*$/i, 'Postcode').allow(null, ''),
        hasAgreedToTermsAndConditions: Joi.boolean().required().valid(true)
    },
    options: {
        allowUnknown: false
    }
};
