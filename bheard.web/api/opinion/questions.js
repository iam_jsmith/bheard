var questions = require("./questions.json");
var express = require('express'),
    router = express.Router();

router.get('/api/opinion/questions', function (req, res, next) {
    res.json(questions);
});

module.exports = router;