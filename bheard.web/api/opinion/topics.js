var express = require('express'),
    _ = require('lodash'),
    router = express.Router();
    
var topics = [
            {
                id: 'fatherhood',
                tile: {
                    title: 'How valued do you think the role of fathers is by society today?',
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/fatherhood.jpg'
                },
                main: {
                    header : {
                        title: 'Fatherhood',
                        subtitle: 'The great UK fatherhood discussion',
                        banner: 'fatherhood',
                        videoUrl: 'http://bheard-videos.s3.amazonaws.com/video-reviews/BARCLAYS%2002.mp4'
                    },
                    content: {
                        surveyTemplate: 'modules/opinion/views/partials/fatherhood-survey.html',
                        videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov',
                        vpmId: 'd342530fde8d84cd3586df6841fa89e1',
                        qualtricsUrl: 'https://surveys.eu.qualtrics.com/SE/?SID=SV_9vgCARewdKWYGTX'

                    }
                },
                link: '#'
            },
            {
                id: 'greatunheard',
                tile: {
                    title: 'Do customers believe companies are actually listening?',
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/unheard.jpg'
                },
                main: {
                    header : {
                        title: 'The Great Unheard',
                        subtitle: 'Do customers believe companies are actually listening?',
                        banner: 'unheard',
                        videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov'
                    },
                    content: {
                        surveyTemplate: 'modules/opinion/views/partials/unheard-survey.html',
                        videoUrl:  'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov',
                        vpmId: 'a43f19fe9873fbe52f3c1c95901b4b49',
                        qualtricsUrl: 'https://surveys.eu.qualtrics.com/SE/?SID=SV_2fZ07RInWiIpYMJ'

                    }
                },
                link: '#'
            },
            {
                id: 'dependabilityindex',
                tile: {
                    title: 'How dependable is your county council?',
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/councils.jpg'
                },
                main: {
                    header: {
                        title: 'Dependability index',
                        subtitle: 'How dependable is your county council?',
                        banner: 'dependability',
                        videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov'                 
                    },
                    content: {
                        surveyTemplate: 'modules/opinion/views/partials/dependability-survey.html',
                        vpmId: '33d375963951a5cca25b3035f91f8eb7',
                        qualtricsUrl: 'https://surveys.eu.qualtrics.com/SE/?SID=SV_5bwEuZYWURDBGSh'
                    }
                },
                link: '#'
            },
            {
                id: 'trustbank',
                tile: {
                    title: 'Do you trust banks?',
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/banking.jpg'
                },
                main: {
                    title: 'Bank trust',
                    videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov'
                },
                link: '#'
            },
            {
                id: 'taxavoidance',
                tile: {
                    title: 'How is it legal for companies to avoid paying so much tax?',
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/tax.jpg'
                },
                main: {
                    title: 'Company tax avoidance',
                    videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov'
                },
                link: '#'
            },
            {
                id: 'brexit',
                tile: {
                    title: "What does Brexit mean for Britian's most disadvantaged families?",
                    action: 'Get involved in the discussion here',
                    imageUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/tiles/referendum.jpg'
                },
                main: {
                    title: 'Brexit and disadvantaged families',
                    videoUrl: 'https://s3-eu-west-1.amazonaws.com/bheard-videos/youropinion/header-videos/fatherhood-video.mov'
                },
                link: '#'
            }
        ];

router.get('/api/opinion/topics', function (req, res, next) {
    res.json(topics);
});

router.get('/api/opinion/topics/:id', function (req, res, next) {
    res.json(_.find(topics, function(topic) {
        return topic.id === req.params.id;
    }));
});

module.exports = router;
