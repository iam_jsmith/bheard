var express = require('express'),
    _ = require('lodash'),
    router = express.Router();
    
var opinions = [
    {
        id: '6746-402557',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402557.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402557.jpg'
    },
    {
        id: '6746-402560',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402560.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402560.jpg'
    },
    // {
    //     id: '6746-402561',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402561.mp4'
    // },
    {
        id: '6746-402564',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402564.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402564.jpg'
    },
    {
        id: '6746-402565',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402565.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402565.jpg'
    },
    {
        id: '6746-402566',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402566.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402566.jpg'
    },
    {
        id: '6746-402567',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402567.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402567.jpg'
    },
    // {
    //     id: '6746-402568',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402568.mp4'
    // },
    // {
    //     id: '6746-402579',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402579.mp4'
    // },
    // {
    //     id: '6746-402629',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402629.mp4'
    // },
    // {
    //     id: '6746-402630',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402630.mp4'
    // },
    {
        id: '6746-402634',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402634.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402634.jpg'
    },
    // {
    //     id: '6746-402635',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402635.mp4'
    // },
    // {
    //     id: '6746-402636',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402636.mp4'
    // },
    // {
    //     id: '6746-402642',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402642.mp4'
    // },
    {
        id: '6746-402653',
        topic: 'fatherhood',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402653.mp4',
        posterUrl: 'http://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/poster-images/6746-402653.jpg'
    },
    // {
    //     id: '6746-402657',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402657.mp4'
    // },
    // {
    //     id: '6746-402659',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402659.mp4'
    // },
    // {
    //     id: '6746-402668',
    //     topic: 'fatherhood',
    //     videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/fatherhood/6746-402668.mp4'
    // }
    {
        id: '6746-402586',
        topic: 'dependabilityindex',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/dependabilityindex/6746-402586.mp4',
    },
    {
        id: '6746-402587',
        topic: 'dependabilityindex',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/dependabilityindex/6746-402587.mp4',
    },
    {
        id: '6746-402590',
        topic: 'dependabilityindex',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/dependabilityindex/6746-402590.mp4',
    },
    {
        id: '6746-402600',
        topic: 'dependabilityindex',
        videoUrl: 'https://bheard-videos.s3.amazonaws.com/youropinion/dependabilityindex/6746-402600.mp4',
    }
];

router.get('/api/opinion/opinions', function (req, res, next) {
    res.json(opinions);
});

router.get('/api/opinion/opinions/:topicId', function (req, res, next) {
    
    var relevantOpinions = _.filter(opinions, function(opinion) {
        return opinion.topic === req.params.topicId;
    });
    if (req.query.limit) {
        var limit = parseInt(req.query.limit);
        var results = relevantOpinions.slice(0, limit); // truncate the array to the limit (only use the first x)
    }

    res.json(results);
});

module.exports = router;
