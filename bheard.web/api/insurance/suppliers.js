var express = require('express'),
    _ = require('lodash'),
    router = express.Router(),
    db = require('bheard').db();

router.get('/api/insurancesuppliers', function (req, res, next) {
    var query = { sector: 'insurance' };

    var insuranceTypes = [
        'car',
        'van',
        'home',
        'bike',
        'travel',
        'breakdown',
        'pet',
        'life',
        'health',
        'youngdriver'
    ];

    //
    if (req.query.type && insuranceTypes.indexOf(req.query.type > -1)) {
        query.$or = [
            { brands: { $elemMatch: { subSector: req.query.type }} }
        ];
    } else {
        throw ':(';
    }

    db.get('suppliers').find(query, { limit: 500, sort: { 'score.overallOpinionPercent': -1 } })
        .then(function (suppliers) {
            res.json(suppliers);
        })
        .catch(function (err) {
            next(err);
        });
});

module.exports = router;
