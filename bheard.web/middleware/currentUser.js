var db = require('bheard').db(),
    encryptor = require('bheard').encryptor();

module.exports = function (req, res, next) {
    var token = req.cookies.bheard;

    if (token) {
        db.get('users').findById(encryptor.decrypt(token), function (err, user) {
            if (!err) {
                req.user = user;
                next();
            } else {
                next(err);
            }
        });
    } else {
        next();
    }
};
