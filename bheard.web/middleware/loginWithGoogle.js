var _ = require('lodash'),
    moment = require('moment'),
    q = require("q"),
    config = require('bheard').config(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    http = require('bheard').http(),
    mailChimp = require('bheard').mailChimp();

module.exports = function () {
    return function (req, res, next) {
        if (!req.query.code && !req.query.error) {
            res.redirect('https://accounts.google.com/o/oauth2/v2/auth' +
                '?client_id=' + config.get('/google/clientId') +
                '&display=popup' +
                '&scope=openid email profile' +
                '&redirect_uri=' + config.get('/server/web/url') + req.path +
                '&response_type=code');
        } else if (req.query.code) {
            var now = moment.utc().toDate(),
                token = null,
                user = null;

            function getGoogleAccessToken() {
                return http.request({
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    },
                    host: 'www.googleapis.com',
                    method: 'POST',
                    path: '/oauth2/v4/token',
                    body: 'client_id=' + config.get('/google/clientId') +
                        '&client_secret=' + config.get('/google/clientSecret') +
                        '&code=' + req.query.code +
                        '&redirect_uri=' + config.get('/server/web/url') + req.path +
                        '&grant_type=authorization_code'
                }).then(function (data) {
                    return token = data.access_token;
                });
            }

            function getGoogleUserData(token) {
                return http.request({
                    headers: {
                        'Authorization': 'Bearer ' + token
                    },
                    host: 'www.googleapis.com',
                    method: 'GET',
                    path: '/oauth2/v3/userinfo'
                });
            }

            function register(userData) {
                var query = { $or: [{ googleId: userData.sub }] };

                if (userData.email) {
                    query.$or.push({ email: userData.email }, { email: { $exists: false }, unconfirmedEmail: userData.email });
                }

                return db.get('users').find(query).then(function (users) {
                    if (!users.length) {
                        user = {
                            firstName: userData.given_name,
                            lastName: userData.family_name,
                            unconfirmedEmail: null,
                            postcode: null,
                            changeCoins: 0,
                            hash: null,
                            password: null,
                            role: null,
                            registerToken: null,
                            resetPasswordToken: null,
                            changeEmailAddressToken: null,
                            googleId: userData.sub,
                            googleToken: token,
                            googlePictureUrl: userData.picture,
                            isSocial: true,
                            created: now,
                            updated: now
                        };

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').insert(user)
                            .then(function (doc) {
                                req.app.emit('event:changecoins:register', { event: 'register', user: user = doc });
                            })
                            .then(function () {
                                if (user.email) {
                                    return mailChimp.subscribe(user);
                                }
                            });
                    } else if (users.length === 1) {
                        user = users[0];
                        user.firstName = userData.given_name;
                        user.lastName = userData.family_name;
                        user.hash = null;
                        user.registerToken = null;
                        user.googleId = userData.sub;
                        user.googleToken = token;
                        user.googlePictureUrl = userData.picture;
                        user.isSocial = true;
                        user.updated = now;

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').findAndModify({ _id: user._id }, { $set: user }, { new: true });
                    } else {
                        return q.reject(new Error('Duplicate user found during sync with Google.'));
                    }
                });
            }

            function login() {
                res.cookie('bheard', encryptor.encrypt(user._id.toString()));
            }

            getGoogleAccessToken()
                .then(getGoogleUserData)
                .then(register)
                .then(login)
                .then(next)
                .catch(function (err) {
                    next(err);
                });
        } else if (req.query.error) {
            next();
        }
    };
};
 