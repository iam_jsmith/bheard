var Joi = require('joi'),
    _ = require('lodash');

module.exports = function (schema) {
    return function (req, res, next) {
        var errors = {},
            allowUnknown = schema.options != null && schema.options.allowUnknown != null
                ? schema.options.allowUnknown
                : true;

        Joi.validate(req.body, schema.body, { allowUnknown: allowUnknown, abortEarly: false }, function (err, value) {
            if (!err) {
                req.body = value;
            } else {
                _.forEach(err.details, function (detail) {
                    var messages = errors[detail.path];

                    if (!messages) {
                        errors[detail.path] = messages = [];
                    }

                    messages.push(detail.message);
                });
            }
        });

        if (_.isEmpty(errors)) {
            next();
        } else {
            res.status(400).json({
                message: 'The request is invalid.',
                errors: errors
            });
        }
    };
};
