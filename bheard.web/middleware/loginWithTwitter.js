var _ = require('lodash'),
    moment = require('moment'),
    oauth = require('oauth'),
    q = require("q"),
    config = require('bheard').config(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    http = require('bheard').http(),
    mailChimp = require('bheard').mailChimp();

module.exports = function () {
    return function (req, res, next) {
        var oa = new oauth.OAuth(
            'https://twitter.com/oauth/request_token',
            'https://twitter.com/oauth/access_token',
            config.get('/twitter/apiKey'),
            config.get('/twitter/apiSecret'),
            '1.0A',
            config.get('/server/web/url') + req.path,
            'HMAC-SHA1');

        if (!req.query.oauth_token && !req.query.oauth_verifier && !req.query.error) {
            oa.getOAuthRequestToken(function (err, oauthToken) {
                if (!err) {
                    res.redirect('https://twitter.com/oauth/authenticate?oauth_token=' + oauthToken);
                } else {
                    next(err);
                }
            });
        } else if (req.query.oauth_token && req.query.oauth_verifier) {
            var now = moment.utc().toDate(),
                token = null,
                user = null;

            function getTwitterAccessToken() {
                var deferred = q.defer();

                oa.getOAuthAccessToken(req.query.oauth_token, null, req.query.oauth_verifier, function (err, oauthAccessToken, oauthAccessTokenSecret) {
                    if (!err) {
                        deferred.resolve(token = { oauthAccessToken: oauthAccessToken, oauthAccessTokenSecret: oauthAccessTokenSecret });
                    } else {
                        deferred.reject(err);
                    }
                });

                return deferred.promise;
            }

            function getTwitterUserData(token) {
                var deferred = q.defer();

                oa.get('https://api.twitter.com/1.1/account/verify_credentials.json?include_entities=false&skip_status=true&include_email=true', token.oauthAccessToken, token.oauthAccessTokenSecret, function(err, str) {
                    if (!err) {
                        deferred.resolve(JSON.parse(str));
                    } else {
                        deferred.reject(err);
                    }
                });

                return deferred.promise;
            }

            function register(userData) {
                var query = { $or: [{ twitterId: userData.id_str }] };

                if (userData.email) {
                    query.$or.push({ email: userData.email }, { email: { $exists: false }, unconfirmedEmail: userData.email });
                }

                return db.get('users').find(query).then(function (users) {
                    var names = userData.name.split(' '),
                        firstName = names[0],
                        lastName = names.slice(1).join(' ');

                    if (!users.length) {
                        user = {
                            firstName: firstName,
                            lastName: lastName,
                            unconfirmedEmail: null,
                            postcode: null,
                            changeCoins: 0,
                            hash: null,
                            password: null,
                            role: null,
                            registerToken: null,
                            resetPasswordToken: null,
                            changeEmailAddressToken: null,
                            twitterId: userData.id_str,
                            twitterToken: token.oauthAccessToken,
                            twitterTokenSecret: token.oauthAccessTokenSecret,
                            twitterPictureUrl: userData.profile_image_url_https,
                            isSocial: true,
                            created: now,
                            updated: now
                        };

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').insert(user)
                            .then(function (doc) {
                                req.app.emit('event:changecoins:register', { event: 'register', user: user = doc });
                            })
                            .then(function () {
                                if (user.email) {
                                    return mailChimp.subscribe(user);
                                }
                            });
                    } else if (users.length === 1) {
                        user = users[0];
                        user.firstName = firstName;
                        user.lastName = lastName;
                        user.hash = null;
                        user.registerToken = null;
                        user.twitterId = userData.id_str;
                        user.twitterToken = token.oauthAccessToken;
                        user.twitterTokenSecret = token.oauthAccessTokenSecret;
                        user.twitterPictureUrl = userData.profile_image_url_https;
                        user.isSocial = true;
                        user.updated = now;

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').findAndModify({ _id: user._id }, { $set: user }, { new: true });
                    } else {
                        return q.reject(new Error('Duplicate user found during sync with Twitter.'));
                    }
                });
            }

            function login() {
                res.cookie('bheard', encryptor.encrypt(user._id.toString()));
            }

            getTwitterAccessToken()
                .then(getTwitterUserData)
                .then(register)
                .then(login)
                .then(next)
                .catch(function (err) {
                    next(err);
                });
        } else if (req.query.error) {
            next();
        }
    };
};
