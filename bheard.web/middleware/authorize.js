var db = require('bheard').db();

module.exports = function (role) {
    return function (req, res, next) {
        if (req.user && (!role || req.user.role === role)) {
            next();
        } else {
            res.status(401).json({ message: 'Authorization failed.' });
        }
    };
};
