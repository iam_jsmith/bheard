module.exports = function (req, res, next) {
    if (req.xhr) {
        res.status(404).json({ message: 'Sorry, the page you requested cannot be found.' })
    } else {
        res.status(404).render('shared/404', { title: 'Oops!', message: 'Sorry, the page you requested cannot be found.' })
    }
};
