var _ = require('lodash'),
    bots = [
        /*'googlebot',
        'yahoo',
        'bingbot',*/
        'baiduspider',
        'facebookexternalhit',
        'twitterbot',
        'rogerbot',
        'linkedinbot',
        'embedly',
        'quora link preview',
        'showyoubot',
        'outbrain',
        'pinterest',
        'developers.google.com/+/web/snippet',
        'slackbot',
        'vkShare',
        'w3c_calidator',
        'redditbot',
        'applebot'
    ];

module.exports = function (req, res, next) {
    var isGet = req.method === 'GET',
        isFile = req.path.indexOf('.') !== -1,
        userAgent = req.headers['user-agent'],
        bufferAgent = req.headers['x-bufferbot'],
        escapedFragment = req.query._escaped_fragment_,
        isBot = isGet && !isFile && userAgent && (bufferAgent || escapedFragment != null || _.any(bots, function (bot) { return userAgent.indexOf(bot.toLowerCase()) !== -1 }));

    if (isBot) {
        var render = res.render;

        res.render = function (view, data, callback) {
            render.call(this, view + '-seo', _.extend({}, data, {layout: 'shared/layout-seo'}), callback);
        };
    }

    next();
};
