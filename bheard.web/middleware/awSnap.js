var mailer = require('bheard').mailer();

module.exports = function (err, req, res, next) {
    var status = err.status || 500;

    console.error(err);

    if (process.env.NODE_ENV === 'production') {
        mailer.send({
            from: 'B.heard',
            to: 'errors@bheard.com',
            subject: 'A wobbly was just thrown on B.heard...',
            text: err.message + '\n\n' + err.stack
        });
    }

    if (req.xhr) {
        res.status(status).json(process.env.NODE_ENV !== 'production'
            ? { message: err.message, errors: err.errors, stack: err.stack }
            : { message: 'Sorry, an error occurred while processing your request.' });
    } else {
        res.status(status).render('shared/error', process.env.NODE_ENV !== 'production'
            ? { title: 'Oops!', message: err.message, errors: err.errors, stack: err.stack }
            : { title: 'Oops!', message: 'Sorry, an error occurred while processing your request.' });
    }
};
