var _ = require('lodash'),
    moment = require('moment'),
    q = require("q"),
    config = require('bheard').config(),
    db = require('bheard').db(),
    encryptor = require('bheard').encryptor(),
    http = require('bheard').http(),
    mailChimp = require('bheard').mailChimp();

module.exports = function () {
    return function (req, res, next) {
        if (!req.query.code && !req.query.error) {
            res.redirect('https://www.facebook.com/dialog/oauth' +
                '?client_id=' + config.get('/facebook/appId') +
                '&display=popup' +
                '&scope=email,public_profile' +
                '&redirect_uri=' + config.get('/server/web/url') + req.path);
        } else if (req.query.code) {
            var now = moment.utc().toDate(),
                token = null,
                user = null;

            function getFacebookAccessToken() {
                return http.request({
                    host: 'graph.facebook.com',
                    method: 'GET',
                    path: '/oauth/access_token' +
                        '?client_id=' + config.get('/facebook/appId') +
                        '&client_secret=' + config.get('/facebook/appSecret') +
                        '&code=' + req.query.code +
                        '&redirect_uri=' + config.get('/server/web/url') + req.path
                }).then(function (data) {
                    return token = data.access_token;
                });
            }

            function getFacebookUserData(token) {
                return http.request({
                    host: 'graph.facebook.com',
                    method: 'GET',
                    path: '/me' +
                        '?access_token=' + token +
                        '&fields=id,email,first_name,last_name,picture'
                });
            }

            function register(userData) {
                var query = { $or: [{ facebookId: userData.id }] };

                if (userData.email) {
                    query.$or.push({ email: userData.email }, { email: { $exists: false }, unconfirmedEmail: userData.email });
                }

                return db.get('users').find(query).then(function (users) {
                    if (!users.length) {
                        user = {
                            firstName: userData.first_name,
                            lastName: userData.last_name,
                            unconfirmedEmail: null,
                            postcode: null,
                            changeCoins: 0,
                            hash: null,
                            password: null,
                            role: null,
                            registerToken: null,
                            resetPasswordToken: null,
                            changeEmailAddressToken: null,
                            facebookId: userData.id,
                            facebookToken: token,
                            facebookPictureUrl: userData.picture.data.url,
                            isSocial: true,
                            created: now,
                            updated: now
                        };

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').insert(user)
                            .then(function (doc) {
                                req.app.emit('event:changecoins:register', { event: 'register', user: user = doc });
                            })
                            .then(function () {
                                if (user.email) {
                                    return mailChimp.subscribe(user);
                                }
                            });
                    } else if (users.length === 1) {
                        user = users[0];
                        user.firstName = userData.first_name;
                        user.lastName = userData.last_name;
                        user.hash = null;
                        user.registerToken = null;
                        user.facebookId = userData.id;
                        user.facebookToken = token;
                        user.facebookPictureUrl = userData.picture.data.url;
                        user.isSocial = true;
                        user.updated = now;

                        if (userData.email) {
                            user.email = userData.email.toLowerCase();
                            user.hash = encryptor.hash(user.email);
                        }

                        return db.get('users').findAndModify({ _id: user._id }, { $set: user }, { new: true });
                    } else {
                        return q.reject(new Error('Duplicate user found during sync with Facebook.'));
                    }
                });
            }

            function login() {
                res.cookie('bheard', encryptor.encrypt(user._id.toString()));
            }

            getFacebookAccessToken()
                .then(getFacebookUserData)
                .then(register)
                .then(login)
                .then(next)
                .catch(function (err) {
                    next(err);
                });
        } else if (req.query.error) {
            next();
        }
    };
};
