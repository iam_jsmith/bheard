var changeCoinsAwarder = function changeCoinsAwarder(coins, ledger) {
    return function (data) {
        // console.log('Responding to event', data);
        ledger.addCoins(data.user, data.event, coins,
        function (err, total) {
            if (err) {
                console.log(err);
            }

            console.log('New total', total);
        });
    };
};

module.exports = function (app, ledger, rewards) {
    for (var i = rewards.length - 1; i >= 0; i--) {
        console.log('Registering handler', rewards[i]);
        app.on(rewards[i].event, changeCoinsAwarder(rewards[i].coins, ledger));
    }
};
