﻿//var HtmlReporter = require('protractor-html-screenshot-reporter');
exports.config = {
    baseUrl: 'http://localhost:5555',
    capabilities: {
        'browserName': 'chrome',
        'chromeOptions': {
            args: ['--test-type']
        }
    },
    framework: 'cucumber',
    rootElement: 'body',
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['./features/stepped_search.feature'/*'./features/!**!/!*.feature'*/]

    //onPrepare: function() {
    //    // Add a screenshot reporter and store screenshots to `/tmp/screnshots`:
    //    jasmine.getEnv().addReporter(new HtmlReporter({
    //        baseDirectory: '/downloads/screenshots/'
    //    }));
    //}
};