/**
 * Created by Bali on 28/08/2015.
 */
//var chai = require('chai');
var support = require('../support');
var should = require('chai').should();
//var chaiAsPromised = require('chai-as-promised').chaiAsPromised();
//chai.use(chaiAsPromised);
//var expect = chai.expect;
//var expect = require('chai').expect();
//var exist = require('chai').exist();
//var _ = require('lodash');
//var db = require('bheard').db();


var register_steps = function() {

//clear users

    //this.Given(/^User does not exist$/, function (callback) {
    // db.users.find({});
    ////db.collection('users').remove();
    //
    //       setTimeout(callback, 1000);
    //
    //});
        //support.db('users', {}, function (er, results) {
        //    users.remove();
        //        db.close();
        //    setTimeout(callback, 5000);
        //    });


//Register
//  Scenario: Ensure the Register page is accessible

        this.When(/^Resister is selected$/, function (callback) {
            support.findById('top-right-login-option', function (result) {
                result.click();
            });
            support.findById('register-now-link', function (result) {
                result.click();
                setTimeout(callback, 1000);
            });
        });

        this.Then(/^I should see the Registration page$/, function (callback) {
            support.get('http://localhost:5555/#!/register', function (result) {
                setTimeout(callback, 2000);
            });
        });

// Scenario: Ensure I can complete the Registration process successfully
        this.Given(/^valid registration details are entered$/, function (callback) {
            support.findById('reg-first-name', function (result) {
                result.click();
                result.sendKeys('Jane');
            });
            support.findById('reg-last-name', function (result) {
                result.click();
                result.sendKeys('Doe');
            });
            support.findById('reg-post-code', function (result) {
                result.click();
                result.sendKeys('MK41 1TH');
            });
            support.findById('reg-email', function (result) {
                result.click();
                result.sendKeys('oscar@bheard.com');
            });
            support.findById('reg-password', function (result) {
                result.click();
                result.sendKeys('password1');
            });
            support.findById('reg-conf-password', function (result) {
                result.click();
                result.sendKeys('password1');
            });
            setTimeout(callback, 1000);
        });

    this.Given(/^I submit the Registration form$/, function (callback) {
        support.findById('reg-submit', function (result) {
            result.click();
            setTimeout(callback, 1000);
        });
    });
    this.Given(/^I should see the email confirmation message$/, function (callback) {
         support.findById('reg-conf-message', function (result) {
            expect(result.getText()).to.eventually.contain("We've sent you an email with instructions on how to activate your account.");
        });
        setTimeout(callback, 1000);
    });


    this.When(/^I complete step 2 of Registration$/, function (callback) {
     db.collection('users').get('registerToken');
           support.post('http://localhost:5555/#!/register/' + 'registerToken', function (result) {

            });
        setTimeout(callback, 1000);

    });

    this.Then(/^I should see the Registration confirmation message$/, function (callback) {
        support.findById('reg-complete', function (result) {
            expect(result.getText()).to.eventually.contain("Thank you for registering");
            setTimeout(callback, 1000);
        });
    });

    };

module.exports = register_steps;
