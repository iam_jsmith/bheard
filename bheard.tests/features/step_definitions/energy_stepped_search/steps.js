var support = require('../support');
//var chai = require('chai');
var should = require('chai').should();
//var expect = chai.expect;
//var expect = require('chai').expect();
//var exist = require('chai').exist();
//var db = require('bheard').db();

//new HtmlReporter({
//    baseDirectory: '/downloads/screenshots/'
//    , takeScreenShotsOnlyForFailedSpecs: true
//    , docTitle: 'stepped search results'
//    , preserveDirectory: true
//    , metaDataBuilder: function metaDataBuilder(spec, descriptions, results, capabilities) {
//        // Return the description of the spec and if it has passed or not:
//        return {
//            description: descriptions.join(' ')
//            , passed: results.passed()
//
//        };
//    }
//});
var steps = function() {


//Scenario: Energy Homepage is displayed when energy is selected
    this.Given(/^I am on the Homepage$/, function (callback) {
        support.get('http://localhost:5555', function (result) {
            setTimeout(callback, 2000);
        });
    });


    this.Given(/^I select Energy$/, function (callback) {
        support.findById('homePage-Energy', function (result) {
            result.click();
            setTimeout(callback, 2000);
        });
    });

    this.Given(/^I should see the Energy Homepage$/, function (callback) {
        support.get('http://localhost:5555/#!/energy', function (result) {
            setTimeout(callback, 2000);
        });
    });

    this.When(/^Get Started is selected$/, function (callback) {
        support.findById('energyPage-GetStarted', function (result) {
            result.click();
            setTimeout(callback, 2000);
        });
    });

    this.Then(/^I should see the Energy Comparison page$/, function (callback) {
        support.get('http://localhost:5555/#!/energy/tariffs', function (result) {
            setTimeout(callback, 2000);
        });
    });



//Scenario: Selecting Dual Supplier Type displays all Dual Suppliers

    this.When(/^Dual Fuel Supplier type is selected$/, function (callback) {
        support.findById('dual-fuel', function (result) {
            result.click();
            setTimeout(callback, 4000);
        });
    });


    this.Then(/^I should see all Dual Fuel Suppliers$/, function (callback) {
        support.findById('energy-suppliers-55faa64d3f97f8c82e4bbcb9',function (result) {
            setTimeout(callback, 4000);
        });
    });


//Scenario Selecting Gas Supplier Type displays all Dual Suppliers

    this.When(/^Gas Supplier type is selected$/, function (callback) {
        support.findById('gas-fuel', function(result){
            result.click();
            setTimeout(callback,4000);
        });
    });

    this.Then(/^I should see all Gas Suppliers$/, function (callback) {
        support.findById('energy-suppliers-55faa64d3f97f8c82e4bbcb9',function (result) {
            setTimeout(callback, 4000);
        });
    });


//Scenario Selecting Electricity Supplier Type displays all Dual Suppliers
    this.When(/^Electricity Supplier type is selected$/, function (callback) {
        support.findById('electricity-fuel', function(result){
            result.click();
            setTimeout(callback,4000);
        });
    });

    this.Then(/^I should see all Electricity Suppliers$/, function (callback) {
        support.findById('energy-suppliers-55faa64d3f97f8c82e4bbcb9',function (result) {
            setTimeout(callback, 4000);
        });
    });

//Scenario:I can successfully carry out a search
//
//    this.Given(/^Monthly DD is selected$/, function (callback) {
//        support.findById('energylist-directdebit', function(result){
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//
//    this.Given(/^step 2 Next is selected$/, function (callback) {
//        support.findById('energy-step2-next', function(result){
//            result.click();
//            setTimeout(callback,4000);
//        });
//    });
//
//
//    this.Given(/^a Post Code is entered$/, function (callback) {
//
//        support.findById('postcode', function (result) {
//            result.click();
//            result.sendKeys('MK401TH');
//            setTimeout(callback, 4000);
//        });
//    });
//
//
//
//    this.Given(/^step 3 Next is selected$/, function (callback) {
//        support.findById('energy-step3-next', function(result){
//            result.click();
//            setTimeout(callback,4000);
//        });
//    });
//    this.Given(/^Mostly Day is selected$/, function (callback) {
//        support.findById('energylist-mostly-day', function(result) {
//            result.click();
//            setTimeout(callback, 1000);
//        });
//    });
//
//    this.Given(/^step 4 Next is selected$/, function (callback) {
//        support.findById('energy-step4-next', function(result){
//            result.click();
//            setTimeout(callback,4000);
//        });
//    });
//    this.Given(/^I select Energy Usage options$/, function (callback) {
//        support.findById('energylist-flat', function (result) {
//            result.click();
//        });
//        support.findById('energylist-three-bed', function (result) {
//            result.click();
//        });
//        support.findById('energylist-four-person', function (result) {
//            result.click();
//        });
//        setTimeout(callback, 4000);
//    });
//
//    this.When(/^Step 5 Next is selected$/, function (callback) {
//        support.findById('energy-step5-next', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//
//    this.Then(/^I should see current search results$/, function (callback) {
//        support.findById('energylist-results-table-step6', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//
//
////Scenario: Upon completing a search I am able to Start my search again
//
//    this.When(/^I select to Start again$/, function (callback) {
//        support.findById('energylist-start-again', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//
//    this.Then(/^I should re-start my Search$/, function (callback) {
//        support.findById('select-supplier-Step-1', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//
//
////Scenario: Upon selecting previous the selected option is still selected
//
//    this.When(/^I go back to Current Supply$/, function (callback) {
//        support.findById('energy-step2-previous', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });
//    this.Then(/^I should see Dual Fuel selected$/, function (callback) {
//        support.findById('dual-fuel', function (result) {
//            result.click();
//            setTimeout(callback, 4000);
//        });
//    });

};
module.exports = steps;
