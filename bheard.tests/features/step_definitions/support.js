
var Support = function() {
};

Support.prototype.get = function(url, callback) {
    browser.get(url).then(function(result) {
        callback(result);
    });
};
Support.prototype.post = function(url, callback) {
    browser.post(url).then(function(result) {
        callback(result);
    });
};
//Support.prototype.getCurrent = function(url, callback) {
//    browser.getCurrent(url).then(function(url) {
//        callback(url);
//    });
//};

Support.prototype.wait = function(url, callback) {
    browser.wait(url).then(function(url) {
        callback(url);
    });
};
//Support.prototype.isElementPresent = function(find, callback) {
//    browser.isElementPresent(by.id(find)).then(function(result) {
//        callback(result)
//    });
//};

Support.prototype.findById = function(find, callback) {
    browser.findElement(by.id(find)).then(function(result) {
        callback(result)
    });
};

Support.prototype.className = function(find, callback){
    browser.findElement(by.class(find)).then(function(result) {
        callback(result)
    });
};
Support.prototype.findByBinding = function(item, callback) {
    browser.get(by.binding(item)).then(function(result) {
        callback(result);
    });

    Support.prototype.sendKeys = function(text, callback) {
        browser.sendKeys(text).then(function(result) {
            callback(result);
        });
    };
Support.prototype.db = function(collection, callback) {
    db.collection(collection).then(function (results) {
        callback(results);
    });
};
    //Support.prototype.db = function(remove, callback) {
    //    db.collection(remove).then(function (results) {
    //        callback(results);
    //    });
    //};
};

module.exports = new Support();