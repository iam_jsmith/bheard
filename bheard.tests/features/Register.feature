Feature: Register/Login/Logout


  As a User,
  I want to be able to Register,
  So that I can save my details

  Background:

#
#    Given User does not exist

   #Register

  Scenario: Ensure the Register page is accessible
    Given I am on the Homepage
    When Resister is selected
    Then I should see the Registration page

#  Scenario: Ensure I can send a Registration request successfully
#    Given I should see the Registration page
#    Given valid registration details are entered
#    When I submit the Registration form
#    Then I should see the email confirmation message

  Scenario: Ensure I can complete the Registration process successfully

    Given I should see the Registration page
    Given valid registration details are entered
    Given I submit the Registration form
    Given I should see the email confirmation message
    When I complete step 2 of Registration
    Then I should see the Registration confirmation message





