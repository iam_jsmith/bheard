Feature: Stepped Search  Energy Comparison
  As a User,
  I want to be able to compare energy prices,
  So that I can find the best deal offered


Scenario: Energy Homepage is displayed when energy is selected

    Given I am on the Homepage
    Given I select Energy
    Given I should see the Energy Homepage
    When Get Started is selected
    Then I should see the Energy Comparison page


Scenario: Selecting Dual Supplier Type displays all Dual Suppliers

    Given I should see the Energy Comparison page
    When Dual Fuel Supplier type is selected
    Then I should see all Dual Fuel Suppliers

Scenario: Selecting Gas Supplier Type displays all Gas Suppliers

    Given I should see the Energy Comparison page
    When Gas Supplier type is selected
    Then I should see all Gas Suppliers

Scenario: Selecting Electricity Supplier Type displays all Electricity Suppliers

    Given I should see the Energy Comparison page
    When Electricity Supplier type is selected
    Then I should see all Electricity Suppliers

#Scenario: I can successfully carry out a search
#
#
#
#Scenario: Upon completing a search I am able to Start my search again
#
#
#
#Scenario: Upon selecting previous the selected option is still selected

