properties {
	$base = resolve-path .
	$common = "bheard"
	$regex = "\`"version\`": \`"\d+(\.\d+)?(\.\d+)?(\.\d+)?\`""
	$tests = "$common.tests"
	$web = "$common.web"
	$webserver = "memphisstaging"
	$webservershare = "\\$webserver\$common"
	$worker = "$common.worker"
}

task default -depends worker

task version {
	foreach ($module in $common,$tests,$web,$worker) {
		(gc "$base\$module\package.json") -replace "$regex", "`"version`": `"$version`"" | sc "$base\$module\package.json"
	}
}

task common -depends version {
	cd "$base\$common"
	foreach ($module in $tests,$web,$worker) {
		copy ".env.$configuration" "$base\$module\.env"
	}
	npm install
}

task web -depends common {
	cd "$base\$web"
	bower install
	npm install
	grunt
	invoke-command -computername "$webserver" -scriptblock { import-module webadministration; stop-website $args[0] } -argumentlist $common
	robocopy "$base\$web\" "$webservershare\$web\" /mir
	invoke-command -computername "$webserver" -scriptblock { import-module webadministration; start-website $args[0] } -argumentlist $common
}

task worker -depends web {
	cd "$base\$worker"
	npm install
	grunt
	robocopy "$base\$worker\" "$webservershare\$worker\" /mir
}