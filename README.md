﻿# B.heard

## Install

### Windows

1. Install [Choclatey].
2. Install [Mongo].
3. Install [Node].
4. Install [Gulp].
5. Install [Bower].
6. Install [lab].
7. Install [Protractor].
8. Install [Cucumber].
9. Install [Nodemon].
10. Install Modules.
11. Gulp.

```PowerShell
> iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
> choco install mongodb
> choco install nodejs
> npm install -g gulp
> npm install -g bower
> npm install -g lab
> npm install -g protractor
> npm install -g cucumber
> npm install -g nodemon
> npm install
> gulp
```

### OSX

// todo @Ramsey did you take some notes which you can add here?
Install mongodb and nodejs on your machine. I used Brew to do this and NVM for Node.

Globally install these:
```
> npm install -g gulp
> npm install -g bower
> npm install -g lab
> npm install -g protractor
> npm install -g cucumber
> npm install -g nodemon
```
Run the following: 
(note `gulp build` may take a while after the `js` step because it compresses images; you can press `ctrl + c` to cancel
as the other steps aren't essential)
```
> npm install
> gulp build
```

## Build

```PowerShell
> gulp
```

## Run

### Web

Run inside the web folder.

NEED TO RUN BOWER INSTALL AS WELL (TODO MAY NEED TO ADD THIS TO AN AUTOMATED PROCESS)

```PowerShell
> gulp run-dev
```

### Worker

Run inside the worker folder.

```PowerShell
> node app.js -j energySuppliers
> node app.js -j ratings
> node app.js -j rankings
```

### Unit Tests

Run inside the bheard, web or worker folder.

```PowerShell
> lab
```

### Acceptance Tests

1. Run the Selenium web driver.
2. Run the Protractor test runner.

```PowerShell
> webdriver-manager start
> protractor config.js
```

### Gulp Tasks

```PowerShell
> gulp <task>
```

|Task|Description|
|----|-----------|
|`js-lib`|Concats, minifies and creates source maps for all of the `js` files from `bower_components`.|
|`js-app`|Concats, minifies and creates source maps for all of the other `js` files in `public`.|
|`js`|Runs both of the previous two.|
|`css`|Concats, minifies and creates source maps for all of the `css` files in the bower folders.|
|`sass`|Concats, minifies and creates source maps for all of the `scss` files.|
|`copy-fonts`|Copies all of the fonts into place.|
|`copy-images`|Copies all of the images into place and also optimises them. (Saves about 48% space)|
|`assets`|Runs both `copy-*` tasks|
|`build`|Runs all of the above|
|`retire`|Inspects all of the `js` ‘modules’ that are included in the project and checks them for known vulnerabilities. http://retirejs.github.io/retire.js/|
|`lint`|Runs `eslint` over all of the `js` files.|
|`watch-js`|Sets up a watcher for changes to the `js` files, which will run the js task.|
|`watch-css`|Sets up a watcher for changes to the `css` files, which will run the `css` task.|
|`watch-sass`|Sets up a watcher for changes to the `scss` files, which will run the `sass` task.|
|`run-dev`|Sets up all of the watchers.|
|`default`|Runs the `build` task. (This is what will run when you execute gulp without any task)|
|`full-build`|This runs `retire`, `lint` and `build`.|

[Bower]: http://bower.io
[Choclatey]: https://chocolatey.org
[gulp]: http://gulpjs.com "Gulp"
[Mongo]: https://www.mongodb.org
[Node]: http://nodejs.org
[lab]: https://github.com/hapijs/lab
[Protractor]: https://angular.github.io/protractor/#/
[Cucumber]: https://cucumber.io
[Nodemon]: http://nodemon.io

### Data and Cron ###

There is a dropbox folder into which data is added, this folder is 
synced with the live (and staging) server and moneyfacts data is 
downloaded daily (by the live server) using cron (see dropbox scripts 
folder). moneyfacts need to be emailed in order to allow the server ip
to access the data at: https://datafeeds.moneyfacts.co.uk/xml/vocho/

THIS DATA SHOULD NEVER BE A PART OF THE REPO, the `files` folder used to 
be and is now just test data.

For local development, symlink this folder as 'data' into bheard.worker
`ln -s /path/to/dropbox/data /path/to/localsite/bheard.worker/data`
see manual for `mklink` for windows

For sample data, copy `sample-data` and call the directory `data` (don't
just rename `sample-data` because it will be removed from the repo).

If you use the node app.js -j all script, there are current some errors
and all data won't be imported.



Money products that are listed on the website must be whitelisted. This
is achieved by adding the moneyfactsId to the relevant whitelist file
e.g. credit-cards-products-whitelist.csv. 
See the tests folder (in bheard.worker) for an example of how to do this.


How to get the moneyfacts ids for the whitelists:
Extracting Useful Information from Moneyfacts XSD and XML Files
1.)
Open up the XSD file in Excel.
2.) 
Click, ‘Use XML source task pane’.
3.)
Click on ‘XML Maps…’ then ‘add’.
4.)
Navigate to the directory containing all the Moneyfacts XML files and open the corresponding XML file, e.g. for Current Accounts.xsd, open up CurrentAccounts.xml
5.)
Click ‘OK’ and the XML will be imported.
6.)
Now you can drag and drop the desired fields into spreadsheet whereupon an empty column will be generated with the header of the selected field.
7.)
Once you have dragged and dropped your desired fields right click the header of any column and expand the ‘XML’ section of the menu and click ‘Import’.
8.)
Reselect the same XML you chose in step 4.
9.)
The column will then be populated with all the data.
10.)
From here on all manipulation and extraction of data can be done.
Enjoy!

