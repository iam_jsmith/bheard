var async = require('async'),
    simpleStats = require('simple-statistics'),
    fs = require('fs'),
    _ = require('lodash');

/**
 * Constructor
 *
 */
var MoneyRanker = function (db, moneyVariables) {
    this.internals = {};
    this.internals.moneyDB = db.get(moneyVariables.database);
    this.internals.type = moneyVariables.type;
    this.internals.rankingVariables = moneyVariables.rankingVariables;
    this.internals.justRates = [];
    this.internals.companyNos = [];
    this.internals.products = [];
};

/**
 * Generate Distribution Lookup Table
 * 
 * @param variance
 * @param mean
 * @param standardDeviation
 * @param callback
 */
MoneyRanker.prototype.generateDistributionLookupTable =
    function (variance, mean, standardDeviation, callback) {
        var table = {};
        var rate = 0;
        var total = 0;
        var index = 0;
        var px = 0;
        
        for (;index < this.internals.rankingVariables.distribution; index++) {
            rate = index / this.internals.rankingVariables.divide;

            px = parseFloat(this.calculateNormalDistributionSync(rate, variance, mean, standardDeviation)
                    .toFixed(this.internals.rankingVariables.cdf));

            total += px;
            table[rate] = {
                units: rate,
                px: px,
                cdf: parseFloat(total.toFixed(this.internals.rankingVariables.cdf))
            };
        }

        this.distributionTable = table;
        callback(null, table);
    };

/**
 * Calculate Cumulative Distribution
 * 
 * @param rate
 * @param callback
 * @returns {*}
 */
MoneyRanker.prototype.calculateCumulativeDistribution =
    function (rate, callback) {
        var rateString = rate.toString();
        rateString = rateString.replace(/(\.[0-9]*?)0+$/, "$1");
        rateString = rateString.replace(/\.$/, "");

        var rank = this.distributionTable[rateString];
        if (rank) {
            return callback(null, rank.cdf / this.internals.rankingVariables.divide); // TODO CHECK IF THIS SHOULD BE 10 FOR ALL OR IF IT NEEDS SOMETHING SPECIFIC PASSING IN
        } else {
            console.log(rate);
            return callback(null, 0);
        }
    };

/**
 * Calculate Normal Distribution Sync
 * 
 * @param rate
 * @param variance
 * @param mean
 * @param standardDeviation
 * @returns {number}
 */
MoneyRanker.prototype.calculateNormalDistributionSync =
    function (rate, variance, mean, standardDeviation) {
        var SQRT_2PI = Math.sqrt(2 * Math.PI);
        var left = 1 / (standardDeviation * SQRT_2PI);
        var right = Math.exp( -Math.pow( rate - mean, 2 ) / (2 * variance));

        return left * right;
    };

/**
 * Calculate Normal Distribution
 * 
 * @param rate
 * @param variance
 * @param mean
 * @param standardDeviation
 * @param callback
 */
MoneyRanker.prototype.calculateNormalDistribution =
    function (rate, variance, mean, standardDeviation, callback) {
        callback(null, this.calculateNormalDistributionSync(rate, variance, mean, standardDeviation));
    };

/**
 * Calculate Mean
 * 
 * @param rates
 * @param callback
 * @returns {*}
 */
MoneyRanker.prototype.calculateMean = function (rates, callback) {
    return callback(null, simpleStats.mean(rates));
};

/**
 * Calculate Standard Deviation
 * 
 * @param rates
 * @param callback
 * @returns {*}
 */
MoneyRanker.prototype.calculateStandardDeviation = function (rates, callback) {
    return callback(null, simpleStats.standardDeviation(rates));
};

/**
 * Calculate Variance
 * 
 * @param rates
 * @param callback
 * @returns {*}
 */
MoneyRanker.prototype.calculateVariance = function (rates, callback) {
    return callback(null, simpleStats.variance(rates));
};

/**
 * Filter Items
 *
 * // TODO CHECK THIS IS DOING WHAT IT IS SUPPOSED TO BE DOING
 *
 * @param items
 * @param callback
 */
MoneyRanker.prototype.filterItems = function (items, callback) {
    var results;
    switch (this.internals.type) {
        case 'creditCard':
            results = items.map(function (item) {
                var rate;
                if (item.rates) {
                    var rates = item.rates.standard.stdRates.filter(function (rate) {
                        return rate.rateType === 'Standard Purchases';
                    });
                    if (rates.length === 0) {
                        fs.appendFile('ranking.log',
                            new Date().toLocaleString() + ' Ignoring: ' + item.cardName + ' (rates length 0)',
                            function (err) {
                                if (err) throw err;
                                console.log('Ignoring: ' + item.cardName + ' (rates length 0)');
                            }
                        );
                        return null;
                    }
                    rate = rates[0];
                    return {
                        moneyFactsID: item.moneyFactsID,
                        rate: parseFloat(rate.apr)
                    };
                } else {
                    fs.appendFile('ranking.log',
                        new Date().toLocaleString() + ' Ignoring: ' + item.cardName + ' (no rates)',
                        function (err) {
                            if (err) throw err;
                            console.log('Ignoring: ' + item.cardName + ' (no rates)');
                        }
                    );
                    return null;
                }
            });
            break;
        case 'savingsAccount':
            results = items.map(function (item) {
                var rate;
                if (item.rates) {
                    var rates = item.rates;
                    if (rates.length === 0) {
                        fs.appendFile('ranking.log',
                            new Date().toLocaleString() + ' Ignoring: ' + item.account + ' (rates length 0)',
                            function (err) {
                                if (err) throw err;
                                console.log('Ignoring: ' + item.account + ' (rates length 0)');
                            }
                        );
                        return null;
                    } else if (rates.length > 1) {
                        rate = rates.reduce(function (highest, current, index, array) {
                            if (current.grossAER > highest.grossAER) {
                                highest.grossAER = parseFloat(current.grossAER);
                            }
                            return highest;
                        }, {
                            grossAER: -1
                        });
                    } else {
                        rate = rates[0];
                    }
                    return {
                        moneyFactsID: item.moneyFactsID,
                        rate: parseFloat(rate.grossAER)
                    };
                } else {
                    fs.appendFile('ranking.log',
                        new Date().toLocaleString() + ' Ignoring: ' + item.account + ' (no rates)',
                        function (err) {
                            if (err) throw err;
                            console.log('Ignoring: ' + item.account + ' (no rates)');
                        }
                    );
                    return null;
                }
            });
            break;
        case 'unsecuredLoans':
            results = items.filter(function (item) {
                return item.rates.repAPRAdvertised; // only show ones with APRAdvertised
            });

            this.internals.justRates = results.map(function (loan) {
                return parseFloat(loan.rates.repAPRAdvertised);
            });

            return callback(null, results); // TODO MAKE SURE PROGRAM STOPS HERE
    } // end switch

    results = results.filter(function (rate) {
        if (rate) {
            return true;
        } else {
            return false;
        }
    });

    this.internals.justRates = results.map(function (rate) {
        return rate.rate;
    });

    callback(null, results);
};

/**
 * Calculate rankings
 *
 * @param items
 * @param callback
 */
MoneyRanker.prototype.calculateRankings = function (items, callback) {
    var self = this;

    this.filterItems(items, function (err, rates) {
        if (err) {
            return callback(err);
        }
        async.parallel({
            mean: function (callback) {
                self.calculateMean(self.internals.justRates, callback);
            },
            variance: function (callback) {
                self.calculateVariance(self.internals.justRates, callback);
            },
            standardDeviation: function (callback) {
                self.calculateStandardDeviation(self.internals.justRates, callback);
            }
        }, function (err, results) {
            self.generateDistributionLookupTable(
                results.variance,
                results.mean,
                results.standardDeviation,
                function (err, table) {
                    async.each(rates, function (currentRate, cb) {
                        switch (self.internals.type) {
                            case 'creditCard':
                                self.calculateCumulativeDistribution(
                                    currentRate.rate,
                                    function (err, cdf) {
                                        // this (i.e. the 100) correlates with the number of decimal points in apr/aer
                                        // 1 - cdf inverts the ranking because high rate in credit cards is bad
                                        currentRate.rank = (1 - cdf) * 100; // convert to percentage
                                        currentRate.rank = parseFloat(currentRate.rank.toFixed(1));
                                        cb(err, currentRate);
                                    });
                                break;
                            case 'savingsAccount':
                                self.calculateCumulativeDistribution(
                                    currentRate.rate,
                                    function (err, cdf) {
                                        // this (i.e. the 100) correlates with the number of decimal points in apr/aer
                                        currentRate.rank = cdf * 100; // convert to percentage
                                        currentRate.rank = parseFloat(currentRate.rank.toFixed(1));
                                        cb(err, currentRate);
                                    });
                                break;
                            case 'unsecuredLoans':
                                self.calculateCumulativeDistribution(
                                    currentRate.rates.repAPRAdvertised,
                                    function (err, cdf) {
                                        currentRate.score = currentRate.score || {};
                                        // this (i.e. the 100) correlates with the number of decimal points in apr/aer
                                        // 1 - cdf inverts the ranking because high rate in credit cards is bad
                                        currentRate.score.valueForMoneyPercent = (1 - cdf) * 100; // convert to percentage
                                        currentRate.score.valueForMoneyPercent =
                                            parseFloat(currentRate.score.valueForMoneyPercent.toFixed(1));
                                        cb(err, currentRate);
                                    });
                                break;
                        }
                    }, function (err) {
                        // put all items into array
                        items.forEach(function(item){
                            self.internals.products.push(item);
                        });

                        callback(err, rates);
                    });
                });
        });
    });
};

/**
 * Calculate Overall Opinion Percent
 *
 * @param card
 * @param callback
 * @returns {*}
 */
MoneyRanker.prototype.calculateOverallOpinionPercent = function (card, callback) {
    var total = 0;
    var items = 0;
    var overallOpinionPercent = null;

    if (card.score) {
        if (card.score.valueForMoneyPercent) {
            total += card.score.valueForMoneyPercent;
            items++;
        }

        if (card.score.customerServicePercent) {
            total += card.score.customerServicePercent;
            items++;
        }

        if (card.score.customerOpinionPercent) {
            total += card.score.customerOpinionPercent;
            items++;
        }

        if (items > 0) {
            overallOpinionPercent = total / items;

            card.score.overallOpinionPercent = parseFloat(overallOpinionPercent.toFixed(4));

            var query = {
                moneyFactsID: card.moneyFactsID
            };
            return this.internals.moneyDB.update(query, card, callback);
        }
    }
    return callback();
};

/**
 * Generate Overall Opinion Percent
 *
 * @param callback
 */
MoneyRanker.prototype.generateOverallOpinionPercent = function (callback) {
    var self = this;

    // todo fcaProduct:true check is done in app.js
    this.internals.moneyDB.find({}, function (err, items) {
        if (err) {
            return callback(err);
        }

        async.each(items, self.calculateOverallOpinionPercent.bind(self), callback);
    });
};

/**
 * Generate Supplier Product Ranks
 * This is used for adding supplierProductRank and supplierWhitelistedProductRank
 * Use separate ranking for whitelisted items so can display one supplier on the site (see api call)
 * e.g. here: http://localhost:5555/money/products/unsecuredloans
 *
 * @param whitelisted
 * @param callback
 */
MoneyRanker.prototype.generateSupplierProductRanks = function (whitelisted, callback) {
    var self = this;
    var products = [];
    var rankingType = 'supplierProductRank';

    if (whitelisted) {
        rankingType = 'supplierWhitelistedProductRank';
    }

    self.internals.companyNos = _.uniq(_.pluck(self.internals.products, 'companyNo')); // get a list of company numbers

    self.internals.companyNos.forEach(function (supplierNo) {
        if (whitelisted) {
            products.push(_.where(self.internals.products, {companyNo: supplierNo, whitelisted: true}));
        } else {
            products.push(_.where(self.internals.products, {companyNo: supplierNo}));
        }
    });

    async.each(products, function(product, cb1) {
        var productPart;
        var sortedProducts = _.sortBy(product, 'score.overallOpinionPercent');
        sortedProducts = sortedProducts.reverse();
        async.forEachOf(sortedProducts, function (product, key, cb2) {
            // just for logging
            switch (self.internals.type) {
                case 'creditCard':
                    productPart = product.cardName;
                    break;
                case 'savingsAccount':
                    productPart = product.account;
                    break;
                case 'unsecuredLoans':
                    productPart = product.loanName;
                    break;
            }
            console.log(product.companyName + ': ' + productPart); // todo check

            var query = {
                moneyFactsID: product.moneyFactsID
            };

            var index = key+1;
            var updateOptions = {};
            updateOptions[rankingType] = index;

            var update = {
                // give it a rank relative to the supplier
                $set: updateOptions
            };

            // safety so doesn't wipe out data
            if (self.internals.type) {
                self.internals.moneyDB.update(query, update, cb2);
            }
        }, function (err) {
            if (err) {
                return cb1(err);
            }
            return cb1();
        });
    }, function (err) {
        if (err) {
            return callback(err);
        }
        return callback();
    });
};

/**
 * Generate Rankings
 *
 * @param items
 * @param importResponseHandler
 */
MoneyRanker.prototype.generateRankings = function (items, importResponseHandler) {
    var self = this;
    self.calculateRankings(items, finishedRanking);

    function finishedRanking (err, ranked) {
        async.each(ranked, function (rank, cb) {
            var update,
                query = {
                moneyFactsID: rank.moneyFactsID
            };

            switch (self.internals.type) {
                case 'creditCard':
                    update = {
                        $set: { 'score.valueForMoneyPercent': rank.rank }
                    };
                    break;
                case 'savingsAccount':
                    update = {
                        $set: { 'score.valueForMoneyPercent': rank.rank }
                    };
                    break;
                case 'unsecuredLoans':
                    update = {
                        $set: { 'score.valueForMoneyPercent': rank.score.valueForMoneyPercent }
                    };
                    break;
            }
            // safety so doesn't wipe out data
            if (self.internals.type) {
                self.internals.moneyDB.update(query, update, cb);
            }
        }, function (err) {
            if (err) {
                return importResponseHandler(err);
            }

            async.series([
                    function(callback){
                        self.generateOverallOpinionPercent(callback);
                    },
                    function(callback){
                        self.generateSupplierProductRanks(false, callback);
                    },
                    function(callback){
                        // whitelisted ranking
                        self.generateSupplierProductRanks(true, callback);
                    }
                ],
                function(err){
                    if (err) {
                        return importResponseHandler(err);
                    }
                    return importResponseHandler();
                }
            );
        });
    }
};

module.exports = MoneyRanker;
