var _ = require('lodash'),
    q = require('q');

var ReviewsBuilder = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._supplierReviews = db.get('supplierReviews');
    this._users = db.get('users');
};

ReviewsBuilder.prototype = {
    build: function (callback) {
        var promises = [
            this._updateReviewCounts(),
            this._updateLatestReviews()
        ];

        console.log('Started building reviews.');

        q.all(promises)
            .then(function () {
                console.log('Finished building reviews.');
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _updateReviewCounts: function () {
        var self = this,
            deferred = q.defer();

        this._supplierReviews.col.aggregate([
                { $match : { isApproved: true }},
                { $group: { _id: '$supplierId', reviewCount: { $sum: 1 } }}
            ],
            function (err, groups) {
                if (!err) {
                    q.all(_.map(groups, function (group) {
                        return self._suppliers.updateById(group._id, { $set : { reviewCount: group.reviewCount }});
                    })).then(
                        function () {
                            deferred.resolve();
                        },
                        function (err) {
                            deferred.reject(err);
                        }
                    );
                } else {
                    deferred.reject(err);
                }
            }
        );

        return deferred.promise;
    },

    _updateLatestReviews: function () {
        var self = this,
            deferred = q.defer();

        this._supplierReviews.col.aggregate([
                { $match : { isApproved: true, title: { $nin: [null, ''] } }},
                { $sort: { created: -1 } },
                { $group: {
                    _id: '$supplierId',
                    id: { $first: '$_id' },
                    overallOpinion: { $first: '$overallOpinion' },
                    title: { $first: '$title' },
                    text: { $first: '$text' },
                    userId: { $first: '$userId' }
                }}
            ],
            function (err, groups) {
                if (!err) {
                    self._users.find({ _id : { $in: _.pluck(groups, 'userId') } })
                        .then(function (users) {
                            return q.all(_.map(groups, function (group) {
                                return self._suppliers.updateById(group._id, { $set: {
                                    latestReview: {
                                        id: group.id,
                                        overallOpinion: group.overallOpinion,
                                        title: group.title,
                                        text: group.text,
                                        user: _.pick(_.find(users, function (user) {
                                            return user._id.equals(group.userId);
                                        }), '_id', 'firstName', 'lastName', 'hash')
                                    }
                                }});
                            }));
                        })
                        .then(function () {
                            deferred.resolve();
                        })
                        .catch(function (err) {
                            deferred.reject(err);
                        });
                } else {
                    deferred.reject(err);
                }
            }
        );

        return deferred.promise;
    }
};

module.exports = ReviewsBuilder;
