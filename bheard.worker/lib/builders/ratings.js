var _ = require('lodash'),
    q = require('q');

var RatingsBuilder = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._supplierReviews = db.get('supplierReviews');
};

RatingsBuilder.prototype = {
    build: function (callback) {
        var self = this;

        console.log('Started building ratings.');

        this._supplierReviews.col.aggregate([
                { $match : {
                    isApproved: true
                }},
                { $group: {
                    _id: '$supplierId',
                    overallOpinion: { $avg: '$overallOpinion' },
                    reviewCount: { $sum: 1 }
                }},
                { $match : {
                    reviewCount: { $gte: 5 }
                }}
            ],
            function (err, groups) {
                if (!err) {
                    q.all(_.map(groups, function (group) {
                        return self._suppliers.findById(group._id).then(function (supplier) {
                            var customerOpinionPercent = parseFloat(group.overallOpinion / 5 * 100),
                                customerServicePercent = supplier.score ? parseFloat(supplier.score.customerServicePercent) ? parseFloat(supplier.score.customerServicePercent) : null : null,
                                valueForMoneyPercent = supplier.score ? parseFloat(supplier.score.valueForMoneyPercent) ? parseFloat(supplier.score.valueForMoneyPercent) : null : null,
                                total = customerOpinionPercent + customerServicePercent + valueForMoneyPercent,
                                count = 1 + (customerServicePercent ? 1 : 0) + (valueForMoneyPercent ? 1 : 0),
                                overallOpinionPercent = count >= 2 ? total / count : null;

                            return self._suppliers.updateById(supplier._id, {
                                $set: {
                                    score: {
                                        customerOpinionPercent: customerOpinionPercent,
                                        customerServicePercent: customerServicePercent,
                                        valueForMoneyPercent: valueForMoneyPercent,
                                        overallOpinionPercent: overallOpinionPercent
                                    }
                                }
                            });
                        });
                    })).then(
                        function () {
                            console.log('Finished building ratings.');
                            callback();
                        },
                        function (err) {
                            callback(err);
                        }
                    );
                } else {
                    callback(err);
                }
            }
        );
    }
};

module.exports = RatingsBuilder;
