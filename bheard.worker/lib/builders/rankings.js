var _ = require('lodash'),
    q = require('q'),
    sectors = require('bheard').sectors();

var RankingsBuilder = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
};

/**
 * Build rankings for suppliers
 * 
 * @type {{build: RankingsBuilder.build}}
 */
RankingsBuilder.prototype = {
    build: function (callback) {
        var self = this;

        console.log('Started building supplier rankings.');

        q.all(_.map(sectors, function (sector) {
            var options = {
                sort: { 'score.overallOpinionPercent': -1 }
            };

            var query = { sector: sector, deleted: {$ne: true}, score: { $exists: true } };

            // only rank fca approved suppliers
            if (sector === "money") {
                query.fcaSupplier = true;
            }

            return self._suppliers.find(query, options).then(
                function (suppliers) {
                    return q.all(_.map(suppliers, function (supplier, i) {
                        return self._suppliers.updateById(supplier._id, { $set : { rank: i + 1 }});
                    }));
                }
            );
        })).then(
            function () {
                console.log('Finished building supplier rankings.');
                callback();
            },
            function (err) {
                callback(err);
            }
        );
    }
};

module.exports = RankingsBuilder;
