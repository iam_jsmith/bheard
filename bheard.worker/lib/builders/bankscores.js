var async = require('async');
var _ = require('lodash');

var BankScoresBuilder = function (db) {
    this.db = db;
    this.suppliers = db.get('suppliers');
    this.creditCards = db.get('creditCards');
    this.currentAccounts = db.get('currentAccounts');
    this.supplierReviews = db.get('supplierReviews');
    this.unsecuredLoans = db.get('unsecuredLoans');
    this.savingsAccounts = db.get('savings');
};

BankScoresBuilder.prototype.loadSavingsAccounts = function (supplier, callback) {
    var query = {
        'companyNo': supplier.no,
        'deleted': false,
        'fcaProduct': true
    };

    this.savingsAccounts.find(query, function (err, savings) {
        var scores = [];
        for (var i = savings.length - 1; i >= 0; i--) {
            if (savings[i].score) {
                scores.push(savings[i].score);
            }
        }

        callback(null, scores);
    });
};

BankScoresBuilder.prototype.loadUnsecuredLoans = function (supplier, callback) {
    var query = {
        'companyNo': supplier.no,
        'deleted': false,
        'fcaProduct': true
    };

    this.unsecuredLoans.find(query, function (err, loans) {
        var scores = [];
        for (var i = loans.length - 1; i >= 0; i--) {
            if (loans[i].score) {
                scores.push(loans[i].score);
            }
        }

        callback(null, scores);
    });
};

BankScoresBuilder.prototype.loadCardScores = function (supplier, callback) {
    var query = {
        'companyNo': supplier.no,
        'deleted': false,
        'fcaProduct': true
    };

    this.creditCards.find(query, function (err, cards) {
        var scores = [];
        for (var i = cards.length - 1; i >= 0; i--) {
            if (cards[i].score) {
                scores.push(cards[i].score);
            }
        }

        callback(null, scores);
    });
};

BankScoresBuilder.prototype.loadCurrentAccountScores = function (supplier, callback) {
    var query = {
        'companyNo': supplier.no,
        'deleted': false,
        'fcaProduct': true
    };

    this.currentAccounts.find(query, function (err, accounts) {
        var scores = [];
        for (var i = accounts.length - 1; i >= 0; i--) {
            if (accounts[i].score) {
                scores.push(accounts[i].score);
            }
        }

        callback(null, scores);
    });
};

BankScoresBuilder.prototype.accumulateScores = function (supplier, scoreData, callback) {
    var cardScores = scoreData.cards;
    var accountScores = scoreData.currentAccounts;
    var unsecuredLoans = scoreData.unsecuredLoans;
    var savingsScores = scoreData.savingsAccounts;
    var customerOpinionPercent = 0;

    // pull through customerOpinionPercent. this will be the seed value until ratings are added
    // todo test that this updates properly and that the seed value is replaced with the new value
    if (supplier.hasOwnProperty("score")) {
        if (supplier.score.hasOwnProperty("customerOpinionPercent")) {
            customerOpinionPercent = supplier.score.customerOpinionPercent;
        }
    }

    var scores = cardScores.concat(accountScores).concat(unsecuredLoans).concat(savingsScores),
        valueForMoneyPercentScores = _.compact(_.pluck(scores, 'valueForMoneyPercent')),
        valueForMoneyPercent = (_.sum(valueForMoneyPercentScores) / valueForMoneyPercentScores.length) || null,
        customerServicePercentScores = _.compact(_.pluck(scores, 'customerServicePercent')),
        customerServicePercent = (_.sum(customerServicePercentScores) / customerServicePercentScores.length) || null,
        total = valueForMoneyPercent + customerServicePercent,
        count = (valueForMoneyPercent ? 1 : 0) + (customerServicePercent ? 1 : 0),
        overallOpinionPercent = count == 2 ? total / count : null,
        score = {
            valueForMoneyPercent: valueForMoneyPercent,
            customerServicePercent: customerServicePercent,
            customerOpinionPercent: customerOpinionPercent,
            overallOpinionPercent: overallOpinionPercent
        };

    callback(null, score);
};

BankScoresBuilder.prototype.calculateScore = function (supplier, callback) {

    if (supplier.no === null) {
        return callback(null);
    }

    var self = this;

    async.parallel({
        cards: function (callback) {
            self.loadCardScores(supplier, callback);
        },
        currentAccounts: function (callback) {
            self.loadCurrentAccountScores(supplier, callback);
        },
        unsecuredLoans: function (callback) {
            self.loadUnsecuredLoans(supplier, callback);
        },
        savingsAccounts: function (callback) {
            self.loadSavingsAccounts(supplier, callback);
        }
    }, function (err, results) {
        self.accumulateScores(supplier, results, function (err, scores) {

            supplier.score = scores;
            self.suppliers.update({ 'no': supplier.no }, supplier, callback);
        });
    });
};

BankScoresBuilder.prototype.build = function (callback) {
    var self = this;
    this.suppliers.find({'sector': 'money', 'fcaSupplier': true}, function (err, suppliers) {
        async.each(suppliers, self.calculateScore.bind(self), callback);
    });
};

module.exports = BankScoresBuilder;
