var async = require('async'),
    simpleStats = require('simple-statistics'),
    fs = require('fs'),
    _ = require('lodash');

/**
 * Build rankings for current accounts
 *
 */
var CurrentAccountsRankings = function (db, moneyVariables) {
    this.db = db;
    this.internals = {};
    this.internals.moneyDB = db.get('currentAccounts');
    this.internals.type = moneyVariables.type;
    this.internals.rankingVariables = moneyVariables.rankingVariables;
    this.internals.inCredits = [];
    this.internals.odRates = [];
    this.internals.companyNos = [];
    this.internals.products = [];
};

/**
 * Calculate rankings
 *
 * @param accounts
 * @param callback
 */
CurrentAccountsRankings.prototype.calculateRankings = function (accounts, callback) {
    var self = this;

    var weightedAverages = [];

    async.each(accounts, function (account, callback) {
        self.calculateWeightedAverage(account, function (err, r) {
            if (!isNaN(parseFloat(r))) {
                weightedAverages.push(r); // this.
            }
        });

        callback();
    }, function (err) {
        async.parallel({
            mean: function (callback) {
                self.calculateMean(weightedAverages, callback);
            },
            variance: function (callback) {
                self.calculateVariance(weightedAverages, callback);
            },
            standardDeviation: function (callback) {
                self.calculateStandardDeviation(weightedAverages, callback);
            }
        }, function (err, results) {
            self.generateDistributionLookupTable(
                results.variance,
                results.mean,
                results.standardDeviation,
                function (err, table) {

                    async.each(accounts, function (currentRate, cb) {
                        self.calculateCumulativeDistribution(
                            currentRate.inCreditWeightedAverage,
                            function (err, cdf) {

                                currentRate.rank = cdf * 100; // convert to percentage
                                currentRate.rank = parseFloat(currentRate.rank.toFixed(1)); // todo why was this 2 before, should it be 2?

                                // console.log('cdf:' + currentRate.rank);
                                cb(err, currentRate);
                            });
                    }, function (err) {
                        // put all accounts into array
                        accounts.forEach(function(account){
                            self.internals.products.push(account);
                        });

                        callback(err, accounts);
                    });
                });
        });
    });
};

/**
 * Weighted average
 *
 * @param currentAccount
 * @param callback
 */
CurrentAccountsRankings.prototype.calculateWeightedAverage =
    function calculateWeightedAverage(currentAccount, callback) {

    var selfCurrentAccount = currentAccount;

    if (currentAccount.hasOwnProperty('inCredits')) {
        this.calculateInCreditWeightedAverage(currentAccount.inCredits, function (err, weightedCredit) {
            if (!isNaN(parseFloat(weightedCredit))) {
                selfCurrentAccount.inCreditWeightedAverage = weightedCredit;
            }
            callback(err, weightedCredit);
        });
    } else {
        // TODO DO SKIPPED PRODUCTS CAUSE NEGATIVE RANKING TO BE APPLIED?
        console.log('Skipped: ' + currentAccount.moneyFactsID + ' has no inCredits');
    }

    // there is currently nothing being returned here, commented out until fixed
    //if (currentAccount.hasOwnProperty('odRateSets')) {
    //    this.calculateOdRate(currentAccount.odRateSets, function (err, weightedOd) {
    //        callback(err, weightedOd);
    //    });
    //}
};

/**
 * InCredit Weighted Average
 *
 * @param inCredit
 * @param callback
 */
CurrentAccountsRankings.prototype.calculateInCreditWeightedAverage =
    function calculateInCreditWeightedAverage(inCredit, callback) {

    var totalRateMaximum = 0; // taken from all rates and used to calculate final weighted average
    var totalMaximumReturn = 0; // taken from all rates and used to calculate final weighted average
    var periods = []; // how many periods there are in the credit rate (InCredit.InCreditPeriod in xml)

    // create an array of the in credit periods
    for (var i = inCredit.length - 1; i >= 0; i--) {
        for (var j = inCredit[i].periods.length - 1; j >= 0; j--) {
            periods.push(inCredit[i].periods[j]);
        }
    }

    // for each period
    for (var periodIndex = periods.length - 1; periodIndex >= 0; periodIndex--) {
        // always looking at 2 year period (24 months): 3 - 24/x converts months to years. this will give 1 or 2 (yrs)
        periods[periodIndex].duration
            = 3 - this.internals.rankingVariables.maxDuration / this.calculatePeriodLength(periods[periodIndex]);

        // skip if doesn't have rates
        if (typeof periods[periodIndex].rates !== "undefined") {
            // loop through the rates
            for (var rateIndex = periods[periodIndex].rates.length - 1; rateIndex >= 0; rateIndex--) {
                var currentRate = periods[periodIndex].rates[rateIndex];
                currentRate.rateRange = this.calculateRateRange(currentRate);
                currentRate.rateMaximum = this.calculateRateMaximum(periods[periodIndex].duration, currentRate.rateRange);
                currentRate.maximumReturn = this.calculateMaximumReturn(currentRate.aer, currentRate.rateMaximum);

                totalRateMaximum += currentRate.rateMaximum;
                totalMaximumReturn += currentRate.maximumReturn;
            }
        } else {
            console.log('Skipped (No Rates)'); // todo display which has been skipped
        }
    }
    var weightedAverage = totalMaximumReturn / totalRateMaximum;
    // todo check for edge case:  (It rounds 1.5 to 1.50)
    weightedAverage = +weightedAverage.toFixed(3); // round to 2 decimal places
    callback(null, weightedAverage);
};

/**
 * Period Length
 * Period length is the length of the period for the rate
 *
 * @param period
 * @returns {number}
 */
CurrentAccountsRankings.prototype.calculatePeriodLength =
    function calculatePeriodLength(period) {

    var lower, upper;

    if (period.hasOwnProperty('mthsLower')) {
        lower = parseFloat(period.mthsLower);
        if (period.hasOwnProperty('mthsUpper')) {
            upper = parseFloat(period.mthsUpper);
            if (upper === 0) {
                upper = this.internals.rankingVariables.maxDuration;
            }
        } else {
            upper = this.internals.rankingVariables.maxDuration;
        }

        // this factors in that the first month starts at 1 and not 0
        return upper - lower + 1; // todo check
    } else {
        // this should never be reached because should always have a mthsLower
        // todo checkme
        return this.internals.rankingVariables.maxDuration;
    }
};

/**
 * Calculate Overall Opinion Percent
 * @param account
 * @param callback
 * @returns {*}
 */
CurrentAccountsRankings.prototype.calculateOverallOpinionPercent = function (account, callback) {
    var total = 0;
    var items = 0;
    var overallOpinionPercent = null;

    if (account.score) {
        if (account.score.valueForMoneyPercent) {
            total += account.score.valueForMoneyPercent;
            items++;
        }

        if (account.score.customerServicePercent) {
            total += account.score.customerServicePercent;
            items++;
        }

        if (account.score.customerOpinionPercent) {
            total += account.score.customerOpinionPercent;
            items++;
        }

        if (items > 0) {
            overallOpinionPercent = total / items;

            account.score.overallOpinionPercent = parseFloat(overallOpinionPercent.toFixed(4));

            var query = {
                moneyFactsID: account.moneyFactsID
            };
            // todo why is this `this` and not self?
            return this.internals.moneyDB.update(query, account, callback);
        }
    }

    return callback();
};

/**
 * Generate Overall Opinion Percent
 *
 * @param callback
 */
CurrentAccountsRankings.prototype.generateOverallOpinionPercent = function (callback) {
    var self = this;

    // fcaProduct:true check is done in app.js
    // todo why is this `this` and not self?
    this.internals.moneyDB.find({}, function (err, accounts) {
        if (err) {
            return callback(err);
        }

        async.each(accounts, self.calculateOverallOpinionPercent.bind(self), callback);
    });
};

/**
 * Max return
 *
 * @param aer
 * @param rateMaximum
 * @returns {number}
 */
CurrentAccountsRankings.prototype.calculateMaximumReturn =
    function calculateMaximumReturn(aer, rateMaximum) {
        return parseFloat(aer) * rateMaximum;
    };

/**
 * Calculate Rate Maximum
 *
 * @param duration
 * @param rateRange
 * @returns {number}
 */
CurrentAccountsRankings.prototype.calculateRateMaximum =
    function calculateRateMaximum(duration, rateRange) {
        return duration * rateRange;
    };

/**
 * Rate range
 *
 * @param rate
 * @returns {number}
 */
CurrentAccountsRankings.prototype.calculateRateRange =
    function calculateRateRange(rate) {
        var lower = parseFloat(rate.tierLower);
        var upper;
        if (!rate.hasOwnProperty('tierUpper')) {
            upper = lower * 2; // if no upper, times the lower by 2 (so this will be the top of the tier tower)
        } else {
            upper = parseFloat(rate.tierUpper);
        }

        var tierSize = Math.abs(upper - lower); // for example using abs will change -x to x
        return tierSize;
    };

/**
 * Mean
 *
 * @param rates
 * @param callback
 * @returns {*}
 */
CurrentAccountsRankings.prototype.calculateMean = function (rates, callback) {
    return callback(null, simpleStats.mean(rates));
};

/**
 * Variance
 *
 * @param rates
 * @param callback
 * @returns {*}
 */
CurrentAccountsRankings.prototype.calculateVariance = function (rates, callback) {
    return callback(null, simpleStats.variance(rates));
};

/**
 * StdDev
 *
 * @param rates
 * @param callback
 * @returns {*}
 */
CurrentAccountsRankings.prototype.calculateStandardDeviation = function (rates, callback) {
    return callback(null, simpleStats.standardDeviation(rates));
};

/**
 * Distribution Lookup Table
 *
 * @param variance
 * @param mean
 * @param standardDeviation
 * @param callback
 */
CurrentAccountsRankings.prototype.generateDistributionLookupTable =
    function (variance, mean, standardDeviation, callback) {
        var table = {};
        var rate = 0;
        var total = 0;
        var index = 0;
        var px = 0;

        for (; index < this.internals.rankingVariables.distribution; index++) {
            // IF YOU CHANGE THIS NUMBER (below), YOU NEED TO CHANGE 2 OTHERS (make a variable?):
            // return callback(null, rank.cdf / 100);
            // currentRate.rank = cdf * 10;
            // also need to change toFixed(4)
            // TODO CHECK THIS
            rate = index / this.internals.rankingVariables.divide; // how much the graph is split into (1000 is 3 decimal places)

            px = parseFloat(this.calculateNormalDistributionSync(rate, variance, mean, standardDeviation)
                .toFixed(this.internals.rankingVariables.cdf));

            total += px;
            table[rate] = {
                units: rate,
                px: px,
                cdf: parseFloat(total.toFixed(this.internals.rankingVariables.cdf))
            };
        }

        this.distributionTable = table;
        callback(null, table);
    };

/**
 * Normal Distribution
 *
 * @param rate
 * @param variance
 * @param mean
 * @param standardDeviation
 * @param callback
 */
//CurrentAccountsRankings.prototype.calculateNormalDistribution =
//    function (rate, variance, mean, standardDeviation, callback) {
//        callback(null, this.calculateNormalDistributionSync(rate, variance, mean, standardDeviation));
//};

/**
 * Normal Distribution Sync
 *
 * @param rate
 * @param variance
 * @param mean
 * @param standardDeviation
 * @returns {number}
 */
CurrentAccountsRankings.prototype.calculateNormalDistributionSync =
    function (rate, variance, mean, standardDeviation) {
        var SQRT_2PI = Math.sqrt(2 * Math.PI);
        var left = 1 / (standardDeviation * SQRT_2PI);
        var right = Math.exp(-Math.pow(rate - mean, 2) / (2 * variance));

        return left * right;
    };

/**
 * Cumulative Distribution
 *
 * @param wa
 * @param callback
 * @returns {*}
 */
CurrentAccountsRankings.prototype.calculateCumulativeDistribution =
    function (wa, callback) {
        if (typeof wa !== "undefined") {
            var waString = wa.toString();
            waString = waString.replace(/(\.[0-9]*?)0+$/, "$1");
            waString = waString.replace(/\.$/, "");

            var rank = this.distributionTable[waString];
            if (rank) {
                var value = rank.cdf / this.internals.rankingVariables.divide;
                // console.log(value);
                return callback(null, value);
            } else {
                // console.log(wa);
                return callback(null, 0); // shouldn't ever get here, but check 0 would be ok
            }
        } else {
            return callback(null, 0); // shouldn't ever get here, but check 0 would be ok
        }
    };


///
// These functions are for odRates, temporarily removed becasue incomplete
///

// CurrentAccountsRankings.prototype.calculateOdRate = function calculateOdRate(odRateSets) {
//     for (var index = odRateSets.length - 1; index >= 0; index--) {
//         for (var i = odRateSets[index].odRateTypes.length - 1; i >= 0; i--) {
//             var odRateType = odRateSets[index].odRateTypes[i];
//
//             console.log(odRateType.odType);
//
//             for (var rateIndex = odRateType.odRatePeriods.length - 1; rateIndex >= 0; rateIndex--) {
//                 var period = odRateType.odRatePeriods[rateIndex];
//
//                 period.multiplier = this.calculateOdPeriodMultiplier(period);
//                 console.log(period);
//             }
//         }
//     }
// };

// CurrentAccountsRankings.prototype.calculateOdPeriodMultiplier =
//     function calculateOdPeriodMultiplier(odPeriod) {
//
//     var lower = upper = 0;
//
//     if (odPeriod.hasOwnProperty('odRatePeriodMthsLower')) {
//         lower = parseFloat(odPeriod.odRatePeriodMthsLower);
//         if (odPeriod.hasOwnProperty('odRatePeriodMthsUpper')) {
//             upper = parseFloat(odPeriod.odRatePeriodMthsUpper);
//         }
//     }
//
//     return Math.abs(upper - lower);
// };

/**
 * Generate Supplier Product Ranks
 *
 * @param whitelisted
 * @param callback
 */
CurrentAccountsRankings.prototype.generateSupplierProductRanks = function (whitelisted, callback) {
    var self = this;
    var products = [];
    var rankingType = 'supplierProductRank';

    if (whitelisted) {
        rankingType = 'supplierWhitelistedProductRank';
    }

    self.internals.companyNos = _.uniq(_.pluck(self.internals.products, 'companyNo')); // get a list of company numbers

    self.internals.companyNos.forEach(function (supplierNo) {
        if (whitelisted) {
            products.push(_.where(self.internals.products, {companyNo: supplierNo, whitelisted: true}));
        } else {
            products.push(_.where(self.internals.products, {companyNo: supplierNo}));
        }
    });

    async.each(products, function(product, cb1) {
        var productPart;
        var sortedProducts = _.sortBy(product, 'score.overallOpinionPercent');
        sortedProducts = sortedProducts.reverse();
        async.forEachOf(sortedProducts, function (product, key, cb2) {
            // just for logging
            switch (self.internals.type) {
                case 'currentAccount':
                    productPart = product.accountName;
                    break;
            }
            console.log(product.companyName + ': ' + productPart); // todo check

            var query = {
                moneyFactsID: product.moneyFactsID
            };

            var index = key+1;
            var updateOptions = {};
            updateOptions[rankingType] = index;

            var update = {
                // give it a rank relative to the supplier
                $set: updateOptions
            };

            // safety so doesn't wipe out data
            if (self.internals.type) {
                self.internals.moneyDB.update(query, update, cb2);
            }
        }, function (err) {
            if (err) {
                return cb1(err);
            }
            return cb1();
        });
    }, function (err) {
        if (err) {
            return callback(err);
        }
        return callback();
    });
};

/**
 * Generate rankings entry function
 *
 * @param accounts
 * @param importResponseHandler
 */
CurrentAccountsRankings.prototype.generateRankings = function (accounts, importResponseHandler) {
    var self = this;
    self.calculateRankings(accounts, finishedRanking);

    function finishedRanking (err, ranked) {
        async.each(ranked, function (account, cb) {
            var query = {
                moneyFactsID: account.moneyFactsID
            };

            var update = {
                $set: { 'score.valueForMoneyPercent': account.rank }
            };
            // console.log('rank (' + account.moneyFactsID + '): ' + account.rank);

            self.internals.moneyDB.update(query, update, cb);
        }, function (err) {
            if (err) {
                return importResponseHandler(err);
            }

            async.series([
                    function(callback){
                        self.generateOverallOpinionPercent(callback);
                    }
                    ,
                    function(callback){
                        self.generateSupplierProductRanks(false, callback); // todo is this needed here?
                    },
                    function(callback){
                        // whitelisted ranking
                        self.generateSupplierProductRanks(true, callback);
                    }
                ],
                function(err){
                    if (err) {
                        return importResponseHandler(err);
                    }
                    return importResponseHandler();
                }
            );
        });
    }
};

module.exports = CurrentAccountsRankings;