var _ = require('lodash'),
    q = require('q'),
    SchoolsParser = require('../parsers/schools');

var SchoolsImporter = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._parser = new SchoolsParser();
};

SchoolsImporter.prototype = {
    import: function (filePath, callback) {
        this._addSchools(filePath)
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addSchools: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (schools) {
            return q.all(_.map(schools, function (school) {
                var query = { sector: school.sector, no: school.no },
                    options = { upsert: true };

                return self._suppliers.update(query, { $set: school }, options).then(function () {
                    console.log('Imported school:', school.name);
                    return school;
                });
            }));
        });
    }
};

module.exports = SchoolsImporter;
