var _ = require('lodash'),
    q = require('q'),
    CouncilsParser = require('../parsers/councils');

var CouncilsImporter = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._parser = new CouncilsParser();
};

CouncilsImporter.prototype = {
    import: function (filePath, callback) {
        this._addSchools(filePath)
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addSchools: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (councils) {
            return q.all(_.map(councils, function (council) {
                var query = { sector: council.sector, no: council.no },
                    options = { upsert: true };

                return self._suppliers.update(query, { $set: council }, options).then(function () {
                    console.log('Imported council:', council.name);
                    return council;
                });
            }));
        });
    }
};

module.exports = CouncilsImporter;
