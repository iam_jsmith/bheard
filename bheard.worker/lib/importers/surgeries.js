var _ = require('lodash'),
    q = require('q'),
    SurgeriesParser = require('../parsers/surgeries');

var surgeryTotal = 0;

var SurgeriesImporter = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._parser = new SurgeriesParser();
};

SurgeriesImporter.prototype = {
    import: function (filePath, callback) {
        this._addSurgeries(filePath)
            .then(function () {
                console.log(surgeryTotal + ' surgeries imported.');
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addSurgeries: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (surgeries) {
            return q.all(_.map(surgeries, function (surgery) {
                var query = { sector: surgery.sector, no: surgery.no },
                    options = { upsert: true };

                return self._suppliers.update(query, { $set: surgery }, options).then(function () {
                    console.log('Imported surgery:', surgery.name);
                    surgeryTotal = surgeryTotal + 1;
                    return surgery;
                });
            }));
        });
    }
};

module.exports = SurgeriesImporter;
