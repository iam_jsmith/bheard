var _ = require('lodash'),
    q = require('q'),
    CharitiesParser = require('../parsers/charities');

var CharitiesImporter = function (db) {
    this._db = db;
    this._charities = db.get('charities');
    this._parser = new CharitiesParser();
};

CharitiesImporter.prototype = {
    import: function (filePath, callback) {
        this._addCharities(filePath)
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addCharities: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (charities) {
            return q.all(_.map(charities, function (charity) {
                var query = { no: charity.no },
                    options = { upsert: true };

                return self._charities.update(query, { $set: charity }, options).then(function () {
                    console.log('Imported council:', charity.name);
                    return charity;
                });
            }));
        });
    }
};

module.exports = CharitiesImporter;
