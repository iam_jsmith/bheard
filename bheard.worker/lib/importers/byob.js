var _ = require('lodash'),
    q = require('q'),
    BYOBParser = require('../parsers/byob');

var BYOBImporter = function (db) {
    this._db = db;
    this._byob = db.get('byob');
    this._parser = new BYOBParser();
};

BYOBImporter.prototype = {
    import: function (filePath, callback) {
        this._addBYOBData(filePath)
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addBYOBData: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (banks) {
            return q.all(_.map(banks, function (bank) {
                var query = { name: bank.name }, // other fields are imported too
                    options = { upsert: true };

                return self._byob.update(query, { $set: bank }, options).then(function () {
                    console.log('Imported byob bank:', bank.name);
                    return bank;
                });
            }));
        });
    }
};

module.exports = BYOBImporter;
