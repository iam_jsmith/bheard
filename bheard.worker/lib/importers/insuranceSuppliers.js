var InsuranceParser = require('../parsers/insuranceSuppliers');
var _ = require('lodash');
var async = require('async');

var InsuranceImporter = function (db) {
    var self = this,
        suppliers = db.get('suppliers');

    this.internals = {
        database: db,
        foundReferences: [],
        timestamp: new Date().toISOString().replace('T', ' ').substr(0, 19) // 2012-11-04 14:55:45 // todo abstract
    };

    this.internals.parser = new InsuranceParser();

    this.internals.ensureSupplier = function (supplierToCheck, callback) {
        var query = { sector: supplierToCheck.sector, no: supplierToCheck.no };

        suppliers.findOne(query, function (err, foundSupplier) {
            if (!err) {
                if (!foundSupplier) {
                    supplierToCheck.createdDate = self.internals.timestamp;

                    suppliers.insert(supplierToCheck, function (err, supplier) {
                        if (!err) {
                            callback(null, supplier);
                        } else {
                            callback(err);
                        }
                    });
                } else {
                    supplierToCheck.updatedDate = self.internals.timestamp;

                    suppliers.update(query, { $set: supplierToCheck, $unset: { subSector: "" } }, function (err) {
                        if (!err) {
                            callback(null, foundSupplier); // todo check if this is better as supplierToCheck?
                        } else {
                            callback(err);
                        }
                    });
                }
            } else {
                console.log(err);
                callback(err);
            }
        });
    };
};

InsuranceImporter.prototype.aggregateScores = function (insuranceProviders) {
    for (var index = insuranceProviders.length - 1; index >= 0; index--) {
        var currentProvider = insuranceProviders[index];

        var opinionTotal = 0;
        var serviceTotal = 0;
        var valueForMoneyTotal = 0;
        var serviceCount = 0;
        var serviceWithValueForMoneyCount = 0;

        if (currentProvider.score.customerServicePercent) {
            currentProvider.score.customerServicePercent = serviceTotal / serviceCount;
        }
        if (currentProvider.score.customerOpinionPercent) {
            currentProvider.score.customerOpinionPercent = opinionTotal / serviceCount;
        }
        currentProvider.score.valueForMoneyPercent = valueForMoneyTotal ? (valueForMoneyTotal / serviceWithValueForMoneyCount) : null;

        var total = currentProvider.score.customerServicePercent + currentProvider.score.customerOpinionPercent + currentProvider.score.valueForMoneyPercent;
        var count = (serviceTotal ? 1 : 0) + (opinionTotal ? 1 : 0) + (valueForMoneyTotal ? 1 : 0);

        currentProvider.score.overallOpinionPercent = count >= 2 ? total / count : null;
    }

    return insuranceProviders;
};

InsuranceImporter.prototype.process = function (insuranceProviders) {
    var providers = [];

    for (var i = insuranceProviders.length - 1; i >= 0; i--) {
        var nextProvider = insuranceProviders[i];
        var isNew = true;
        var provider = {
            sector: 'insurance',
            subSector: nextProvider.subSector,
            no: nextProvider.no,
            name: nextProvider.name,
            brand: nextProvider.brand,
            brands: [],
            score: {},
            twitterTimelineId: nextProvider.twitterTimelineId,
            compareSiteUrl: nextProvider.compareSiteUrl, // at the moment, this is used to show the link (supplier level)
            affiliateUrl: nextProvider.affiliateUrl,
            whichUrl: nextProvider.whichUrl,
            trustPilotUrl: nextProvider.trustPilotUrl
        };

        var brand = {
            brand: nextProvider.brand,
            subSector: nextProvider.subSector,
            compareSiteUrl: nextProvider.compareSiteUrl,
            affiliateUrl: nextProvider.affiliateUrl
        };
        provider.brands.push(brand);

        // this will add supplier names with the same number as brands
        for (var j = providers.length - 1; j >= 0; j--) {
            if (providers[j].no === nextProvider.no) {
                provider = providers[j];

                provider.brands.push(brand);

                isNew = false;
                j = -1;
            }
        }

        delete provider.brand;
        delete provider.subSector;

        if (isNew) {
            providers.push(provider);
        }
    }

    return providers;
};

InsuranceImporter.prototype.import = function (filePath, cb) {
    var self = this;

    self.internals.parser.on('end', function (insurance) {
        var aggregatedSuppliers = self.process(insurance);
        var suppliers = self.aggregateScores(aggregatedSuppliers);

        async.each(suppliers, self.internals.ensureSupplier,
            function (err) {
                cb();
            });
    });

    self.internals.parser.parse(filePath);
};

module.exports = InsuranceImporter;
