var _ = require('lodash');
var EnergySupplierScoresParser = require('../parsers/energysuppliersscoresparser');
var EnergySupplierParser = require('../parsers/energySuppliers');
var async = require('async');

var ensureEnergySupplier = function ensureEnergySupplier(internals,
                                                         energySupplierToCheck,
                                                         callback) {
    var query = { no: energySupplierToCheck.no };

    internals.foundReferences.push(energySupplierToCheck.no);

    internals.energySuppliersModel.findOne(query,
        function (err, foundEnergySupplier) {
        if (err) {
            console.log(err);
            return callback(err);
        }

        if (foundEnergySupplier) {
            internals.energySuppliersModel.update(
                query,
                energySupplierToCheck,
                function (err) {

                if (err) {
                    console.log('Error updating energy supplier', err);
                }

                console.log('Finished updating supplier');
                callback(err, foundEnergySupplier);
            });
        } else {
            console.log('Inserting new energy supplier');
            internals.energySuppliersModel.insert(energySupplierToCheck, callback);
        }
    });
};

var deleteEnergySuppliers = function deleteEnergySuppliers(internals, foundReferences, cb) {
    // TODO: This should be a soft delete
    cb();
    // internals.energySuppliersModel.remove({ no: { '$nin': foundReferences } }, cb);
};

var EnergySuppliersImporter = function (db) {
    var self = this;
    var energySuppliersModel = db.get('suppliers');

    this.internals = {
        database: db,
        foundReferences: [],
        energySuppliersModel: energySuppliersModel,
        ensureEnergySupplier: ensureEnergySupplier,
        deleteEnergySuppliers: deleteEnergySuppliers,
        parser: new EnergySupplierParser(),
        scoresParser: new EnergySupplierScoresParser()
    };
};

EnergySuppliersImporter.prototype.import = function (filePath, scoresFilePath, callback) {
    var self = this;
    var scores = [];

    if (_.isFunction(scoresFilePath)) {
        callback = scoresFilePath;
        scoresFilePath = '';
    }

    this.internals.foundReferences = [];

    self.internals.parser.on('suppliers', function (energySuppliers) {
        async.eachSeries(energySuppliers, function (energySupplier, asyncCb) {
            energySupplier.sector = 'energy';
            energySupplier.subsector = '';

            var score;
            for (var i = scores.length - 1; i >= 0; i--) {
                if (scores[i].companyNo == energySupplier.no) {
                    score = scores[i];
                    i = -1;
                }
            }

            if (score) {
                energySupplier.score = {};
                energySupplier.score.customerServicePercent = score.trustworthiness * 100;
                energySupplier.score.customerOpinionPercent = score.personalExperience * 100;
                energySupplier.score.valueForMoneyPercent = score.valueForMoney * 100;

                var total = energySupplier.score.customerServicePercent + energySupplier.score.customerOpinionPercent + energySupplier.score.valueForMoneyPercent;
                var count = (energySupplier.score.customerServicePercent ? 1 : 0) + (energySupplier.score.customerOpinionPercent ? 1 : 0) + (energySupplier.score.valueForMoneyPercent ? 1 : 0);

                energySupplier.score.overallOpinionPercent = count >= 2 ? total / count : null;
            } else {
                console.log('Unable to find score for ' + energySupplier.no);
            }

            self.internals.ensureEnergySupplier(self.internals, energySupplier, asyncCb);
        }, function (err) {
            if (err) {
                console.log('There was an error importing', err);
                return callback(err);
            }

            self.internals.deleteEnergySuppliers(self.internals, self.internals.foundReferences, callback);
        });
    });

    self.internals.parser.on('error', callback);

    self.internals.scoresParser.on('error', function (err) {
        callback(err);
    });

    self.internals.scoresParser.on('end', function (err, parsedScores) {
        if (err) {
            console.log(err);
        }

        scores = parsedScores;
        self.internals.parser.parse(filePath);
    });

    self.internals.scoresParser.parse(scoresFilePath);
};

module.exports = EnergySuppliersImporter;
