var _ = require('lodash'),
    q = require('q'),
    SuppliersParser = require('../parsers/suppliers');

var SuppliersImporter = function (db) {
    this._db = db;
    this._suppliers = db.get('suppliers');
    this._parser = new SuppliersParser();
};

SuppliersImporter.prototype = {
    import: function (filePath, callback) {
        var promises = [
            this._addIndexes(),
            this._addSuppliers(filePath)
        ];

        q.all(promises)
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    },

    _addIndexes: function () {
        var self = this;

        return q.all(_.map(['name', 'no', 'sector'], function (index) {
            return self._suppliers.index(index);
        }));
    },

    _addSuppliers: function (filePath) {
        var self = this;

        return this._parser.parse(filePath).then(function (suppliers) {
            return q.all(_.map(suppliers, function (supplier) {
                var query = { sector: supplier.sector, no: supplier.no },
                    options = { upsert: true };

                return self._suppliers.update(query, { $set: supplier }, options).then(function () {
                    console.log('Imported supplier:', supplier.name);
                    return supplier;
                });
            }));
        });
    }
};

module.exports = SuppliersImporter;
