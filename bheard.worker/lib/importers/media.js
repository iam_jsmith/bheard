var MediaParser = require('../parsers/media');
var _ = require('lodash');
var async = require('async');

var MediaImporter = function (db) {
    var self = this,
        suppliers = db.get('suppliers');

    this.internals = {
        database: db,
        foundReferences: []
    };

    this.internals.parser = new MediaParser();

    this.internals.ensureSupplier = function (supplierToCheck, callback) {
        var query = { sector: supplierToCheck.sector, no: supplierToCheck.no };

        suppliers.findOne(query, function (err, foundSupplier) {
            if (!err) {
                if (!foundSupplier) {
                    suppliers.insert(supplierToCheck, function (err, supplier) {
                        if (!err) {
                            callback(null, supplier);
                        } else {
                            callback(err);
                        }
                    });
                } else {
                    suppliers.update(query, { $set: supplierToCheck }, function (err) {
                        if (!err) {
                            callback(null, foundSupplier);
                        } else {
                            callback(err);
                        }
                    });
                }
            } else {
                console.log(err);
                callback(err);
            }
        });
    };
};

MediaImporter.prototype.aggregateScores = function (mediaProviders) {
    for (var index = mediaProviders.length - 1; index >= 0; index--) {
        var currentProvider = mediaProviders[index];

        var opinionTotal = 0;
        var serviceTotal = 0;
        var valueForMoneyTotal = 0;
        var serviceCount = 0;
        var serviceWithValueForMoneyCount = 0;

        if (currentProvider.mobile_provider) {
            opinionTotal += currentProvider.mobile_score.opinion;
            serviceTotal += currentProvider.mobile_score.service;
            serviceCount++;

            if (currentProvider.mobile_score.valueForMoney) {
                valueForMoneyTotal += currentProvider.mobile_score.valueForMoney;
                serviceWithValueForMoneyCount++;
            }
        }

        if (currentProvider.broadband_provider) {
            opinionTotal += currentProvider.broadband_score.opinion;
            serviceTotal += currentProvider.broadband_score.service;
            serviceCount++;

            if (currentProvider.broadband_score.valueForMoney) {
                valueForMoneyTotal += currentProvider.broadband_score.valueForMoney;
                serviceWithValueForMoneyCount++;
            }
        }

        if (currentProvider.fixed_line_provider) {
            opinionTotal += currentProvider.fixed_line_score.opinion;
            serviceTotal += currentProvider.fixed_line_score.service;
            serviceCount++;

            if (currentProvider.fixed_line_score.valueForMoney) {
                valueForMoneyTotal += currentProvider.fixed_line_score.valueForMoney;
                serviceWithValueForMoneyCount++;
            }
        }

        if (currentProvider.tv_provider && currentProvider.tv_score.opinion && currentProvider.tv_score.service) {
            opinionTotal += currentProvider.tv_score.opinion;
            serviceTotal += currentProvider.tv_score.service;
            serviceCount++;

            if (currentProvider.tv_score.valueForMoney) {
                valueForMoneyTotal += currentProvider.tv_score.valueForMoney;
                serviceWithValueForMoneyCount++;
            }
        }

        if (currentProvider.score.customerServicePercent) {
            currentProvider.score.customerServicePercent = serviceTotal / serviceCount;
        }
        if (currentProvider.score.customerOpinionPercent) {
            currentProvider.score.customerOpinionPercent = opinionTotal / serviceCount;
        }
        currentProvider.score.valueForMoneyPercent = valueForMoneyTotal ? (valueForMoneyTotal / serviceWithValueForMoneyCount) : null;

        var total = currentProvider.score.customerServicePercent + currentProvider.score.customerOpinionPercent + currentProvider.score.valueForMoneyPercent;
        var count = (serviceTotal ? 1 : 0) + (opinionTotal ? 1 : 0) + (valueForMoneyTotal ? 1 : 0);

        currentProvider.score.overallOpinionPercent = count >= 2 ? total / count : null;

        delete currentProvider.mobile_score;
        delete currentProvider.broadband_score;
        delete currentProvider.fixed_line_score;
        delete currentProvider.tv_score;
    }

    return mediaProviders;
};

MediaImporter.prototype.process = function (mediaProviders) {
    var providers = [];

    for (var i = mediaProviders.length - 1; i >= 0; i--) {
        var nextProvider = mediaProviders[i];
        var isNew = true;
        var provider = {
            sector: 'phoneandinternet',
            name: nextProvider.name,
            no: nextProvider.no,
            mobile_provider: false,
            broadband_provider: false,
            fixed_line_provider: false,
            tv_provider: false,
            mobile_score: {
                opinion: 0,
                service: 0,
                valueForMoney: 0
            },
            broadband_score: {
                opinion: 0,
                service: 0,
                valueForMoney: 0
            },
            fixed_line_score: {
                opinion: 0,
                service: 0,
                valueForMoney: 0
            },
            tv_score: {
                opinion: 0,
                service: 0,
                valueForMoney: 0
            },
            score: {},
            twitterTimelineId: nextProvider.twitterTimelineId,
            affiliateUrl: nextProvider.affiliateUrl,
            whichUrl: nextProvider.whichUrl,
            trustPilotUrl: nextProvider.trustPilotUrl,
            compareSiteUrl: nextProvider.compareSiteUrl
        };

        for (var j = providers.length - 1; j >= 0; j--) {
            if (providers[j].name === nextProvider.name) {
                provider = providers[j];
                isNew = false;
                j = -1;
            }
        }

        if (nextProvider.subSector.toLowerCase() === 'mobile') {
            provider.mobile_provider = true;
            provider.mobile_score.opinion = nextProvider.score.customerOpinionPercent;
            provider.mobile_score.service = nextProvider.score.customerServicePercent;
            provider.mobile_score.valueForMoney = nextProvider.score.valueForMoneyPercent;
        }

        if (nextProvider.subSector.toLowerCase() === 'broadband') {
            provider.broadband_provider = true;
            provider.broadband_score.opinion = nextProvider.score.customerOpinionPercent;
            provider.broadband_score.service = nextProvider.score.customerServicePercent;
            provider.broadband_score.valueForMoney = nextProvider.score.valueForMoneyPercent;
        }

        if (nextProvider.subSector.toLowerCase() === 'fixed line') {
            provider.fixed_line_provider = true;
            provider.fixed_line_score.opinion = nextProvider.score.customerOpinionPercent;
            provider.fixed_line_score.service = nextProvider.score.customerServicePercent;
            provider.fixed_line_score.valueForMoney = nextProvider.score.valueForMoneyPercent;
        }

        if (nextProvider.subSector.toLowerCase() === 'pay tv') {
            provider.tv_provider = true;
            provider.tv_score.opinion = nextProvider.score.customerOpinionPercent;
            provider.tv_score.service = nextProvider.score.customerServicePercent;
            provider.tv_score.valueForMoney = nextProvider.score.valueForMoneyPercent;
        }

        if (isNew) {
            providers.push(provider);
        }
    }

    return providers;
};

MediaImporter.prototype.import = function (filePath, cb) {
    var that = this;

    that.internals.parser.on('end', function (media) {
        var aggregatedSuppliers = that.process(media);
        var suppliers = that.aggregateScores(aggregatedSuppliers);

        async.each(suppliers, that.internals.ensureSupplier,
            function (err) {

            cb();
        });
    });

    that.internals.parser.parse(filePath);
};

module.exports = MediaImporter;
