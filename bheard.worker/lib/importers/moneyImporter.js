var _ = require('lodash'),
    q = require('q');

/**
 * Constructor
 *
 * @param db
 * @param moneyVariables // this might be better set as constants on the object
 * @param ProductsParser
 * @param SuppliersParser
 * @param WhitelistParser
 * @constructor
 */
var MoneyImporter = function (db, moneyVariables, ProductsParser, SuppliersParser, WhitelistParser) {
    var self = this,
        suppliers = db.get('suppliers'),
        moneyDB = db.get(moneyVariables.database);

    this.internals = {};
    this.internals.foundReferences = [];
    this.internals.moneyVariables = moneyVariables;
    this.internals.productsParser = new ProductsParser();
    this.internals.suppliersParser = new SuppliersParser();
    this.internals.whitelistParser = new WhitelistParser();
    this.internals.suppliersData = [];
    this.internals.moneyFactsIds = [];
    this.internals.productsWhitelist = [];
    this.internals.timestamp = new Date().toISOString().replace('T', ' ').substr(0, 19); // 2012-11-04 14:55:45

    /**
     * Ensure Supplier
     *
     * @param supplierToCheck
     * @param score
     * @param callback
     */
    this.internals.ensureSupplier = function (supplierToCheck, score, callback) {
        var query = { sector: supplierToCheck.sector, no: supplierToCheck.no };
        // mark supplier as fca supplier if in fca data
        supplierToCheck.fcaSupplier = self.internals.moneyFactsIds.indexOf(supplierToCheck.no) > -1;

        suppliers.findOne(query, function (err, foundSupplier) {
            if (!err) {
                if (!foundSupplier) {
                    supplierToCheck.createdDate = self.internals.timestamp;
                    supplierToCheck = self.internals.addScore(supplierToCheck, score);
                    supplierToCheck = self.internals.addSwitchingLink(supplierToCheck);
                    suppliers.insert(supplierToCheck, function (err, supplier) {
                        if (!err) {
                            callback(null, supplier);
                        } else {
                            callback(err);
                        }
                    });
                } else {
                    // if fcaSupplier has already been set to true (e.g. it could be for currentAccounts but not
                    // Unsecured Loans), then ensure it is persistently true. If as an fcaSupplier it needs to apply
                    // as an 'fcaSupplier' (or not) for each product category, then will need a different solution.
                    if (foundSupplier.fcaSupplier === true) {
                        supplierToCheck.fcaSupplier = true;
                    }

                    supplierToCheck.updatedDate = self.internals.timestamp;
                    supplierToCheck.urls = {};
                    // we want to replace supplier details with new, but we want to take urls added by previous imports
                    if ('urls' in foundSupplier) {
                        _.assign(supplierToCheck.urls, foundSupplier.urls);
                    }
                    supplierToCheck = self.internals.addScore(supplierToCheck, score);
                    supplierToCheck = self.internals.addSwitchingLink(supplierToCheck); // add most recent link

                    suppliers.update(query, { $set: supplierToCheck }, function (err) {
                        if (!err) {
                            callback(null, foundSupplier);
                        } else {
                            callback(err);
                        }
                    });
                }
            } else {
                console.log('error finding supplier', err);
                callback(err);
            }
        });
    };

    /**
     * Add switching link
     * This function only adds the link based on which subtype is being imported
     *
     * @param supplier
     */
    this.internals.addSwitchingLink = function (supplier) {
        // add switching Link if in the csv (these are shown on the site)
        var fcaSupplier = _.find(self.internals.suppliersData, {moneyFactsID: supplier.no});
        if (typeof fcaSupplier !== "undefined") {
            if (!("urls" in supplier)) {
                supplier.urls = {};
            }

            if ("creditCardListings" in fcaSupplier) {
                supplier.urls.creditCardListings = fcaSupplier.creditCardListings;
            } else if ("currentAccountListings" in fcaSupplier) {
                supplier.urls.currentAccountListings = fcaSupplier.currentAccountListings;
            } else if ("savingsAccountListings" in fcaSupplier) {
                supplier.urls.savingsAccountListings = fcaSupplier.savingsAccountListings;
            } else if ("unsecuredLoanListings" in fcaSupplier) {
                supplier.urls.unsecuredLoanListings = fcaSupplier.unsecuredLoanListings;
            }
        }
        return supplier;
    };

    /**
     * Add customerOpinionPercent from CSV
     * This is so that a 'seed' value is used initially, which is later replaced with the scores from ratings
     *
     * @param supplier
     * @param score
     * @returns {*}
     */
    this.internals.addScore = function (supplier, score) {
        if (typeof score != 'undefined') {
            if (score.hasOwnProperty("customerOpinionPercent")) {
                if (!supplier.hasOwnProperty("score")) {
                    supplier.score = {};
                    supplier.score.customerOpinionPercent = score.customerOpinionPercent;
                }
            }
        }
        return supplier;
    };

    /**
     * Ensure Account
     *
     * @param accountToCheck
     * @param callback
     */
    this.internals.ensureAccount = function (accountToCheck, callback) {
        var query = { moneyFactsID: accountToCheck.moneyFactsID };
        self.internals.foundReferences.push(accountToCheck.moneyFactsID);

        // mark as fca if supplier is in fca data
        accountToCheck.fcaProduct = self.internals.moneyFactsIds.indexOf(accountToCheck.companyNo) > -1;

        // mark as whitelisted and add switching Link if in the whitelist csv (these are shown on the site)
        var whitelisted = _.find(self.internals.productsWhitelist, {moneyFactsID:accountToCheck.moneyFactsID});
        if (typeof whitelisted !== "undefined") {
            accountToCheck.whitelisted = true;
            accountToCheck.switchingUrl = whitelisted.switchingUrl;
        } else {
            accountToCheck.whitelisted = false;
            accountToCheck.switchingUrl = null;
        }

        moneyDB.findOne(query, function (err, foundAccount) {
            if (!err) {
                if (!foundAccount) {
                    accountToCheck.createdDate = self.internals.timestamp;
                    moneyDB.insert(accountToCheck, function (err, account) {
                        if (!err) {
                            callback(null, account);
                        } else {
                            callback(err);
                        }
                    });
                } else {
                    accountToCheck.updatedDate = self.internals.timestamp;
                    moneyDB.update(query, accountToCheck, function (err) {
                        if (!err) {
                            callback(null, foundAccount);
                        } else {
                            callback(err);
                        }
                    });
                }
            } else {
                console.log('error finding account', err);
                callback(err);
            }
        });
    };

    /**
     * Delete Accounts
     * delete any accounts that have not been found
     *
     * @param foundReferences
     * @param callback
     */
    this.internals.deleteAccounts = function (foundReferences, callback) {
        moneyDB.find({}, {})
            .then(function (accountsArray) {
                return q.all(_.map(accountsArray, function (account, i) {
                    if ((foundReferences.indexOf(account.moneyFactsID) > -1) === false) {
                        // if it is no longer in the moneyfacts data, set to 'deleted' (by bank) in the database
                        return moneyDB.updateById(account._id, {$set: {deleted: true, deletedDate: self.internals.timestamp}});
                    } else {
                        return moneyDB.updateById(account._id, {$set: {deleted: false}});
                    }
                }));
            })
            .then(function () {
                callback();
            })
            .catch(function (err) {
                callback(err);
            });
    };
};

/**
 * Main entry point
 *
 * @param filePath
 * @param fcaFilePath
 * @param whitelistPath
 * @param callback
 */
MoneyImporter.prototype.import = function (filePath, fcaFilePath, whitelistPath, callback) {
    var self = this;

    if (_.isFunction(fcaFilePath)) {
        callback = fcaFilePath;
        fcaFilePath = '';
    }

    // when the parser emits an account event, process it
    self.internals.productsParser.on(self.internals.moneyVariables.type, function (account) {
        self.internals.productsParser.stop();

        var supplier = {
            sector: 'money',
            subSector: self.internals.moneyVariables.type,
            no: account.companyNo,
            name: account.companyName
        };

        var score = _.find(self.internals.suppliersData, { 'moneyFactsID': account.companyNo } );

        self.internals.ensureSupplier(supplier, score, function (err, supplier) {
            if (!err) {
                account.supplierId = supplier._id;

                // use as a seed value for the account. should ignore if property is null
                if (score) {
                    account.score = _.pick(score, [
                        'valueForMoneyPercent',
                        'customerServicePercent',
                        'customerOpinionPercent',
                        'overallOpinionPercent'
                    ]);
                }

                self.internals.ensureAccount(account, function (err) {
                    if (!err) {
                        self.internals.productsParser.resume();
                    } else {
                        callback(err);
                    }
                });
            } else {
                callback(err);
            }
        });
    });

    self.internals.productsParser.on('end', function () {
        self.internals.deleteAccounts(self.internals.foundReferences, callback);
    });

    self.internals.productsParser.on('error', function (err) {
        callback(err);
    });

    self.internals.whitelistParser.on('error', function (err) {
        callback(err);
    });

    self.internals.whitelistParser.on('end', function (err, parsedData) {
        if (err) {
            console.log('error', err);
        }
        self.internals.productsWhitelist = parsedData;
        // entry point for products functions
        self.internals.productsParser.parse(filePath, self.internals.suppliersData);
    });

    self.internals.suppliersParser.on('error', function (err) {
        callback(err);
    });

    self.internals.suppliersParser.on('end', function (err, parsedData) {
        if (err) {
            console.log(err);
        }

        self.internals.suppliersData = parsedData;
        self.internals.moneyFactsIds = self.internals.suppliersData.map(function(company){
            return company.moneyFactsID;
        });

        // entry point for products functions
        self.internals.whitelistParser.parse(whitelistPath);
    });

    // entry point for program
    self.internals.suppliersParser.parse(fcaFilePath);
};

module.exports = MoneyImporter;
