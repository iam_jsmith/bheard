var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var SuppliersParser = function () {};

SuppliersParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: true, trim: true }),
            suppliers = [],
            i = 0;

        parser.on('readable', function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0) {
                    var supplier = {
                        no: record[0] ? record[0].toString() : null,
                        name: record[1],
                        sector: _.camelCase(record[2]).toLowerCase(),
                        subSector: _.camelCase(record[3])
                    };

                    if (supplier.no) {
                        suppliers.push(supplier);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(suppliers);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = SuppliersParser;
