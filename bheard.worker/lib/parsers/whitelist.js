var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var WhitelistParser = function () {
    EventEmitter.call(this);
};

util.inherits(WhitelistParser, EventEmitter);

/**
 * Whitelist parser
 * 
 * @param filePath
 */
WhitelistParser.prototype.parse = function (filePath) {
    console.log('Parsing whitelist.');

    var self = this;

    if (filePath === '') {
        self.emit('end', null, []);
        return;
    }

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                ids = [];

            lr.on('line', function (line) {
                if (i !== 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var id = {
                            moneyFactsID: items[0],
                            switchingUrl: items[1]
                        };

                        ids.push(id);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing whitelist.');
                self.emit('end', null, ids);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = WhitelistParser;
