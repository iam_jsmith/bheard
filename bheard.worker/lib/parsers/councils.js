var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var CouncilsParser = function () {};

CouncilsParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: false, trim: true }),
            councils = [],
            i = 0;

        parser.on("readable", function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0) {
                    var council = {
                        no: record[0] || null,
                        name: record[0],
                        sector: 'government',
                        subSector: 'council',
                        addressLine1: record[2],
                        addressLine2: record[3] || null,
                        town: record[4],
                        county: record[6] || null,
                        postcode: record[7],
                        telephoneNumber: record[9],
                        url: record[1],
                        crimeRank: record[12] !== '#N/A' ? parseInt(record[12]) ? parseInt(record[12]) : null : null,
                        planningRank: record[13] !== '#N/A' ? parseInt(record[13]) ? parseInt(record[13]) : null : null,
                        deprivationRank: record[14] !== '#N/A' ? parseInt(record[14]) ? parseInt(record[14]) : null : null,
                        fireRank: record[15] !== '#N/A' ? parseInt(record[15]) ? parseInt(record[15]) : null : null,
                        homelessnessRank: record[16] !== '#N/A' ? parseInt(record[16]) ? parseInt(record[16]) : null : null,
                        overallRank: record[17] !== '#N/A' ? parseInt(record[17]) ? parseInt(record[17]) : null : null
                    };

                    // replace weird character. may have to write code for individual use case e.g. school 138950
                    if (council.telephoneNumber.indexOf('�') !== -1) {
                        council.telephoneNumber = council.telephoneNumber.replace(/�/g,' ')
                    }

                    if (council.no) {
                        councils.push(council);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(councils);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = CouncilsParser;
