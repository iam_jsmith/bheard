var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var SavingsAccountScoresParser = function () {
    EventEmitter.call(this);
};

util.inherits(SavingsAccountScoresParser, EventEmitter);

SavingsAccountScoresParser.prototype.parse = function (filePath) {
    console.log('Started parsing savings account fca file.');

    var self = this;

    if (filePath === '') {
        self.emit('end', null, []);
        return;
    }

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                scores = [];

            lr.on('line', function (line) {
                if (i !== 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var score = {
                            moneyFactsID: items[0],
                            company: items[1],
                            valueForMoneyPercent: items[2] ? parseFloat(items[2]) ? parseFloat(items[2]) : null : null,
                            customerServicePercent: items[3] ? parseFloat(items[3]) ? parseFloat(items[3]) : null : null,
                            customerOpinionPercent: items[4] ? parseFloat(items[4]) ? parseFloat(items[4]) : null : null,
                            overallOpinionPercent: items[5] ? parseFloat(items[5]) ? parseFloat(items[5]) : null : null,
                            savingsAccountListings: items[6]
                        };

                        scores.push(score);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing savings account fca file.');
                self.emit('end', null, scores);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = SavingsAccountScoresParser;
