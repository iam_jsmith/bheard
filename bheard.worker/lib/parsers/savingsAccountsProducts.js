var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    Path = require('path'),
    util = require('util'),
    xml = require('node-xml');

var BASE_ELEMENT = 'Savings.Account';

var SavingsAccountsParser = function () {
    this.internals = {};
    EventEmitter.call(this);
};

util.inherits(SavingsAccountsParser, EventEmitter);

SavingsAccountsParser.prototype.stop = function () {
    this.internals.parser.pause();
};

SavingsAccountsParser.prototype.resume = function () {
    this.internals.parser.resume();
};

SavingsAccountsParser.prototype.parse = function (filePath, fcaData) {
    console.log('Started parsing savings accounts file.');

    var moneyFactsIds = fcaData.map(function(company){
        return company.moneyFactsID;
    });
    
    var parser = new xml.SaxParser(function (cb) {
        var currentElement = '',
            savingsAccount = {};

        cb.onError(function (error) {
            this.emit('error', error);
        }.bind(this));

        cb.onStartElementNS(function (name, attrs) {
            // for some reason, have to do Savings here and Savings.Account later
            // this is because there is Account.Account too
            if (currentElement === 'Savings' && name === 'Account') {
                savingsAccount = {
                    access: {}
                };

                for (var i = attrs.length - 1; i >= 0; i--) {
                    var attr = attrs[i];

                    if (attr[0] === 'MoneyfactsID') {
                        savingsAccount.moneyFactsID = attr[1];
                    }
                }
            }

            // create any objects or arrays
            // for some reason, there is no 'rates' element in the xml (unlike in the other xml files) #consistency
            if (name === 'Rate') {
                savingsAccount.rates = savingsAccount.rates || [];
                savingsAccount.rates.push({});
            } else if (name === 'TransferOption') {
                savingsAccount.transferOptions = savingsAccount.transferOptions || [];
                savingsAccount.transferOptions.push({});
            }

            if (currentElement) {
                currentElement += '.';
            }

            currentElement += name;
        });

        cb.onEndElementNS(function (name) {
            // for some reason, have to do Savings.Account here and Savings Account previously
            // this is because there is Account.Account too
            if (currentElement === BASE_ELEMENT && name === 'Account') {
                    // only add the loan to the list if its company number is in the fca data
                // if(moneyFactsIds.indexOf(savingsAccount.companyNo) > -1) {
                    this.emit('savingsAccount', savingsAccount);
                // }
            }

            currentElement = currentElement.substring(0, currentElement.lastIndexOf(name));

            if (currentElement) {
                currentElement = currentElement.substring(0, currentElement.lastIndexOf('.'));
            }
        }.bind(this));

        cb.onCharacters(function (text) {
            if (!text || !(/[^\s]/).test(text)) {
                return;
            }

            // obj.param = obj.param ? obj.param + text : text; // this is to accommodate &amp;
            switch (currentElement) {
                case BASE_ELEMENT + '.Account' : {
                    savingsAccount.accountName = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountType' : {
                    savingsAccount.accountType = text;
                    break;
                }
                case BASE_ELEMENT + '.CompNo' : {
                    savingsAccount.companyNo = text;
                    break;
                }
                case BASE_ELEMENT + '.Company' : {
                    savingsAccount.companyName = savingsAccount.companyName ? savingsAccount.companyName + text : text;
                    break;
                }
                case BASE_ELEMENT + '.MinInv' : {
                    savingsAccount.minInv = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountFee' : {
                    savingsAccount.accountFee = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountFeeFrq' : {
                    savingsAccount.accountFeeFrq = text;
                    break;
                }
                case BASE_ELEMENT + '.MinAge' : {
                    savingsAccount.minAge = text;
                    break;
                }
                case BASE_ELEMENT + '.MinOperateBal' : {
                    savingsAccount.minOperateBal = text;
                    break;
                }
                case BASE_ELEMENT + '.BranchTick' : {
                    savingsAccount.access.branchTick = text;
                    break;
                }
                case BASE_ELEMENT + '.TeleTick' : {
                    savingsAccount.access.teleTick = text;
                    break;
                }
                case BASE_ELEMENT + '.RateType' : {
                    savingsAccount.rateType = text;
                    break;
                }
                case BASE_ELEMENT + '.PostTick' : {
                    savingsAccount.access.postTick = text;
                    break;
                }
                case BASE_ELEMENT + '.INetTick' : {
                    savingsAccount.access.iNetTick = text;
                    break;
                }
                case BASE_ELEMENT + '.MobileAppTick' : {
                    savingsAccount.access.mobileAppTick = text;
                    break;
                }
                case BASE_ELEMENT + '.MaxInv' : {
                    savingsAccount.maxInv = text;
                    break;
                }
                case BASE_ELEMENT + '.IntroBonusRate' : {
                    savingsAccount.introBonusRate = text;
                    break;
                }
                case BASE_ELEMENT + '.IntroBonusPeriod' : {
                    savingsAccount.introBonusPeriod = text;
                    break;
                }
                case BASE_ELEMENT + '.IntPaid' : {
                    savingsAccount.intPaid = text;
                    break;
                }
                case BASE_ELEMENT + '.Notice' : {
                    savingsAccount.notice = text;
                    break;
                }
                case BASE_ELEMENT + '.Term' : {
                    savingsAccount.term = text;
                    break;
                }
                case BASE_ELEMENT + '.TermDays' : {
                    savingsAccount.termDays = text;
                    break;
                }
                case BASE_ELEMENT + '.NoticeDays' : {
                    savingsAccount.noticeDays = text;
                    break;
                }
                case BASE_ELEMENT + '.IntPaidAwayCompoundedType' : {
                    savingsAccount.intPaidAwayCompoundedType = text;
                    break;
                }
                case BASE_ELEMENT + '.Rate.TierLower' : {
                    savingsAccount.rates[savingsAccount.rates.length - 1].tierLower = text;
                    break;
                }
                case BASE_ELEMENT + '.Rate.TierUpper' : {
                    savingsAccount.rates[savingsAccount.rates.length - 1].tierUpper = text;
                    break;
                }
                // todo un-needed?
                case BASE_ELEMENT + '.Rate.GrossAER' : {
                    savingsAccount.rates[savingsAccount.rates.length - 1].grossAER = text;
                    break;
                }
                case BASE_ELEMENT + '.TransferOption.TransferInDetail' : {
                    savingsAccount.transferOptions[savingsAccount.transferOptions.length - 1].transferInDetail = text;
                    break;
                }
                case BASE_ELEMENT + '.ImageURL' : {
                    savingsAccount.imageURL = text;
                    break;
                }
                default: {
                    break;
                }
            }
        });

        cb.onEndDocument(function () {
            console.log('Finished parsing savings accounts file.');
            this.emit('end');
        }.bind(this));
    }.bind(this));

    this.internals.parser = parser;

    Fs.stat(filePath, function (err) {
        if (!err) {
            parser.parseFile(filePath);
        } else {
            console.log('Error getting stats for file: ', filePath);
            this.emit('error', err);
        }
    }.bind(this));
};

module.exports = SavingsAccountsParser;
