var fs = require('fs');
var EventEmitter = require('events').EventEmitter;
var util = require('util');
var parse = require('csv-parse');

var EnergySuppliersParser = function () {
    this.internals = {};
    EventEmitter.call(this);
};

util.inherits(EnergySuppliersParser, EventEmitter);

EnergySuppliersParser.prototype.parse = function (filePath) {
    console.log('Started parsing energy suppliers');

    var parser = parse({ auto_parse: true, trim: true });
    var row = 0;
    var energySuppliers = [];

    parser.on('readable', function () {
        var record;

        while (record = parser.read()) {
            if (row !== 0) {
                if (record[0] !== '') {
                    var energySupplier = {
                        no: record[0],
                        name: record[1],
                        coal: record[2],
                        gas: record[3],
                        nuclear: record[4],
                        renewable: record[5],
                        other: record[6],
                        gas_provider: record[7] === 'Y',
                        electricity_provider: record[8] === 'Y',
                        dualfuel_provider: record[9] === 'Y',
                        logo_url: record[14],
                        trustPilotUrl: record[15],
                        whichUrl: record[16],
                        twitterTimelineId: String(record[17]),
                        compareSiteUrl: record[19],
                        affiliateUrl: record[20]
                    };

                    energySuppliers.push(energySupplier);
                }
            }

            row++;
        }
    });

    parser.on('finish', function () {
        console.log('Finished parsing file');

        this.emit('suppliers', energySuppliers);
    }.bind(this));

    parser.on('error', function (err) {
        this.emit('error', err);
    }.bind(this));


    fs.stat(filePath, function (err) {
        if (!err) {
            var input = fs.createReadStream(filePath);
            input.pipe(parser);
        } else {
            console.log('Error getting stats for file: ', filePath);
            this.emit('error', err);
        }
    }.bind(this));
};

module.exports = EnergySuppliersParser;
