var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var EnergySuppliersScoresParser = function () {
    EventEmitter.call(this);
};

util.inherits(EnergySuppliersScoresParser, EventEmitter);

EnergySuppliersScoresParser.prototype.parse = function (filePath) {
    console.log('Started parsing energy supplier scores file.');

    var self = this;

    if (filePath === '') {
        self.emit('end', null, []);
        return;
    }

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                scores = [];

            lr.on('line', function (line) {
                if (i !== 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var score = {
                            companyNo: items[0],
                            company: items[1],
                            trustworthiness: items[2] ? parseFloat(items[2]) : undefined,
                            personalExperience: items[3] ? parseFloat(items[3]) : undefined,
                            valueForMoney: items[4] && items[4] !== 'TBC' ? parseFloat(items[4]) : undefined
                        };

                        scores.push(score);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing energy supplier scores file.');
                self.emit('end', null, scores);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = EnergySuppliersScoresParser;
