var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var SchoolsParser = function () {};

SchoolsParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: false, trim: true }),
            schools = [],
            i = 0;

        parser.on("readable", function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0) {
                    var school = {
                        no: record[1] || null,
                        name: record[3],
                        sector: 'education',
                        subSector: 'school',
                        postcode: record[2],
                        ofstedRegion: record[4] === '-' ? null : record[4],
                        overallEffectiveness: parseInt(record[12]) ? parseInt(record[12]) : null,
                        achievementOfPupils: parseInt(record[15]) ? parseInt(record[15]) : null,
                        qualityOfTeaching: parseInt(record[17]) ? parseInt(record[17]) : null,
                        behaviourAndSafetyOfPupils: parseInt(record[18]) ? parseInt(record[18]) : null,
                        leadershipAndManagement: parseInt(record[19]) ? parseInt(record[19]) : null,
                        previousOverallEffectiveness: parseInt(record[21]) ? parseInt(record[21]) : null
                    };

                    // replace weird character. may have to write code for individual use case e.g. school 138950
                    if (school.name.indexOf('�') !== -1) {
                        school.name = school.name.replace(/�/g,' ')
                    }

                    if (school.no) {
                        schools.push(school);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(schools);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = SchoolsParser;
