var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var MediaParser = function () {
    EventEmitter.call(this);
};

util.inherits(MediaParser, EventEmitter);

MediaParser.prototype.parse = function (filePath) {
    console.log('Started parsing media suppliers');
    var self = this;

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                media = [];

            lr.on('line', function (line) {
                if (i > 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var company = {
                            sector: 'phoneandinternet',
                            subSector: items[1],
                            name: items[2],
                            no: items[0],
                            twitterTimelineId: String(items[9]),
                            affiliateUrl: items[10],
                            whichUrl: items[11],
                            trustPilotUrl: items[12],
                            compareSiteUrl: items[13]
                        };

                        if (company.no === '8') {
                            console.log('here')
                        }

                        // set to 0 if not there, or causes undefined values to be created (todo may need to change this)
                        var trust = items[5] && items[5] !== 'TBC' ? parseFloat(items[5]) ? parseFloat(items[5]) * 100 : null : null;
                        var experience = items[6] && items[6] !== 'TBC' ? parseFloat(items[6]) ? parseFloat(items[6]) : null : null;
                        var valueForMoney = items[8] && items[8] !== 'TBC' ? parseFloat(items[8]) ? parseFloat(items[8]) * 100 : null : null;

                        var score = {};
                        var scoreCount = 0;
                        var total = 0;

                        if (trust) {
                            score.customerServicePercent = trust;
                            total += trust;
                            scoreCount++;
                        } else {
                            score.customerServicePercent = null;
                        }

                        if (experience) {
                            score.customerOpinionPercent = experience;
                            total += experience;
                            scoreCount++;
                        } else {
                            score.customerOpinionPercent = null;
                        }

                        if (valueForMoney) {
                            score.valueForMoneyPercent = valueForMoney;
                            total += valueForMoney;
                            scoreCount++;
                        } else {
                            score.valueForMoneyPercent = null;
                        }

                        // only calculate overall opinion percent if all the other values exist
                        // TO DO THIS DOES NOT SEEM TO PUSH OVER NULL VALUES
                        if (scoreCount > 0) {
                            score.overallOpinionPercent = total / scoreCount;
                        }

                        company.score = score;
                        media.push(company);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing file');
                self.emit('end', media);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = MediaParser;
