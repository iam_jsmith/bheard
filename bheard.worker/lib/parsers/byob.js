var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var BYOBParser = function () {};

BYOBParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: false, trim: true }),
            banks = [],
            i = 0;

        parser.on("readable", function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0) {
                    var bank = {
                        name: record[0],
                        values: [
                            !!(record[1].toLowerCase() === 'yes' ||
                            record[1].toLowerCase() === 'true'),
                            // if is a string e.g. 'n/a' or 'not disclosed' then ignore
                            record[2] && !isNaN(record[2]) ? parseFloat(record[2]) : null,
                            record[3] && !isNaN(record[3]) ? parseFloat(record[3]) : null,
                            record[4] && !isNaN(record[4]) ? parseFloat(record[4]) : null,
                            record[5] && !isNaN(record[5]) ? parseFloat(record[5]) : null,
                            record[6] && !isNaN(record[6]) ? parseFloat(record[6]) : null,
                            record[7] && !isNaN(record[7]) ? parseFloat(record[7]) : null,
                            record[8] && !isNaN(record[8]) ? parseFloat(record[8]) : null,
                            record[9] && !isNaN(record[9]) ? parseFloat(record[9]) : null
                        ]
                    };

                    if (bank.name) {
                        banks.push(bank);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(banks);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = BYOBParser;
