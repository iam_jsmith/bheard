var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var SurgeriesParser = function () {};

SurgeriesParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: false, trim: true }),
            surgeries = [],
            i = 0;

        parser.on("readable", function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0 && record[0] != 0) { // ignore the first row (header row) and also ignore if the ODS code is 0 != means string and int (empty row)
                    // these need to be 'else' 0 or the total can be calculated wrongly //todo check
                    var personalExperienceVeryGood = record[4] && !isNaN(record[4]) ? parseInt(record[4]) : 0,
                        personalExperienceFairlyGood = record[5] && !isNaN(record[5]) ? parseInt(record[5]) : 0,
                        personalExperienceNeutral = record[6] && !isNaN(record[6]) ? parseInt(record[6]) : 0,
                        personalExperienceFairlyPoor = record[7] && !isNaN(record[7]) ? parseInt(record[7]) : 0,
                        personalExperienceVeryPoor = record[8] && !isNaN(record[8]) ? parseInt(record[8]) : 0,
                        personalExperienceTotal = personalExperienceVeryGood +
                                                  personalExperienceFairlyGood +
                                                  personalExperienceNeutral +
                                                  personalExperienceFairlyPoor +
                                                  personalExperienceVeryPoor,
                        surgery = {
                            no: record[1] || null,
                            name: record[2] && isNaN(record[2]) ? record[2] : null, // check for isNaN because somtimes a 0
                            sector: 'healthcare',
                            subSector: 'gp',
                            trustScore: record[3] && !isNaN(record[3]) ? parseInt(record[3]) : null,
                            personalExperienceVeryGood: personalExperienceVeryGood,
                            personalExperienceVeryGoodPercent: personalExperienceVeryGood !== 0 ?
                                personalExperienceVeryGood / personalExperienceTotal * 100 : null,
                            personalExperienceFairlyGood: personalExperienceFairlyGood,
                            personalExperienceFairlyGoodPercent: personalExperienceFairlyGood !== 0 ?
                                personalExperienceFairlyGood / personalExperienceTotal * 100 : null,
                            personalExperienceNeutral: personalExperienceNeutral,
                            personalExperienceNeutralPercent: personalExperienceNeutral !== 0 ?
                                personalExperienceNeutral / personalExperienceTotal * 100 : null,
                            personalExperienceFairlyPoor: personalExperienceFairlyPoor,
                            personalExperienceFairlyPoorPercent: personalExperienceFairlyPoor !== 0 ?
                                personalExperienceFairlyPoor / personalExperienceTotal * 100 : null,
                            personalExperienceVeryPoor: personalExperienceVeryPoor,
                            personalExperienceVeryPoorPercent: personalExperienceVeryPoor !== 0 ?
                                personalExperienceVeryPoor / personalExperienceTotal * 100 : null,
                            personalExperiencePercentageGood: record[9] && !isNaN(record[9]) ? parseFloat(record[9]) * 100 : null,
                            personalExperiencePercentageBad: record[10] && !isNaN(record[10]) ? parseFloat(record[10]) * 100 : null,
                            website: record[11] && isNaN(record[11]) ? record[11] : null, // check for isNaN because somtimes a 0
                            telephoneNumber: record[12] ? record[12] : null,
                            addressLine1: record[13] && isNaN(record[13]) ? record[13] : null, // check for isNaN because somtimes a 0
                            addressLine2: record[14] && isNaN(record[14]) ? record[14] : null, // check for isNaN because somtimes a 0
                            city: record[15] && isNaN(record[15]) ? record[15] : null, // check for isNaN because somtimes a 0
                            postcode: record[16] && isNaN(record[16]) ? record[16] : null, // check for isNaN because somtimes a 0
                            score: record[17] && !isNaN(record[17]) ? parseFloat(record[17]) * 100 : null,
                            normalDistributionRankingPercentage: null,
                            normalDistributionRankingURL: record[19] && isNaN(record[19]) ? record[19] : null, // check for isNaN because somtimes a 0
                            caring: record[20] && !isNaN(record[20]) ? parseFloat(record[20]) * 100 : null,
                            effective: record[21] && !isNaN(record[21]) ? parseFloat(record[21]) * 100 : null,
                            responsive: record[22] && !isNaN(record[22]) ? parseFloat(record[22]) * 100 : null,
                            safe: record[23] && !isNaN(record[23]) ? parseFloat(record[23]) * 100 : null,
                            wellLed: record[24] && !isNaN(record[24]) ? parseFloat(record[24]) * 100 : null,
                            overall: record[25] && !isNaN(record[25]) ? parseFloat(record[25]) * 100 : null
                        };

                    if (surgery.no) {
                        surgeries.push(surgery);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(surgeries);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = SurgeriesParser;
