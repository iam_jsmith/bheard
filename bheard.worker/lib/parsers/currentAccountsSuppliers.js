var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var CurrentAccountScoresParser = function () {
    EventEmitter.call(this);
};

util.inherits(CurrentAccountScoresParser, EventEmitter);

CurrentAccountScoresParser.prototype.parse = function (filePath) {
    console.log('Started parsing current account fca file.');

    var self = this;

    if (filePath === '') {
        self.emit('end', null, []);
        return;
    }

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                scores = [];

            lr.on('line', function (line) {
                if (i !== 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var score = {
                            // moneyFactsID: items[0],
                            // company: items[1],
                            // valueForMoney: items[2] ? parseFloat(items[2]) : undefined,
                            // valueForMoneyPercent: items[3] ? parseFloat(items[3]) : undefined,
                            // customerService: items[4] ? parseFloat(items[4]) : undefined,
                            // customerServicePercent: items[5] ? parseFloat(items[5]) : undefined,
                            // customerOpinion: items[6] ? parseFloat(items[6]) : undefined,
                            // customerOpinionPercent: items[7] ? parseFloat(items[7]) : undefined,
                            // overallOpinionPercent: items[8] ? parseFloat(items[8]) : undefined,
                            // currentAccountListings: items[6],
                            moneyFactsID: items[0],
                            company: items[1],
                            valueForMoneyPercent: items[2] ? parseFloat(items[2]) ? parseFloat(items[2]) : null : null,
                            customerServicePercent: items[3] ? parseFloat(items[3]) ? parseFloat(items[3]) : null : null,
                            customerOpinionPercent: items[4] ? parseFloat(items[4]) ? parseFloat(items[4]) : null : null,
                            overallOpinionPercent: items[5] ? parseFloat(items[5]) ? parseFloat(items[5]) : null : null,
                            currentAccountListings: items[6]
                        };

                        scores.push(score);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing current account fca file.');
                self.emit('end', null, scores);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = CurrentAccountScoresParser;
