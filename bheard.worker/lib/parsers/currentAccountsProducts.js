var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    Path = require('path'),
    util = require('util'),
    xml = require('node-xml');

var BASE_ELEMENT = 'CurrentAccounts.CurrentAccount';

var CurrentAccountsParser = function () {
    this.internals = {};
    EventEmitter.call(this);
};

util.inherits(CurrentAccountsParser, EventEmitter);

CurrentAccountsParser.prototype.stop = function () {
    this.internals.parser.pause();
};

CurrentAccountsParser.prototype.resume = function () {
    this.internals.parser.resume();
};

CurrentAccountsParser.prototype.parse = function (filePath, fcaData) {
    console.log('Started parsing current accounts file.');

    var moneyFactsIds = fcaData.map(function(company){
        return company.moneyFactsID;
    });
    
    var parser = new xml.SaxParser(function (cb) {
        var currentElement = '',
            currentAccount = {},
            currentCredit,
            currentOdRateSet,
            currentOdRateType,
            currentOdRatePeriod,
            currentOdRate;

        cb.onError(function (error) {
            this.emit('error', error);
        }.bind(this));

        cb.onStartElementNS(function (name, attrs) {
            if (name === 'CurrentAccount') {
                currentAccount = {};
                currentOdRateSet = {};
                currentOdRateType = {};
                currentOdRatePeriod = {};
                currentOdRate = {};
                currentCredit = {};

                for (var i = attrs.length - 1; i >= 0; i--) {
                    var attr = attrs[i];

                    if (attr[0] === 'MoneyfactsID') {
                        currentAccount.moneyFactsID = attr[1];
                    }
                }
            }

            // create any objects or arrays
            if (name === 'AccountOpening' && currentElement === BASE_ELEMENT) {
                currentAccount.accountOpenings = currentAccount.accountOpenings || [];
                currentAccount.accountOpenings.push({});
            } else if (name === 'AccountOperation' && currentElement === BASE_ELEMENT) {
                currentAccount.accountOperation = currentAccount.accountOperation || [];
                currentAccount.accountOperation.push({});
            } else if (name === 'Benefit' && currentElement === BASE_ELEMENT) {
                currentAccount.benefits = currentAccount.benefits || [];
                currentAccount.benefits.push({});
            } else if (name === 'Eligibility' && currentElement === BASE_ELEMENT) {
                currentAccount.eligibility = currentAccount.eligibility || {};
            } else if (name === 'FacilitySet' && currentElement === BASE_ELEMENT) {
                currentAccount.facilitySet = {};
            } else if (name === 'ChargeSet' && currentElement === BASE_ELEMENT) {
                currentAccount.chargeSets = currentAccount.chargeSets || [];
                currentAccount.chargeSets.push({});
            } else if (name === 'Charge' && currentElement === BASE_ELEMENT + '.ChargeSet') {
                currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges = currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges || [];
                currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges.push({});
            } else if (name === 'InCredit' && currentElement === BASE_ELEMENT) {
                currentAccount.inCredits = currentAccount.inCredits || [];
                currentCredit = {};
                currentAccount.inCredits.push(currentCredit);
            } else if (name === 'InCreditPeriod' && currentElement === BASE_ELEMENT + '.InCredit') {
                currentCredit.periods = currentCredit.periods || [];
                currentCredit.periods.push({});
            } else if (name === 'InCreditRate' && currentElement === BASE_ELEMENT + '.InCredit.InCreditPeriod') {
                currentCredit.periods[currentCredit.periods.length - 1].rates = currentCredit.periods[currentCredit.periods.length - 1].rates || [];
                currentCredit.periods[currentCredit.periods.length - 1].rates.push({});
            } else if (name === 'OdRateSet' && currentElement === BASE_ELEMENT) {
                currentAccount.odRateSets = currentAccount.odRateSets || [];
                currentAccount.odRateSets.push({});
                currentOdRateSet = currentAccount.odRateSets[currentAccount.odRateSets.length - 1];
            } else if (name === 'OdRateType' &&
                currentElement !== BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate') {
                currentOdRateSet.rateTypes = currentOdRateSet.rateTypes || [];
                currentOdRateSet.rateTypes.push({});
                currentOdRateType = currentOdRateSet.rateTypes[currentOdRateSet.rateTypes.length - 1];
            } else if (name === 'OdRatePeriod' &&
                currentElement !== BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate') {
                currentOdRateType.ratePeriods = currentOdRateType.ratePeriods || [];
                currentOdRateType.ratePeriods.push({});
                currentOdRatePeriod = currentOdRateType.ratePeriods[currentOdRateType.ratePeriods.length - 1];
            } else if (name === 'OdRate' &&
                currentElement !== BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate') {
                currentOdRatePeriod.rates = currentOdRatePeriod.rates || [];
                currentOdRatePeriod.rates.push({});
                currentOdRate = currentOdRatePeriod.rates[currentOdRatePeriod.rates.length - 1];
            }

            if (currentElement) {
                currentElement += '.';
            }

            currentElement += name;
        });

        cb.onEndElementNS(function (name) {
            if (name === 'CurrentAccount') {
                // only add the loan to the list if its company number is in the fca data
                // if(moneyFactsIds.indexOf(currentAccount.companyNo) > -1) {
                    this.emit('currentAccount', currentAccount);
                // }
            }

            currentElement = currentElement.substring(0, currentElement.lastIndexOf(name));

            if (currentElement) {
                currentElement = currentElement.substring(0, currentElement.lastIndexOf('.'));
            }
        }.bind(this));

        cb.onCharacters(function (text) {
            if (!text || !(/[^\s]/).test(text)) {
                return;
            }

            // obj.param = obj.param ? obj.param + text : text; // this is to accommodate &amp;
            switch (currentElement) {
                case BASE_ELEMENT + '.CurrentAccountType' : {
                    currentAccount.currentAccountType = text;
                    break;
                }
                case BASE_ELEMENT + '.CompNo' : {
                    currentAccount.companyNo = text;
                    break;
                }
                case BASE_ELEMENT + '.Company' : {
                    currentAccount.companyName = currentAccount.companyName ? currentAccount.companyName + text : text;
                    break;
                }
                case BASE_ELEMENT + '.AccountOptionName' : {
                    currentAccount.accountName = currentAccount.accountName ? currentAccount.accountName + text : text;
                    break;
                }
                case BASE_ELEMENT + '.AccountLaunchDate' : {
                    currentAccount.accountLaunchDate = text;
                    break;
                }
                case BASE_ELEMENT + '.OptionLaunchDate' : {
                    currentAccount.optionLaunchDate = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountOpening.ChannelName' : {
                    currentAccount.accountOpenings[currentAccount.accountOpenings.length - 1].channelName = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountOperation.ChannelName' : {
                    currentAccount.accountOperation[currentAccount.accountOperation.length - 1].channelName = text;
                    break;
                }
                case BASE_ELEMENT + '.AccountOpening.ChannelDetail' : {
                    currentAccount.accountOpenings[currentAccount.accountOpenings.length - 1].channelDetail = text;
                    break;
                }
                case BASE_ELEMENT + '.Eligibility.EligibilityAgeMinTxt' : {
                    currentAccount.eligibility.ageMinTxt = text;
                    break;
                }
                case BASE_ELEMENT + '.Eligibility.SetupFee' : {
                    currentAccount.eligibility.setupFee = text;
                    break;
                }
                case BASE_ELEMENT + '.Eligibility.IntroFee' : {
                    currentAccount.eligibility.introFee = text;
                    break;
                }
                case BASE_ELEMENT + '.Eligibility.StdFee' : {
                    currentAccount.eligibility.stdFee = text;
                    break;
                }
                case BASE_ELEMENT + '.Eligibility.EligibilityCriteria_Funding' : {
                    currentAccount.eligibility.criteriaFunding = text;
                    break;
                }
                case BASE_ELEMENT + '.Benefit.BenefitSection' : {
                    currentAccount.benefits[currentAccount.benefits.length - 1].section = text;
                    break;
                }
                case BASE_ELEMENT + '.Benefit.BenefitType' : {
                    currentAccount.benefits[currentAccount.benefits.length - 1].type = text;
                    break;
                }
                case BASE_ELEMENT + '.Benefit.BenefitNote' : {
                    currentAccount.benefits[currentAccount.benefits.length - 1].note =
                        currentAccount.benefits[currentAccount.benefits.length - 1].note ?
                        currentAccount.benefits[currentAccount.benefits.length - 1].note + text : text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.MinOpenBal' : {
                    currentAccount.facilitySet.minOpenBal = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.MinOperateBal' : {
                    currentAccount.facilitySet.minOperateBal = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.MaxInv' : {
                    currentAccount.facilitySet.maxInv = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.MinWithdrawal' : {
                    currentAccount.facilitySet.minWithdrawal = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.CardType' : {
                    currentAccount.facilitySet.cardType = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.DebitSystemType' : {
                    currentAccount.facilitySet.debitSystemType = text;
                    break;
                }
                case BASE_ELEMENT + '.FacilitySet.ATMLimit' : {
                    currentAccount.facilitySet.atmLimit = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferType' : {
                    currentAccount.odBufferType = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferAppliesToType' : {
                    currentAccount.odBufferAppliesToType = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferAmt' : {
                    currentAccount.odBufferAmt = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferPeriod' : {
                    currentAccount.odBufferPeriod = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferDependsCreditRatingTick' : {
                    currentAccount.odBufferDependsCreditRatingTick = text;
                    break;
                }
                case BASE_ELEMENT + '.OdBuffer.OdBufferNote' : {
                    currentAccount.odBufferNote = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.ChargeSetType' : {
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].chargeSetType = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.ChargeSetChargingFreq' : {
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].chargeSetChargingFreq = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.ChargeSetWefDef' : {
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].chargeSetWefDef = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.Charge.ChargeSection' : {
                    var chargesCount = currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges.length;
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges[chargesCount - 1].chargeSection = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.Charge.ChargeType' : {
                    var chargesCount = currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges.length;
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges[chargesCount - 1].chargeType = text;
                    break;
                }
                case BASE_ELEMENT + '.ChargeSet.Charge.ChargeTxt' : {
                    var chargesCount = currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges.length;
                    currentAccount.chargeSets[currentAccount.chargeSets.length - 1].charges[chargesCount - 1].chargeTxt = text;
                    break;
                }
                case BASE_ELEMENT + '.InCredit.InCreditPeriod.ICPeriodMthsLower' : {
                    currentCredit.periods[currentCredit.periods.length - 1].mthsLower = text;
                    break;
                }
                case BASE_ELEMENT + '.InCredit.InCreditPeriod.ICPeriodMthsUpper' : {
                    currentCredit.periods[currentCredit.periods.length - 1].mthsUpper = text;
                    break;
                }
                case BASE_ELEMENT + '.InCredit.InCreditPeriod.InCreditRate.ICTierLower' : {
                    var rates = currentCredit.periods[currentCredit.periods.length - 1].rates;
                    rates[rates.length - 1].tierLower = text;
                    break;
                }
                case BASE_ELEMENT + '.InCredit.InCreditPeriod.InCreditRate.ICTierUpper' : {
                    var rates = currentCredit.periods[currentCredit.periods.length - 1].rates;
                    rates[rates.length - 1].tierUpper = text;
                    break;
                }
                case BASE_ELEMENT + '.InCredit.InCreditPeriod.InCreditRate.ICAER' : {
                    var rates = currentCredit.periods[currentCredit.periods.length - 1].rates;
                    rates[rates.length - 1].aer = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdType' : {
                    currentOdRateType.type = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRatePeriodMthsLower' : {
                    currentOdRatePeriod.mthsLower = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRatePeriodMthsUpper' : {
                    currentOdRatePeriod.mthsUpper = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate.OdRateTierLower' : {
                    currentOdRate.tierLower = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate.OdRateTierUpper' : {
                    currentOdRate.tierUpper = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate.OdRateYly' : {
                    currentOdRate.yly = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate.OdRateYlyType' : {
                    currentOdRate.ylyType = text;
                    break;
                }
                case BASE_ELEMENT + '.OdRateSet.OdRateType.OdRatePeriod.OdRate.OdRateType' : {
                    currentOdRate.type = text;
                    break;
                }
                case BASE_ELEMENT + '.ImageURL' : {
                    currentAccount.imageURL = text;
                    break;
                }
                default: {
                    break;
                }
            }
        });

        cb.onEndDocument(function () {
            console.log('Finished parsing current accounts file.');
            this.emit('end');
        }.bind(this));
    }.bind(this));

    this.internals.parser = parser;

    Fs.stat(filePath, function (err) {
        if (!err) {
            parser.parseFile(filePath);
        } else {
            console.log('Error getting stats for file: ', filePath);
            this.emit('error', err);
        }
    }.bind(this));
};

module.exports = CurrentAccountsParser;
