var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    Path = require('path'),
    util = require('util'),
    xml = require('node-xml');

var BASE_ELEMENT = 'Cards.Card';

var CreditCardParser = function () {
    this.internals = {};
    EventEmitter.call(this);
};

util.inherits(CreditCardParser, EventEmitter);

CreditCardParser.prototype.stop = function () {
    this.internals.parser.pause();
};

CreditCardParser.prototype.resume = function () {
    this.internals.parser.resume();
};

CreditCardParser.prototype.parse = function (filePath, fcaData) {
    console.log('Started parsing credit cards file.');

    var moneyFactsIds = fcaData.map(function(company){
        return company.moneyFactsID;
    });
    
    var parser = new xml.SaxParser(function (cb) {
        var currentElement = '',
            currentCreditCard = {};

        cb.onError(function (error) {
            this.emit('error', error);
        }.bind(this));

        cb.onStartElementNS(function (name, attrs) {
            if (name === 'Card') {
                currentCreditCard = {};

                for (var i = attrs.length - 1; i >= 0; i--) {
                    var attr = attrs[i];

                    if (attr[0] === 'MoneyfactsID') {
                        currentCreditCard.moneyFactsID = attr[1];
                    }
                }
            }

            // create any objects or arrays
            if (name === 'PerAppCriteria' && currentElement === BASE_ELEMENT) {
                currentCreditCard.perAppCriteria = currentCreditCard.perAppCriteria || {};
            } else if (name === 'Rates') {
                currentCreditCard.rates = currentCreditCard.rates || {};
            } else if (name === 'Introductory' && currentElement === BASE_ELEMENT + '.Rates') {
                currentCreditCard.rates.introductory = currentCreditCard.rates.introductory || {};
            } else if (name === 'IntroRate' && currentElement === BASE_ELEMENT + '.Rates.Introductory') {
                currentCreditCard.rates.introductory.introRates = currentCreditCard.rates.introductory.introRates || [];
                currentCreditCard.rates.introductory.introRates.push({});
            } else if (name === 'Standard' && currentElement === BASE_ELEMENT + '.Rates') {
                currentCreditCard.rates.standard = currentCreditCard.rates.standard || {};
            } else if (name === 'StdRate' && currentElement === BASE_ELEMENT + '.Rates.Standard') {
                currentCreditCard.rates.standard.stdRates = currentCreditCard.rates.standard.stdRates || [];
                currentCreditCard.rates.standard.stdRates.push({});
            } else if (name === 'BalanceTransfers' && currentElement === BASE_ELEMENT + '.Rates') {
                currentCreditCard.rates.balanceTransfers = currentCreditCard.rates.balanceTransfers || {};
            } else if (name === 'TrfRate' && currentElement === BASE_ELEMENT + '.Rates.BalanceTransfers') {
                currentCreditCard.rates.balanceTransfers.trfRate
                    = currentCreditCard.rates.balanceTransfers.trfRate || {};
            } else if (name === 'IntroTrf' && currentElement === BASE_ELEMENT + '.Rates.BalanceTransfers') {
                currentCreditCard.rates.balanceTransfers.introTrf
                    = currentCreditCard.rates.balanceTransfers.introTrf || {};
            } else if (name === 'ITrfUpfrontFees' && currentElement === BASE_ELEMENT + '.Rates.BalanceTransfers.IntroTrf') {
                currentCreditCard.rates.balanceTransfers.introTrf.iTrfUpfrontFees
                    = currentCreditCard.rates.balanceTransfers.introTrf.iTrfUpfrontFees || {};
            } else if (name === 'ITrfRate' && currentElement === BASE_ELEMENT + '.Rates.BalanceTransfers.IntroTrf') {
                currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates
                    = currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates || [];
                currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates.push({});
            } else if (name === 'Fee' && currentElement === BASE_ELEMENT) {
                currentCreditCard.fee = currentCreditCard.fee || {};
            } else if (name === 'Charge' && currentElement === BASE_ELEMENT + '.Charges') {
                currentCreditCard.charges = currentCreditCard.charges || [];
                currentCreditCard.charges.push({});
            } else if (name === 'Benefit' && currentElement === BASE_ELEMENT + '.Benefits') {
                currentCreditCard.benefits = currentCreditCard.benefits || [];
                currentCreditCard.benefits.push({});
            } else if (name === 'Loyalty' && currentElement === BASE_ELEMENT) {
                currentCreditCard.loyalty = currentCreditCard.loyalty || {};
            } else if (name === 'BillPayment' && currentElement === BASE_ELEMENT) {
                currentCreditCard.billPayment = currentCreditCard.billPayment || {};
            }

            if (currentElement) {
                currentElement += '.';
            }

            currentElement += name;
        });

        cb.onEndElementNS(function (name) {
            if (name === 'Card') {
                // only add the loan to the list if its company number is in the fca data
                // if(moneyFactsIds.indexOf(currentCreditCard.companyNo) > -1) {
                    this.emit('creditCard', currentCreditCard);
                // }
            }

            currentElement = currentElement.substring(0, currentElement.lastIndexOf(name));

            if (currentElement) {
                currentElement = currentElement.substring(0, currentElement.lastIndexOf('.'));
            }
        }.bind(this));

        cb.onCharacters(function (text) {
            if (!text || !(/[^\s]/).test(text)) {
                return;
            }

            // &amp; in xml must be handled like so:
            // obj.param = obj.param ? obj.param + text : text; // this is to accommodate &amp;
            switch (currentElement) {
                case BASE_ELEMENT + '.Company' : {
                    currentCreditCard.companyName
                        = currentCreditCard.companyName
                        ? currentCreditCard.companyName + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.CompNo' : {
                    currentCreditCard.companyNo = text;
                    break;
                }
                case BASE_ELEMENT + '.CardName' : {
                    currentCreditCard.cardName = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.InvitationOnlyTick' : {
                    currentCreditCard.perAppCriteria.invitationOnlyTick = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.ManagersDiscretionTick' : {
                    currentCreditCard.perAppCriteria.managersDiscretionTick = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.StatusNote' : {
                    currentCreditCard.perAppCriteria.statusNote
                        = currentCreditCard.perAppCriteria.statusNote
                        ? currentCreditCard.perAppCriteria.statusNote + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.ExistCustTick' : {
                    currentCreditCard.perAppCriteria.existCustTick = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.ExistCustNote' : {
                    currentCreditCard.perAppCriteria.existCustNote
                        = currentCreditCard.perAppCriteria.existCustNote
                        ? currentCreditCard.perAppCriteria.existCustNote + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.MemberTick' : {
                    currentCreditCard.perAppCriteria.memberTick = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.MemberNote' : {
                    currentCreditCard.perAppCriteria.memberNote
                        = currentCreditCard.perAppCriteria.memberNote
                        ? currentCreditCard.perAppCriteria.memberNote + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.MinAgeYrs' : {
                    currentCreditCard.perAppCriteria.minAgeYrs = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.PremierCardTick' : {
                    currentCreditCard.perAppCriteria.premierCardTick = text;
                    break;
                }
                case BASE_ELEMENT + '.PerAppCriteria.CreditRepairCardTick' : {
                    currentCreditCard.perAppCriteria.creditRepairCardTick = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Introductory.IntroRate.RateType' : {
                    currentCreditCard.rates.introductory
                        .introRates[currentCreditCard.rates.introductory.introRates.length - 1].rateType = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Introductory.IntroRate.RatePeriodMths' : {
                    currentCreditCard.rates.introductory
                        .introRates[currentCreditCard.rates.introductory.introRates.length - 1].ratePeriodMths = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Introductory.IntroRate.PARate' : {
                    currentCreditCard.rates.introductory
                        .introRates[currentCreditCard.rates.introductory.introRates.length - 1].paRate = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Standard.StdRate.RateType' : {
                    currentCreditCard.rates.standard
                        .stdRates[currentCreditCard.rates.standard.stdRates.length - 1].rateType = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Standard.StdRate.PARate' : {
                    currentCreditCard.rates.standard
                        .stdRates[currentCreditCard.rates.standard.stdRates.length - 1].paRate = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Standard.StdRate.APR' : {
                    currentCreditCard.rates.standard
                        .stdRates[currentCreditCard.rates.standard.stdRates.length - 1].apr = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.BalanceTransfers.TrfRate.PARate' : {
                    currentCreditCard.rates.balanceTransfers.trfRate.paRate = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.BalanceTransfers.ITrfUpfrontFees.FeePct' : {
                    currentCreditCard.rates.balanceTransfers.iTrfUpfrontFees.feePct = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.BalanceTransfers.IntroTrf.ITrfRate.RateType' : {
                    currentCreditCard.rates.balanceTransfers
                        .introTrf.iTrfRates[currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates.length - 1]
                        .rateType = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.BalanceTransfers.IntroTrf.ITrfRate.PARate' : {
                    currentCreditCard.rates.balanceTransfers
                        .introTrf.iTrfRates[currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates.length - 1]
                        .paRate = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.BalanceTransfers.IntroTrf.ITrfRate.RatePeriodMths' : {
                    currentCreditCard.rates.balanceTransfers
                        .introTrf.iTrfRates[currentCreditCard.rates.balanceTransfers.introTrf.iTrfRates.length - 1]
                        .ratePeriodMths = text;
                    break;
                }
                case BASE_ELEMENT + '.Charges.Charge.Type' : {
                    currentCreditCard.charges[currentCreditCard.charges.length - 1].type = text;
                    break;
                }
                case BASE_ELEMENT + '.Charges.Charge.Detail' : {
                    currentCreditCard.charges[currentCreditCard.charges.length - 1].detail = text;
                    break;
                }
                case BASE_ELEMENT + '.ImageURL' : {
                    currentCreditCard.imageURL = text;
                    break;
                }
                case BASE_ELEMENT + '.Fee.FeeDetail' : {
                    currentCreditCard.fee.feeDetail = text;
                    break;
                }
                case BASE_ELEMENT + '.Benefits.Benefit.Type' : {
                    currentCreditCard.benefits[currentCreditCard.benefits.length - 1].type = text;
                    break;
                }
                case BASE_ELEMENT + '.Benefits.Benefit.Note' : {
                    currentCreditCard.benefits[currentCreditCard.benefits.length - 1].note
                        = currentCreditCard.benefits[currentCreditCard.benefits.length - 1].note
                        ? currentCreditCard.benefits[currentCreditCard.benefits.length - 1].note + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.Loyalty.LoyaltyScheme' : {
                    currentCreditCard.loyalty.loyaltyScheme = text;
                    break;
                }
                case BASE_ELEMENT + '.Loyalty.LoyaltyNote' : {
                    currentCreditCard.loyalty.loyaltyNote
                        = currentCreditCard.loyalty.loyaltyNote
                        ? currentCreditCard.loyalty.loyaltyNote + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.BillPayment.MinPayMthNote' : {
                    currentCreditCard.billPayment.minPayMthNote
                        = currentCreditCard.billPayment.minPayMthNote
                        ? currentCreditCard.billPayment.minPayMthNote + text : text; // this is to accommodate &amp;
                    break;
                }
                case BASE_ELEMENT + '.BillPayment.RepaymentNote' : {
                    currentCreditCard.billPayment.repaymentNote
                        = currentCreditCard.billPayment.repaymentNote
                        ? currentCreditCard.billPayment.repaymentNote + text : text; // this is to accommodate &amp;
                    break;
                }
                default: {
                    break;
                }
            }
        });

        cb.onEndDocument(function () {
            console.log('Finished parsing credit card file.');
            this.emit('end');
        }.bind(this));
    }.bind(this));

    this.internals.parser = parser;

    Fs.stat(filePath, function (err) {
        if (!err) {
            parser.parseFile(filePath);
        } else {
            console.log('Error getting stats for file: ', filePath);
            this.emit('error', err);
        }
    }.bind(this));
};

module.exports = CreditCardParser;
