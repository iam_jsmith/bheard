var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    Path = require('path'),
    util = require('util'),
    xml = require('node-xml');

var BASE_ELEMENT = 'UnsecuredLoans.UnsecuredLoan';

var UnsecuredLoansParser = function () {
    this.internals = {};
    EventEmitter.call(this);
};

util.inherits(UnsecuredLoansParser, EventEmitter);

UnsecuredLoansParser.prototype.stop = function () {
    this.internals.parser.pause();
};

UnsecuredLoansParser.prototype.resume = function () {
    this.internals.parser.resume();
};

UnsecuredLoansParser.prototype.parse = function (filePath, fcaData) {
    console.log('Started parsing unsecured loans file.');

    var moneyFactsIds = fcaData.map(function(company){
        return company.moneyFactsID;
    });

    var parser = new xml.SaxParser(function (cb) {
        var currentElement = '',
            currentLoan = {};

        cb.onError(function (error) {
            this.emit('error', error);
        }.bind(this));

        cb.onStartElementNS(function (name, attrs) {
            if (name === 'UnsecuredLoan') {
                currentLoan = {};

                for (var i = attrs.length - 1; i >= 0; i--) {
                    var attr = attrs[i];

                    if (attr[0] === 'MoneyfactsID') {
                        currentLoan.moneyFactsID = attr[1];
                    }
                }
            }

            // create any objects or arrays
            if (name === 'Rates') {
                currentLoan.rates = currentLoan.rates || {};
            } else if (name === 'Rate' && currentElement === BASE_ELEMENT + '.Rates') {
                currentLoan.rates.rates = currentLoan.rates.rates || [];
                currentLoan.rates.rates.push({});
            } else if (name === 'Example' && currentElement === BASE_ELEMENT + '.Rates') {
                currentLoan.rates.examples = currentLoan.rates.examples || [];
                currentLoan.rates.examples.push({});
            } else if (name === 'Fees' && currentElement === BASE_ELEMENT) {
                currentLoan.fees = currentLoan.fees || [];
                currentLoan.fees.push({});
            } else if (name === 'Features' && currentElement === BASE_ELEMENT) {
                currentLoan.features = currentLoan.features || [];
                currentLoan.features.push({});
            } else if (name === 'Usages' && currentElement === BASE_ELEMENT) {
                currentLoan.usages = currentLoan.usages || [];
                currentLoan.usages.push({});
            }

            if (currentElement) {
                currentElement += '.';
            }

            currentElement += name;
        });

        cb.onEndElementNS(function (name) {
            if (name === 'UnsecuredLoan') {
                // only add the loan to the list if its company number is in the fca data
                // if(moneyFactsIds.indexOf(currentLoan.companyNo) > -1) {
                    this.emit('unsecuredLoans', currentLoan); // unsecured loans (pl) because this was the name of the subsector instead of singular like the other types #consistencyFTW
                // }
            }

            currentElement = currentElement.substring(0, currentElement.lastIndexOf(name));

            if (currentElement) {
                currentElement = currentElement.substring(0, currentElement.lastIndexOf('.'));
            }
        }.bind(this));

        cb.onCharacters(function (text) {
            if (!text || !(/[^\s]/).test(text)) {
                return;
            }

            // obj.param = obj.param ? obj.param + text : text; // this is to accommodate &amp;
            switch (currentElement) {
                case BASE_ELEMENT + '.Company' : {
                    currentLoan.companyName = currentLoan.companyName ? currentLoan.companyName + text : text;
                    break;
                }
                case BASE_ELEMENT + '.CompNo' : {
                    currentLoan.companyNo = text;
                    break;
                }
                case BASE_ELEMENT + '.LoanName' : {
                    currentLoan.loanName = text;
                    break;
                }
                case BASE_ELEMENT + '.LoanType' : {
                    currentLoan.loanType = text;
                    break;
                }
                case BASE_ELEMENT + '.LoanRateType' : {
                    currentLoan.loanRateType = text;
                    break;
                }
                case BASE_ELEMENT + '.CustomerType' : {
                    currentLoan.customerType = text;
                    break;
                }
                case BASE_ELEMENT + '.AcceptanceCriteriaNote' : {
                    currentLoan.acceptanceCriteriaNote = text;
                    break;
                }
                case BASE_ELEMENT + '.MinAgeYrs' : {
                    currentLoan.minAgeYrs = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.RepAPRAdvertised' : {
                    currentLoan.rates.repAPRAdvertised = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.APRAdvertisedAdvMin' : {
                    currentLoan.rates.aprAdvertisedAdvMin = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.APRAdvertisedAdvMax' : {
                    currentLoan.rates.aprAdvertisedAdvMax = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Rate.AdvanceMin' : {
                    currentLoan.rates.rates[currentLoan.rates.rates.length - 1].advanceMin = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Rate.AdvanceMax' : {
                    currentLoan.rates.rates[currentLoan.rates.rates.length - 1].advanceMax = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Rate.RepresentativeAPR' : {
                    currentLoan.rates.rates[currentLoan.rates.rates.length - 1].apr = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleDescription' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleDescription = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleAdvance' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleAdvance = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleTermDays' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleTermDays = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleAnnualFlatRate' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleAnnualFlatRate = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleRepresentativeAPR' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleRepresentativeAPR = text;
                    break;
                }
                case BASE_ELEMENT + '.Rates.Example.ExampleMonthlyPayment' : {
                    currentLoan.rates.examples[currentLoan.rates.examples.length - 1].exampleMonthlyPayment = text;
                    break;
                }
                case BASE_ELEMENT + '.ImageURL' : {
                    currentLoan.imageURL = text;
                    break;
                }
                case BASE_ELEMENT + '.RepaymentChangeableTick' : {
                    currentLoan.repaymentChangeableTick = text;
                    break;
                }
                case BASE_ELEMENT + '.LumpSumPaymentTick' : {
                    currentLoan.lumpSumPaymentTick = text;
                    break;
                }
                case BASE_ELEMENT + '.RepaymentHolidayTick' : {
                    currentLoan.repaymentHolidayTick = text;
                    break;
                }
                case BASE_ELEMENT + '.Fees.Fee.FeeType' : {
                    currentLoan.fees[currentLoan.fees.length - 1].feeType = text;
                    break;
                }
                case BASE_ELEMENT + '.Fees.Fee.FeeAmtMin' : {
                    currentLoan.fees[currentLoan.fees.length - 1].feeAmtMin = text;
                    break;
                }
                case BASE_ELEMENT + '.Fees.Fee.FeePct' : {
                    currentLoan.fees[currentLoan.fees.length - 1].feePct = text;
                    break;
                }
                case BASE_ELEMENT + '.Fees.Fee.FeePartOfMlyInstalTick' : {
                    currentLoan.fees[currentLoan.fees.length - 1].feePartOfMlyInstalTick = text;
                    break;
                }
                case BASE_ELEMENT + '.Features.Feature.FeatureType' : {
                    currentLoan.features[currentLoan.features.length - 1].featureType = text;
                    break;
                }
                case BASE_ELEMENT + '.Features.Feature.LoanSpecificFeatureTick' : {
                    currentLoan.features[currentLoan.features.length - 1].loanSpecificFeatureTick = text;
                    break;
                }
                case BASE_ELEMENT + '.Usages.Usage.UsageType' : {
                    currentLoan.usages[currentLoan.usages.length - 1].usageType = text;
                    break;
                }
                default: {
                    break;
                }
            }
        });

        cb.onEndDocument(function () {
            console.log('Finished parsing unsecured loans file.');
            this.emit('end');
        }.bind(this));
    }.bind(this));

    this.internals.parser = parser;

    Fs.stat(filePath, function (err) {
        if (!err) {
            parser.parseFile(filePath);
        } else {
            console.log('Error getting stats for file: ', filePath);
            this.emit('error', err);
        }
    }.bind(this));
};

module.exports = UnsecuredLoansParser;
