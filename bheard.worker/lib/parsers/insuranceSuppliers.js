var EventEmitter = require('events').EventEmitter,
    Fs = require('fs'),
    LineByLineReader = require('line-by-line'),
    Path = require('path'),
    util = require('util');

var InsuranceParser = function () {
    EventEmitter.call(this);
};

util.inherits(InsuranceParser, EventEmitter);

InsuranceParser.prototype.parse = function (filePath) {
    console.log('Started parsing insurance suppliers');
    var self = this;

    Fs.stat(filePath, function (err) {
        if (!err) {
            var lr = new LineByLineReader(filePath),
                i = 0,
                insurers = [];

            lr.on('line', function (line) {
                if (i > 0) {
                    var items = line.split(',');
                    if (items.length > 1) {
                        var company = {
                            no: String(items[0]),
                            sector: 'insurance',
                            name: items[1],
                            brand: items[2],
                            subSector: String(items[3]).toLowerCase(),
                            logoUrl: items[8],
                            trustPilotUrl: items[9],
                            whichUrl: items[10],
                            twitterTimelineId: String(items[11]),
                            compareSiteUrl: items[13],
                            // items 14 is ignored: items[14],
                            affiliateUrl: items[15]
                        };

                        // todo add desired values
                        // set to 0 if not there, or causes undefined values to be created (todo may need to change this)
                        var trust = items[4] && items[4] !== 'TBC' ? parseFloat(items[4]) ? parseFloat(items[4]) * 100 : null : null;
                        var experience = items[5] && items[5] !== 'TBC' ? parseFloat(items[5]) ? parseFloat(items[5]) : null : null;
                        var valueForMoney = items[6] && items[6] !== 'TBC' ? parseFloat(items[6]) ? parseFloat(items[6]) * 100 : null : null;

                        var score = {};
                        var scoreCount = 0;
                        var total = 0;

                        if (trust) {
                            score.customerServicePercent = trust;
                            total += trust;
                            scoreCount++;
                        } else {
                            score.customerServicePercent = null;
                        }

                        if (experience) {
                            score.customerOpinionPercent = experience;
                            total += experience;
                            scoreCount++;
                        } else {
                            score.customerOpinionPercent = null;
                        }

                        if (valueForMoney) {
                            score.valueForMoneyPercent = valueForMoney;
                            total += valueForMoney;
                            scoreCount++;
                        } else {
                            score.valueForMoneyPercent = null;
                        }

                        // only calculate overall opinion percent if all the other values exist
                        // TO DO THIS DOES NOT SEEM TO PUSH OVER NULL VALUES
                        if (scoreCount > 0) {
                            score.overallOpinionPercent = total / scoreCount;
                        }

                        company.score = score;
                        insurers.push(company);
                    }
                }

                i++;
            });

            lr.on('end', function () {
                console.log('Finished parsing file');
                self.emit('end', insurers);
            });

            lr.on('error', function (err) {
                self.emit('error', err);
            });
        } else {
            self.emit('error', err);
        }
    });
};

module.exports = InsuranceParser;
