var fs = require('fs'),
    _ = require('lodash'),
    parse = require('csv-parse'),
    q = require('q');

var CharitiesParser = function () {};

CharitiesParser.prototype = {
    parse: function (filePath) {
        var deferred = q.defer();

        console.log('Started parsing file.');

        var input = fs.createReadStream(filePath),
            parser = parse({ auto_parse: false, trim: true }),
            charities = [],
            i = 0;

        parser.on("readable", function (){
            var record;

            while (record = parser.read()) {
                if (i !== 0) {
                    var charity = {
                        no: record[0] || null,
                        name: record[1],
                        sector: record[2].toLowerCase()
                    };

                    if (charity.no) {
                        charities.push(charity);
                    }
                }

                i++;
            }
        });

        parser.on('finish', function (){
            console.log('Finished parsing file.');
            deferred.resolve(charities);
        });

        parser.on('error', function (err){
            deferred.reject(err);
        });

        input.pipe(parser);

        return deferred.promise;
    }
};

module.exports = CharitiesParser;
