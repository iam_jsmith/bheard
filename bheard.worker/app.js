#! /usr/bin/env node

var async = require('async'),
    Bossy = require('bossy'),
    db = require('bheard').db(),
    path = require('path'),
    _ = require('lodash'),
    filesPath = path.join(__dirname, 'data'),
    moneyfactsDir = 'moneyfacts',
    otherDir = 'csvs',
    schedule = require('node-schedule');

// Builders, Parsers and Importers
var BankScoresBuilder = require('./lib/builders/bankscores'),
    BYOBImporter = require('./lib/importers/byob'),
    CharitiesImporter = require('./lib/importers/charities'),
    CouncilsImporter = require('./lib/importers/councils'),
    CreditCardsProductsParser = require('./lib/parsers/creditCardsProducts'),
    CreditCardsSuppliersParser = require('./lib/parsers/creditCardsSuppliers'),
    CurrentAccountsSuppliersParser = require('./lib/parsers/currentAccountsSuppliers'),
    CurrentAccountsProductsParser = require('./lib/parsers/currentAccountsProducts'),
    CurrentAccountsRankings = require('./lib/builders/currentaccountsrankings'),
    EnergySuppliersImporter = require('./lib/importers/energySuppliers'),
    InsuranceSuppliersImporter = require('./lib/importers/insuranceSuppliers'),
    MediaImporter = require('./lib/importers/media'),
    MoneyImporter = require('./lib/importers/moneyImporter'),
    MoneyRanker = require('./lib/builders/moneyRanker'),
    RankingsBuilder = require('./lib/builders/rankings'),
    RatingsBuilder = require('./lib/builders/ratings'),
    ReviewsBuilder = require('./lib/builders/reviews'),
    SavingsAccountsProductsParser = require('./lib/parsers/savingsAccountsProducts'),
    SavingsAccountsSuppliersParser = require('./lib/parsers/savingsAccountsSuppliers'),
    SchoolsImporter = require('./lib/importers/schools'),
    SuppliersImporter = require('./lib/importers/suppliers'),
    SurgeriesImporter = require('./lib/importers/surgeries'),
    UnsecuredLoansProductsParser = require('./lib/parsers/unsecuredLoansProducts'),
    UnsecuredLoansSuppliersParser = require('./lib/parsers/unsecuredLoansSuppliers'),
    WhitelistParser = require('./lib/parsers/whitelist');

// Files
var byobFile = path.join(filesPath, otherDir + '/byob-data.csv'),
    initialCharitiesFile = path.join(filesPath, otherDir + '/Initial_charities.csv'),
    governmentFile = path.join(filesPath, otherDir + '/government.csv'),
    creditCardsProductsFile = path.join(filesPath, moneyfactsDir + '/VochoCards_9_0.xml'),
    creditCardsSuppliersDataFile = path.join(filesPath, otherDir + '/credit-cards-suppliers-whitelist.csv'), // fca data & scores
    creditCardsWhitelistFile = path.join(filesPath, otherDir + '/credit-cards-products-whitelist.csv'),
    currentAccountsProductsFile = path.join(filesPath, moneyfactsDir + '/VochoCurrentAccounts_10_0.xml'),
    currentAccountsSuppliersDataFile = path.join(filesPath, otherDir + '/current-accounts-suppliers-whitelist.csv'), // fca data & scores
    currentAccountsWhitelistFile = path.join(filesPath, otherDir + '/current-accounts-products-whitelist.csv'),
    energySuppliersFile = path.join(filesPath, otherDir + '/energy-suppliers.csv'),
    energySuppliersScoresFile = path.join(filesPath, otherDir + '/energy-norm.csv'),
    insuranceSuppliersFile = path.join(filesPath, otherDir + '/insurance-suppliers.csv'),
    mediaSuppliersFile = path.join(filesPath, otherDir + '/media.csv'),
    savingsAccountsProductsFile = path.join(filesPath, moneyfactsDir + '/VochoSavings_8_0.xml'),
    savingsAccountsSuppliersDataFile = path.join(filesPath, otherDir + '/savings-accounts-suppliers-whitelist.csv'), // fca data & scores
    savingsAccountsWhitelistFile = path.join(filesPath, otherDir + '/savings-accounts-products-whitelist.csv'),
    schoolsFile = path.join(filesPath, otherDir + '/schools-combined.csv'),
    suppliersPrivateSectorFile = path.join(filesPath, otherDir + '/SupplierListPrivateSector.csv'),
    suppliersPublicSectorFile = path.join(filesPath, otherDir + '/SupplierListPublicSector.csv'),
    surgeriesFile = path.join(filesPath, otherDir +'/GP-data-final.csv'),
    unsecuredLoansProductsFile = path.join(filesPath, moneyfactsDir + '/VochoUnsecuredLoans_7_1.xml'),
    unsecuredLoansSuppliersDataFile = path.join(filesPath, otherDir + '/unsecured-loans-suppliers-whitelist.csv'), // fca data & scores
    unsecuredLoansWhitelistFile = path.join(filesPath, otherDir + '/unsecured-loans-products-whitelist.csv');

var moneyVariables = {
        creditCards: {
            database: 'creditCards',
            type: 'creditCard', // used for subSector and for emit event from the parser // todo check working properly
            rankingVariables: {
                // e.g. distribution / divide covers all standard purchase rates from 0.1 to 100%
                distribution: 1000, // incremental steps for distribution lookup table
                divide: 10, // size of each increment in the distribution lookup
                cdf: 3 // number of digits in interest rate
            }
        },
        currentAccounts: {
            database: 'currentAccounts',
            type: 'currentAccount',
            rankingVariables: {
                maxDuration: 24,
                monthsPerYear: 12,
                // e.g. distribution / divide covers all inCredit rates from 0.1 to 100% (todo and odRates)
                distribution: 10000, // incremental steps for distribution lookup table
                divide: 100, // size of each increment in the distribution lookup
                cdf: 4 // number of digits in interest rate
            }
        },
        savingsAccounts: {
            database: 'savings',
            type: 'savingsAccount',
            rankingVariables: {
                // e.g. distribution / divide covers all gross AER rates from 0.1 to 100%
                distribution: 10000, // incremental steps for distribution lookup table
                divide: 100, // size of each increment in the distribution lookup
                cdf: 4 // number of digits in interest rate
            }
        },
        unsecuredLoans: {
            database: 'unsecuredLoans',
            type: 'unsecuredLoans',
            rankingVariables: {
                // e.g. distribution / divide covers all rep APR rates from 0.1 to 100%
                // todo change this (maybe wonga)
                distribution: 3010, // incremental steps for distribution lookup table
                divide: 10, // size of each increment in the distribution lookup
                cdf: 3 // number of digits in interest rate
            }
        }
};

// used to make sure all tasks run
var runningAllScript = false;

// This will run the jobs in the correct order
// TODO: FROM BYOB TO SURGERIES HASN'T BEEN CHECKED, BUT SHOULD WORK IN THIS ORDER
var orderedJobs = [
    'suppliersprivate',
    'supplierspublic',
    'byob',
    'charities',
    'councils',
    'energysuppliers',
    'insurancesuppliers',
    'media',
    'schools',
    'surgeries',
    'creditcards',
    'currentaccounts',
    'savingsaccounts',
    'unsecuredloans',
    'reviews',
    'ratings',
    'rankings',
    'creditcardsrankings',
    'savingsaccountsrankings',
    'unsecuredloansrankings',
    'currentaccountsrankings',
    'bankscores'
];

// useful for debugging the money section
var moneyJobs = [
    'suppliersprivate',
    'supplierspublic',
    'creditcards',
    'currentaccounts',
    'savingsaccounts',
    'unsecuredloans',
    'reviews',
    'ratings',
    'rankings',
    'creditcardsrankings',
    'savingsaccountsrankings',
    'unsecuredloansrankings',
    'currentaccountsrankings',
    'bankscores'
];

var definition = {
    h: {
        description: 'Show help',
        alias: 'help',
        type: 'boolean'
    },
    c: {
        description: 'Schedule cron jobs',
        alias: 'cron',
        type: 'boolean'
    },
    j: {
        description: 'Job type to run',
        alias: 'job',
        valid: orderedJobs.concat([
            'all',
            'money'
        ])
    }
};

var args = Bossy.parse(definition);

/**
 * Bind causes 'this' to be the 'done' callback which is passed to the async library in the all task
 * This is instead of changing all the import scripts
 */
var importResponseHandler = function importResponseHandler (err) {
    if (!err) {
        if(!runningAllScript) {
            console.log('--- All Tasks Completed ---');
            process.exit();
        } else {
            this(); // this seems a hacky, but is used so async works. this() is the done function passed to async.each
        }
    } else {
        console.error('Error:', err);
        process.exit(err);
    }
};

if (args instanceof Error) {
    console.error(args.message);
} else if (args.h || (!args.j && !args.c)) {
    // display how to use example on command line
    console.log(Bossy.usage(definition, '-j currentaccounts'));
    process.exit();
} else if (args.c) {
    schedule.scheduleJob('0 0 * * *', function () {
        async.series([
            function (callback) {
                var rankingsBuilder = new RankingsBuilder(db);

                rankingsBuilder.build(callback);
            }
        ]);
    });

    schedule.scheduleJob('*/5 * * * *', function () {
        async.series([
            function (callback) {
                var reviewsBuilder = new ReviewsBuilder(db);

                reviewsBuilder.build(callback);
            },
            function (callback) {
                var ratingsBuilder = new RatingsBuilder(db);

                ratingsBuilder.build(callback);
            }
        ]);
    });
} else if (args.j) {
    if (args.j === 'all') {
        runningAllScript = true;

        // make sure that each task finishes before running the next one
        async.eachSeries(orderedJobs, function(item, done) {
            handleImport(item, done);
        }, function (err) {
            runningAllScript = false;
            importResponseHandler(err);
        });
    } else if (args.j === 'money') {
        runningAllScript = true;

        // make sure that each task finishes before running the next one
        async.eachSeries(moneyJobs, function(item, done) {
            handleImport(item, done);
        }, function (err) {
            runningAllScript = false;
            importResponseHandler(err);
        });
    } else {
        handleImport(args.j);
    }
}

/**
 * Handle Import
 * Command line tasks
 * 
 * @param arg
 * @param done
 */
function handleImport(arg, done) {
    console.log('--- Running ' + arg + ' task ---');
    switch (arg) {
        case 'bankscores':
            var bankScores = new BankScoresBuilder(db);
            bankScores.build(importResponseHandler.bind(done));
            break;
        case 'byob':
            var byobImporter = new BYOBImporter(db);
            byobImporter.import(byobFile, importResponseHandler.bind(done));
            break;
        case 'charities':
            var charitiesImporter = new CharitiesImporter(db);
            charitiesImporter.import(initialCharitiesFile, importResponseHandler.bind(done));
            break;
        case 'councils':
            var councilsImporter = new CouncilsImporter(db);
            councilsImporter.import(governmentFile, importResponseHandler.bind(done));
            break;
        case 'creditcards':
            var creditCardsImporter = new MoneyImporter(
                    db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                    WhitelistParser);
            creditCardsImporter.import(
                creditCardsProductsFile, creditCardsSuppliersDataFile, creditCardsWhitelistFile, importResponseHandler.bind(done));
            break;
        case 'creditcardsrankings':
            var creditCardRanker = new MoneyRanker(db, moneyVariables.creditCards);
            db.get(moneyVariables.creditCards.database).find({fcaProduct:true, deleted:false}, function (err, cards) {
                if (err) {
                    console.log(err);
                }
                creditCardRanker.generateRankings(cards, importResponseHandler.bind(done));
            });
            break;
        case 'currentaccounts':
            var currentAccountsImporter = new MoneyImporter(
                    db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                    WhitelistParser);
            currentAccountsImporter.import(
                currentAccountsProductsFile, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                importResponseHandler.bind(done));
            break;
        case 'currentaccountsrankings':
            // this has its own ranking script
            var currentAccountRanking = new CurrentAccountsRankings(db, moneyVariables.currentAccounts);
            db.get(moneyVariables.currentAccounts.database)
                .find({fcaProduct:true, deleted:false}, function (err, accounts) {
                    if (err) {
                        console.log(err);
                    }
                    currentAccountRanking.generateRankings(accounts, importResponseHandler.bind(done));
                });
            break;
        case 'energysuppliers':
            var energySuppliersImporter = new EnergySuppliersImporter(db);
            energySuppliersImporter.import(energySuppliersFile, energySuppliersScoresFile, importResponseHandler.bind(done));
            break;
        case 'insurancesuppliers':
            var insuranceSuppliersImporter = new InsuranceSuppliersImporter(db);
            insuranceSuppliersImporter.import(insuranceSuppliersFile, importResponseHandler.bind(done));
            break;
        case 'media':
            var mediaImporter = new MediaImporter(db);
            mediaImporter.import(mediaSuppliersFile, importResponseHandler.bind(done));
            break;
        case 'rankings':
            var rankingsBuilder = new RankingsBuilder(db);
            rankingsBuilder.build(importResponseHandler.bind(done));
            break;
        case 'ratings':
            var ratingsBuilder = new RatingsBuilder(db);
            ratingsBuilder.build(importResponseHandler.bind(done));
            break;
        case 'reviews':
            var reviewsBuilder = new ReviewsBuilder(db);
            reviewsBuilder.build(importResponseHandler.bind(done));
            break;
        case 'savingsaccounts':
            var savingsAccountsImporter = new MoneyImporter(
                    db, moneyVariables.savingsAccounts, SavingsAccountsProductsParser, SavingsAccountsSuppliersParser,
                    WhitelistParser);
            savingsAccountsImporter.import(
                savingsAccountsProductsFile, savingsAccountsSuppliersDataFile, savingsAccountsWhitelistFile,
                importResponseHandler.bind(done));
            break;
        case 'savingsaccountsrankings':
            var savingsAccountRanker = new MoneyRanker(db, moneyVariables.savingsAccounts);
            db.get(moneyVariables.savingsAccounts.database)
                .find({fcaProduct:true, deleted:false}, function (err, accounts) {
                    if (err) {
                        console.log(err);
                    }
                    savingsAccountRanker.generateRankings(accounts, importResponseHandler.bind(done));
                });
            break;
        case 'schools':
            var schoolsImporter = new SchoolsImporter(db);
            schoolsImporter.import(schoolsFile, importResponseHandler.bind(done));
            break;
        case 'suppliersprivate':
            var suppliersImporter = new SuppliersImporter(db);
            suppliersImporter.import(suppliersPrivateSectorFile, importResponseHandler.bind(done));
            break;
        case 'supplierspublic':
            var suppliersImporter = new SuppliersImporter(db);
            suppliersImporter.import(suppliersPublicSectorFile, importResponseHandler.bind(done));
            break;
        case 'surgeries':
            var surgeriesImporter = new SurgeriesImporter(db);
            surgeriesImporter.import(surgeriesFile, importResponseHandler.bind(done));
            break;
        case 'unsecuredloans':
            var unsecuredLoansImporter = new MoneyImporter(
                    db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                    WhitelistParser);
            unsecuredLoansImporter.import(
                unsecuredLoansProductsFile, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                importResponseHandler.bind(done));
            break;
        case 'unsecuredloansrankings':
            var unsecuredLoansRanker = new MoneyRanker(db, moneyVariables.unsecuredLoans);
            db.get(moneyVariables.unsecuredLoans.database)
                .find({fcaProduct:true, deleted:false}, function (err, loans) {
                    if (err) {
                        console.log(err);
                    }
                    unsecuredLoansRanker.generateRankings(loans, importResponseHandler.bind(done));
                });
            break;
        default:
            importResponseHandler('Invalid arguments passed.');
    }
}
