var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');

var EnergySupplierParser = require('../../lib/parsers/energysuppliers');

var lab = exports.lab = Lab.script();

lab.experiment('Energy Supplier Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new EnergySupplierParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('');
    });

    lab.test('it reports when it has parsed everything', function (done) {
        var parser = new EnergySupplierParser();

        parser.on('suppliers', function (energySuppliers) {
            Code.expect(energySuppliers).to.have.length(3);

            done();
        });

        parser.parse(Path.join(__dirname, 'threeenergysuppliers.csv'));
    });

    lab.test('it emits a new energy supplier when it finishes', function (done) {
        var parser = new EnergySupplierParser();

        parser.on('suppliers', function (energySuppliers) {
            var schema = Joi.object().keys({
                no: Joi.number(),
                name: Joi.string(),
                coal: Joi.number(),
                gas: Joi.number(),
                nuclear: Joi.number(),
                renewable: Joi.number(),
                other: Joi.number(),
                gas_provider: Joi.boolean(),
                electricity_provider: Joi.boolean(),
                dualfuel_provider: Joi.boolean(),
                logo_url: Joi.string()
            });

            Joi.validate(energySuppliers[0], schema, function (err, value) {
                if (err) {
                    console.log('Error validating loan', err);
                    throw err;
                } else {
                    Code.expect(err).to.not.exist();
                    done();
                }
            });
        });

        parser.parse(Path.join(__dirname, 'threeenergysuppliers.csv'));
    });
});
