var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');

var EnergySuppliersScoresParser = require('../../lib/parsers/energysuppliersscoresparser');

var lab = exports.lab = Lab.script();

lab.experiment('Energy Supplier Scores Parser', function () {
    lab.test('it ends immediately if there is no file to load', function (done) {
        var parser = new EnergySuppliersScoresParser();

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse('');
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new EnergySuppliersScoresParser();

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse(Path.join(__dirname, 'energy-norm.csv'));
    });

    lab.test('it returns valid normalised scores', function (done) {
        var parser = new EnergySuppliersScoresParser();

        parser.on('end', function (err, scores) {
            Code.expect(err).to.not.exist();

            var schema = Joi.array().items(
                Joi.object().keys({
                    companyNo: Joi.string().required(),
                    company: Joi.string().required(),
                    trustworthiness: Joi.number(),
                    personalExperience: Joi.number(),
                    valueForMoney: Joi.number()
                })
            );

            Joi.validate(scores, schema, function (err, value) {
                if (err) {
                    console.log('Error validating scores:', err);
                }

                Code.expect(err).to.not.exist();
            });

            done();
        });

        parser.parse(Path.join(__dirname, 'energy-norm.csv'));
    });
});
