var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var EnergySupplierImporter = require('../../lib/importers/energySuppliers');

var lab = exports.lab = Lab.script();

lab.experiment('Energy Supplier Importer', function () {
    var suppliersModel;
    var energySuppliersModel;

    lab.beforeEach(function (done) {
        suppliersModel = db.get('suppliers');
        suppliersModel.remove({}, function () {
            energySuppliersModel = db.get('suppliers');
            energySuppliersModel.remove({}, done);
        });
    });

    lab.afterEach(function (done) {
        suppliersModel.remove({}, function () {
            energySuppliersModel.remove({}, done);
        });
    });

    lab.test('it inserts a supplier record', function (done) {
        suppliersModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'singleenergysupplier.csv'),
                function (err) {
                if (err) {
                    console.log('Error importing:', err);
                }

                Code.expect(err).to.not.exist();

                suppliersModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate supplier record', function (done) {
        suppliersModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'duplicateenergysuppliers.csv'),
                function (err) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();

                suppliersModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    console.log(newSuppliers);
                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);

                    done();
                });
            });
        });
    });

    lab.test('it inserts a new energy supplier', function (done) {
        energySuppliersModel.find({}, function (err, energySuppliers) {
            if (err) {
                console.log(err);
            }

            Code.expect(err).to.not.exist();
            Code.expect(energySuppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'singleenergysupplier.csv'),
                function (err) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                energySuppliersModel.find({},
                    function (err, newEnergySuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newEnergySuppliers).to.have.length(1);

                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate energy supplier', function (done) {
        energySuppliersModel.find({}, function (err, energySuppliers) {
            if (err) {
                console.log(err);
            }

            Code.expect(err).to.not.exist();
            Code.expect(energySuppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'duplicateenergysuppliers.csv'),
                function (err) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                energySuppliersModel.find({},
                    function (err, newEnergySuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newEnergySuppliers).to.have.length(1);

                    done();
                });
            });
        });
    });

    lab.test('it inserts multiple energy suppliers', function (done) {
        energySuppliersModel.find({}, function (err, energySuppliers) {
            if (err) {
                console.log(err);
            }

            Code.expect(err).to.not.exist();
            Code.expect(energySuppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'threeenergysuppliers.csv'),
                function (err) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                energySuppliersModel.find({},
                    function (err, newEnergySuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newEnergySuppliers).to.have.length(3);

                    done();
                });
            });
        });
    });

    lab.test('it associates scoring with the energy supplier', function (done) {
        energySuppliersModel.find({}, function (err, energySuppliers) {
            if (err) {
                console.log(err);
            }

            Code.expect(err).to.not.exist();
            Code.expect(energySuppliers).to.be.empty();

            var importer = new EnergySupplierImporter(db);
            var suppliersFile = Path.join(__dirname, 'threeenergysuppliers.csv');
            var scoresFile = Path.join(__dirname, 'energy-norm.csv');

            importer.import(suppliersFile, scoresFile, function (err) {
                Code.expect(err).to.not.exist();

                energySuppliersModel.find({}, function (err, newEnergySuppliers) {

                    console.log(newEnergySuppliers);

                    Code.expect(err).to.not.exist();
                    Code.expect(newEnergySuppliers).to.have.length(3);
                    Code.expect(newEnergySuppliers[0].score).to.deep.equal({
                        customerServicePercent: 21.840804,
                        customerOpinionPercent: 65.226,
                        valueForMoneyPercent: 11.9819452,
                        overallOpinionPercent: 33.01624973333333
                    });
                    done();
                });
            });
        });
    });

    // lab.test('it removes unused suppliers', function (done) {
    //     energySuppliersModel.find({}, function (err, energySuppliers) {
    //         if (err) {
    //             console.log(err);
    //         }
    //
    //         Code.expect(err).to.not.exist();
    //         Code.expect(energySuppliers).to.have.length(0);
    //
    //         var importer = new EnergySupplierImporter(db);
    //         importer.import(Path.join(__dirname, 'threeenergysuppliers.csv'),
    //             function (err) {
    //             if (err) {
    //                 console.log(err);
    //             }
    //
    //             Code.expect(err).to.not.exist();
    //             energySuppliersModel.find({},
    //                 function (err, newEnergySuppliers) {
    //                 if (err) {
    //                     console.log(err);
    //                 }
    //
    //                 Code.expect(err).to.not.exist();
    //                 console.log('newEnergySuppliers', newEnergySuppliers);
    //                 Code.expect(newEnergySuppliers).to.have.length(3);
    //
    //                 importer = new EnergySupplierImporter(db);
    //                 importer.import(Path.join(__dirname, 'singleenergysupplier.csv'),
    //                     function (err) {
    //                     if (err) {
    //                         console.log(err);
    //                     }
    //
    //                     Code.expect(err).to.not.exist();
    //                     energySuppliersModel.find({},
    //                         function (err, updatedEnergySupplier) {
    //                         if (err) {
    //                             console.log(err);
    //                         }
    //
    //                         Code.expect(err).to.not.exist();
    //                         console.log('updatedEnergySupplier', updatedEnergySupplier);
    //
    //                         var deletedCount = 0;
    //
    //                         for (var i = updatedEnergySupplier.length - 1; i >= 0; i--) {
    //                             var supplier = updatedEnergySupplier[i];
    //                             if (supplier.deleted) {
    //                                 deletedCount++;
    //                             }
    //                         }
    //
    //                         Code.expect(deletedCount).to.equal(2);
    //
    //                         done();
    //                     });
    //                 });
    //             });
    //         });
    //     });
    // });

    lab.test('it updates an energy supplier', function (done) {
        energySuppliersModel.find({}, function (err, energySuppliers) {
            if (err) {
                console.log(err);
            }

            Code.expect(err).to.not.exist();
            Code.expect(energySuppliers).to.have.length(0);

            var importer = new EnergySupplierImporter(db);
            importer.import(Path.join(__dirname, 'duplicateenergysuppliers.csv'),
                function (err) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                energySuppliersModel.find({},
                    function (err, newEnergySuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newEnergySuppliers).to.have.length(1);
                    Code.expect(newEnergySuppliers[0].name).to.equal('E.ON1');

                    done();
                });
            });
        });
    });
});
