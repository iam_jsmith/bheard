var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var async = require('async');
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var CreditCardRanking = require('../../../lib/builders/moneyRanker');

var moneyVariables = {
    creditCards: {
        database: 'creditCards',
        type: 'creditCard', // used for subSector and for emit event from the parser // todo check working properly
        rankingVariables: {
            distribution: 1000, // for distribution lookup table // TODO WHY 1000??
            divide: 10, // for distribution lookup table
            cdf: 3 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();
var banksModel = db.get('suppliers');
var creditCardModel = db.get('creditCards');
var creditCardRanking = new CreditCardRanking(db, moneyVariables['creditCards']);

var source = [
{
  'moneyFactsID': 'CARDS000002',
  'companyNo': '5705',
  'companyName': 'Bank of Ireland UK',
  'cardName': 'Bank of Ireland UK MasterCard',
  'rates': {
      'standard': {
          'stdRates': [
              {
                  'rateType': 'Intro Balance Transfer',
                  'apr': '0'
              },
              {
                  'rateType': 'Intro Purchases',
                  'apr': '0'
              },
              {
                  'rateType': 'Standard Balance Transfer',
                  'apr': '1.9'
              },
              {
                  'rateType': 'Standard Cash',
                  'apr': '2.942'
              },
              {
                  'rateType': 'Standard Purchases',
                  'apr': '17.9'
              },
              {
                  'rateType': 'Standard Purchases',
                  'apr': '17.9'
              }
          ]
      }
  },
  'score': {
    'moneyFactsID': '5705',
    'company': 'Bank of Ireland UK',
    'valueForMoneyPercent': null,
    'customerServicePercent': 83,
    'customerOpinionPercent': 59,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'CARDS000006',
  'companyNo': '27400',
  'companyName': 'smile',
  'cardName': 'Smile Classic Visa',
  'rates': {
    'standard': {
        'stdRates': [
              {
                  'rateType': 'Standard Balance Transfer',
                  'apr': '2.9'
              },
              {
                  'rateType': 'Standard Cash',
                  'apr': '2.9'
              },
              {
                  'rateType': 'Standard Purchases',
                  'apr': '20.9'
              },
              {
                  'rateType': 'Standard Purchases',
                  'apr': '20.9'
              }
          ]
      }
  },
  'score': {
    'moneyFactsID': '27400',
    'company': 'smile',
    'valueForMoneyPercent': null,
    'customerServicePercent': 60,
    'customerOpinionPercent': 70,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'CARDS000008',
  'companyNo': '13500',
  'companyName': 'First Trust Bank (NI)',
  'cardName': 'First Trust Bank (NI) Visa Option 1',
        'rates': {
            'standard': {
                'stdRates': [
                    {
                        'rateType': 'Standard Balance Transfer',
                        'apr': '1.9'
                    },
                    {
                        'rateType': 'Standard Cash',
                        'apr': '1.9'
                    },
                    {
                        'rateType': 'Standard Purchases',
                        'apr': '18.9'
                    },
                    {
                        'rateType': 'Standard Purchases',
                        'apr': '18.9'
                    }
                ]
            }
        },
  'score': {
    'moneyFactsID': '13500',
    'company': 'First Trust Bank (NI)',
    'valueForMoneyPercent': null,
    'customerServicePercent': 97,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}
];

lab.experiment('Credit Card Ranking', function () {
    lab.beforeEach(function (done) {
        creditCardModel.remove({}, function () {
            async.each(source, function (card, callback) {
                creditCardModel.insert(card, callback);
            }, done);
        });
    });

    lab.test('it calculates the correct mean', function (done) {
        creditCardRanking.calculateMean([17.9, 20.9, 18.9], function (err, mean) {
            Code.expect(err).to.not.exist();
            Code.expect(mean).to.equal(19.23333333333333);
            done();
        });
    });

    lab.test('it calculates the variance properly', function (done) {
        creditCardRanking.calculateVariance([17.9, 20.9, 18.9], function (err, variance) {
            Code.expect(err).to.not.exist();
            Code.expect(variance).to.equal(1.5555555555555556);
            done();
        });
    });

    lab.test('it calculates the standard deviation properly', function (done) {
        creditCardRanking.calculateStandardDeviation([17.9, 20.9, 18.9], function (err, stdev) {
            Code.expect(err).to.not.exist();
            Code.expect(stdev).to.equal(1.247219128924647);
            done();
        });
    });

    lab.test('it calculates the probability mass function properly', function (done) {
        creditCardRanking.calculateNormalDistribution(
            17.9,
            1.5555555555555556,
            19.23333333333333,
            1.247219128924647,
            function (err, norm) {
            Code.expect(err).to.not.exist();
            Code.expect(norm).to.equal(0.18063380375831414);
            done();
        });
    });

    lab.test('it generates a lookup table based on the normal', function (done) {
        creditCardRanking.generateDistributionLookupTable(
            1.5555555555555556,
            19.23333333333333,
            1.247219128924647,
            function (err, distributionTable) {
            Code.expect(err).to.not.exist();
            var expectedTable = require('../data/sampletable');

            Code.expect(distributionTable).to.deep.equal(expectedTable);
            done();
        });
    });

    lab.test('it calculates the cumulative distribution properly', function (done) {
        creditCardRanking.generateDistributionLookupTable(
            1.5555555555555556,
            19.23333333333333,
            1.247219128924647,
            function (err, table) {
            Code.expect(err).to.not.exist();
            creditCardRanking.calculateCumulativeDistribution(
                17.9,
                function (err, distribution) {
                Code.expect(err).to.not.exist();
                Code.expect(distribution).to.equal(0.1518);
                done();
            });
        });
    });

    lab.test('it extracts the ratings correctly', function (done) {
        creditCardModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            creditCardRanking.filterItems(cards, function (err, rates) {
                Code.expect(err).to.not.exist();
                Code.expect(rates).to.have.length(3);

                Code.expect(rates).to.deep.equal([
                    { moneyFactsID: 'CARDS000002', rate: 17.9 },
                    { moneyFactsID: 'CARDS000006', rate: 20.9 },
                    { moneyFactsID: 'CARDS000008', rate: 18.9 }
                ]);

                done();
            });
        });
    });

    lab.test('it calculates the ranking', function (done) {
        creditCardModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            creditCardRanking.calculateRankings(cards, function (err, ranked) {

                Code.expect(err).to.not.exist();
                Code.expect(ranked).to.deep.equal([
                    { moneyFactsID: 'CARDS000002', rate: 17.9, rank: 84.8 },
                    { moneyFactsID: 'CARDS000006', rate: 20.9, rank: 8.4 },
                    { moneyFactsID: 'CARDS000008', rate: 18.9, rank: 59 }
                ]);

                done();
            });
        });
    });

    lab.test('it ignores cards with no rankings', function (done) {
        creditCardRanking.generateRankings([{
            moneyFactsID: 'CARDS000292',
            companyNo: '13500'}],
            function (err) {

            Code.expect(err).to.not.exist();
            done();
        });
    });

    // TODO IS IT SUPPOSED TO BE DOING THIS?
    lab.test('it ignores cards with no standard purchases rate', function (done) {
        creditCardRanking.generateRankings([{
            moneyFactsID: 'CARDS000292',
            companyNo: '13500',
                'rates': {
                    'standard': {
                        'stdRates': [
                            {
                                'rateType': 'Standard Balance Transfer',
                                'apr': '1.9'
                            }
                        ]
                    }
                }}],
            function (err) {

            Code.expect(err).to.not.exist();
            done();
        });
    });

    lab.test('it updates the card rankings', function (done) {
        creditCardModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            creditCardRanking.generateRankings(cards, function (err) {
                Code.expect(err).to.not.exist();
                creditCardModel.find({}, function (err, cards) {
                    Code.expect(err).to.not.exist();
                    for (var i = cards.length - 1; i >= 0; i--) {
                        var card = cards[i];
                        if (card.companyNo === '5705') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(84.8);
                        } else if (card.companyNo === '27400') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(8.4);
                        } else if (card.companyNo === '13500') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(59);
                        } else {
                            throw new Error('Unexpected value in cards');
                        }
                    }

                    done();
                });
            });
        });
    });

    lab.test('it updates the total points', function (done) {
        creditCardModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            creditCardRanking.generateRankings(cards, function (err) {
                Code.expect(err).to.not.exist();
                creditCardModel.find({}, function (err, cards) {
                    Code.expect(err).to.not.exist();
                    for (var i = cards.length - 1; i >= 0; i--) {
                        var card = cards[i];
                        if (card.companyNo === '5705') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(75.6);
                        } else if (card.companyNo === '27400') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(46.1333);
                        } else if (card.companyNo === '13500') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(78);
                        } else {
                            throw new Error('Unexpected value in cards');
                        }
                    }

                    done();
                });
            });
        });
    });
});
