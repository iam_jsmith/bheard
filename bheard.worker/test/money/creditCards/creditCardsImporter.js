var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var singleCreditCard = Path.join(filesPath, moneyfactsDir + '/singleCreditCard.xml');
var duplicateCreditCard = Path.join(filesPath, moneyfactsDir + '/duplicateCreditCard.xml');
var threeCreditCards = Path.join(filesPath, moneyfactsDir + '/threeCreditCards.xml');
var creditCardsSuppliersDataFile = Path.join(filesPath, otherDir + '/credit-cards-suppliers-whitelist.csv');
var creditCardsWhitelistFile = Path.join(filesPath, otherDir + '/credit-cards-products-whitelist.csv');

var CreditCardsImporter = require('../../../lib/importers/moneyImporter');
var CreditCardsProductsParser = require('../../../lib/parsers/creditCardsProducts');
var CreditCardsSuppliersParser = require('../../../lib/parsers/creditCardsSuppliers');
var WhitelistParser = require('../../../lib/parsers/whitelist');

// hard code these because safer for tests (they should be the same as in the data/csvs)
var whitelistArray = ['CARDS001649','CARDS001647'];
var fcaArray = ['3050'];

var moneyVariables = {
    creditCards: {
        database: 'creditCards',
        type: 'creditCard', // used for subSector and for emit event from the parser // todo check working properly
        rankingVariables: {
            distribution: 1000, // for distribution lookup table // TODO WHY 1000??
            divide: 10, // for distribution lookup table
            cdf: 3 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();

lab.experiment('Credit Cards Importer', function () {
    var companyModel;
    var creditCardModel;

    lab.beforeEach(function (done) {
        companyModel = db.get('suppliers');
        companyModel.remove({}, function () {
            creditCardModel = db.get('creditCards');
            creditCardModel.remove({}, done);
        });
    });

    lab.test('it inserts a supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(singleCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.creditCardListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(duplicateCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.creditCardListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it inserts a new credit card', function (done) {
        creditCardModel.find({}, function (err, creditCards) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCards).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(singleCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCreditCards).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate credit card', function (done) {
        creditCardModel.find({}, function (err, creditCards) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCards).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(duplicateCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCreditCards).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it inserts multiple credit cards', function (done) {
        creditCardModel.find({}, function (err, creditCards) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCards).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(threeCreditCards, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCreditCards).to.have.length(3);
                    done();
                });
            });
        });
    });

    lab.test('it correctly assigns marked as whitelisted', function (done) {
        creditCardModel.find({}, function (err, creditCard) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCard).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(threeCreditCards, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    creditCardModel.find({},
                        function (err, newCreditCards) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newCreditCards).to.have.length(3);

                            for (var i = newCreditCards.length - 1; i >= 0; i--) {
                                if (whitelistArray.indexOf(newCreditCards[i].moneyFactsID) > -1) {
                                    Code.expect(newCreditCards[i].whitelisted).to.equal(true);
                                    Code.expect(newCreditCards[i].switchingUrl).to.equal('http://switchingurl.com');
                                } else {
                                    Code.expect(newCreditCards[i].whitelisted).to.equal(false);
                                    Code.expect(newCreditCards[i].switchingUrl).to.equal(null);
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it correctly assigns marked as fca product', function (done) {
        creditCardModel.find({}, function (err, creditCard) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCard).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(threeCreditCards, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    creditCardModel.find({},
                        function (err, newCreditCards) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newCreditCards).to.have.length(3);
                            for (var i = newCreditCards.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newCreditCards[i].companyNo) > -1) {
                                    Code.expect(newCreditCards[i].fcaProduct).to.equal(true);
                                } else {
                                    Code.expect(newCreditCards[i].fcaProduct).to.equal(false);
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it associates scoring with fca products', function (done) {
        creditCardModel.find({}, function (err, creditCard) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCard).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(threeCreditCards, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                        Code.expect(err).to.not.exist();
                        Code.expect(newCreditCards).to.have.length(3);
                        for (var i = newCreditCards.length - 1; i >= 0; i--) {
                            if (fcaArray.indexOf(newCreditCards[i].companyNo) > -1) {
                                Code.expect(newCreditCards[i].score).to.exist();
                            } else {
                                Code.expect(newCreditCards[i].score).to.not.exist();
                            }


                        }
                        done();
                });
            });
        });
    });

    lab.test('it removes old credit cards', function (done) {
        creditCardModel.find({}, function (err, creditCards) {
            Code.expect(err).to.not.exist();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(threeCreditCards, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCreditCards).to.have.length(3);

                        var importer = new CreditCardsImporter(
                            db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                            WhitelistParser);
                        importer.import(duplicateCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                            function (err) {

                        Code.expect(err).to.not.exist();
                        creditCardModel.find({},
                            function (err, newCreditCards) {
                            Code.expect(err).to.not.exist();

                            var deletedCount = 0;

                            for (var i = newCreditCards.length - 1; i >= 0; i--) {
                                if (newCreditCards[i].deleted) {
                                    deletedCount++;
                                }
                            }

                            Code.expect(deletedCount).to.equal(2);

                            done();
                        });
                    });
                });
            });
        });
    });

    lab.test('it updates credit cards', function (done) {
        creditCardModel.find({}, function (err, creditCards) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCards).to.be.empty();

            var importer = new CreditCardsImporter(
                db, moneyVariables.creditCards, CreditCardsProductsParser, CreditCardsSuppliersParser,
                WhitelistParser);
            importer.import(duplicateCreditCard, creditCardsSuppliersDataFile, creditCardsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                creditCardModel.find({},
                    function (err, newCreditCards) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCreditCards).to.have.length(1);

                    Code.expect(newCreditCards[0].cardName)
                    .to.equal('AA Balance Transfer Credit Card MasterCard');

                    done();
                });
            });
        });
    });
});
