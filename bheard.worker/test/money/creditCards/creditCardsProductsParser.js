var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';

var CreditCardsParser = require('../../../lib/parsers/creditCardsProducts');
var threeCreditCards = Path.join(filesPath, moneyfactsDir + '/threeCreditCards.xml');
var creditCardsSuppliersData = [
    { moneyfactsID: 27400 }
];

var lab = exports.lab = Lab.script();

lab.experiment('Credit Cards Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new CreditCardsParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('', creditCardsSuppliersData);
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new CreditCardsParser();
        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            done();
        });

        parser.parse(threeCreditCards, creditCardsSuppliersData);
    });

    lab.test('it reports each credit card found', function (done) {
        var parser = new CreditCardsParser();

        var creditCardCount = 0;

        parser.on('creditCard', function (creditCard) {
            creditCardCount++;
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCardCount).to.equal(3);

            done();
        });

        parser.parse(threeCreditCards, creditCardsSuppliersData);
    });

    lab.test('it pauses the parser when a credit card is found', function (done) {
        var parser = new CreditCardsParser();
        var stopped = false;
        var creditCardCount = 0;

        parser.on('creditCard', function (creditCard) {
            Code.expect(stopped).to.be.false();
            creditCardCount++;
            parser.stop();
            stopped = true;

            setTimeout(function () {
                stopped = false;
                parser.resume();
            }, 20);
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            Code.expect(creditCardCount).to.equal(3);

            done();
        });

        parser.parse(threeCreditCards, creditCardsSuppliersData);
    });

    lab.test('it emits a valid credit card each time', function (done) {
        var parser = new CreditCardsParser();

        parser.on('CreditCard', function (creditCard) {
            var schema = Joi.object().keys({
                moneyFactsID: Joi.string().regex(/CARDS[0-9]{6}/).required(),
                companyNo: Joi.number().required(),
                companyName: Joi.string().required(),
                cardName: Joi.string().required(),
                rates: Joi.array().items(Joi.object().keys({
                    rateType: Joi.string().required(),
                    yearlyRate: Joi.string().required()
                }))
            });

            Joi.validate(creditCard, schema, function (err, value) {
                if (err) {
                    console.log('Error validating creditCard', err);
                }

                Code.expect(err).to.not.exist();
            });
        });

        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse(threeCreditCards, creditCardsSuppliersData);
    });
});
