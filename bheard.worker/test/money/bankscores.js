// var Lab = require('lab');
// var Code = require('code');
// var Path = require('path');
// var Joi = require('joi');
// var config = require('bheard').config();
// var async = require('async');
// var db = require('bheard')
//             .db(config.get('/database/mongodb/url', {env: 'devtest'}));
//
// var BankScores = require('../../lib/builders/bankscores');
//
// var lab = exports.lab = Lab.script();
// var banksModel = db.get('suppliers');
// var creditCardModel = db.get('creditCards');
// var currentAccountModel = db.get('currentAccounts');
// var unsecuredLoansModel = db.get('unsecuredLoans');
//
// var bankScores = new BankScores(db);
//
// var banks = [
//     {
//         'no': '1',
//         'name': 'Company1',
//         'sector': 'money',
//         'subSector': ''
//     },
//     {
//         'no': '2',
//         'name': 'Company2',
//         'sector': 'money',
//         'subSector': ''
//     }
// ];
//
// var creditCards = [
//     {
//         'moneyFactsID': '1',
//         'companyNo': '1',
//         'companyName': 'Company1',
//         'score': {
//             'valueForMoneyPercent': 1,
//             'customerServicePercent': 2,
//             'overallOpinionPercent': 6
//         }
//     },
//     {
//         'moneyFactsID': '2',
//         'companyNo': '2',
//         'companyName': 'Company2',
//         'score': {
//             'valueForMoneyPercent': 11,
//             'customerServicePercent': 12,
//             'overallOpinionPercent': 36
//         }
//     },
//     {
//         'moneyFactsID': '3',
//         'companyNo': '1',
//         'companyName': 'Company1',
//         'score': {
//             'valueForMoneyPercent': 2,
//             'customerServicePercent': 4,
//             'overallOpinionPercent': 12
//         }
//     }
// ];
//
// var currentAccounts = [
//     {
//         'moneyFactsID': '1',
//         'companyNo': '1',
//         'companyName': 'Company1',
//         'score': {
//             'price': 2,
//             'valueForMoneyPercent': 4,
//             'trustworthiness': 10,
//             'customerServicePercent': 5,
//             'personalExperience': 20,
//             'overallOpinionPercent': 15
//         }
//     },
//     {
//         'moneyFactsID': '2',
//         'companyNo': '2',
//         'companyName': 'Company2',
//         'score': {
//             'price': 3,
//             'valueForMoneyPercent': 21,
//             'trustworthiness': 20,
//             'customerServicePercent': 22,
//             'personalExperience': 30,
//             'overallOpinionPercent': 66
//         }
//     },
//     {
//         'moneyFactsID': '3',
//         'companyNo': '2',
//         'companyName': 'Company2',
//         'score': {
//             'price': 6,
//             'valueForMoneyPercent': 40,
//             'trustworthiness': 60,
//             'customerServicePercent': 62,
//             'personalExperience': 60,
//             'overallOpinionPercent': 96
//         }
//     }
// ];
//
// var unsecuredLoans = [
//     {
//   '_id': ObjectId('56349d7f2db1f8013467cbf5'),
//   'moneyFactsID': 'LOANS010402',
//   'companyNo': '21650',
//   'companyName': 'Nationwide BS',
//   'loanRateType': 'Fixed',
//   'loanName': 'Existing Customer Personal Loan',
//   'repAPRAdvertised': '3.6',
//   'aprAdvertisedAdvMin': '7500.00',
//   'aprAdvertisedAdvMax': '14999.00',
//   'supplierId': ObjectId('55f96271e20907093a6bf033'),
//   'score': {
//     'moneyFactsID': '21650',
//     'company': 'Nationwide BS',
//     'priceNormalised': 97,
//     'trustworthinessNormalised': 80.400000000000005684,
//     'personalExperienceNormalised': null,
//     'valueForMoneyPercent': 97,
//     'overallOpinionPercent': 97
//   }
// }
// ];
//
// lab.experiment('Bank Scores', function () {
//     lab.beforeEach(function (done) {
//         async.parallel([
//             function (callback) {
//                 banksModel.remove({}, callback);
//             },
//             function (callback) {
//                 creditCardModel.remove({}, callback);
//             },
//             function (callback) {
//                 currentAccountModel.remove({}, callback);
//             }
//         ], function (err) {
//             Code.expect(err).to.not.exist();
//             async.each(banks, function (bank, cb) {
//                 banksModel.insert(bank, cb);
//             }, done);
//         });
//     });
//
//     lab.test('it gets scores from credit cards', function (done) {
//         async.each(creditCards, function (card, cb) {
//             creditCardModel.insert(card, cb);
//         }, function (err) {
//             Code.expect(err).to.not.exist();
//             bankScores.build(function (err) {
//                 Code.expect(err).to.not.exist();
//                 banksModel.find({}, function (err, banks) {
//                     Code.expect(err).to.not.exist();
//                     for (var i = banks.length - 1; i >= 0; i--) {
//                         if (banks[i].no === '1') {
//                             Code.expect(banks[i].score.valueForMoneyPercent).to.equal(1);
//                             Code.expect(banks[i].score.customerServicePercent).to.equal(2);
//                             Code.expect(banks[i].score.customerOpinionPercent).to.equal(3);
//                             Code.expect(banks[i].score.overallOpinionPercent).to.equal(6);
//                         } else if (banks[i].no === '2') {
//                             Code.expect(banks[i].score.valueForMoneyPercent).to.equal(11);
//                             Code.expect(banks[i].score.customerServicePercent).to.equal(12);
//                             Code.expect(banks[i].score.customerOpinionPercent).to.equal(13);
//                             Code.expect(banks[i].score.overallOpinionPercent).to.equal(36);
//                         }
//                     }
//
//                     done();
//                 });
//             });
//         });
//     });
//
//     lab.test('it gets scores from current accounts', function (done) {
//         async.each(currentAccounts, function (account, cb) {
//             currentAccountModel.insert(account, cb);
//         }, function (err) {
//             Code.expect(err).to.not.exist();
//             bankScores.build(function (err) {
//                 Code.expect(err).to.not.exist();
//                 banksModel.find({}, function (err, banks) {
//                     Code.expect(err).to.not.exist();
//                     for (var i = banks.length - 1; i >= 0; i--) {
//                         if (banks[i].no === '1') {
//                             Code.expect(banks[i].score.valueForMoneyPercent).to.equal(4);
//                             Code.expect(banks[i].score.customerServicePercent).to.equal(5);
//                             Code.expect(banks[i].score.customerOpinionPercent).to.equal(6);
//                             Code.expect(banks[i].score.overallOpinionPercent).to.equal(15);
//                         } else if (banks[i].no === '2') {
//                             Code.expect(banks[i].score.valueForMoneyPercent).to.equal(21);
//                             Code.expect(banks[i].score.customerServicePercent).to.equal(22);
//                             Code.expect(banks[i].score.customerOpinionPercent).to.equal(23);
//                             Code.expect(banks[i].score.overallOpinionPercent).to.equal(66);
//                         }
//                     }
//
//                     done();
//                 });
//             });
//         });
//     });
//
//     lab.test('it gets scores from both current accounts and cards', function (done) {
//         async.each(creditCards, function (card, cb) {
//             creditCardModel.insert(card, cb);
//         }, function (err) {
//             Code.expect(err).to.not.exist();
//             async.each(currentAccounts, function (account, cb) {
//                 currentAccountModel.insert(account, cb);
//             }, function (err) {
//                 Code.expect(err).to.not.exist();
//                 bankScores.build(function (err) {
//                     Code.expect(err).to.not.exist();
//                     banksModel.find({}, function (err, banks) {
//                         Code.expect(err).to.not.exist();
//                         for (var i = banks.length - 1; i >= 0; i--) {
//                             if (banks[i].no === '1') {
//                                 Code.expect(banks[i].score.valueForMoneyPercent).to.equal(2.5);
//                                 Code.expect(banks[i].score.customerServicePercent).to.equal(3.5);
//                                 Code.expect(banks[i].score.customerOpinionPercent).to.equal(4.5);
//                                 Code.expect(banks[i].score.overallOpinionPercent).to.equal(10.5);
//                             } else if (banks[i].no === '2') {
//                                 Code.expect(banks[i].score.valueForMoneyPercent).to.equal(16);
//                                 Code.expect(banks[i].score.customerServicePercent).to.equal(17);
//                                 Code.expect(banks[i].score.customerOpinionPercent).to.equal(18);
//                                 Code.expect(banks[i].score.overallOpinionPercent).to.equal(51);
//                             }
//                         }
//
//                         done();
//                     });
//                 });
//             });
//         });
//     });
// });
