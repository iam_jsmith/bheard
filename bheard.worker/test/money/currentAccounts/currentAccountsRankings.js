// var Lab = require('lab');
// var Code = require('code');
// var Path = require('path');
// var Joi = require('joi');
// var config = require('bheard').config();
// var async = require('async');
// var db = require('bheard')
//             .db(config.get('/database/mongodb/url', {env: 'devtest'}));
//
// var CurrentAccountRanking = require('../../../lib/builders/currentaccountranking');
//
// var lab = exports.lab = Lab.script();
// var banksModel = db.get('suppliers');
// var currentAccountModel = db.get('currentAccounts');
// var currentAccountRanking = new CurrentAccountRanking(db);
//
// var source = [{
//   'moneyFactsID': 'CURRENT000008',
//   'currentAccountType': 'Basic',
//   'companyNo': '5750',
//   'companyName': 'Bank of Scotland',
//   'odRateSets': [
//     {
//       'odRateTypes': [
//         {
//           'odType': 'Authorised',
//           'odRatePeriods': [
//             {
//               'odRatePeriodMthsLower': '1',
//               'odRatePeriodMthsUpper': '0',
//               'odRates': [
//                 {
//                   'odRateTierLower': '0.01',
//                   'odRateTierUpper': '1000.00',
//                   'odRateYly': '11.0203'
//                 }
//               ]
//             }
//           ]
//         }
//       ]
//     }
//   ],
//   'score': {
//     'moneyFactsID': 'CURRENT000009',
//     'company': 'Coventry Building Society',
//     'valueForMoney': 25,
//     'valueForMoneyPercent': 62.5,
//     'customerService': 29.457754009999998601,
//     'customerServicePercent': 73.64438502999999514,
//     'customerOpinion': 90.674481170000007069,
//     'customerOpinionPercent': 75.562067639999995095,
//     'overallOpinionPercent': 70.568817559999999389
//   }
// }, {
//   'moneyFactsID': 'CURRENT000015',
//   'currentAccountType': 'Current',
//   'companyNo': '5357',
//   'companyName': 'Bank of China (UK)',
//   'inCredits': [
//     {
//       'inCreditPeriods': [
//         {
//           'icPeriodMthsLower': '1',
//           'icPeriodMthsUpper': '0',
//           'inCreditRates': [
//             {
//               'icTierLower': '0.01',
//               'icTierUpper': '9999.99',
//               'icAER': '0'
//             }, {
//               'icTierLower': '10000.00',
//               'icTierUpper': '24999.99',
//               'icAER': '0.05'
//             }, {
//               'icTierLower': '25000.00',
//               'icTierUpper': '49999.99',
//               'icAER': '0.1'
//             }, {
//               'icTierLower': '50000.00',
//               'icTierUpper': '99999.99',
//               'icAER': '0.15'
//             }, {
//               'icTierLower': '100000.00',
//               'icAER': '0.2'
//             }
//           ]
//         }
//       ]
//     }
//   ],
//   'odRateSets': [
//     {
//       'odRateTypes': [
//         {
//           'odType': 'Unauthorised',
//           'odRatePeriods': [
//             {
//               'odRatePeriodMthsLower': '1',
//               'odRatePeriodMthsUpper': '0',
//               'odRates': [
//                 {
//                   'odRateTierLower': '0.01',
//                   'odRateTierUpper': '0.00',
//                   'odRateYly': '11.0203'
//                 }
//               ]
//             }
//           ]
//         }
//       ]
//     }
//   ],
//   'score': {
//     'moneyFactsID': 'CURRENT000015',
//     'company': 'Coventry Building Society',
//     'valueForMoney': 25,
//     'valueForMoneyPercent': 62.5,
//     'customerService': 29.457754009999998601,
//     'customerServicePercent': 73.64438502999999514,
//     'customerOpinion': 90.674481170000007069,
//     'customerOpinionPercent': 75.562067639999995095,
//     'overallOpinionPercent': 70.568817559999999389
//   }
// }, {
//   'moneyFactsID': 'CURRENT000025',
//   'currentAccountType': 'Current',
//   'companyNo': '10750',
//   'companyName': 'Coventry BS',
//   'accountOptionName': 'Coventry First',
//   'inCredits': [
//     {
//       'inCreditPeriods': [
//         {
//           'icPeriodMthsLower': '1',
//           'icPeriodMthsUpper': '12',
//           'inCreditRates': [
//             {
//               'icTierLower': '1.00',
//               'icTierUpper': '9999.99',
//               'icAER': '1.100'
//             }, {
//               'icTierLower': '10000.00',
//               'icTierUpper': '250000.00',
//               'icAER': '1.100'
//             }
//           ]
//         }, {
//           'icPeriodMthsLower': '13',
//           'icPeriodMthsUpper': '0',
//           'inCreditRates': [
//             {
//               'icTierLower': '1.00',
//               'icTierUpper': '9999.99',
//               'icAER': '0.250'
//             }, {
//               'icTierLower': '10000.00',
//               'icTierUpper': '250000.00',
//               'icAER': '0.850'
//             }
//           ]
//         }
//       ]
//     }
//   ],
//   'odRateSets': [
//     {
//       'odRateTypes': [
//         {
//           'odType': 'Authorised',
//           'odRatePeriods': [
//             {
//               'odRatePeriodMthsLower': '1',
//               'odRatePeriodMthsUpper': '0',
//               'odRates': [
//                 {
//                   'odRateTierLower': '0.01',
//                   'odRateTierUpper': '1000.00',
//                   'odRateYly': '0.0000'
//                 }
//               ]
//             }
//           ]
//         },
//         {
//           'odType': 'Unauthorised',
//           'odRatePeriods': [
//             {
//               'odRatePeriodMthsLower': '1',
//               'odRatePeriodMthsUpper': '0',
//               'odRates': [
//                 {
//                   'odRateTierLower': '0.01',
//                   'odRateTierUpper': '0.00',
//                   'odRateYly': '0.0000'
//                 }
//               ]
//             }
//           ]
//         }
//       ]
//     }
//   ],
//   'score': {
//     'moneyFactsID': 'CURRENT000025',
//     'company': 'Coventry Building Society',
//     'valueForMoney': 25,
//     'valueForMoneyPercent': 62.5,
//     'customerService': 29.457754009999998601,
//     'customerServicePercent': 73.64438502999999514,
//     'customerOpinion': 90.674481170000007069,
//     'customerOpinionPercent': 75.562067639999995095,
//     'overallOpinionPercent': 70.568817559999999389
//   }
// }];
//
// lab.experiment('Current Account Ranking', function () {
//
//     lab.beforeEach(function (done) {
//         currentAccountModel.remove({}, function () {
//             async.each(source, function (account, callback) {
//                 currentAccountModel.insert(account, callback);
//             }, done);
//         });
//     });
//
//     lab.test('it calculates the correct weighted average in credit rate multiple period',
//         function (done) {
//         currentAccountRanking.calculateWeightedAverage(source[2],
//             function (err, weightedRate) {
//             Code.expect(err).to.not.exist();
//             Code.expect(weightedRate).to.equal(0.9630011635247007);
//
//             done();
//         });
//     });
//
//     lab.test('it calculates the correct weighted average in credit rate single period',
//         function (done) {
//         currentAccountRanking.calculateWeightedAverage(source[1],
//             function (err, weightedRate) {
//             Code.expect(err).to.not.exist();
//             Code.expect(weightedRate).to.equal(0.15375002343750588);
//
//             done();
//         });
//     });
//
//     lab.test('it calculates the correct weighted average authorised over draft rate', function (done) {
//         currentAccountRanking.calculateWeightedAverage(source[0], function (err, weightedRate) {
//             Code.expect(err).to.not.exist();
//             Code.expect(weightedRate).to.equal(11.02);
//
//             done();
//         });
//     });
// });
