var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var inspect = require('eyes').inspector({ maxLength: 40048 });
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';

var CurrentAccountParser = require('../../../lib/parsers/currentAccountsProducts');
var threeAccounts = Path.join(filesPath, moneyfactsDir + '/threeAccounts.xml');
var currentAccountsSuppliersData = [
    { moneyfactsID: 5750 }
];

var lab = exports.lab = Lab.script();

lab.experiment('Current Account Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new CurrentAccountParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('', currentAccountsSuppliersData);
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new CurrentAccountParser();
        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();
            done();
        });

        parser.parse(threeAccounts, currentAccountsSuppliersData);
    });

    lab.test('it reports each account found', function (done) {
        var parser = new CurrentAccountParser();

        var accountCount = 0;

        parser.on('currentAccount', function (account) {
            accountCount++;
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            Code.expect(accountCount).to.equal(3);

            done();
        });

        parser.parse(threeAccounts, currentAccountsSuppliersData);
    });

    lab.test('it pauses the parser when an account is found', function (done) {
        var parser = new CurrentAccountParser();
        var stopped = false;
        var accountCount = 0;

        parser.on('currentAccount', function (account) {
            Code.expect(stopped).to.be.false();
            accountCount++;
            parser.stop();
            stopped = true;

            setTimeout(function () {
                stopped = false;
                parser.resume();
            }, 20);
        });

        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();

            Code.expect(accountCount).to.equal(3);

            done();
        });

        parser.parse(threeAccounts, currentAccountsSuppliersData);
    });

    lab.test('it emits a new currentAccountRecord when it finishes',
        function (done) {
        var parser = new CurrentAccountParser();

        parser.on('currentAccount', function (account) {
            var schema = Joi.object().keys({
                moneyFactsID: Joi.string().regex(/CURRENT[0-9]{6}/).required(),
                currentAccountType: Joi.string().required(),
                companyNo: Joi.number().required(),
                companyName: Joi.string().required(),
                accountOptionName: Joi.string().required(),
                accountLaunchDate: Joi.date().iso(),
                optionLaunchDate: Joi.date().iso(),
                accountOpenings: Joi.array().items(Joi.object().keys({
                    channelName: Joi.string().required(),
                    channelDetail: Joi.string()
                })).required(),
                eligibilityAgeMinTxt: Joi.string(),
                eligibilitySetupFee: Joi.string(),
                eligibilityIntroFee: Joi.string(),
                eligibilityStdFee: Joi.string(),
                chargeSets: Joi.array().items(Joi.object().keys({
                    chargeSetType: Joi.string(),
                    chargeSetWefDef: Joi.string(),
                    chargeSetChargingFreq: Joi.string(),
                    charges: Joi.array().items(Joi.object().keys({
                        chargeSection: Joi.string(),
                        chargeType: Joi.string(),
                        chargeTxt: Joi.string()
                    }))
                })),
                benefits: Joi.array().items(Joi.object().keys({
                    benefitSection: Joi.string().required(),
                    benefitType: Joi.string().required(),
                    benefitNote: Joi.string().required()
                })),
                facilitySet: Joi.object().keys({
                    minOpenBal: Joi.string().required(),
                    minOperateBal: Joi.string().required(),
                    maxInv: Joi.string().required(),
                    minWithdrawal: Joi.string().required(),
                    cardType: Joi.string(),
                    debitSystemType: Joi.string(),
                    atmLimit: Joi.string()
                }).required(),
                inCredits: Joi.array().items(Joi.object().keys({
                    inCreditPeriods: Joi.array().items(Joi.object().keys({
                        icPeriodMthsLower: Joi.number().required(),
                        icPeriodMthsUpper: Joi.number().required(),
                        inCreditRates: Joi.array().items(Joi.object().keys({
                            icTierLower: Joi.number().required(),
                            icTierUpper: Joi.number().required(),
                            icAER: Joi.number().required()
                        }))
                    }))
                })),
                odRateSets: Joi.array().items(Joi.object().keys({
                    odRateTypes: Joi.array().items(Joi.object().keys({
                        odType: Joi.string().required(),
                        odRatePeriods: Joi.array().items(Joi.object().keys({
                            odRatePeriodMthsLower: Joi.number().required(),
                            odRatePeriodMthsUpper: Joi.number().required(),
                            odRates: Joi.array().items(Joi.object().keys({
                                odRateTierLower: Joi.number().required(),
                                odRateTierUpper: Joi.number().required(),
                                odRateYly: Joi.number().optional()
                            }))
                        }))
                    }))
                })),
                odBufferType: Joi.string(),
                odBufferAppliesToType: Joi.string(),
                odBufferAmt: Joi.string(),
                odBufferPeriod: Joi.string(),
                odBufferDependsCreditRatingTick: Joi.string(),
                odBufferNote: Joi.string()
            });

            Joi.validate(account, schema, function (err, value) {
                if (err) {
                    console.log('Error validating account', err);
                }

                Code.expect(err).to.not.exist();
            });
        });

        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();

            done();
        });

            parser.parse(threeAccounts, currentAccountsSuppliersData);
    });
});
