var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var accountSingle = Path.join(filesPath, moneyfactsDir + '/singleAccount.xml');
var accountDuplicate = Path.join(filesPath, moneyfactsDir + '/duplicateAccount.xml');
var threeAccounts = Path.join(filesPath, moneyfactsDir + '/threeAccounts.xml');
var currentAccountsSuppliersDataFile = Path.join(filesPath, otherDir + '/current-accounts-suppliers-whitelist.csv');
var currentAccountsWhitelistFile = Path.join(filesPath, otherDir + '/current-accounts-products-whitelist.csv');

var CurrentAccountsImporter = require('../../../lib/importers/moneyImporter');
var CurrentAccountsProductsParser = require('../../../lib/parsers/currentAccountsProducts');
var CurrentAccountsSuppliersParser = require('../../../lib/parsers/currentAccountsSuppliers');
var WhitelistParser = require('../../../lib/parsers/whitelist');

// hard code these because safer for tests (they should be the same as in the data/csvs)
var whitelistArray = ['CURRENT000008','CURRENT0000015'];
var fcaArray = ['5750'];

var moneyVariables = {
    currentAccounts: {
        database: 'currentAccounts',
        type: 'currentAccount'
    }
};

var lab = exports.lab = Lab.script();

lab.experiment('Current Accounts Importer', function () {
    var companyModel;
    var currentAccountModel;

    lab.beforeEach(function (done) {
        companyModel = db.get('suppliers');
        companyModel.remove({}, function () {
            currentAccountModel = db.get('currentAccounts');
            currentAccountModel.remove({}, done);
        });
    });

    lab.afterEach(function (done) {
        companyModel.remove({}, function () {
            currentAccountModel.remove({}, done);
        });
    });

    lab.test('it inserts a supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(accountSingle, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.currentAccountListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(accountDuplicate, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.currentAccountListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it inserts a new account', function (done) {
        currentAccountModel.find({}, function (err, currentAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccounts).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(accountSingle, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                currentAccountModel.find({},
                    function (err, newCurrentAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCurrentAccounts).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate current account', function (done) {
        currentAccountModel.find({}, function (err, currentAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccounts).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(accountDuplicate, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                currentAccountModel.find({},
                    function (err, newCurrentAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCurrentAccounts).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it inserts multiple accounts', function (done) {
        currentAccountModel.find({}, function (err, currentAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccounts).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(threeAccounts, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                currentAccountModel.find({},
                    function (err, newCurrentAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCurrentAccounts).to.have.length(3);
                    done();
                });
            });
        });
    });

    lab.test('it correctly assigns marked as whitelisted', function (done) {
        currentAccountModel.find({}, function (err, currentAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccount).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(threeAccounts, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    currentAccountModel.find({},
                        function (err, newCurrentAccounts) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newCurrentAccounts).to.have.length(3);

                            for (var i = newCurrentAccounts.length - 1; i >= 0; i--) {
                                if (whitelistArray.indexOf(newCurrentAccounts[i].moneyFactsID) > -1) {
                                    Code.expect(newCurrentAccounts[i].whitelisted).to.equal(true);
                                    Code.expect(newCurrentAccounts[i].switchingUrl).to.equal('http://switchingurl.com');
                                } else {
                                    Code.expect(newCurrentAccounts[i].whitelisted).to.equal(false);
                                    Code.expect(newCurrentAccounts[i].switchingUrl).to.equal(null);
                                }
                            }

                            done();
                        });
                });
        });
    });

    lab.test('it correctly assigns marked as fca product', function (done) {
        currentAccountModel.find({}, function (err, currentAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccount).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(threeAccounts, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    currentAccountModel.find({},
                        function (err, newCurrentAccounts) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newCurrentAccounts).to.have.length(3);
                            for (var i = newCurrentAccounts.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newCurrentAccounts[i].companyNo) > -1) {
                                    Code.expect(newCurrentAccounts[i].fcaProduct).to.equal(true);
                                } else {
                                    Code.expect(newCurrentAccounts[i].fcaProduct).to.equal(false);
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it associates scoring with fca products', function (done) {
        currentAccountModel.find({}, function (err, currentAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccount).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(threeAccounts, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    currentAccountModel.find({},
                        function (err, newCurrentAccounts) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newCurrentAccounts).to.have.length(3);

                            for (var i = newCurrentAccounts.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newCurrentAccounts[i].companyNo) > -1) {
                                    Code.expect(newCurrentAccounts[i].score).to.exist();
                                } else {
                                    Code.expect(newCurrentAccounts[i].score).to.not.exist();
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it removes old accounts', function (done) {
        currentAccountModel.find({}, function (err, currentAccounts) {
            Code.expect(err).to.not.exist();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(threeAccounts, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                currentAccountModel.find({},
                    function (err, newCurrentAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCurrentAccounts).to.have.length(3);

                        var importer = new CurrentAccountsImporter(
                            db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                            WhitelistParser);
                        importer.import(accountDuplicate, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                            function (err) {

                        Code.expect(err).to.not.exist();
                        currentAccountModel.find({},
                            function (err, newCurrentAccounts) {
                            Code.expect(err).to.not.exist();
                            var deletedCount = 0;

                            for (var i = newCurrentAccounts.length - 1; i >= 0; i--) {
                                if (newCurrentAccounts[i].deleted) {
                                    deletedCount++;
                                }
                            }

                            Code.expect(deletedCount).to.equal(2);

                            done();
                        });
                    });
                });
            });
        });
    });

    lab.test('it updates accounts', function (done) {
        currentAccountModel.find({}, function (err, currentAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(currentAccounts).to.be.empty();

            var importer = new CurrentAccountsImporter(
                db, moneyVariables.currentAccounts, CurrentAccountsProductsParser, CurrentAccountsSuppliersParser,
                WhitelistParser);
            importer.import(accountDuplicate, currentAccountsSuppliersDataFile, currentAccountsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                currentAccountModel.find({},
                    function (err, newCurrentAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCurrentAccounts).to.have.length(1);

                    Code.expect(newCurrentAccounts[0].accountOptionName)
                    .to.equal('Credit Account');

                    done();
                });
            });
        });
    });
});
