var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var async = require('async');
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var UnsecuredLoansRanking = require('../../../lib/builders/moneyRanker');

var moneyVariables = {
    unsecuredLoans: {
        database: 'unsecuredLoans',
        type: 'unsecuredLoans',
        rankingVariables: {
            distribution: 3010, // for distribution lookup table // TODO WHY 3010??
            divide: 10, // for distribution lookup table
            cdf: 3 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();
var banksModel = db.get('suppliers');
var unsecuredLoanModel = db.get('unsecuredLoans');
var unsecuredLoansRanking = new UnsecuredLoansRanking(db, moneyVariables['unsecuredLoans']);

var source = [{
  'moneyFactsID': 'LOANS000002',
  'companyNo': '21650',
  'companyName': 'Nationwide BS',
  'loanRateType': 'Fixed',
  'loanName': 'New Customer Personal Loan',
  'repAPRAdvertised': '3.9',
  'aprAdvertisedAdvMin': '7500.00',
  'aprAdvertisedAdvMax': '14999.00',
  'score': {
    'moneyFactsID': '21650',
    'company': 'Nationwide BS',
    'valueForMoneyPercent': null,
    'customerServicePercent': 80.400000000000005684,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'LOANS000010',
  'companyNo': '21820',
  'companyName': 'NatWest',
  'loanRateType': 'Fixed',
  'loanName': 'Existing Customer Online Personal Loan',
  'repAPRAdvertised': '4.9',
  'aprAdvertisedAdvMin': '7500.00',
  'aprAdvertisedAdvMax': '14950.00',
  'score': {
    'moneyFactsID': '21820',
    'company': 'NatWest',
    'valueForMoneyPercent': null,
    'customerServicePercent': 59.369999999999997442,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'LOANS000012',
  'companyNo': '29600',
  'companyName': 'Ulster Bank',
  'loanRateType': 'Fixed',
  'loanName': 'Personal Loan',
  'repAPRAdvertised': '1.9',
  'aprAdvertisedAdvMin': '7500.00',
  'aprAdvertisedAdvMax': '14999.00',
  'score': {
    'moneyFactsID': '29600',
    'company': 'Ulster Bank',
    'valueForMoneyPercent': null,
    'customerServicePercent': 96.459999999999993747,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}];

lab.experiment('Unsecured Loan Ranking', function () {
    lab.beforeEach(function (done) {
        unsecuredLoanModel.remove({}, function () {
            async.each(source, function (loan, callback) {
                unsecuredLoanModel.insert(loan, callback);
            }, done);
        });
    });

    lab.test('it calculates the correct mean', function (done) {
        unsecuredLoansRanking.calculateMean([3.9, 4.9, 1.9], function (err, mean) {
            Code.expect(err).to.not.exist();
            Code.expect(mean).to.equal(3.566666666666667);
            done();
        });
    });

    lab.test('it calculates the variance properly', function (done) {
        unsecuredLoansRanking.calculateVariance([3.9, 4.9, 1.9], function (err, variance) {
            Code.expect(err).to.not.exist();
            Code.expect(variance).to.equal(1.555555555555556);
            done();
        });
    });

    lab.test('it calculates the standard deviation properly', function (done) {
        unsecuredLoansRanking.calculateStandardDeviation([3.9, 4.9, 1.9], function (err, stdev) {
            Code.expect(err).to.not.exist();
            Code.expect(stdev).to.equal(1.2472191289246473);
            done();
        });
    });

    lab.test('it calculates the probability mass function properly', function (done) {
        unsecuredLoansRanking.calculateNormalDistribution(
            3.9,
            1.555555555555556,
            3.566666666666667,
            1.2472191289246473,
            function (err, norm) {
            Code.expect(err).to.not.exist();
            Code.expect(norm).to.equal(0.3086432514673791);
            done();
        });
    });

    lab.test('it generates a lookup table based on the normal', function (done) {
        unsecuredLoansRanking.generateDistributionLookupTable(
            1.555555555555556,
            3.566666666666667,
            1.2472191289246473,
            function (err, distributionTable) {
            Code.expect(err).to.not.exist();
            var expectedTable = require('../data/loanssampletable');
            Code.expect(distributionTable).to.deep.equal(expectedTable);
            done();
        });
    });

    lab.test('it calculates the cumulative distribution properly', function (done) {
        unsecuredLoansRanking.generateDistributionLookupTable(
            1.555555555555556,
            3.566666666666667,
            1.2472191289246473,
            function (err, table) {
            Code.expect(err).to.not.exist();
            unsecuredLoansRanking.calculateCumulativeDistribution(
                3.9,
                function (err, distribution) {
                Code.expect(err).to.not.exist();
                Code.expect(distribution).to.equal(0.6186);
                done();
            });
        });
    });

    lab.test('it ignores loans with apr', function (done) {
        unsecuredLoansRanking.calculateRankings(
            [{
                'moneyFactsID': 'LOANS000002',
                'companyNo': '21650',
                'companyName': 'Nationwide BS',
                'loanRateType': 'Fixed',
                'loanName': 'New Customer Personal Loan',
                'aprAdvertisedAdvMin': '7500.00',
                'aprAdvertisedAdvMax': '14999.00',
                'score': {
                    'moneyFactsID': '21650',
                    'company': 'Nationwide BS',
                    'valueForMoneyPercent': 38.1,
                    'customerServicePercent': 80.400000000000005684,
                    'customerOpinionPercent': null,
                    'overallOpinionPercent': null
                }
            }], function (err, ranked) {
            Code.expect(err).to.not.exist();
            done();
        });
    });

    lab.test('it calculates the ranking', function (done) {
        unsecuredLoanModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            unsecuredLoansRanking.calculateRankings(cards, function (err, ranked) {

                Code.expect(err).to.not.exist();
                Code.expect(ranked).to.deep.equal([
                  {
                    '_id': cards[0]._id,
                    'moneyFactsID': 'LOANS000002',
                    'companyNo': '21650',
                    'companyName': 'Nationwide BS',
                    'loanRateType': 'Fixed',
                    'loanName': 'New Customer Personal Loan',
                    'repAPRAdvertised': '3.9',
                    'aprAdvertisedAdvMin': '7500.00',
                    'aprAdvertisedAdvMax': '14999.00',
                    'score': {
                      'moneyFactsID': '21650',
                      'company': 'Nationwide BS',
                      'valueForMoneyPercent': 38.1,
                      'customerServicePercent': 80.400000000000005684,
                      'customerOpinionPercent': null,
                      'overallOpinionPercent': null
                    }
                  }, {
                    '_id': cards[1]._id,
                    'moneyFactsID': 'LOANS000010',
                    'companyNo': '21820',
                    'companyName': 'NatWest',
                    'loanRateType': 'Fixed',
                    'loanName': 'Existing Customer Online Personal Loan',
                    'repAPRAdvertised': '4.9',
                    'aprAdvertisedAdvMin': '7500.00',
                    'aprAdvertisedAdvMax': '14950.00',
                    'score': {
                      'moneyFactsID': '21820',
                      'company': 'NatWest',
                      'valueForMoneyPercent': 13.6,
                      'customerServicePercent': 59.369999999999997442,
                      'customerOpinionPercent': null,
                      'overallOpinionPercent': null
                    }
                  }, {
                    '_id': cards[2]._id,
                    'moneyFactsID': 'LOANS000012',
                    'companyNo': '29600',
                    'companyName': 'Ulster Bank',
                    'loanRateType': 'Fixed',
                    'loanName': 'Personal Loan',
                    'repAPRAdvertised': '1.9',
                    'aprAdvertisedAdvMin': '7500.00',
                    'aprAdvertisedAdvMax': '14999.00',
                    'score': {
                      'moneyFactsID': '29600',
                      'company': 'Ulster Bank',
                      'valueForMoneyPercent': 90.5,
                      'customerServicePercent': 96.459999999999993747,
                      'customerOpinionPercent': null,
                      'overallOpinionPercent': null
                    }
                  }
                ]);

                done();
            });
        });
    });

    lab.test('it updates the loan rankings', function (done) {
        unsecuredLoanModel.find({}, function (err, loans) {
            Code.expect(err).to.not.exist();
            unsecuredLoansRanking.generateRankings(loans, function (err) {
                Code.expect(err).to.not.exist();
                unsecuredLoanModel.find({}, function (err, loans) {
                Code.expect(err).to.not.exist();
                    for (var i = loans.length - 1; i >= 0; i--) {
                        var card = loans[i];
                        if (card.companyNo === '21650') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(38.1);
                        } else if (card.companyNo === '21820') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(13.6);
                        } else if (card.companyNo === '29600') {
                            Code.expect(card.score.valueForMoneyPercent).to.equal(90.5);
                        } else {
                            throw new Error('Unexpected value in loans');
                        }
                    }

                    done();
                });
            });
        });
    });

    lab.test('it updates the total points', function (done) {
        unsecuredLoanModel.find({}, function (err, loans) {
            Code.expect(err).to.not.exist();
            unsecuredLoansRanking.generateRankings(loans, function (err) {
                Code.expect(err).to.not.exist();
                unsecuredLoanModel.find({}, function (err, loans) {
                Code.expect(err).to.not.exist();
                    for (var i = loans.length - 1; i >= 0; i--) {
                        var card = loans[i];
                        if (card.companyNo === '21650') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(59.25);
                        } else if (card.companyNo === '21820') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(36.485);
                        } else if (card.companyNo === '29600') {
                            Code.expect(card.score.overallOpinionPercent).to.equal(93.48);
                        } else {
                            throw new Error('Unexpected value in loans');
                        }
                    }

                    done();
                });
            });
        });
    });
});
