var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';

var UnsecuredLoansParser = require('../../../lib/parsers/unsecuredLoansProducts');
var threeLoans = Path.join(filesPath, moneyfactsDir + '/threeLoans.xml');
var loansSuppliersData = [
    { moneyfactsID: 21820 },
    { moneyfactsID: 21650 },
];

var lab = exports.lab = Lab.script();

lab.experiment('Unsecured Loans Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new UnsecuredLoansParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('', loansSuppliersData);
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new UnsecuredLoansParser();
        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            done();
        });

        parser.parse(threeLoans, loansSuppliersData);
    });

    lab.test('it reports each loan found', function (done) {
        var parser = new UnsecuredLoansParser();

        var loanCount = 0;

        parser.on('unsecuredLoans', function (loan) {
            loanCount++;
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            Code.expect(loanCount).to.equal(3);

            done();
        });

        parser.parse(threeLoans, loansSuppliersData);
    });

    lab.test('it pauses the parser when a loan is found', function (done) {
        var parser = new UnsecuredLoansParser();
        var stopped = false;
        var loanCount = 0;

        parser.on('unsecuredLoans', function (loan) {
            Code.expect(stopped).to.be.false();
            loanCount++;
            parser.stop();
            stopped = true;

            setTimeout(function () {
                stopped = false;
                parser.resume();
            }, 20);
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            Code.expect(loanCount).to.equal(3);

            done();
        });

        parser.parse(threeLoans, loansSuppliersData);
    });

    lab.test('it emits a new unsecuredLoan when it finishes', function (done) {
        var parser = new UnsecuredLoansParser();

        parser.on('unsecuredLoans', function (loan) {
            var schema = Joi.object().keys({
                moneyFactsID: Joi.string().regex(/LOANS[0-9]{6}/).required(),
                companyNo: Joi.number().required(),
                companyName: Joi.string().required(),
                repaymentChangeableTick: Joi.string().required(),
                lumpSumPaymentTick: Joi.string().required(),
                repaymentHolidayTick: Joi.string().required(),
                loanName: Joi.string().required(),
                loanRateType: Joi.string().required(),
                loanType: Joi.string().required(),
                customerType: Joi.string().required(),
                minAgeYrs: Joi.string().required(),
                acceptanceCriteriaNote: Joi.string().required(),
                imageURL: Joi.string().required(),
                rates: Joi.object().keys({
                    // representativeAPR: Joi.number()
                    repAPRAdvertised: Joi.string().required(),
                    aprAdvertisedAdvMin: Joi.string().required(),
                    aprAdvertisedAdvMax: Joi.string().required(),
                    examples: Joi.array().items(Joi.object().keys({
                        exampleDescription: Joi.string().required(),
                        exampleAdvance: Joi.string().required(),
                        exampleTermDays: Joi.string().required(),
                        exampleAnnualFlatRate: Joi.string().required(),
                        exampleRepresentativeAPR: Joi.string().required(),
                        exampleMonthlyPayment: Joi.string().required()
                    }))
                }),
                features: Joi.array().items(Joi.object().keys({
                    featureType: Joi.string().required(),
                    loanSpecificFeatureTick: Joi.string().required()
                })),
                fees: Joi.array().items(Joi.object().keys({
                    feeType: Joi.string().required(),
                    feePct: Joi.string().required(),
                    feePartOfMlyInstalTick: Joi.string().required()
                })),
                usages: Joi.array().items(Joi.object().keys({
                    usageType: Joi.string().required()
                }))
            });

            Joi.validate(loan, schema, function (err, value) {
                if (err) {
                    console.log('Error validating loan', err);
                }

                Code.expect(err).to.not.exist();
            });
        });

        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse(threeLoans, loansSuppliersData);
    });
});
