var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var unsecuredLoansSingleLoan = Path.join(filesPath, moneyfactsDir + '/singleLoan.xml');
var unsecuredLoansDuplicateLoan = Path.join(filesPath, moneyfactsDir + '/duplicateLoan.xml');
var threeUnsecuredLoans = Path.join(filesPath, moneyfactsDir + '/threeLoans.xml');
var unsecuredLoansSuppliersDataFile = Path.join(filesPath, otherDir + '/unsecured-loans-suppliers-whitelist.csv');
var unsecuredLoansWhitelistFile = Path.join(filesPath, otherDir + '/unsecured-loans-products-whitelist.csv');

var UnsecuredLoansImporter = require('../../../lib/importers/moneyImporter');
var UnsecuredLoansProductsParser = require('../../../lib/parsers/unsecuredLoansProducts');
var UnsecuredLoansSuppliersParser = require('../../../lib/parsers/unsecuredLoansSuppliers');
var WhitelistParser = require('../../../lib/parsers/whitelist');

// hard code these because safer for tests (they should be the same as in the data/csvs)
var whitelistArray = ['LOANS000002','LOANS000011'];
var fcaArray = ['21820', '21650'];

var moneyVariables = {
    unsecuredLoans: {
        database: 'unsecuredLoans',
        type: 'unsecuredLoans',
        rankingVariables: {
            distribution: 3010, // for distribution lookup table // TODO WHY 3010??
            divide: 10, // for distribution lookup table
            cdf: 3 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();

lab.experiment('Unsecured Loans Importer', function () {
    var companyModel;
    var unsecuredLoansModel;

    lab.beforeEach(function (done) {
        companyModel = db.get('suppliers');
        companyModel.remove({}, function () {
            unsecuredLoansModel = db.get('unsecuredLoans');
            unsecuredLoansModel.remove({}, done);
        });
    });

    lab.afterEach(function (done) {
        companyModel.remove({}, function () {
            unsecuredLoansModel.remove({}, done);
        });
    });

    lab.test('it inserts a supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(unsecuredLoansSingleLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.unsecuredLoanListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(unsecuredLoansDuplicateLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.unsecuredLoanListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it inserts a new loan', function (done) {
        unsecuredLoansModel.find({}, function (err, currentLoans) {
            Code.expect(err).to.not.exist();
            Code.expect(currentLoans).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(unsecuredLoansSingleLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                unsecuredLoansModel.find({},
                    function (err, newLoans) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newLoans).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate loan', function (done) {
        unsecuredLoansModel.find({}, function (err, currentLoans) {
            Code.expect(err).to.not.exist();
            Code.expect(currentLoans).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(unsecuredLoansDuplicateLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                unsecuredLoansModel.find({},
                    function (err, newLoans) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newLoans).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it inserts multiple loans', function (done) {
        unsecuredLoansModel.find({}, function (err, currentLoans) {
            Code.expect(err).to.not.exist();
            Code.expect(currentLoans).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(threeUnsecuredLoans, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                unsecuredLoansModel.find({},
                    function (err, newLoans) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newLoans).to.have.length(3);
                    done();
                });
            });
        });
    });

    lab.test('it correctly assigns marked as whitelisted', function (done) {
        unsecuredLoansModel.find({}, function (err, unsecuredLoan) {
            Code.expect(err).to.not.exist();
            Code.expect(unsecuredLoan).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(threeUnsecuredLoans, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    unsecuredLoansModel.find({},
                        function (err, newUnsecuredLoans) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newUnsecuredLoans).to.have.length(3);

                            for (var i = newUnsecuredLoans.length - 1; i >= 0; i--) {
                                if (whitelistArray.indexOf(newUnsecuredLoans[i].moneyFactsID) > -1) {
                                    Code.expect(newUnsecuredLoans[i].whitelisted).to.equal(true);
                                    Code.expect(newUnsecuredLoans[i].switchingUrl).to.equal('http://switchingurl.com');
                                } else {
                                    Code.expect(newUnsecuredLoans[i].whitelisted).to.equal(false);
                                    Code.expect(newUnsecuredLoans[i].switchingUrl).to.equal(null);
                                }
                            }

                            done();
                        });
                });
        });
    });

    lab.test('it correctly assigns marked as fca product', function (done) {
        unsecuredLoansModel.find({}, function (err, unsecuredLoan) {
            Code.expect(err).to.not.exist();
            Code.expect(unsecuredLoan).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(threeUnsecuredLoans, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    unsecuredLoansModel.find({},
                        function (err, newUnsecuredLoans) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newUnsecuredLoans).to.have.length(3);
                            for (var i = newUnsecuredLoans.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newUnsecuredLoans[i].companyNo) > -1) {
                                    Code.expect(newUnsecuredLoans[i].fcaProduct).to.equal(true);
                                } else {
                                    Code.expect(newUnsecuredLoans[i].fcaProduct).to.equal(false);
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it associates scoring with fca products', function (done) {
        unsecuredLoansModel.find({}, function (err, unsecuredLoan) {
            Code.expect(err).to.not.exist();
            Code.expect(unsecuredLoan).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(threeUnsecuredLoans, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                    Code.expect(err).to.not.exist();
                    unsecuredLoansModel.find({},
                        function (err, newUnsecuredLoans) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newUnsecuredLoans).to.have.length(3);
                            for (var i = newUnsecuredLoans.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newUnsecuredLoans[i].companyNo) > -1) {
                                    Code.expect(newUnsecuredLoans[i].score).to.exist();
                                } else {
                                    Code.expect(newUnsecuredLoans[i].score).to.not.exist();
                                }


                            }
                            done();
                        });
                });
        });
    });

    lab.test('it removes old loans', function (done) {
        unsecuredLoansModel.find({}, function (err, currentLoans) {
            Code.expect(err).to.not.exist();
            Code.expect(currentLoans).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(threeUnsecuredLoans, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                unsecuredLoansModel.find({},
                    function (err, newLoans) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newLoans).to.have.length(3);

                        var importer = new UnsecuredLoansImporter(
                            db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                            WhitelistParser);
                        importer.import(unsecuredLoansSingleLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                            function (err) {

                        Code.expect(err).to.not.exist();
                        unsecuredLoansModel.find({},
                            function (err, newLoans) {
                            Code.expect(err).to.not.exist();

                            var deletedCount = 0;

                            for (var i = newLoans.length - 1; i >= 0; i--) {
                                if (newLoans[i].deleted) {
                                    deletedCount++;
                                }
                            }

                            Code.expect(deletedCount).to.equal(2);

                            done();
                        });
                    });
                });
            });
        });
    });

    lab.test('it updates loans', function (done) {
        unsecuredLoansModel.find({}, function (err, currentLoans) {
            Code.expect(err).to.not.exist();
            Code.expect(currentLoans).to.be.empty();

            var importer = new UnsecuredLoansImporter(
                db, moneyVariables.unsecuredLoans, UnsecuredLoansProductsParser, UnsecuredLoansSuppliersParser,
                WhitelistParser);
            importer.import(unsecuredLoansDuplicateLoan, unsecuredLoansSuppliersDataFile, unsecuredLoansWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                unsecuredLoansModel.find({},
                    function (err, newLoans) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newLoans).to.have.length(1);

                    Code.expect(newLoans[0].loanRateType)
                    .to.equal('Variable');

                    done();
                });
            });
        });
    });
});
