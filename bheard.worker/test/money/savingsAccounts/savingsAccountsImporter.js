var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var savingsSingle = Path.join(filesPath, moneyfactsDir + '/singleSavings.xml');
var savingsDuplicate = Path.join(filesPath, moneyfactsDir + '/duplicateSavings.xml');
var threeSavings = Path.join(filesPath, moneyfactsDir + '/threeSavings.xml');
var savingsSuppliersDataFile = Path.join(filesPath, otherDir + '/savings-accounts-suppliers-whitelist.csv');
var savingsWhitelistFile = Path.join(filesPath, otherDir + '/savings-accounts-products-whitelist.csv');

var SavingsImporter = require('../../../lib/importers/moneyImporter');
var SavingsProductsParser = require('../../../lib/parsers/savingsAccountsProducts');
var SavingsSuppliersParser = require('../../../lib/parsers/savingsAccountsSuppliers');
var WhitelistParser = require('../../../lib/parsers/whitelist');

// hard code these because safer for tests (they should be the same as in the data/csvs)
var whitelistArray = ['SAVINGS000000901Monthly','SAVINGS000000501Yearly'];
var fcaArray = ['5705'];

var moneyVariables = {
    savingsAccounts: {
        database: 'savings',
        type: 'savingsAccount',
        rankingVariables: {
            distribution: 10000, // for distribution lookup table
            divide: 100, // for distribution lookup table
            cdf: 4 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();

lab.experiment('Savings Accounts Importer', function () {
    var companyModel;
    var savingsAccountModel;

    lab.beforeEach(function (done) {
        companyModel = db.get('suppliers');
        companyModel.remove({}, function () {
            savingsAccountModel = db.get('savings');
            savingsAccountModel.remove({}, done);
        });
    });

    lab.afterEach(function (done) {
        companyModel.remove({}, function () {
            savingsAccountModel.remove({}, done);
        });
    });

    lab.test('it inserts a supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(savingsSingle, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.savingsAccountListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate supplier record', function (done) {
        companyModel.find({}, function (err, companies) {
            Code.expect(err).to.not.exist();
            Code.expect(companies).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(savingsDuplicate, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                companyModel.find({}, function (err, newCompanies) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newCompanies).to.have.length(1);
                    Code.expect(newCompanies[0].urls.savingsAccountListings).to.equal("http://bank/cards.com");
                    done();
                });
            });
        });
    });

    lab.test('it inserts a new account', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccounts).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(savingsSingle, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                savingsAccountModel.find({},
                    function (err, newSavingsAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newSavingsAccounts).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it does not insert a duplicate account', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccounts).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(savingsDuplicate, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                savingsAccountModel.find({},
                    function (err, newSavingsAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newSavingsAccounts).to.have.length(1);
                    done();
                });
            });
        });
    });

    lab.test('it inserts multiple accounts', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccounts).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(threeSavings, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                savingsAccountModel.find({},
                    function (err, newSavingsAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newSavingsAccounts).to.have.length(3);
                    done();
                });
            });
        });
    });

    lab.test('it correctly assigns marked as whitelisted', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccount).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(threeSavings, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    savingsAccountModel.find({},
                        function (err, newSavings) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newSavings).to.have.length(3);

                            for (var i = newSavings.length - 1; i >= 0; i--) {
                                if (whitelistArray.indexOf(newSavings[i].moneyFactsID) > -1) {
                                    Code.expect(newSavings[i].whitelisted).to.equal(true);
                                    Code.expect(newSavings[i].switchingUrl).to.equal('http://switchingurl.com');
                                } else {
                                    Code.expect(newSavings[i].whitelisted).to.equal(false);
                                    Code.expect(newSavings[i].switchingUrl).to.equal(null);
                                }
                            }

                            done();
                        });
                });
        });
    });

    lab.test('it correctly assigns marked as fca product', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccount).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(threeSavings, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {
                    Code.expect(err).to.not.exist();
                    savingsAccountModel.find({},
                        function (err, newSavings) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newSavings).to.have.length(3);
                            for (var i = newSavings.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newSavings[i].companyNo) > -1) {
                                    Code.expect(newSavings[i].fcaProduct).to.equal(true);
                                } else {
                                    Code.expect(newSavings[i].fcaProduct).to.equal(false);
                                }
                            }
                            done();
                        });
                });
        });
    });

    lab.test('it associates scoring with fca products', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccount) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccount).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(threeSavings, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                    Code.expect(err).to.not.exist();
                    savingsAccountModel.find({},
                        function (err, newSavings) {
                            Code.expect(err).to.not.exist();
                            Code.expect(newSavings).to.have.length(3);
                            for (var i = newSavings.length - 1; i >= 0; i--) {
                                if (fcaArray.indexOf(newSavings[i].companyNo) > -1) {
                                    Code.expect(newSavings[i].score).to.exist();
                                } else {
                                    Code.expect(newSavings[i].score).to.not.exist();
                                }


                            }
                            done();
                        });
                });
        });
    });

    lab.test('it removes old accounts', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccounts) {
            Code.expect(err).to.not.exist();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(threeSavings, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                savingsAccountModel.find({},
                    function (err, newSavingsAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newSavingsAccounts).to.have.length(3);

                        var importer = new SavingsImporter(
                            db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                            WhitelistParser);
                        importer.import(savingsDuplicate, savingsSuppliersDataFile, savingsWhitelistFile,
                            function (err) {

                        Code.expect(err).to.not.exist();
                        savingsAccountModel.find({},
                            function (err, newSavingsAccounts) {
                            Code.expect(err).to.not.exist();
                            var deletedCount = 0;

                            for (var i = newSavingsAccounts.length - 1; i >= 0; i--) {
                                if (newSavingsAccounts[i].deleted) {
                                    deletedCount++;
                                }
                            }

                            Code.expect(deletedCount).to.equal(2);

                            done();
                        });
                    });
                });
            });
        });
    });

    lab.test('it updates accounts', function (done) {
        savingsAccountModel.find({}, function (err, savingsAccounts) {
            Code.expect(err).to.not.exist();
            Code.expect(savingsAccounts).to.be.empty();

            var importer = new SavingsImporter(
                db, moneyVariables.savingsAccounts, SavingsProductsParser, SavingsSuppliersParser,
                WhitelistParser);
            importer.import(savingsDuplicate, savingsSuppliersDataFile, savingsWhitelistFile,
                function (err) {

                Code.expect(err).to.not.exist();
                savingsAccountModel.find({},
                    function (err, newSavingsAccounts) {
                    Code.expect(err).to.not.exist();
                    Code.expect(newSavingsAccounts).to.have.length(1);

                    Code.expect(newSavingsAccounts[0].accountType)
                    .to.equal('Adult');

                    done();
                });
            });
        });
    });
});
