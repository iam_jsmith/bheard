var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';

var SavingsParser = require('../../../lib/parsers/savingsAccountsProducts');
var threeSavings = Path.join(filesPath, moneyfactsDir + '/threeSavings.xml');
var badSavings = Path.join(filesPath, moneyfactsDir + '/badnamesavings.xml');
var savingsSuppliersData = [
    { moneyfactsID: 5705 }
];

var lab = exports.lab = Lab.script();

lab.experiment('Savings Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new SavingsParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('', savingsSuppliersData);
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new SavingsParser();
        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();
            done();
        });

        parser.parse(threeSavings, savingsSuppliersData);
    });

    lab.test('it reports each account found', function (done) {
        var parser = new SavingsParser();

        var accountCount = 0;

        parser.on('savingsAccount', function (account) {
            accountCount++;
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            Code.expect(accountCount).to.equal(3);

            done();
        });

        parser.parse(threeSavings, savingsSuppliersData);
    });

    lab.test('it pauses the parser when an account is found', function (done) {
        var parser = new SavingsParser();
        var stopped = false;
        var accountCount = 0;

        parser.on('savingsAccount', function (account) {
            Code.expect(stopped).to.be.false();
            accountCount++;
            parser.stop();
            stopped = true;

            setTimeout(function () {
                stopped = false;
                parser.resume();
            }, 20);
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            Code.expect(accountCount).to.equal(3);

            done();
        });

        parser.parse(threeSavings, savingsSuppliersData);
    });

    lab.test('it emits a new savingsAccountRecord when it finishes',
        function (done) {
        var parser = new SavingsParser();

        parser.on('savingsAccount', function (account) {
            console.log('Account', account);
            var schema = Joi.object().keys({
                moneyFactsID: Joi.string(), // .regex(/SAVINGS[0-9]{9}/).required(),
                accountType: Joi.string().required(),
                accountName: Joi.string().required(),
                companyNo: Joi.number().required(),
                companyName: Joi.string().required(),
                noticeDays: Joi.number().required(),
                minAge: Joi.string(),
                maxInv: Joi.number().required(),
                minInv: Joi.number().required(),
                notice: Joi.string(),
                term: Joi.string(),
                minOperateBal: Joi.string(),
                rateType: Joi.string(),
                termDays: Joi.number(),
                rates: Joi.array().items(Joi.object().keys({
                    grossAER: Joi.number().required(),
                    tierLower: Joi.string().required(),
                    tierUpper: Joi.string().required()
                })),
                access: Joi.object().keys({
                  branchTick: Joi.string().required(),
                  teleTick: Joi.string().required(),
                  postTick: Joi.string().required(),
                  iNetTick: Joi.string().required(),
                  mobileAppTick: Joi.string().required()
                }),
                introBonusRate: Joi.number(),
                introBonusPeriod: Joi.number(),
                imageURL: Joi.string().required()
            });

            Joi.validate(account, schema, function (err, value) {
                if (err) {
                    console.log('Error validating account', err);
                }

                Code.expect(err).to.not.exist();
            });
        });

        parser.on('end', function (err) {
            console.log('END CALLED');
            Code.expect(err).to.not.exist();

            done();
        });

            parser.parse(threeSavings, savingsSuppliersData);
        // parser.parse('/Users/aiden/Downloads/VochoCurrentAccounts_Iota.xml');
    });

    lab.test('it reads company names with ampersands in them', function (done) {
        var parser = new SavingsParser();
        parser.on('savingsAccount', function (account) {
            Code.expect(account.companyName).to.equal('C. Hoare & Co');
        });

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();
            done();
        });

        parser.parse(badSavings, savingsSuppliersData);
    });
});
