var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var filesPath = './test/money/data';
var moneyfactsDir = 'moneyfacts';
var otherDir = 'csvs';

var SavingsAccountScoresParser = require('../../../lib/parsers/savingsAccountsSuppliers');
var savingsAccountSuppliersDataFile = Path.join(filesPath, otherDir + '/savings-accounts-suppliers-whitelist.csv');

var lab = exports.lab = Lab.script();

lab.experiment('Savings Account Score Parser', function () {
    lab.test('it end immediately if there is no file to load', function (done) {
        var parser = new SavingsAccountScoresParser();

        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse('');
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new SavingsAccountScoresParser();
        parser.on('end', function (err) {
            Code.expect(err).to.not.exist();

            done();
        });

        parser.parse(savingsAccountSuppliersDataFile);
    });

    lab.test('it returns valid normalised scores', function (done) {
        var parser = new SavingsAccountScoresParser();

        parser.on('end', function (err, scores) {
            Code.expect(err).to.not.exist();

            console.log(scores);

            var schema = Joi.array().items(
                Joi.object().keys({
                    moneyFactsID: Joi.string().required(),
                    company: Joi.string().required(),
                    valueForMoneyPercent: Joi.number().optional(),
                    customerServicePercent: Joi.number().optional(),
                    customerOpinionPercent: Joi.number().optional(),
                    overallOpinionPercent: Joi.number().optional(),
                    savingsAccountListings: Joi.string().optional()
                })
            );

            Joi.validate(scores, schema, function (err, value) {
                if (err) {
                    console.log('Error validating scores:', err);
                    throw err;
                }

                Code.expect(err).to.not.exist();
            });

            done();
        });

        parser.parse(savingsAccountSuppliersDataFile);
    });
});
