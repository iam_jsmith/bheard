var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var async = require('async');
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var SavingsAccountRanking = require('../../../lib/builders/moneyRanker');

var moneyVariables = {
    savingsAccounts: {
        database: 'savings',
        type: 'savingsAccount',
        rankingVariables: {
            distribution: 10000, // for distribution lookup table
            divide: 100, // for distribution lookup table
            cdf: 4 // for distribution lookup table
        }
    }
};

var lab = exports.lab = Lab.script();
var banksModel = db.get('suppliers');
var savingsAccountModel = db.get('savings');
var savingsAccountRanking = new SavingsAccountRanking(db, moneyVariables['savingsAccounts']);

var source = [
{
  'moneyFactsID': 'SAVINGS000000501Yearly',
  'accountType': 'Childrens',
  'companyNo': '5705',
  'companyName': 'Bank of Ireland UK',
  'account': 'KidSave',
  'minInv': '1',
  'maxInv': '0',
  'notice': 'Instant',
  'noticeDays': '0',
  'termDays': '0',
  'rates': [
    {
      'grossAER': '0.1'
    },
    {
      'grossAER': '0.25'
    }
  ],
  'score': {
    'moneyFactsID': '5705',
    'company': 'Bank of Ireland UK',
    'valueForMoneyPercent': null,
    'customerServicePercent': 30,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'SAVINGS000007501Yearly',
  'accountType': 'Childrens',
  'companyNo': '27500',
  'companyName': 'Stafford Railway BS',
  'account': 'First Track',
  'minInv': '1',
  'maxInv': '50000',
  'notice': 'Instant',
  'noticeDays': '0',
  'termDays': '0',
  'rates': [
    {
      'grossAER': '1.75'
    }
  ],
  'score': {
    'moneyFactsID': '27500',
    'company': 'Stafford Railway BS',
    'valueForMoneyPercent': null,
    'customerServicePercent': 98,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}, {
  'moneyFactsID': 'SAVINGS000001001Yearly',
  'accountType': 'Childrens',
  'companyNo': '6550',
  'companyName': 'Barnsley BS',
  'account': 'Falcon First',
  'minInv': '1',
  'maxInv': '50000',
  'notice': 'Instant',
  'noticeDays': '0',
  'termDays': '0',
  'rates': [
    {
      'grossAER': '2.00'
    }
  ],
  'score': {
    'moneyFactsID': '6550',
    'company': 'Barnsley BS',
    'valueForMoneyPercent': null,
    'customerServicePercent': 67,
    'customerOpinionPercent': null,
    'overallOpinionPercent': null
  }
}];

lab.experiment('Savings Account Ranking', function () {
    lab.beforeEach(function (done) {
        savingsAccountModel.remove({}, function () {
            async.each(source, function (account, callback) {
                savingsAccountModel.insert(account, callback);
            }, done);
        });
    });

    lab.test('it calculates the correct mean', function (done) {
        savingsAccountRanking.calculateMean([0.25, 1.75, 2], function (err, mean) {
            Code.expect(err).to.not.exist();
            Code.expect(mean).to.equal(1.3333333333333333);
            done();
        });
    });

    lab.test('it calculates the variance properly', function (done) {
        savingsAccountRanking.calculateVariance([0.25, 1.75, 2], function (err, variance) {
            Code.expect(err).to.not.exist();
            Code.expect(variance).to.equal(0.5972222222222222);
            done();
        });
    });

    lab.test('it calculates the standard deviation properly', function (done) {
        savingsAccountRanking.calculateStandardDeviation([0.25, 1.75, 2], function (err, stdev) {
            Code.expect(err).to.not.exist();
            Code.expect(stdev).to.equal(0.7728015412913086);
            done();
        });
    });

    lab.test('it calculates the probability mass function properly', function (done) {
        savingsAccountRanking.calculateNormalDistribution(
            1.75,
            1.3333333333333333,
            5.5972222222222222,
            0.7728015412913086,
            function (err, norm) {
            Code.expect(err).to.not.exist();
            Code.expect(norm).to.equal(0.0020059749205706204);
            done();
        });
    });

    lab.test('it generates a lookup table based on the normal', function (done) {
        savingsAccountRanking.generateDistributionLookupTable(
            1.3333333333333333,
            5.5972222222222222,
            0.7728015412913086,
            function (err, distributionTable) {
            Code.expect(err).to.not.exist();
            var expectedTable = require('../data/sampletablesavings');

            Code.expect(distributionTable).to.deep.equal(expectedTable);
            done();
        });
    });

    lab.test('it calculates the cumulative distribution properly', function (done) {
        savingsAccountRanking.generateDistributionLookupTable(
            1.3333333333333333,
            5.5972222222222222,
            0.7728015412913086,
            function (err, table) {
            Code.expect(err).to.not.exist();
            savingsAccountRanking.calculateCumulativeDistribution(
                1.75,
                function (err, distribution) {
                Code.expect(err).to.not.exist();
                Code.expect(distribution).to.equal(0.00645);
                done();
            });
        });
    });

    lab.test('it extracts the ratings correctly', function (done) {
        savingsAccountModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            savingsAccountRanking.filterItems(cards, function (err, rates) {
                Code.expect(err).to.not.exist();
                Code.expect(rates).to.have.length(3);

                Code.expect(rates).to.deep.equal([
                    { moneyFactsID: 'SAVINGS000000501Yearly', rate: 0.25 },
                    { moneyFactsID: 'SAVINGS000007501Yearly', rate: 1.75 },
                    { moneyFactsID: 'SAVINGS000001001Yearly', rate: 2 }
                ]);

                done();
            });
        });
    });

    lab.test('it calculates the ranking', function (done) {
        savingsAccountModel.find({}, function (err, cards) {
            Code.expect(err).to.not.exist();
            savingsAccountRanking.calculateRankings(cards, function (err, ranked) {

                Code.expect(err).to.not.exist();
                Code.expect(ranked).to.deep.equal([
                    { moneyFactsID: 'SAVINGS000000501Yearly', rate: 0.25, rank: 4 },
                    { moneyFactsID: 'SAVINGS000007501Yearly', rate: 1.75, rank: 66.6 },
                    { moneyFactsID: 'SAVINGS000001001Yearly', rate: 2, rank: 76.6 }
                ]);

                done();
            });
        });
    });

    lab.test('it accounts with no rates', function (done) {
        savingsAccountRanking.generateRankings([{
            moneyFactsID: 'SAVINGS000001001Yearly',
            companyNo: '13500'}],
            function (err) {

            Code.expect(err).to.not.exist();
            done();
        });
    });

    lab.test('it updates the accounts rankings', function (done) {
        savingsAccountModel.find({}, function (err, accounts) {
            Code.expect(err).to.not.exist();
            savingsAccountRanking.generateRankings(accounts, function (err) {
                Code.expect(err).to.not.exist();
                savingsAccountModel.find({}, function (err, accounts) {
                Code.expect(err).to.not.exist();
                    for (var i = accounts.length - 1; i >= 0; i--) {
                        var account = accounts[i];
                        if (account.companyNo === '5705') {
                            Code.expect(account.score.valueForMoneyPercent).to.equal(4);
                        } else if (account.companyNo === '27500') {
                            Code.expect(account.score.valueForMoneyPercent).to.equal(66.6);
                        } else if (account.companyNo === '6550') {
                            Code.expect(account.score.valueForMoneyPercent).to.equal(76.6);
                        } else {
                            throw new Error('Unexpected value in accounts');
                        }
                    }

                    done();
                });
            });
        });
    });

    lab.test('it updates the total points', function (done) {
        savingsAccountModel.find({}, function (err, accounts) {
            Code.expect(err).to.not.exist();
            savingsAccountRanking.generateRankings(accounts, function (err) {
                Code.expect(err).to.not.exist();
                savingsAccountModel.find({}, function (err, accounts) {
                Code.expect(err).to.not.exist();
                    for (var i = accounts.length - 1; i >= 0; i--) {
                        var account = accounts[i];
                        if (account.companyNo === '5705') {
                            Code.expect(account.score.overallOpinionPercent).to.equal(17);
                        } else if (account.companyNo === '27500') {
                            Code.expect(account.score.overallOpinionPercent).to.equal(82.3);
                        } else if (account.companyNo === '6550') {
                            Code.expect(account.score.overallOpinionPercent).to.equal(71.8);
                        } else {
                            throw new Error('Unexpected value in accounts');
                        }
                    }

                    done();
                });
            });
        });
    });
});
