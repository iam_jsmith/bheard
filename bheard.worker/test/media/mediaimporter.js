var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');
var config = require('bheard').config();
var db = require('bheard')
            .db(config.get('/database/mongodb/url', {env: 'devtest'}));

var MediaImporter = require('../../lib/importers/media');

var lab = exports.lab = Lab.script();

lab.experiment('Media Importer', function () {
    var companyModel;

    lab.beforeEach(function (done) {
        companyModel = db.get('suppliers');
        companyModel.remove({}, done);
    });

    lab.afterEach(function (done) {
        companyModel.remove({}, done);
    });

    lab.test('it inserts a supplier record', function (done) {
        companyModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new MediaImporter(db);
            importer.import(Path.join(__dirname, 'singlemedia.csv'),
                function (err) {

                if (err) {
                    console.log('Error importing: ', err);
                }

                Code.expect(err).to.not.exist();

                companyModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);

                    var schema = Joi.array().items(Joi.object().keys({
                        _id: Joi.any().required(),
                        sector: Joi.string().required(),
                        name: Joi.string().required(),
                        no: Joi.number().required(),
                        mobile_provider: Joi.boolean().required(),
                        broadband_provider: Joi.boolean().required(),
                        fixed_line_provider: Joi.boolean().required(),
                        tv_provider: Joi.boolean().required(),
                        score: Joi.object().keys({
                            overallOpinion: Joi.number(),
                            overallOpinionPercent: Joi.number(),
                            customerOpinion: Joi.number(),
                            customerOpinionPercent: Joi.number(),
                            customerService: Joi.number(),
                            customerServicePercent: Joi.number(),
                            valueForMoney: Joi.number(),
                            valueForMoneyPercent: Joi.number().valid(null)
                        })
                    }));

                    Joi.validate(newSuppliers, schema, function (err, value) {
                        if (err) {
                            console.log(err);
                        }

                        Code.expect(err).to.not.exist();
                    });

                    done();
                });
            });
        });
    });

    lab.test('it selects correct provider attributes', function (done) {
        companyModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new MediaImporter(db);
            importer.import(Path.join(__dirname, 'singlemedia.csv'),
                function (err) {

                if (err) {
                    console.log('Error importing: ', err);
                }

                Code.expect(err).to.not.exist();

                companyModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);

                    Code.expect(newSuppliers[0].mobile_provider).to.be.true();
                    Code.expect(newSuppliers[0].broadband_provider).to.be.false();
                    Code.expect(newSuppliers[0].fixed_line_provider).to.be.false();
                    Code.expect(newSuppliers[0].tv_provider).to.be.false();

                    done();
                });
            });
        });
    });

    lab.test('it combines provider offerings', function (done) {
        companyModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new MediaImporter(db);
            importer.import(Path.join(__dirname, 'duplicatemedia.csv'),
                function (err) {

                if (err) {
                    console.log('Error importing: ', err);
                }

                Code.expect(err).to.not.exist();

                companyModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);

                    Code.expect(newSuppliers[0].mobile_provider).to.be.true();
                    Code.expect(newSuppliers[0].broadband_provider).to.be.false();
                    Code.expect(newSuppliers[0].fixed_line_provider).to.be.true();
                    Code.expect(newSuppliers[0].tv_provider).to.be.false();

                    done();
                });
            });
        });
    });

    lab.test('it calcualtes the correct scores', function (done) {
        companyModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new MediaImporter(db);
            importer.import(Path.join(__dirname, 'duplicatemedia.csv'),
                function (err) {

                if (err) {
                    console.log('Error importing: ', err);
                }

                Code.expect(err).to.not.exist();

                companyModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();
                    Code.expect(newSuppliers).to.have.length(1);

                    Code.expect(newSuppliers[0].score.customerOpinionPercent).to.equal(62);
                    Code.expect(newSuppliers[0].score.customerServicePercent).to.equal(60);
                    Code.expect(newSuppliers[0].score.valueForMoneyPercent).to.equal(61);
                    Code.expect(newSuppliers[0].score.overallOpinionPercent).to.equal(61);

                    done();
                });
            });
        });
    });

    lab.test('it creates valid entities when combining', function (done) {
        companyModel.find({}, function (err, suppliers) {
            Code.expect(err).to.not.exist();
            Code.expect(suppliers).to.have.length(0);

            var importer = new MediaImporter(db);
            importer.import(Path.join(__dirname, 'media.csv'),
                function (err) {

                if (err) {
                    console.log('Error importing: ', err);
                }

                Code.expect(err).to.not.exist();

                companyModel.find({}, function (err, newSuppliers) {
                    if (err) {
                        console.log(err);
                    }

                    Code.expect(err).to.not.exist();

                    var schema = Joi.array().items(Joi.object().keys({
                        _id: Joi.any().required(),
                        sector: Joi.string().required(),
                        name: Joi.string().required(),
                        no: Joi.number().required(),
                        mobile_provider: Joi.boolean().required(),
                        broadband_provider: Joi.boolean().required(),
                        fixed_line_provider: Joi.boolean().required(),
                        tv_provider: Joi.boolean().required(),
                        score: Joi.object().keys({
                            overallOpinion: Joi.number(),
                            overallOpinionPercent: Joi.number(),
                            customerOpinion: Joi.number(),
                            customerOpinionPercent: Joi.number(),
                            customerService: Joi.number(),
                            customerServicePercent: Joi.number(),
                            valueForMoney: Joi.number(),
                            valueForMoneyPercent: Joi.number().valid(null)
                        })
                    }));

                    Joi.validate(newSuppliers, schema, function (err, value) {
                        if (err) {
                            console.log(err);
                        }

                        Code.expect(err).to.not.exist();
                    });

                    done();
                });
            });
        });
    });
});
