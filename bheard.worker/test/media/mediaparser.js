var Lab = require('lab');
var Code = require('code');
var Path = require('path');
var Joi = require('joi');

var MediaParser = require('../../lib/parsers/media');

var lab = exports.lab = Lab.script();

lab.experiment('Media Parser', function () {
    lab.test('it reports an error if there is no file to load',
        function (done) {

        var parser = new MediaParser();
        parser.on('error', function (err) {
            Code.expect(err).to.exist();
            done();
        });

        parser.parse('');
    });

    lab.test('it reports when the parsing has ended', function (done) {
        var parser = new MediaParser();
        parser.on('end', function () {
            done();
        });

        parser.parse(Path.join(__dirname, 'singlemedia.csv'));
    });

    lab.test('it creates a list of valid media', function (done) {
        var parser = new MediaParser();

        parser.on('end', function (media) {
            var schema = Joi.array().items(Joi.object().keys({
                sector: Joi.string().required(),
                subSector: Joi.string().required(),
                name: Joi.string().required(),
                no: Joi.number().required(),
                score: Joi.object().keys({
                    customerServicePercent: Joi.number(),
                    customerOpinionPercent: Joi.number(),
                    valueForMoneyPercent: Joi.number(),
                    overallOpinionPercent: Joi.number().required()
                })
            }));

            Joi.validate(media, schema, function (err, value) {
                if (err) {
                    console.log(err);
                }

                Code.expect(err).to.not.exist();
            });

            done();
        });

        parser.parse(Path.join(__dirname, 'media.csv'));
    });
});
