var config = require('./config')(),
    log4js = require('log4js');

log4js.configure(config.get('/logging'));

module.exports = function () {
    return log4js;
};
