var config = require('./config')(),
    http = require('./http')(),
    apiKey = config.get('/mailChimp/apiKey'),
    usersListId = config.get('/mailChimp/usersListId');

module.exports = function () {
    return {
        subscribe: function (user) {
            return http.request({
                headers: {
                    'Authorization': 'Basic ' + new Buffer('apiKey:' + apiKey).toString('base64')
                },
                host: 'us12.api.mailchimp.com',
                method: 'PUT',
                path: '/3.0/lists/' + usersListId + '/members/' + user.hash,
                body: {
                    'email_address': user.email,
                    'email_type': 'html',
                    'status': 'subscribed',
                    'merge_fields': {
                        'FNAME': user.firstName,
                        'LNAME': user.lastName
                    }
                }
            });
        }
    };
};
