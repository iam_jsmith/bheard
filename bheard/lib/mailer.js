var nodemailer = require('nodemailer'),
    q = require('q'),
    config = require('./config')();

module.exports = function () {
    return {
        send: function (options) {
            var deferred = q.defer(),
                transporter = nodemailer.createTransport({
                    service: 'Gmail',
                    auth: {
                        user: config.get('/email/username'),
                        pass: config.get('/email/password')
                    }
                });

            if (options.to === 'contact@bheard.com' && process.env.NODE_ENV !== 'production') {
                options.to = 'errors@bheard.com';
            }

            transporter.sendMail(options, function (err) {
                if (!err) {
                    deferred.resolve();
                } else {
                    deferred.reject(err);
                }
            });

            return deferred.promise;
        }
    };
};
