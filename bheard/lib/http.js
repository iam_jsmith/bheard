var https = require('https'),
    _ = require('lodash'),
    q = require('q'),
    querystring = require('querystring');

module.exports = function () {
    return {
        request: function (options) {
            var deferred = q.defer();

            options = _.extend({}, options);

            if (options.params) {
                options.path += '?' + querystring.stringify(options.params);
            }

            if (options.body) {
                if (!options.headers) {
                    options.headers = {};
                }

                if (_.isPlainObject(options.body)) {
                    options.headers['Content-Type'] = 'application/json';
                    options.body = JSON.stringify(options.body);
                }

                options.headers['Content-Length'] = Buffer.byteLength(options.body);
            }

            var req = https.request(options);

            if (options.body) {
                req.write(options.body);
            }

            req.on('response', function (res) {
                if (res.statusCode === 200) {
                    var chunks = [];

                    res.setEncoding('utf8');

                    res.on('data', function (chunk) {
                        chunks.push(chunk);
                    });

                    res.on('end', function () {
                        var str = chunks.join(''),
                            data = null;

                        try {
                            data = JSON.parse(str);
                        } catch (err) {
                            data = querystring.parse(str);
                        }

                        deferred.resolve(data);
                    });
                } else {
                    deferred.reject(new Error(res.statusMessage));
                }
            });

            req.on('error', function(err) {
                deferred.reject(err);
            });

            req.end();

            return deferred.promise;
        }
    };
};
