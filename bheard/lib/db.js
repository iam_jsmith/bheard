﻿var config = require('./config')(),
    monk = require('monk'),
    db = null;

monk.Promise.prototype.__proto__.catch = function (rejected) {
    return this.then(undefined, rejected);
};

module.exports = function (url) {
    if (!db) {
        if (!url) {
            url = config.get('/database/mongodb/url');
        }

        db = monk(url);

        db.get('users').index('email', { sparse: true, unqiue: true });
        db.get('users').index('facebookId', { sparse: true, unqiue: true });
        db.get('suppliers').update({ sector: 'phoneInternet' }, { $set: { sector: 'phoneandinternet' } }, { multi: true });
        db.get('supplierReviews').update({ sector: 'phoneInternet' }, { $set: { sector: 'phoneandinternet' } }, { multi: true });
        db.get('supplierReviews').update({}, { $rename: { rating: 'helpful' } }, { multi: true });
        db.get('supplierReviews').update({ unhelpful: { $exists: false } }, { $set: { unhelpful: 0 } }, { multi: true });
    }

    return db;
};
