var _ = require('lodash'),
    config = require('./config')(),
    http = require('./http')(),
    apiKey = config.get('/blogger/apiKey'),
    blogId = config.get('/blogger/blogId'),
    host = 'www.googleapis.com',
    pathPrefix = '/blogger/v3/blogs/' + blogId;

module.exports = function () {
    return {
        getPosts: function (pageToken) {
            return this._get('/posts', pageToken ? { pageToken: pageToken } : null);
        },
        
        getPostsWithLabels: function(pageToken, labels) {
            return this._get('/posts', pageToken ? { pageToken: pageToken, labels: labels } : { labels: labels});
        },

        getPost: function (id) {
            return this._get('/posts/' + id);
        },

        _get: function (path, params) {
            return http.request({
                host: host,
                method: 'GET',
                path: pathPrefix + path,
                params: _.extend({}, params, { key: apiKey })
            });
        }
    };
};
