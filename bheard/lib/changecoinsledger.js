var async = require('async');

var internals = {
    updateUserCoins: function updateUserCoins(db, entry, cb) {
        db.get('users').updateById(entry.user, { $inc: { changeCoins: entry.coins } }, cb);
    },

    updateCharityCoins: function updateUserCoins(db, entry, cb) {
        db.get('charities').updateById(entry.charity, { $inc: { changeCoins: entry.coins } }, cb);
    },

    insertEntry: function (db, entry, cb) {
        entry.user = entry.user._id;

        if (entry.charity) {
            entry.charity = entry.charity._id;
        }

        db.get('changeCoinsLedger').insert(entry, cb);
    },

    loadCharity: function (db, entry, cb) {
        var query = {
            _id: entry.charity
        };

        db.get('charities').findOne(query, cb);
    },
    loadUser: function (db, entry, cb) {
        var query;

        if (typeof entry.user === 'string') {
            query = { _id: entry.user };
        } else if (entry.user.unconfirmedEmail) {
            query = {
                unconfirmedEmail: entry.user.unconfirmedEmail
            };
        } else {
            query = {
                email: entry.user.email
            };
        }

        db.get('users').findOne(query, cb);
    },

    insertLedgerEntry: function insertLedgerEntry(db, entry, cb) {
        var self = this;

        async.parallel({
            loadUser: function (callback) {
                if (!entry.user.hasOwnProperty('_id')) {
                    self.loadUser(db, entry, function (err, user) {
                        if (err) {
                            return callback(err);
                        }

                        entry.user = user;
                        callback(null, entry);
                    });
                } else {
                    callback(null);
                }
            },
            loadCharity: function (callback) {
                if (entry.charity && !entry.charity.hasOwnProperty('_id')) {
                    self.loadCharity(db, entry, function (err, charity) {
                        if (err) {
                            return callback(err);
                        }

                        entry.charity = charity;
                        callback(null, entry);
                    });
                } else {
                    callback(null);
                }
            }
        }, function (err) {
            if (err) {
                return cb(err);
            }

            self.insertEntry(db, entry, cb);
        });
    }
};

module.exports = function (db) {
    return {
        internals: internals,
        donate: function (user, charity, coins, cb) {
            async.parallel({
                user: function (callback) {
                    internals.loadUser(db, { user: user }, callback);
                },
                charity: function (callback) {
                    internals.loadCharity(db, { charity: charity }, callback);
                }
            }, function (err, data) {
                if (err) {
                    return cb(err);
                }

                var charity = data.charity;

                var removeCoinEntry = {
                    user: data.user,
                    event: 'donationsent',
                    charity: data.charity,
                    coins: -coins,
                    entryDate: new Date()
                };

                var addCoinEntry = {
                    user: data.user,
                    event: 'donationreceived',
                    charity: data.charity,
                    coins: +coins,
                    entryDate: new Date()
                };

                internals.insertEntry(db, removeCoinEntry, function () {
                    internals.updateUserCoins(db, removeCoinEntry, function () {});
                    internals.insertEntry(db, addCoinEntry, function () {
                      internals.updateCharityCoins(db, addCoinEntry, function () {
                        cb(null, charity);
                      });
                    });
                });
            });
        },
        addCoins: function (user, event, coins, charity, cb) {
            if (typeof charity === 'function') {
                cb = charity;
                charity = undefined;
            }

            var ledgerEntry = {
                user: user,
                event: event,
                coins: +coins,
                entryDate: new Date()
            };

            if (charity) {
                ledgerEntry.charity = charity;
            }

            internals.insertLedgerEntry(db, ledgerEntry, function (err, entry) {
                if (err) {
                    // roll back something?
                    return cb(err);
                }

                if (event === 'donationreceived') {
                    internals.updateCharityCoins(db, entry, cb);
                } else {
                    internals.updateUserCoins(db, entry, cb);
                }
            });
        },
        removeCoins: function (user, event, coins, charity, cb) {
            // Check that the user has this many coins to actually consume

            if (typeof charity === 'function') {
                cb = charity;
                charity = undefined;
            }

            var ledgerEntry = {
                user: user,
                event: event,
                coins: -coins,
                entryDate: new Date()
            };

            if (charity) {
                ledgerEntry.charity = charity;
            }

            internals.insertLedgerEntry(db, ledgerEntry, function (err, entry) {
                if (err) {
                    // roll back something?
                    return cb(err);
                }

                internals.updateUserCoins(db, entry, cb);
            });
        },
        entriesForCharity: function (charity, cb) {
            var query = {
                charity: charity._id
            };

            db.get('changeCoinsLedger').find(query, { sort: { 'entryDate': 1 }}, function (err, entries) {
                if (err) {
                    return cb(err);
                }

                var total = 0;
                var balanceEntries = entries.reduce(function (accumulation, current, index, array) {
                    total += current.coins;
                    current.balance = total;
                    accumulation.push(current);

                    return accumulation;
                }, []);

                return cb(err, balanceEntries);
            });
        },
        entriesForUser: function (user, cb) {
            var query = {
                user: user._id,
                event: { $ne: 'donationreceived' }
            };

            var self = this;

            db.get('changeCoinsLedger').find(query, { sort: { 'entryDate': 1 }}, function (err, entries) {
                if (err) {
                    return cb(err);
                }

                var total = 0;
                var balanceEntries = entries.reduce(function (accumulation, current, index, array) {
                    total += current.coins;
                    current.balance = total;
                    accumulation.push(current);

                    return accumulation;
                }, []);

                var rehydrated = [];
                async.each(balanceEntries, function (entry, cb) {
                    if (entry.event === 'donationsent') {
                        self.internals.loadCharity(db, entry, function(err, updatedEntry) {
                            entry.charity = updatedEntry;
                            cb();
                        });
                    } else {
                        cb();
                    }
                }, function (err) {
                    return cb(err, balanceEntries);
                });
            });
        },
        latestEntry: function (cb) {
            var query = {
                $and: [
                    { event: { $ne: 'donationsent' } },
                    { event: { $ne: 'donationreceived' } }
                ]
            };

            db.get('changeCoinsLedger').findOne(query, { sort: { entryDate: -1 } }, function (err, entry) {
                return cb(err, entry);
            });
        }
    };
};
