var bcrypt = require('bcrypt-nodejs'),
    crypto = require('crypto'),
    NodeRSA = require('node-rsa'),
    config = require('./config')(),
    privateKey = config.get('/encryption/privateKey');

module.exports = function () {
    return {
        comparePasswords: function (unencryptedPassword, encryptedPassword) {
            return bcrypt.compareSync(unencryptedPassword, encryptedPassword);
        },
        encryptPassword: function (value) {
            return bcrypt.hashSync(value);
        },
        decrypt: function (value) {
            var key = new NodeRSA(privateKey);
            return key.decrypt(value, 'utf8');
        },
        encrypt: function (value) {
            var key = new NodeRSA(privateKey);
            return key.encrypt(value, 'base64');
        },
        hash: function (value) {
            var hash = crypto.createHash('md5');
            hash.update(value);
            return hash.digest('hex');
        }
    };
};
