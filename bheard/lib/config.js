var Confidence = require('confidence'),
    dotenv = require('dotenv'),
    Hoek = require('hoek'),
    path = require('path');

dotenv.load({silent: true});

var config = {
    $meta: 'This file configures the application.',
    projectName: 'bheard',
    blogger: {
        $filter: 'env',
        production: {
            apiKey: process.env.BLOGGER_API_KEY,
            blogId: process.env.BLOGGER_BLOG_ID
        },
        test: {
            apiKey: process.env.BLOGGER_API_KEY,
            blogId: process.env.BLOGGER_BLOG_ID
        },
        $default: {
            apiKey: 'AIzaSyDIL5uKxk_nWUkLfsoo3deYwnwI1e09O9g',
            blogId: '1554919191091654101'
        }
    },
    database: {
        $filter: 'env',
        production: {
            mongodb: {
                url: process.env.MONGODB_URI
            }
        },
        test: {
            mongodb: {
                url: process.env.MONGODB_URI
            }
        },
        devtest: {
            mongodb: {
                url: 'mongodb://localhost:27017/bheard-test'
            }
        },
        $default: {
            mongodb: {
                url: 'mongodb://localhost:27017/bheard'
            }
        }
    },
    email: {
        $filter: 'env',
        production: {
            username: process.env.EMAIL_USERNAME,
            password: process.env.EMAIL_PASSWORD
        },
        test: {
            username: process.env.EMAIL_USERNAME,
            password: process.env.EMAIL_PASSWORD
        },
        $default: {
            username: 'errors@unomee.com',
            password: 'un0M33!un0M33!'
        }
    },
    encryption: {
        $filter: 'env',
        production: {
            privateKey: process.env.PRIVATE_KEY
        },
        test: {
            privateKey: process.env.PRIVATE_KEY
        },
        $default: {
            privateKey: '-----BEGIN RSA PRIVATE KEY-----' +
                        'MIIBOQIBAAJAeQvV/SHdIs9LV7nbeIu5WbrOT3PTmeRS+5v9F2pVvXPX0oUmRAlL' +
                        'OE5jbMY7GSrKwzIKSXPPmZSq8pYX3o+8vQIDAQABAkBvLI+9jfysm/d2+xy3+pyh' +
                        '7RDPqHIQRcRxkzNA7kkRVrDJ/GhiRAEIBzPiAauWODVLLKDvwRng7yebPo9G/3wB' +
                        'AiEAtLvipxE7Fasa9iXPnEzbwwXqDT5/sGYFNZ7CW+bl1WUCIQCrdJ9J2o+57G8w' +
                        '8lrd2IqXlJRshbSF8lTDihqYschgeQIgDu150fy9WZPRXUVQOYOrnxcM9t0TpNk4' +
                        'Uu5y9S5EC3UCIQCnoCAva1BDPKXE29z8sb1lzuyBYQMKRc6bUgUIrh+yOQIgJ0en' +
                        'ruWOVuw9dz8kVyjVnojiImvoqhg8gVM8Na+3c3E=' +
                        '-----END RSA PRIVATE KEY-----'
        }
    },
    energyHelpline: {
        $filter: 'env',
        production: {
            apiKey: process.env.ENERGY_HELPLINE_API_KEY,
            basicAuthorizationToken: process.env.ENERGY_HELPLINE_BASIC_AUTHORIZATION_TOKEN,
            host: process.env.ENERGY_HELPLINE_HOST
        },
        test: {
            apiKey: process.env.ENERGY_HELPLINE_API_KEY,
            basicAuthorizationToken: process.env.ENERGY_HELPLINE_BASIC_AUTHORIZATION_TOKEN,
            host: process.env.ENERGY_HELPLINE_HOST
        },
        $default: {
            apiKey: '006A9B53-7BA4-4C63-A605-4C91A756EF69',
            basicAuthorizationToken: 'YmhlYXJkX3Rlc3Q6Q3ZhdVA3M1ZOd3BQY0xsUEF5YmlHZU9UZDlDMGlseG0=',
            host: 'rest-predeploy.energyhelpline.com'
        }
    },
    facebook: {
        $filter: 'env',
        production: {
            appId: process.env.FACEBOOK_APP_ID,
            appSecret: process.env.FACEBOOK_APP_SECRET
        },
        test: {
            appId: process.env.FACEBOOK_APP_ID,
            appSecret: process.env.FACEBOOK_APP_SECRET
        },
        $default: {
            appId: '421925384672267',
            appSecret: '6722824f6bf6310cd2709957835ef010'
        }
    },
    google: {
        $filter: 'env',
        production: {
            clientId: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            apiKey: process.env.GOOGLE_API_KEY
        },
        test: {
            clientId: process.env.GOOGLE_CLIENT_ID,
            clientSecret: process.env.GOOGLE_CLIENT_SECRET,
            apiKey: process.env.GOOGLE_API_KEY
        },
        $default: {
            clientId: '915099572359-5jrvl7q3e6r93re593d4f9fpvid1marc.apps.googleusercontent.com',
            clientSecret: 'zWwfzdlDd0k1IeATi1V_8WXJ',
            apiKey: 'AIzaSyBve4cwzJE3ew0OKnzihyxYVEUVYKW3RO8'
        }
    },
    logging: {
        $filter: 'env',
        $base: {
            replaceConsole: true
        },
        production: {
            appenders: [{
                type: 'file',
                filename: 'console.log'
            }]
        },
        test: {
            appenders: [{
                type: 'file',
                filename: 'console.log'
            }]
        },
        $default: {
            appenders: [{
                type: 'console'
            }]
        }
    },
    mailChimp: {
        $filter: 'env',
        production: {
            apiKey: process.env.MAILCHIMP_API_KEY,
            usersListId: process.env.MAILCHIMP_USERS_LIST_ID
        },
        test: {
            apiKey: process.env.MAILCHIMP_API_KEY,
            usersListId: process.env.MAILCHIMP_USERS_LIST_ID
        },
        $default: {
            apiKey: 'f895b1764741e19d4cc65e5ae2515615-us12',
            usersListId: 'e86fcef581'
        }
    },
    server: {
        web: {
            port: {
                $filter: 'env',
                production: process.env.PORT,
                test: process.env.PORT,
                $default: 5555
            },
            url: {
                $filter: 'env',
                production: process.env.URL,
                test: process.env.URL,
                $default: 'http://localhost:5555'
            }
        }
    },
    twitter: {
        $filter: 'env',
        production: {
            apiKey: process.env.TWITTER_API_KEY,
            apiSecret: process.env.TWITTER_API_SECRET
        },
        test: {
            apiKey: process.env.TWITTER_API_KEY,
            apiSecret: process.env.TWITTER_API_SECRET
        },
        $default: {
            apiKey: 'AxEP3eRua0Ipnm76DYkfoEjgv',
            apiSecret: 'tGiIHHju0yYecazrhcvdWAe4SCBIfMY67EM89K9bTwbQ9MZfRG'
        }
    },
    changeCoins: {
        events: [
            {
                event: 'event:changecoins:register',
                coins: 100
            }, {
                event: 'event:changecoins:reviewsubmission',
                coins: 50
            }, {
                event: 'event:changecoins:login',
                coins: 10
            }, {
                event: 'event:changecoins:donationsent'
            }, {
                event: 'event:changecoins:donationreceived'
            }
        ]
    }
};

var store = new Confidence.Store(config);

module.exports = function () {
    return {
        get: function (key, criteria) {
            return store.get(key, Hoek.applyToDefaults({ env: process.env.NODE_ENV }, criteria || {}));
        },
        meta: function (key, criteria) {
            return store.meta(key, criteria);
        }
    };
};
